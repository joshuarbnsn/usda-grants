package gov.usda.grants.nifa.service.impl;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.out.CalculationStatus;
import gov.usda.grants.nifa.dto.calculations.out.InstitutionResponse;
import gov.usda.grants.nifa.dto.calculations.out.InstitutionResponseInstitution;
import gov.usda.grants.nifa.mapper.CalculationFactory;
import gov.usda.grants.nifa.mapper.InstitutionMapper;
import gov.usda.grants.nifa.mapper.MapperFactory;
import gov.usda.grants.nifa.service.InstitutionCalculationService;
import gov.usda.grants.nifa.validation.ValidationMessages;
import gov.usda.nifa.cgc.api.CalculationApi;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AHADRInstitution;
import gov.usda.nifa.cgc.model.AHADRState;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.AuditLog;
import gov.usda.nifa.cgc.model.EFNEPInstitution;
import gov.usda.nifa.cgc.model.EvansAllenInstitution;
import gov.usda.nifa.cgc.model.HAMInstitution;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.MSCFRInstitution;
import gov.usda.nifa.cgc.model.MSCFRState;
import gov.usda.nifa.cgc.model.StationInstitution;

public class InstitutionCalculationServiceImpl implements InstitutionCalculationService {

	private Logger log = Logger.getLogger(this.getClass().getName());

	@Override
	public InstitutionResponse CalculateAllocations(InstitutionRequest request) {

		InstitutionResponse response = new InstitutionResponse();
		InstitutionDTO output = new InstitutionDTO();
		InstitutionDTO input = new InstitutionDTO();
		
		InstitutionMapper mapper = MapperFactory.getInstitutionMapper(
				request.getProgram().getParentProgramId(),
				request.getProgram().getProgramId());
		
		CalculationApi calculator = CalculationFactory.getInstitutionCalculator(
				request.getProgram().getParentProgramId());

		try {
			
			input = mapper.map(request);
			output = (InstitutionDTO) calculator.calculateAppropriation(input, "Dev");

		} catch (Exception e) {
			
			log.error(e.getMessage(), e);
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.CALCULATION_ERROR_ID);
			status.setDescription(ValidationMessages.CALCULATION_ERROR_TEXT);
			status.setMessage(e.getMessage());
			response.getStatus().add(status);
		}
		
		if (output.getProgram() == null) {
			
			log.error("Program is null");
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.NULL_PROGRAM_ERROR_ID);
			status.setDescription(ValidationMessages.NULL_PROGRAM_ERROR_TEXT);
			status.setMessage(ValidationMessages.NULL_PROGRAM_ERROR_TEXT);
			response.getStatus().add(status);
			
		} else {
			
			if (StringUtils.equals(input.getProgram().getAppropriationType(), 
					AppropriationType.CONTINUING_RESOLUTIONS.getCode())) {
				response.setPaymentPercentage(output.getProgram().getCrAppropriationPercentage());
			}
			
			if (output.getProgram().getAppropriation() != null) {
				response.setCalculatedPaymentToStates(output.getProgram().getAppropriation());
			}
		}
		
		if (output.getEligibleInstitutions() == null) {
			
			log.error("Eligible Institutions list is null");
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.NULL_INSTITUTION_LIST_ERROR_ID);
			status.setDescription(ValidationMessages.NULL_INSTITUTION_LIST_ERROR_TEXT);
			status.setMessage(ValidationMessages.NULL_INSTITUTION_LIST_ERROR_TEXT);
			response.getStatus().add(status);
			
		} else {
			
			for (Institution ins : output.getEligibleInstitutions()) {
				
				InstitutionResponseInstitution inr = new InstitutionResponseInstitution();
				inr.setInstitutionId(ins.getInstitutionId());
				inr.setInstitutionName(ins.getName());
				inr.setAllocationAmount(ins.getAppropriation());
				inr.setRequiredMatchAmount(ins.getMatchingAppropriation());
				
				//TODO implement factory pattern for Institution Response Mapper
				if (ins instanceof EvansAllenInstitution) {
					inr.setBaseAmount(((EvansAllenInstitution) ins).getBaseAppropriation());
				} 
				
				if (ins instanceof HAMInstitution) {
					inr.setBaseAmount(((HAMInstitution) ins).getBaseAppropriation());
				}
				
				if (ins instanceof StationInstitution) {
					inr.setStation(((StationInstitution) ins).getStation());
				}
				if (ins instanceof AHADRInstitution) {
					if (ins.getState() != null) {
						
						AHADRState state = (AHADRState)ins.getState();
						
						inr.setState( state.getAbbreviation());
						
						if (state.getCurrentCalculatedLivestockIncome() != null) {
							inr.setCalculatedLivestockIncome(state.getCurrentCalculatedLivestockIncome());
						}
						
						if (state.getCurrentCalculatedLivestockValue() != null) {
							inr.setCalculatedLivestockValue(state.getCurrentCalculatedLivestockValue());
						}
					}
				}
				
				if (ins instanceof MSCFRInstitution) {
					if (ins.getState() != null) {
						MSCFRState state = (MSCFRState) ins.getState();
						
						inr.setState(state.getAbbreviation());
						
						if (state.getCurrentFYStateRank() != null) {
							inr.setStateRank(new BigDecimal(state.getCurrentFYStateRank()));
						}
					}
				}
				
				if (ins instanceof EFNEPInstitution) {
					EFNEPInstitution efnep = (EFNEPInstitution) ins;
					if (efnep.getWebNeersSystemAppropriation() != null) {
						inr.setWebNeersSystemAppropriation(efnep.getWebNeersSystemAppropriation());
					}
				}
				
				response.getEligibleInstitutions().add(inr);
			}
		}
		
		if (output.getAuditLogs() == null) {
			
			log.error("Audit Log List is null");
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.NULL_AUDIT_LOG_ERROR_ID);
			status.setDescription(ValidationMessages.NULL_AUDIT_LOG_ERROR_TEXT);
			status.setMessage(ValidationMessages.NULL_AUDIT_LOG_ERROR_TEXT);
			response.getStatus().add(status);
			
		} else {
			
			StringBuilder sb = new StringBuilder();
			for (AuditLog alog : output.getAuditLogs()) {
				log.info(alog.getCalculationLog());
				sb.append(alog.getCalculationLog());
			}
	
			response.setAuditLog(sb.toString());
		}

		return response;
	}
}
