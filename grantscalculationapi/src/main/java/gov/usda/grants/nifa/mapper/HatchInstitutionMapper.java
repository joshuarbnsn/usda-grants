package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.constants.CalculationConstants;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.HARInstitution;
import gov.usda.nifa.cgc.model.HatchActProgram;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.Section1444Program;
import gov.usda.nifa.cgc.model.SmithLeverProgram;
import gov.usda.nifa.cgc.model.State;
import gov.usda.nifa.cgc.model.SubProgram;

public class HatchInstitutionMapper extends InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		
		InstitutionDTO input = super.map(request);
		
		mapProgramLevel(request, input);
		
		List<HARInstitution> institutionList = new ArrayList<>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			HARInstitution institutionOut = new HARInstitution();
			
			mapInstitutionLevel(request, institutionIn, institutionOut);
			
			institutionList.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionList);
		
		return input;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		input.setFiscalYear(request.getProgram().getFiscalYear());
		
		//map Smith Lever specific attributes
		HatchActProgram program = getProgram(input);
		program.setName(request.getProgram().getParentProgramId());
		program.setFullName(request.getProgram().getParentProgramDescription());
		program.setFiscalYear(request.getProgram().getFiscalYear());
		program.setAppropriationType(request.getProgram().getAppropriationType());
		
		if (request.getProgram().getNonInsularAreaMatching() != null) {
			program.setRequiredMatchingPercentage(request.getProgram().getNonInsularAreaMatching());
		}
		
		if (StringUtils.equals(request.getProgram().getAppropriationType(),
				AppropriationType.FINAL.getCode()) ||
				StringUtils.equals(request.getProgram().getAppropriationType(),
						AppropriationType.RECESSION.getCode())) {
			
			if (request.getProgram().getEqualProportion() != null) {
				program.setEqualWeight(request.getProgram().getEqualProportion());
			}
			
			if (request.getProgram().getRuralProportion() != null) {
				program.setRpWeight(request.getProgram().getRuralProportion());
			}
			
			if (request.getProgram().getFarmProportion() != null){
				program.setFpWeight(request.getProgram().getFarmProportion());
			}
			
		}

		if (request.getProgram().getInsularAreaMatching() != null){
			program.setInsularWeight(request.getProgram().getInsularAreaMatching());
		}
		
		if (request.getProgram().getInsularAreaAllotment() != null) {
			program.setInsularAreaAppropriation(request.getProgram().getInsularAreaAllotment());
		}
		
		//THIS DOESN'T SEEM CORRECT
		program.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());
		
		input.setProgram(program);
		
		//Set Previous Program
		HatchActProgram prevProgram = getPrevProgram(input);
		prevProgram.setAppropriation(request.getProgram().getPriorYearAmount());
		prevProgram.setFiscalYear(request.getProgram().getFiscalYear() - 1);
		prevProgram.setName(request.getProgram().getParentProgramId());
		prevProgram.setFullName(request.getProgram().getParentProgramDescription());
		
		input.setPreviousYearProgram(prevProgram);		
		
		SubProgram subProgram = getSubProgram(input);
		subProgram.setName(request.getProgram().getProgramId());
		subProgram.setFullName(request.getProgram().getProgramDescription());
		//subProgram.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());	
		//input.setSubProgram(subProgram);
		
		SubProgram prevSubProgram = getPrevSubProgram(input);
		prevSubProgram.setName(request.getProgram().getProgramId());
		prevSubProgram.setFullName(request.getProgram().getProgramDescription());
		//prevSubProgram.setAppropriation(request.getProgram().getPriorYearAmount());
		//input.setPreviousSubProgram(prevSubProgram);

	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, HARInstitution institutionOut) {
		
		State state = getState(institutionOut);
		
		//use the state code when country = US otherwise use the country code as the state
		state.setAbbreviation(StringUtils.equals(institutionIn.getCountry(), 
				CalculationConstants.COUNTRY_CODE_US) ?
				institutionIn.getState() : institutionIn.getCountry());
		
		if (StringUtils.equals(request.getProgram().getAppropriationType(),
				AppropriationType.FINAL.getCode()) ||
				StringUtils.equals(request.getProgram().getAppropriationType(),
						AppropriationType.RECESSION.getCode())) {
			
			state.setFarmPopulation(institutionIn.getFarmPopulation());
			state.setRuralPopulation(institutionIn.getRuralPopulation());
			institutionOut.setAllocWeight(institutionIn.getAllocationPercentage());
			
		}
		
		state.setYear(request.getProgram().getFiscalYear());
		state.setIsInsular(institutionIn.getInsularArea());
		
		institutionOut.setInstitutionId(institutionIn.getInstitutionId());
		institutionOut.setName(institutionIn.getInstitutionName());
		institutionOut.setDunsNumber(institutionIn.getDunsNumber());
		institutionOut.setAppropriation(institutionIn.getPriorYearAllocation());
		institutionOut.setStation(institutionIn.getStation());
		
		if (institutionIn.isApplyInstitutionMatching()) {
			if (institutionIn.getMatchingPercentage() != null) {
				institutionOut.setRequiredMatchingPercentage(institutionIn.getMatchingPercentage());
			}
			if (institutionIn.getMatchingAmount() != null) {
				institutionOut.setMatchingAppropriation(institutionIn.getMatchingAmount());
			}
		}
		
		if (institutionIn.getAllocationPercentage() != null) {
			institutionOut.setAllocWeight(institutionIn.getAllocationPercentage());
		}
	}
	
	public State getState(HARInstitution institutionOut) {
		State state = institutionOut.getState();
		
		if (state == null) {
			state = new State();
			institutionOut.setState(state);
		}
		
		return state;
	}
	
	public List<? extends Institution> getInstitutionListOut(InstitutionDTO input) {
		@SuppressWarnings("unchecked")
		List<HARInstitution> institutionListOut = (List<HARInstitution>) input.getEligibleInstitutions();
		
		if (institutionListOut == null) {
			institutionListOut = new ArrayList<>();
			input.setEligibleInstitutions(institutionListOut);
		}
		
		return institutionListOut;
	}
	
	public HatchActProgram getProgram(InstitutionDTO input) {
		HatchActProgram program = (HatchActProgram) input.getProgram();
		
		if (program == null) {
			program = new HatchActProgram();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public HatchActProgram getPrevProgram(InstitutionDTO input) {
		HatchActProgram program = (HatchActProgram) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new HatchActProgram();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public SubProgram getSubProgram (InstitutionDTO input) {
		
		SubProgram subProgram = input.getSubProgram();
		if (subProgram == null) {
			subProgram = new SubProgram();
			input.setSubProgram(subProgram);
		}
		
		return subProgram;
	}
	
	public SubProgram getPrevSubProgram (InstitutionDTO input) {
		
		SubProgram subProgram = input.getPreviousSubProgram();
		if (subProgram == null) {
			subProgram = new SubProgram();
			input.setPreviousSubProgram(subProgram);
		}
		
		return subProgram;
	}
	
}
