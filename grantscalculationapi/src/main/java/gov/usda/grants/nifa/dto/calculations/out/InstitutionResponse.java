package gov.usda.grants.nifa.dto.calculations.out;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstitutionResponse implements Serializable, ServiceResponse {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("PaymentPercentage")
	protected BigDecimal paymentPercentage;
	
	@JsonProperty("EligibleInstitutions")
	public List<InstitutionResponseInstitution> eligibleInstitutions;

	@JsonProperty("AuditLog")
	public String auditLog;
	
	@JsonProperty("Status")
	public List<CalculationStatus> status;
	
	@JsonProperty("CalculatedPaymentToStates")
	public BigDecimal calculatedPaymentToStates;
	
	public String getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(String auditLog) {
		this.auditLog = auditLog;
	}

	public List<CalculationStatus> getStatus() {
		if (status == null) {
			status = new ArrayList<CalculationStatus>();
		}
		return status;
	}

	public void setStatus(List<CalculationStatus> status) {
		this.status = status;
	}

	public List<InstitutionResponseInstitution> getEligibleInstitutions() {
		if (eligibleInstitutions == null) {
			eligibleInstitutions = new ArrayList<InstitutionResponseInstitution>();
		}
		return eligibleInstitutions;
	}

	public void setEligibleInstitutions(List<InstitutionResponseInstitution> eligibleInstitutions) {
		this.eligibleInstitutions = eligibleInstitutions;
	}

	public BigDecimal getPaymentPercentage() {
		return paymentPercentage;
	}

	public void setPaymentPercentage(BigDecimal paymentPercentage) {
		this.paymentPercentage = paymentPercentage;
	}

	public BigDecimal getCalculatedPaymentToStates() {
		return calculatedPaymentToStates;
	}

	public void setCalculatedPaymentToStates(BigDecimal calculatedPaymentToStates) {
		this.calculatedPaymentToStates = calculatedPaymentToStates;
	}

}
