package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.dto.calculations.in.Livestock;
import gov.usda.grants.nifa.dto.calculations.in.LivestockGroup;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AHADRFundWrapper;
import gov.usda.nifa.cgc.model.AHADRInstitution;
import gov.usda.nifa.cgc.model.AHADRLivestock;
import gov.usda.nifa.cgc.model.AHADRLivestockType;
import gov.usda.nifa.cgc.model.AHADRProgram;
import gov.usda.nifa.cgc.model.AHADRState;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.EFNEPInstitution;
import gov.usda.nifa.cgc.model.EFNEPProgram;
import gov.usda.nifa.cgc.model.EFNEPState;
import gov.usda.nifa.cgc.model.HAMInstitution;
import gov.usda.nifa.cgc.model.HAMProgram;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.MSCFRInstitution;
import gov.usda.nifa.cgc.model.MSCFRProgram;
import gov.usda.nifa.cgc.model.MSCFRState;
import gov.usda.nifa.cgc.model.RREAInstitution;
import gov.usda.nifa.cgc.model.RREAProgram;
import gov.usda.nifa.cgc.model.RREAState;
import gov.usda.nifa.cgc.model.State;

public class AHADRInstitutionMapper extends InstitutionMapper {

	String appropriationType = null;
	
	public InstitutionDTO map(InstitutionRequest request) {
		
		if (request.getProgram() != null) {
			appropriationType = request.getProgram().getAppropriationType();
		}
		
		InstitutionDTO input = super.map(request);
		
		/*  
		    The local map method must be called before the super class method
			in 	order to create the proper objects and avoid class cast exception		
		 */
		
		//Map local program level fields
		mapProgramLevel(request, input);
		
		// 	map the parent Program Fields
		super.mapProgramLevel(request, input);
		
		//reset input list of institutions
		input.setEligibleInstitutions(new ArrayList<>());
		
		List<AHADRInstitution> institutionList = new ArrayList<>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			AHADRInstitution institutionOut = new AHADRInstitution();
			
			//Map RREA Institution fields first
			mapInstitutionLevel(request, institutionIn, institutionOut);
			
			super.mapInstitutionLevel(request, institutionIn, institutionOut);
			
			institutionList.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionList);
		
		return input;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		AHADRProgram program = getProgram(input);
		
		if (request.getProgram().getMatchingThresholdAmount() != null) {
			program.setRequiredMatchThreshold(request.getProgram().getMatchingThresholdAmount());
		}
		
		if (request.getProgram().getScientificYearsProportion() != null) {
			program.setScientificYearsWeight(request.getProgram().getScientificYearsProportion());
		}
		
		if (request.getProgram().getResearchCapacityProportion() != null) {
			program.setResearchCapacityWeight(request.getProgram().getResearchCapacityProportion());
		}
		
		if (request.getProgram().getLivestockValueProportion() != null) {
			program.setLivestockValueWeight(request.getProgram().getLivestockValueProportion());
		}
		
		if (request.getProgram().getLivestockIncomeProportion() != null) {
			program.setLivestockIncomeWeight(request.getProgram().getLivestockIncomeProportion());
		}
	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, AHADRInstitution institutionOut) {
		
		AHADRState state = getState(institutionOut);
		
		if (institutionIn.getLivestockIncome() != null) {
			//initialize livestock income for state
			state.setLivestockIncome(new AHADRLivestock());
			
			LivestockGroup group = institutionIn.getLivestockIncome();
			if (group.getCurrentYearLivestock() != null) {
				
				//initialize current year livestock income object
				state.getLivestockIncome().setCurrentFYTypes(new ArrayList<AHADRLivestockType>());
				
				for (Livestock livestock : group.getCurrentYearLivestock()) {
					
					AHADRLivestockType type = new AHADRLivestockType();
					
					type.setName(StringUtils.upperCase(livestock.getName()));
					
					if (livestock.getAmount() != null) {
						type.setAmount(livestock.getAmount());
					}
					
					state.getLivestockIncome().getCurrentFYTypes().add(type);
				}
			}
			
			if (group.getPriorYearLivestock() != null) {
				
				//initialize current year livestock income object
				state.getLivestockIncome().setPriorFYTypes(new ArrayList<AHADRLivestockType>());
				
				for (Livestock livestock : group.getPriorYearLivestock()) {
					
					AHADRLivestockType type = new AHADRLivestockType();
					
					type.setName(StringUtils.upperCase(livestock.getName()));
					
					if (livestock.getAmount() != null) {
						type.setAmount(livestock.getAmount());
					}
					
					state.getLivestockIncome().getPriorFYTypes().add(type);
				}
			}
			
			if (group.getPriorYearMinusOneLivestock() != null) {
				
				//initialize current year livestock income object
				state.getLivestockIncome().setPriorFYMinusOneTypes(new ArrayList<AHADRLivestockType>());
				
				for (Livestock livestock : group.getPriorYearMinusOneLivestock()) {
					
					AHADRLivestockType type = new AHADRLivestockType();
					
					type.setName(StringUtils.upperCase(livestock.getName()));
					
					if (livestock.getAmount() != null) {
						type.setAmount(livestock.getAmount());
					}
					
					state.getLivestockIncome().getPriorFYMinusOneTypes().add(type);
				}
			}	
		}
		
		if (institutionIn.getLivestockValue() != null) {
			//initialize livestock income for state
			state.setLivestockValue(new AHADRLivestock());
			
			LivestockGroup group = institutionIn.getLivestockValue();
			if (group.getCurrentYearLivestock() != null) {
				
				//initialize current year livestock income object
				state.getLivestockValue().setCurrentFYTypes(new ArrayList<AHADRLivestockType>());
				
				for (Livestock livestock : group.getCurrentYearLivestock()) {
					
					AHADRLivestockType type = new AHADRLivestockType();
					
					type.setName(StringUtils.upperCase(livestock.getName()));
					
					if (livestock.getAmount() != null) {
						type.setAmount(livestock.getAmount());
					}
					
					state.getLivestockValue().getCurrentFYTypes().add(type);
				}
			}
			
			if (group.getPriorYearLivestock() != null) {
				
				//initialize current year livestock income object
				state.getLivestockValue().setPriorFYTypes(new ArrayList<AHADRLivestockType>());
				
				for (Livestock livestock : group.getPriorYearLivestock()) {
					
					AHADRLivestockType type = new AHADRLivestockType();
					
					type.setName(StringUtils.upperCase(livestock.getName()));
					
					if (livestock.getAmount() != null) {
						type.setAmount(livestock.getAmount());
					}
					
					state.getLivestockValue().getPriorFYTypes().add(type);
				}
			}
			
			if (group.getPriorYearMinusOneLivestock() != null) {
				
				//initialize current year livestock income object
				state.getLivestockValue().setPriorFYMinusOneTypes(new ArrayList<AHADRLivestockType>());
				
				for (Livestock livestock : group.getPriorYearMinusOneLivestock()) {
					
					AHADRLivestockType type = new AHADRLivestockType();
					
					type.setName(StringUtils.upperCase(livestock.getName()));
					
					if (livestock.getAmount() != null) {
						type.setAmount(livestock.getAmount());
					}
					
					state.getLivestockValue().getPriorFYMinusOneTypes().add(type);
				}
			}	
		}
		
		if (institutionIn.getPriorYearCalculatedLivestockIncome() != null) {
			state.setPriorFYCalculatedLivestockIncome(institutionIn.getPriorYearCalculatedLivestockIncome());
		}
		
		if (institutionIn.getPriorYearCalculatedLivestockValue() != null) {
			state.setPriorFYCalculatedLivestockValue(institutionIn.getPriorYearCalculatedLivestockValue());
		}
		
		if (institutionIn.getResearchCapacity() != null) {
			AHADRFundWrapper wrapper = getResearchCapacity(institutionOut);
			
			if (institutionIn.getResearchCapacity().getCurrentYearAmount() != null) {
				wrapper.setCurrentFYAmount(institutionIn.getResearchCapacity().getCurrentYearAmount());
			}
			
			if (institutionIn.getResearchCapacity().getPriorYearAmount() != null) {
				wrapper.setPriorFYAmount(institutionIn.getResearchCapacity().getPriorYearAmount());
			}
			
			if (institutionIn.getResearchCapacity().getPriorYearMinusOneAmount() != null) {
				wrapper.setPriorFYMinusOneAmount(institutionIn.getResearchCapacity().getPriorYearMinusOneAmount());
			}
		}
		
		if (institutionIn.getScientificYears() != null) {
			AHADRFundWrapper wrapper = getScientificYears(institutionOut);
			
			if (institutionIn.getScientificYears().getCurrentYearAmount() != null) {
				wrapper.setCurrentFYAmount(institutionIn.getScientificYears().getCurrentYearAmount());
			}
			
			if (institutionIn.getScientificYears().getPriorYearAmount() != null) {
				wrapper.setPriorFYAmount(institutionIn.getScientificYears().getPriorYearAmount());
			}
			
			if (institutionIn.getScientificYears().getPriorYearMinusOneAmount() != null) {
				wrapper.setPriorFYMinusOneAmount(institutionIn.getScientificYears().getPriorYearMinusOneAmount());
			}
		}
		
		if (StringUtils.equals(appropriationType, AppropriationType.CONTINUING_RESOLUTIONS.getCode())) {
			if (institutionIn.getPriorYearAllocation() != null) {
				institutionOut.setAppropriation(institutionIn.getPriorYearAllocation());
			}
		}
		
		state.setIsInsular(institutionIn.getInsularArea());
		
		if (institutionIn.isApplyInstitutionMatching()) {
			if (institutionIn.getMatchingPercentage() != null) {
				institutionOut.setRequiredMatchingPercentage(institutionIn.getMatchingPercentage());
			}
			if (institutionIn.getMatchingAmount() != null) {
				institutionOut.setMatchingAppropriation(institutionIn.getMatchingAmount());
			}
		}
		
		if (institutionIn.getAllocationPercentage() != null) {
			institutionOut.setAllocWeight(institutionIn.getAllocationPercentage());
		}
		
		if (institutionIn.getStation() != null) {
			institutionOut.setStation(institutionIn.getStation());
		}
	}
	
	public AHADRProgram getProgram(InstitutionDTO input) {
		AHADRProgram program = (AHADRProgram) input.getProgram();
		
		if (program == null) {
			program = new AHADRProgram();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public AHADRProgram getPrevProgram(InstitutionDTO input) {
		AHADRProgram program = (AHADRProgram) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new AHADRProgram();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public List<? extends Institution> getInstitutionListOut(InstitutionDTO input) {
		@SuppressWarnings("unchecked")
		List<AHADRInstitution> institutionListOut = (List<AHADRInstitution>) input.getEligibleInstitutions();
		
		if (institutionListOut == null) {
			institutionListOut = new ArrayList<>();
			input.setEligibleInstitutions(institutionListOut);
		}
		
		return institutionListOut;
	}
	
	public AHADRState getState(AHADRInstitution institutionOut) {
		AHADRState state = (AHADRState) institutionOut.getState();
		
		if (state == null) {
			state = new AHADRState();
			institutionOut.setState(state);
		}
		
		return state;
	}
	
	public AHADRFundWrapper getResearchCapacity(AHADRInstitution institutionOut) {
		AHADRFundWrapper wrapper = (AHADRFundWrapper) institutionOut.getResearchCapacity();
		
		if (wrapper == null) {
			wrapper = new AHADRFundWrapper();
			institutionOut.setResearchCapacity(wrapper);
		}
		
		return wrapper;
	}
	
	public AHADRFundWrapper getScientificYears(AHADRInstitution institutionOut) {
		AHADRFundWrapper wrapper = (AHADRFundWrapper) institutionOut.getScientificYears();
		
		if (wrapper == null) {
			wrapper = new AHADRFundWrapper();
			institutionOut.setScientificYears(wrapper);
		}
		
		return wrapper;
	}
}