package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.HAMInstitution;
import gov.usda.nifa.cgc.model.HAMProgram;
import gov.usda.nifa.cgc.model.HARInstitution;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.State;

public class HatchMultistateInstitutionMapper extends HatchInstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		
		InstitutionDTO input = super.map(request);
		
		HAMProgram program = new HAMProgram();
		input.setProgram(program);
		
		/*
		 *  Map the Hatch Multi-State fields
		 *  
		 *  The local map method must be called before the super class method
			in 	order to create the proper objects and avoid class cast exception		
		 */
		mapProgramLevel(request, input);
		
		// 	map the Hatch Regular Fields
		super.mapProgramLevel(request, input);
		
		//reset input list of institutions
		input.setEligibleInstitutions(new ArrayList<>());
		
		List<HAMInstitution> hamInsList = new ArrayList<>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			HAMInstitution institutionOut = new HAMInstitution();
			
			//Map Hatch Multi-state fields first
			mapInstitutionLevel(request, institutionIn, institutionOut);
			
			super.mapInstitutionLevel(request, institutionIn, institutionOut);
			
			hamInsList.add(institutionOut);
		}
		
		input.setEligibleInstitutions(hamInsList);
		
		return input;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		HAMProgram program = getProgram(input);
		
		if (request.getProgram().getNationalTrustAmount() != null) {
			program.setNationalTrust(request.getProgram().getNationalTrustAmount());
		}
		
		//Set Previous Program
		HAMProgram prevProgram = getPrevProgram(input);
		
		prevProgram.setBaseAppropriation(request.getProgram().getPriorYearBaseAmount());
	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, HAMInstitution institutionOut) {
		
		State state = getState(institutionOut);
		
		if (StringUtils.equals(request.getProgram().getAppropriationType(),
				AppropriationType.FINAL.getCode()) ||
				StringUtils.equals(request.getProgram().getAppropriationType(),
						AppropriationType.RECESSION.getCode())) {
			
			state.setFarmPopulation(institutionIn.getFarmPopulation());
			state.setRuralPopulation(institutionIn.getRuralPopulation());
		}
		
		state.setYear(request.getProgram().getFiscalYear());
		state.setIsInsular(institutionIn.getInsularArea());
		institutionOut.setStation(institutionIn.getStation());
		
		institutionOut.setRegionName(institutionIn.getRegion());
		institutionOut.setIsNationalTrustRecipient(institutionIn.isNationalTrustRecipient());
		
		if (institutionIn.isApplyInstitutionMatching()) {
			if (institutionIn.getMatchingPercentage() != null) {
				institutionOut.setRequiredMatchingPercentage(institutionIn.getMatchingPercentage());
			}
			if (institutionIn.getMatchingAmount() != null) {
				institutionOut.setMatchingAppropriation(institutionIn.getMatchingAmount());
			}
		}
		
		if (institutionIn.getPriorYearBaseAmount() != null) {
			institutionOut.setBaseAppropriation(institutionIn.getPriorYearBaseAmount());
		}
		
		if (institutionIn.getNationalFundAmount() != null) {
			institutionOut.setInterregionalTrustFund(institutionIn.getNationalFundAmount());
		}
		
		if (institutionIn.getRegionalFundAmount() != null) {
			institutionOut.setRegionalFund(institutionIn.getRegionalFundAmount());
		}
	}
	
	public HAMProgram getProgram(InstitutionDTO input) {
		HAMProgram program = (HAMProgram) input.getProgram();
		
		if (program == null) {
			program = new HAMProgram();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public HAMProgram getPrevProgram(InstitutionDTO input) {
		HAMProgram program = (HAMProgram) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new HAMProgram();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public List<? extends Institution> getInstitutionListOut(InstitutionDTO input) {
		@SuppressWarnings("unchecked")
		List<HAMInstitution> institutionListOut = (List<HAMInstitution>) input.getEligibleInstitutions();
		
		if (institutionListOut == null) {
			institutionListOut = new ArrayList<>();
			input.setEligibleInstitutions(institutionListOut);
		}
		
		return institutionListOut;
	}
}