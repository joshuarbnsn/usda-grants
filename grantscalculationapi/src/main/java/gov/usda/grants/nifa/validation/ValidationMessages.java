package gov.usda.grants.nifa.validation;

public class ValidationMessages {

	public static final int SUCCESS_ID = 1;
	public static final String SUCCESS_TEXT = "Success";
	
	public static final int CALCULATION_ERROR_ID = -100;
	public static final String CALCULATION_ERROR_TEXT = "Error occurred in calculation engine";
	
	public static final int VALIDATION_OBJECT_ERROR_ID = -101;
	public static final String VALIDATION_OBJECT_ERROR_TEXT = "Invalid request or response object types for validation";

	public static final int NULL_INSTITUTION_LIST_ERROR_ID = -110;
	public static final String NULL_INSTITUTION_LIST_ERROR_TEXT = "Institution list is null in the calculation response object";
	
	public static final int NULL_AUDIT_LOG_ERROR_ID = -120;
	public static final String NULL_AUDIT_LOG_ERROR_TEXT = "Audit Log is null in the calculation response object";
	
	public static final int NULL_PROGRAM_ERROR_ID = -130;
	public static final String NULL_PROGRAM_ERROR_TEXT = "Program is null in the calculation response object";
	
	public static final int NULL_SUBPROGRAM_LIST_ERROR_ID = -140;
	public static final String NULL_SUBPROGRAM_LIST_ERROR_TEXT = "Sub Program list is null in the calculation response object";
	
	//PROGRAM OBJECT
	public static final int PROGRAM_REQUIRED_ID = -200;
	public static final String PROGRAM_REQUIRED_TEXT = "A Program is required";
	
	public static final int VALID_FISCAL_YEAR_ID = -210;
	public static final String VALID_FISCAL_YEAR_TEXT = "Please enter a valid four digit Fiscal Year (YYYY)";
	
	public static final int VALID_APPROPRIATION_TYPE_ID = -211;
	public static final String VALID_APPROPRIATION_TYPE_TEXT = "Please enter a valid Appropriation Type. (FB, CR, RB, BC, SB)";
	
	public static final int VALID_PAYMENT_TO_STATES_ID = -212;
	public static final String VALID_PAYMENT_TO_STATES_TEXT = "Please enter a valid Appropriation Amount";
	
	public static final int VALID_FORECASTED_PAYMENT_ID = -213;
	public static final String VALID_FORECASTED_PAYMENT_TEXT = "Please enter a valid Forecasted Payment To States";
	
	public static final int VALID_MB_SET_ASIDE_ID = -214;
	public static final String VALID_MB_SET_ASIDE_TEXT = "Please enter a valid MB Set Aside amount";
	
	public static final int VALID_PPERA_INCREASE_ID = -215;
	public static final String VALID_PPERA_INCREASE_TEXT = "Please enter a valid PPERA Increase amount";
	
	public static final int SUBPROGRAM_LIST_ID = -300;
	public static final String SUBPROGRAM_LIST_TEXT = "Prior amounts for subprograms are required";
	
	public static final int SUBPROGRAM_NAME_ID = -301;
	public static final String SUBPROGRAM_NAME_TEXT = "Please enter a valid name for sub program. (12-NIPPERAXXX, 12-NISLSNXXXX, 12-NIMBXXXXXX, 12-NISLBCXXXX)";
	
	public static final int INSTITUTION_PRIOR_AMOUNT_ID = -450;
	public static final String INSTITUTION_PRIOR_AMOUNT_TEXT = "Prior year allocation is required";
	
}
