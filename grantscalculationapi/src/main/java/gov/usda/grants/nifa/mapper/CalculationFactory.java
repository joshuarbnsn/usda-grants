package gov.usda.grants.nifa.mapper;

import org.apache.commons.lang3.StringUtils;

import gov.usda.nifa.cgc.api.AHADRCapacityCalculationApi;
import gov.usda.nifa.cgc.api.CalculationApi;
import gov.usda.nifa.cgc.api.EFNEPCapacityCalculationApi;
import gov.usda.nifa.cgc.api.EvansAllenCapacityCalculationApi;
import gov.usda.nifa.cgc.api.HatchActCapacityCalculationApi;
import gov.usda.nifa.cgc.api.MSCFRCapacityCalculationApi;
import gov.usda.nifa.cgc.api.RREACapacityCalculationApi;
import gov.usda.nifa.cgc.api.Section1444CapacityCalculationApi;
import gov.usda.nifa.cgc.api.SmithLeverCapacityCalculationApi;
import gov.usda.nifa.cgc.model.ProgramType;

public class CalculationFactory {

	public static CalculationApi getInstitutionCalculator(String programName) 
	{
		
		if (StringUtils.equals(ProgramType.SMITH_LEVER.getName(), programName)) 
		{
			return new SmithLeverCapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.SECTION_1444.getName(), programName))
		{
			return new Section1444CapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.EVANS_ALLEN.getName(), programName))
		{
			return new EvansAllenCapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.RENEWABLE_RESOURCES.getName(), programName))
		{
			return new RREACapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.HATCH_ACT.getName(), programName))
		{
			return new HatchActCapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.FOOD_NUTRITION.getName(), programName))
		{
			return new EFNEPCapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.MCINTIRE_STENNIS.getName(), programName))
		{
			return new MSCFRCapacityCalculationApi();
		}
		else if (StringUtils.equals(ProgramType.ANIMAL_HEALTH.getName(), programName))
		{
			return new AHADRCapacityCalculationApi();
		}
		else  
		{
			return new SmithLeverCapacityCalculationApi();
		}
	}
}
