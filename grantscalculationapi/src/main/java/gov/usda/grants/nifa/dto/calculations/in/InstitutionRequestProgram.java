package gov.usda.grants.nifa.dto.calculations.in;

import java.io.Serializable;
import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstitutionRequestProgram implements Serializable, Program {

	private static final long serialVersionUID = 1L;

	@JsonProperty("Agency")
	protected String agencyId;
	
	@JsonProperty("FiscalYear")
	protected int fiscalYear;
	
	@JsonProperty("ParentProgramId")
	protected String parentProgramId;
	
	@JsonProperty("ParentProgramDescription")
	protected String parentProgramDescription;
	
	@JsonProperty("ProgramId")
	protected String programId;
	
	@JsonProperty("ProgramDescription")
	protected String programDescription;
		
	@JsonProperty("PriorYearAmount")
	protected BigDecimal priorYearAmount;
	
	@JsonProperty("CurrentYearApprovedAmount")
	protected BigDecimal currentYearApprovedAmount;
	
	@JsonProperty("PriorYearGrossAmount")
	protected BigDecimal priorYearGrossAmount;
	
	@JsonProperty("CurrentYearGrossAmount")
	protected BigDecimal currentYearGrossAmount;
	
	@JsonProperty("PriorYearAdminAmount")
	protected BigDecimal priorYearAdminAmount;
	
	@JsonProperty("CurrentYearAdminAmount")
	protected BigDecimal currentYearAdminAmount;
	
	@JsonProperty("PriorYearBaseAmount")
	protected BigDecimal priorYearBaseAmount;
	
	@JsonProperty("NationalTrustAmount")
	protected BigDecimal nationalTrustAmount;
	
	@JsonProperty("SbirAmount")
	protected BigDecimal sbirAmount;
	
	@JsonProperty("BraAmount")
	protected BigDecimal braAmount;
	
	@JsonProperty("OtherAmount")
	protected BigDecimal otherAmount;
	
	@JsonProperty("AppropriationType")
	protected String appropriationType;
	
	@JsonProperty("InsularAreaAllotment")
	protected BigDecimal insularAreaAllotment;
	
	@JsonProperty("EqualProportion")
	protected BigDecimal equalProportion;
	
	@JsonProperty("RuralProportion")
	protected BigDecimal ruralProportion;

	@JsonProperty("FarmProportion")
	protected BigDecimal farmProportion;
	
	@JsonProperty("InsularAreaMatching")
	protected BigDecimal insularAreaMatching;
	
	@JsonProperty("NonInsularAreaMatching")
	protected BigDecimal nonInsularAreaMatching;
	
	@JsonProperty("MESetAsideAmount")
	protected BigDecimal meSetAsideAmount;
	
	@JsonProperty("MBSetAsideAmount")
	protected BigDecimal mbSetAsideAmount;
	
	@JsonProperty("PperaIncreaseAmount")
	protected BigDecimal pperaIncreaseAmount;
	
	@JsonProperty("NffpSetAsideAmount")
	protected BigDecimal nffpSetAsideAmount;
	
	@JsonProperty("Base1862InsAmount")
	protected BigDecimal base1862InsAmount;
	
	@JsonProperty("Fixed1890InsAmount")
	protected BigDecimal fixed1890InsAmount;
	
	@JsonProperty("ForestlandProportion")
	protected BigDecimal forestlandProportion;
	
	@JsonProperty("NetGrowthProportion")
	protected BigDecimal netGrowthProportion;
	
	@JsonProperty("GrazingLandProportion")
	protected BigDecimal grazingLandProportion;
	
	@JsonProperty("WoodIndustryEmploymentProportion")
	protected BigDecimal woodIndustryEmploymentProportion;
	
	@JsonProperty("TimberRemovalsProportion")
	protected BigDecimal timberRemovalsProportion;
	
	@JsonProperty("UrbanPopulationProportion")
	protected BigDecimal urbanPopulationProportion;
	
	@JsonProperty("TotalStatePopulationProportion")
	protected BigDecimal totalStatePopulationProportion;
	
	@JsonProperty("ConstantFactorProportion")
	protected BigDecimal constantFactorProportion;
	
	@JsonProperty("AverageIncrementAmount")
	protected BigDecimal averageIncrementAmount;
	
	@JsonProperty("BaseRankCriteria")
	protected BigDecimal baseRankCriteria;
	
	@JsonProperty("FY1981BaseAmount")
	protected BigDecimal fy1981BaseAmount;
	
	@JsonProperty("FY2007BaseAmount")
	protected BigDecimal fy2007BaseAmount;
	
	@JsonProperty("EqualAllocationAmount")
	protected BigDecimal equalAllocationAmount;
	
	@JsonProperty("WebNEERSAmount")
	protected BigDecimal webNEERSAmount;
	
	@JsonProperty("LGU1862FundProportion")
	protected BigDecimal lgu1862FundProportion;
	
	@JsonProperty("LGU1890FundProportion")
	protected BigDecimal lgu1890FundProportion;
	
	@JsonProperty("InsularAreaThreshold")
	protected BigDecimal insularAreaThreshold;
	
	@JsonProperty("CommercialForestProportion")
	protected BigDecimal commercialForestProportion;
	
	@JsonProperty("TimberCutProportion")
	protected BigDecimal timberCutProportion;

	@JsonProperty("ForestryResearchProportion")
	protected BigDecimal forestryResearchProportion;

	@JsonProperty("MatchingThresholdAmount")
	protected BigDecimal matchingThresholdAmount;
	
	@JsonProperty("ScientificYearsProportion")
	protected BigDecimal scientificYearsProportion;
	
	@JsonProperty("ResearchCapacityProportion")
	protected BigDecimal researchCapacityProportion;
	
	@JsonProperty("LivestockValueProportion")
	protected BigDecimal livestockValueProportion;

	@JsonProperty("LivestockIncomeProportion")
	protected BigDecimal livestockIncomeProportion;

	@JsonProperty("ApplyAdminAdjustment")
	protected boolean applyAdminAdjustment;
	
	@JsonProperty("AdminAdjustmentReason")
	protected String adminAdjustmentReason;
	
	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public int getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(int fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public String getParentProgramId() {
		return parentProgramId;
	}

	public void setParentProgramId(String parentProgramId) {
		this.parentProgramId = parentProgramId;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public BigDecimal getPriorYearAmount() {
		return priorYearAmount;
	}

	public void setPriorYearAmount(BigDecimal priorYearAmount) {
		this.priorYearAmount = priorYearAmount;
	}

	public BigDecimal getCurrentYearApprovedAmount() {
		return currentYearApprovedAmount;
	}

	public void setCurrentYearApprovedAmount(BigDecimal currentYearApprovedAmount) {
		this.currentYearApprovedAmount = currentYearApprovedAmount;
	}

	public String getAppropriationType() {
		return appropriationType;
	}

	public void setAppropriationType(String appropriationType) {
		this.appropriationType = appropriationType;
	}

	public BigDecimal getInsularAreaAllotment() {
		return insularAreaAllotment;
	}

	public void setInsularAreaAllotment(BigDecimal insularAreaAllotment) {
		this.insularAreaAllotment = insularAreaAllotment;
	}

	public BigDecimal getEqualProportion() {
		return equalProportion;
	}

	public void setEqualProportion(BigDecimal equalProportion) {
		this.equalProportion = equalProportion;
	}

	public BigDecimal getRuralProportion() {
		return ruralProportion;
	}

	public void setRuralProportion(BigDecimal ruralProportion) {
		this.ruralProportion = ruralProportion;
	}

	public BigDecimal getFarmProportion() {
		return farmProportion;
	}

	public void setFarmProportion(BigDecimal farmProportion) {
		this.farmProportion = farmProportion;
	}

	public BigDecimal getInsularAreaMatching() {
		return insularAreaMatching;
	}

	public void setInsularAreaMatching(BigDecimal insularAreaMatching) {
		this.insularAreaMatching = insularAreaMatching;
	}

	public BigDecimal getNonInsularAreaMatching() {
		return nonInsularAreaMatching;
	}

	public void setNonInsularAreaMatching(BigDecimal nonInsularAreaMatching) {
		this.nonInsularAreaMatching = nonInsularAreaMatching;
	}

	public BigDecimal getMeSetAsideAmount() {
		return meSetAsideAmount;
	}

	public void setMeSetAsideAmount(BigDecimal meSetAsideAmount) {
		this.meSetAsideAmount = meSetAsideAmount;
	}

	public BigDecimal getMbSetAsideAmount() {
		return mbSetAsideAmount;
	}

	public void setMbSetAsideAmount(BigDecimal mbSetAsideAmount) {
		this.mbSetAsideAmount = mbSetAsideAmount;
	}

	public BigDecimal getPperaIncreaseAmount() {
		return pperaIncreaseAmount;
	}

	public void setPperaIncreaseAmount(BigDecimal pperaIncreaseAmount) {
		this.pperaIncreaseAmount = pperaIncreaseAmount;
	}

	public BigDecimal getPriorYearGrossAmount() {
		return priorYearGrossAmount;
	}

	public void setPriorYearGrossAmount(BigDecimal priorYearGrossAmount) {
		this.priorYearGrossAmount = priorYearGrossAmount;
	}

	public BigDecimal getPriorYearAdminAmount() {
		return priorYearAdminAmount;
	}

	public void setPriorYearAdminAmount(BigDecimal priorYearAdminAmount) {
		this.priorYearAdminAmount = priorYearAdminAmount;
	}

	public BigDecimal getSbirAmount() {
		return sbirAmount;
	}

	public void setSbirAmount(BigDecimal sbirAmount) {
		this.sbirAmount = sbirAmount;
	}

	public BigDecimal getBraAmount() {
		return braAmount;
	}

	public void setBraAmount(BigDecimal braAmount) {
		this.braAmount = braAmount;
	}

	public BigDecimal getOtherAmount() {
		return otherAmount;
	}

	public void setOtherAmount(BigDecimal otherAmount) {
		this.otherAmount = otherAmount;
	}

	public String getParentProgramDescription() {
		return parentProgramDescription;
	}

	public void setParentProgramDescription(String parentProgramDescription) {
		this.parentProgramDescription = parentProgramDescription;
	}

	public BigDecimal getCurrentYearGrossAmount() {
		return currentYearGrossAmount;
	}

	public void setCurrentYearGrossAmount(BigDecimal currentYearGrossAmount) {
		this.currentYearGrossAmount = currentYearGrossAmount;
	}

	public BigDecimal getCurrentYearAdminAmount() {
		return currentYearAdminAmount;
	}

	public void setCurrentYearAdminAmount(BigDecimal currentYearAdminAmount) {
		this.currentYearAdminAmount = currentYearAdminAmount;
	}

	public String getProgramDescription() {
		return programDescription;
	}

	public void setProgramDescription(String programDescription) {
		this.programDescription = programDescription;
	}

	public BigDecimal getNationalTrustAmount() {
		return nationalTrustAmount;
	}

	public void setNationalTrustAmount(BigDecimal nationalTrustAmount) {
		this.nationalTrustAmount = nationalTrustAmount;
	}

	public BigDecimal getNffpSetAsideAmount() {
		return nffpSetAsideAmount;
	}

	public void setNffpSetAsideAmount(BigDecimal nffpSetAsideAmount) {
		this.nffpSetAsideAmount = nffpSetAsideAmount;
	}

	public BigDecimal getBase1862InsAmount() {
		return base1862InsAmount;
	}

	public void setBase1862InsAmount(BigDecimal base1862InsAmount) {
		this.base1862InsAmount = base1862InsAmount;
	}

	public BigDecimal getFixed1890InsAmount() {
		return fixed1890InsAmount;
	}

	public void setFixed1890InsAmount(BigDecimal fixed1890InsAmount) {
		this.fixed1890InsAmount = fixed1890InsAmount;
	}

	public BigDecimal getForestlandProportion() {
		return forestlandProportion;
	}

	public void setForestlandProportion(BigDecimal forestlandProportion) {
		this.forestlandProportion = forestlandProportion;
	}

	public BigDecimal getNetGrowthProportion() {
		return netGrowthProportion;
	}

	public void setNetGrowthProportion(BigDecimal netGrowthProportion) {
		this.netGrowthProportion = netGrowthProportion;
	}

	public BigDecimal getGrazingLandProportion() {
		return grazingLandProportion;
	}

	public void setGrazingLandProportion(BigDecimal grazingLandProportion) {
		this.grazingLandProportion = grazingLandProportion;
	}

	public BigDecimal getWoodIndustryEmploymentProportion() {
		return woodIndustryEmploymentProportion;
	}

	public void setWoodIndustryEmploymentProportion(BigDecimal woodIndustryEmploymentProportion) {
		this.woodIndustryEmploymentProportion = woodIndustryEmploymentProportion;
	}

	public BigDecimal getTimberRemovalsProportion() {
		return timberRemovalsProportion;
	}

	public void setTimberRemovalsProportion(BigDecimal timberRemovalsProportion) {
		this.timberRemovalsProportion = timberRemovalsProportion;
	}

	public BigDecimal getUrbanPopulationProportion() {
		return urbanPopulationProportion;
	}

	public void setUrbanPopulationProportion(BigDecimal urbanPopulationProportion) {
		this.urbanPopulationProportion = urbanPopulationProportion;
	}

	public BigDecimal getTotalStatePopulationProportion() {
		return totalStatePopulationProportion;
	}

	public void setTotalStatePopulationProportion(BigDecimal totalStatePopulationProportion) {
		this.totalStatePopulationProportion = totalStatePopulationProportion;
	}

	public BigDecimal getConstantFactorProportion() {
		return constantFactorProportion;
	}

	public void setConstantFactorProportion(BigDecimal constantFactorProportion) {
		this.constantFactorProportion = constantFactorProportion;
	}

	public BigDecimal getAverageIncrementAmount() {
		return averageIncrementAmount;
	}

	public void setAverageIncrementAmount(BigDecimal averageIncrementAmount) {
		this.averageIncrementAmount = averageIncrementAmount;
	}

	public BigDecimal getBaseRankCriteria() {
		return baseRankCriteria;
	}

	public void setBaseRankCriteria(BigDecimal baseRankCriteria) {
		this.baseRankCriteria = baseRankCriteria;
	}

	public BigDecimal getPriorYearBaseAmount() {
		return priorYearBaseAmount;
	}

	public void setPriorYearBaseAmount(BigDecimal priorYearBaseAmount) {
		this.priorYearBaseAmount = priorYearBaseAmount;
	}

	public BigDecimal getFy1981BaseAmount() {
		return fy1981BaseAmount;
	}

	public void setFy1981BaseAmount(BigDecimal fy1981BaseAmount) {
		this.fy1981BaseAmount = fy1981BaseAmount;
	}

	public BigDecimal getFy2007BaseAmount() {
		return fy2007BaseAmount;
	}

	public void setFy2007BaseAmount(BigDecimal fy2007BaseAmount) {
		this.fy2007BaseAmount = fy2007BaseAmount;
	}

	public BigDecimal getEqualAllocationAmount() {
		return equalAllocationAmount;
	}

	public void setEqualAllocationAmount(BigDecimal equalAllocationAmount) {
		this.equalAllocationAmount = equalAllocationAmount;
	}

	public BigDecimal getWebNEERSAmount() {
		return webNEERSAmount;
	}

	public void setWebNEERSAmount(BigDecimal webNEERSAmount) {
		this.webNEERSAmount = webNEERSAmount;
	}

	public BigDecimal getLgu1862FundProportion() {
		return lgu1862FundProportion;
	}

	public void setLgu1862FundProportion(BigDecimal lgu1862FundProportion) {
		this.lgu1862FundProportion = lgu1862FundProportion;
	}

	public BigDecimal getLgu1890FundProportion() {
		return lgu1890FundProportion;
	}

	public void setLgu1890FundProportion(BigDecimal lgu1890FundProportion) {
		this.lgu1890FundProportion = lgu1890FundProportion;
	}

	public BigDecimal getInsularAreaThreshold() {
		return insularAreaThreshold;
	}

	public void setInsularAreaThreshold(BigDecimal insularAreaThreshold) {
		this.insularAreaThreshold = insularAreaThreshold;
	}

	public BigDecimal getCommercialForestProportion() {
		return commercialForestProportion;
	}

	public void setCommercialForestProportion(BigDecimal commercialForestProportion) {
		this.commercialForestProportion = commercialForestProportion;
	}

	public BigDecimal getTimberCutProportion() {
		return timberCutProportion;
	}

	public void setTimberCutProportion(BigDecimal timberCutProportion) {
		this.timberCutProportion = timberCutProportion;
	}

	public BigDecimal getForestryResearchProportion() {
		return forestryResearchProportion;
	}

	public void setForestryResearchProportion(BigDecimal forestryResearchProportion) {
		this.forestryResearchProportion = forestryResearchProportion;
	}

	public BigDecimal getMatchingThresholdAmount() {
		return matchingThresholdAmount;
	}

	public void setMatchingThresholdAmount(BigDecimal matchingThresholdAmount) {
		this.matchingThresholdAmount = matchingThresholdAmount;
	}

	public BigDecimal getScientificYearsProportion() {
		return scientificYearsProportion;
	}

	public void setScientificYearsProportion(BigDecimal scientificYearsProportion) {
		this.scientificYearsProportion = scientificYearsProportion;
	}

	public BigDecimal getResearchCapacityProportion() {
		return researchCapacityProportion;
	}

	public void setResearchCapacityProportion(BigDecimal researchCapacityProportion) {
		this.researchCapacityProportion = researchCapacityProportion;
	}

	public BigDecimal getLivestockValueProportion() {
		return livestockValueProportion;
	}

	public void setLivestockValueProportion(BigDecimal livestockValueProportion) {
		this.livestockValueProportion = livestockValueProportion;
	}

	public BigDecimal getLivestockIncomeProportion() {
		return livestockIncomeProportion;
	}

	public void setLivestockIncomeProportion(BigDecimal livestockIncomeProportion) {
		this.livestockIncomeProportion = livestockIncomeProportion;
	}

	public boolean isApplyAdminAdjustment() {
		return applyAdminAdjustment;
	}

	public void setApplyAdminAdjustment(boolean applyAdminAdjustment) {
		this.applyAdminAdjustment = applyAdminAdjustment;
	}

	public String getAdminAdjustmentReason() {
		return adminAdjustmentReason;
	}

	public void setAdminAdjustmentReason(String adminAdjustmentReason) {
		this.adminAdjustmentReason = adminAdjustmentReason;
	}
	
}