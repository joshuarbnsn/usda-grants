package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.constants.CalculationConstants;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.EvansAllenInstitution;
import gov.usda.nifa.cgc.model.EvansAllenProgram;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.State;
import gov.usda.nifa.cgc.model.SubProgram;

public class EvansAllenInstitutionMapper extends InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		EvansAllenProgram program = new EvansAllenProgram();
		InstitutionDTO input = super.map(request);
		
		input.setFiscalYear(request.getProgram().getFiscalYear());
		
		//map Smith Lever specific attributes
		program.setName(request.getProgram().getParentProgramId());
		program.setFullName(request.getProgram().getParentProgramDescription());
		program.setFiscalYear(request.getProgram().getFiscalYear());
		program.setAppropriationType(request.getProgram().getAppropriationType());
		
		if (request.getProgram().getEqualProportion() != null) {
			program.setEqualWeight(request.getProgram().getEqualProportion());
		}
		
		if (request.getProgram().getRuralProportion() != null) {
			program.setRpWeight(request.getProgram().getRuralProportion());
		}
		
		if (request.getProgram().getFarmProportion() != null){
			program.setFpWeight(request.getProgram().getFarmProportion());
		}
		
		if (request.getProgram().getNonInsularAreaMatching() != null) {
			program.setRequiredMatchingPercentage(request.getProgram().getNonInsularAreaMatching());
		}
		
		//THIS DOESN'T SEEM CORRECT
		program.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());
		program.setGrossAppropriation(request.getProgram().getCurrentYearGrossAmount());
		program.setFedAdminAppropriation(request.getProgram().getCurrentYearAdminAmount());
		program.setSbirAppropriation(request.getProgram().getSbirAmount());
		program.setBraAppropriation(request.getProgram().getBraAmount());
		program.setOtherAppropriation(request.getProgram().getOtherAmount());
		input.setProgram(program);
		
		//Set Previous Program
		EvansAllenProgram prevProgram = new EvansAllenProgram();
		prevProgram.setFiscalYear(request.getProgram().getFiscalYear() - 1);
		prevProgram.setName(request.getProgram().getParentProgramId());
		prevProgram.setFullName(request.getProgram().getParentProgramDescription());
		prevProgram.setAppropriation(request.getProgram().getPriorYearAmount());
		prevProgram.setGrossAppropriation(request.getProgram().getPriorYearGrossAmount());
		prevProgram.setFedAdminAppropriation(request.getProgram().getPriorYearAdminAmount());
		input.setPreviousYearProgram(prevProgram);
		
		SubProgram subProgram = new SubProgram();
		subProgram.setName(request.getProgram().getProgramId());
		subProgram.setFullName(request.getProgram().getProgramDescription());
		subProgram.setApplyAdminAdjustment(request.getProgram().isApplyAdminAdjustment());
		subProgram.setAdminAdjustmentReason(request.getProgram().getAdminAdjustmentReason());
		//subProgram.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());
		input.setSubProgram(subProgram);
		
		SubProgram prevSubProgram = new SubProgram();
		prevSubProgram.setName(request.getProgram().getProgramId());
		prevSubProgram.setFullName(request.getProgram().getProgramDescription());
		//prevSubProgram.setAppropriation(request.getProgram().getPriorYearAmount());
		input.setPreviousSubProgram(prevSubProgram);
		
		List<Institution> institutionListOut = new ArrayList<Institution>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			State state = new State();
			
			//use the state code when country = US otherwise use the country code as the state
			state.setAbbreviation(StringUtils.equals(institutionIn.getCountry(), 
					CalculationConstants.COUNTRY_CODE_US) ?
					institutionIn.getState() : institutionIn.getCountry());
			
			state.setFarmPopulation(institutionIn.getFarmPopulation());
			state.setRuralPopulation(institutionIn.getRuralPopulation());
			state.setYear(request.getProgram().getFiscalYear());
			state.setIsInsular(institutionIn.getInsularArea());
			
			EvansAllenInstitution institutionOut = new EvansAllenInstitution();
			institutionOut.setInstitutionId(institutionIn.getInstitutionId());
			institutionOut.setName(institutionIn.getInstitutionName());
			institutionOut.setState(state);
			institutionOut.setDunsNumber(institutionIn.getDunsNumber());
			institutionOut.setApplyAdminAdjustment(institutionIn.isApplyAdminAdjustment());
			
			if (institutionIn.getAdminAdjustmentAmount() != null) {
				institutionOut.setAdminAdjustmentAmount(institutionIn.getAdminAdjustmentAmount());
			}
			
			//Only set prior appropriation for  CRs
			if (StringUtils.equals(AppropriationType.CONTINUING_RESOLUTIONS.getCode(), 
					request.getProgram().getAppropriationType())) {
				institutionOut.setAppropriation(institutionIn.getPriorYearAllocation());
			}
			
			
			//Evans Allen specific
			institutionOut.setBaseAppropriation(institutionIn.getPriorYearBaseAmount());
			
			if (institutionIn.isApplyInstitutionMatching()) {
				if (institutionIn.getMatchingPercentage() != null) {
					institutionOut.setRequiredMatchingPercentage(institutionIn.getMatchingPercentage());
				}
				if (institutionIn.getMatchingAmount() != null) {
					institutionOut.setMatchingAppropriation(institutionIn.getMatchingAmount());
				}
			}
			
			institutionListOut.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionListOut);
		
		return input;
	}
}
