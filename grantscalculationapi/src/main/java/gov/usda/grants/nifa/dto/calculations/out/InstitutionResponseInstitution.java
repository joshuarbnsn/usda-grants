package gov.usda.grants.nifa.dto.calculations.out;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstitutionResponseInstitution implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("InstitutionId")
	protected String institutionId;
	
	@JsonProperty("InstitutionName")
	protected String institutionName;
	
	@JsonProperty("AllocationAmount")
	protected BigDecimal allocationAmount;
	
	@JsonProperty("RequiredMatchAmount")
	protected BigDecimal requiredMatchAmount;
	
	@JsonProperty("BaseAmount")
	protected BigDecimal baseAmount;
	
	@JsonProperty("State")
	protected String state;
	
	@JsonProperty("CalculatedLivestockIncome")
	protected BigDecimal calculatedLivestockIncome;
	
	@JsonProperty("WebNeersSystemAppropriation")
	protected BigDecimal webNeersSystemAppropriation;

	@JsonProperty("StateRank")
	protected BigDecimal stateRank;
	
	@JsonProperty("Station")
	protected String station;

	@JsonProperty("CalculatedLivestockValue")
	protected BigDecimal calculatedLivestockValue;
	
	public String getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public BigDecimal getAllocationAmount() {
		return allocationAmount;
	}

	public void setAllocationAmount(BigDecimal allocationAmount) {
		this.allocationAmount = allocationAmount;
	}

	public BigDecimal getRequiredMatchAmount() {
		return requiredMatchAmount;
	}

	public void setRequiredMatchAmount(BigDecimal requiredMatchAmount) {
		this.requiredMatchAmount = requiredMatchAmount;
	}

	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public BigDecimal getCalculatedLivestockIncome() {
		return calculatedLivestockIncome;
	}

	public void setCalculatedLivestockIncome(BigDecimal calculatedLivestockIncome) {
		this.calculatedLivestockIncome = calculatedLivestockIncome;
	}

	public BigDecimal getCalculatedLivestockValue() {
		return calculatedLivestockValue;
	}

	public void setCalculatedLivestockValue(BigDecimal calculatedLivestockValue) {
		this.calculatedLivestockValue = calculatedLivestockValue;
	}

	public BigDecimal getStateRank() {
		return stateRank;
	}

	public void setStateRank(BigDecimal stateRank) {
		this.stateRank = stateRank;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public BigDecimal getWebNeersSystemAppropriation() {
		return webNeersSystemAppropriation;
	}

	public void setWebNeersSystemAppropriation(BigDecimal webNeersSystemAppropriation) {
		this.webNeersSystemAppropriation = webNeersSystemAppropriation;
	}
	
}