package gov.usda.grants.nifa.dto.calculations.in;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstitutionRequest implements Serializable, ServiceRequest {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Program")
	public InstitutionRequestProgram program;
	
	@JsonProperty("EligibleInstitutions")
	public List<InstitutionRequestInstitution> eligibleInstitutions;

	public InstitutionRequestProgram getProgram() {
		return program;
	}

	public void setProgram(InstitutionRequestProgram program) {
		this.program = program;
	}

	public List<InstitutionRequestInstitution> getEligibleInstitutions() {
		return eligibleInstitutions;
	}

	public void setEligibleInstitutions(List<InstitutionRequestInstitution> eligibleInstitutions) {
		this.eligibleInstitutions = eligibleInstitutions;
	}
}
