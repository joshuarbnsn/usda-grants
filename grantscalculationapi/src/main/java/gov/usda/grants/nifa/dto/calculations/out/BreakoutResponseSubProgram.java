package gov.usda.grants.nifa.dto.calculations.out;

import java.io.Serializable;
import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonProperty;

public class BreakoutResponseSubProgram implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("SubProgramId")
	protected String subProgramId;
	
	@JsonProperty("Amount")
	protected BigDecimal amount;
	
	@JsonProperty("ForecastedAmount")
	protected BigDecimal forcastedAmount;

	public String getSubProgramId() {
		return subProgramId;
	}

	public void setSubProgramId(String subProgramId) {
		this.subProgramId = subProgramId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getForcastedAmount() {
		return forcastedAmount;
	}

	public void setForcastedAmount(BigDecimal forcastedAmount) {
		this.forcastedAmount = forcastedAmount;
	}
}
