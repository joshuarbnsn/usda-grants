package gov.usda.grants.nifa.mapper;

import org.apache.commons.lang3.StringUtils;

import gov.usda.nifa.cgc.model.ProgramType;
import gov.usda.nifa.cgc.model.SubProgramType;

public class MapperFactory {

	public static InstitutionMapper getInstitutionMapper(String programName, String subProgramName) 
	{
		
		if (StringUtils.equals(ProgramType.SMITH_LEVER.getName(), programName)) 
		{
			return new SmithLeverInstitutionMapper();
		}
		else if (StringUtils.equals(ProgramType.SECTION_1444.getName(), programName))
		{
			return new Section1444InstitutionMapper();
		}
		else if (StringUtils.equals(ProgramType.EVANS_ALLEN.getName(), programName))
		{
			return new EvansAllenInstitutionMapper();
		} 
		else if (StringUtils.equals(ProgramType.RENEWABLE_RESOURCES.getName(), programName))
		{
			return new RREAInstitutionMapper();
		} 
		else if (StringUtils.equals(ProgramType.HATCH_ACT.getName(), programName))
		{
			if(StringUtils.equals(SubProgramType.HA_REG.getName(), subProgramName)) {
				return new HatchInstitutionMapper();
			} else {
				return new HatchMultistateInstitutionMapper();
			}
		} 
		else if (StringUtils.equals(ProgramType.FOOD_NUTRITION.getName(), programName))
		{
			return new EFNEPInstitutionMapper();
		} 
		else if (StringUtils.equals(ProgramType.MCINTIRE_STENNIS.getName(), programName))
		{
			return new MSCFRInstitutionMapper();
		}
		else if (StringUtils.equals(ProgramType.ANIMAL_HEALTH.getName(), programName))
		{
			return new AHADRInstitutionMapper();
		}
		else 
		{
			return new SmithLeverInstitutionMapper();
		}
	}
	
	public static BreakoutMapper getBreakoutMapper(String programName) 
	{
		
		if (StringUtils.equals(ProgramType.SMITH_LEVER.getName(), programName)) 
		{
			return new SmithLeverBreakoutMapper();
		}
		else if (StringUtils.equals(ProgramType.RENEWABLE_RESOURCES.getName(), programName))
		{
			return new RreaBreakoutMapper();
		}
		else 
		{
			return new SmithLeverBreakoutMapper();
		}
	}
}
