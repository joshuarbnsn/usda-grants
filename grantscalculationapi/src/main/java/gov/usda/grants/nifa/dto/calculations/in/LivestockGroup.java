package gov.usda.grants.nifa.dto.calculations.in;

import java.util.List;

public class LivestockGroup {

	public List<Livestock> currentYearLivestock;
	
	public List<Livestock> priorYearLivestock;

	public List<Livestock> priorYearMinusOneLivestock;

	public List<Livestock> getCurrentYearLivestock() {
		return currentYearLivestock;
	}

	public void setCurrentYearLivestock(List<Livestock> currentYearLivestock) {
		this.currentYearLivestock = currentYearLivestock;
	}

	public List<Livestock> getPriorYearLivestock() {
		return priorYearLivestock;
	}

	public void setPriorYearLivestock(List<Livestock> priorYearLivestock) {
		this.priorYearLivestock = priorYearLivestock;
	}

	public List<Livestock> getPriorYearMinusOneLivestock() {
		return priorYearMinusOneLivestock;
	}

	public void setPriorYearMinusOneLivestock(List<Livestock> priorYearMinusOneLivestock) {
		this.priorYearMinusOneLivestock = priorYearMinusOneLivestock;
	}

}
