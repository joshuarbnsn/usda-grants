package gov.usda.grants.nifa.dto.calculations.in;

import java.io.Serializable;
import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonProperty;

public class BreakoutRequestSubProgram implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("SubProgramId")
	protected String subProgramId;
	
	@JsonProperty("PriorYearAmount")
	protected BigDecimal priorYearAmount;
	
	public String getSubProgramId() {
		return subProgramId;
	}

	public void setSubProgramId(String subProgramId) {
		this.subProgramId = subProgramId;
	}

	public BigDecimal getPriorYearAmount() {
		return priorYearAmount;
	}

	public void setPriorYearAmount(BigDecimal priorYearAmount) {
		this.priorYearAmount = priorYearAmount;
	}
}
