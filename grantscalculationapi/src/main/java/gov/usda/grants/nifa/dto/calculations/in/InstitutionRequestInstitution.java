package gov.usda.grants.nifa.dto.calculations.in;

import java.io.Serializable;
import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstitutionRequestInstitution implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("InstitutionId")
	protected String institutionId;
	
	@JsonProperty("InstitutionName")
	protected String institutionName;
	
	@JsonProperty("Country")
	protected String country;
	
	@JsonProperty("State")
	protected String state;
	
	@JsonProperty("InsularArea")
	protected boolean insularArea;
	
	@JsonProperty("RuralPopulation")
	protected long ruralPopulation;

	@JsonProperty("FarmPopulation")
	protected long farmPopulation;

	@JsonProperty("PriorYearAllocation")
	protected BigDecimal priorYearAllocation;
	
	@JsonProperty("ApplyInstitutionMatching")
	protected boolean applyInstitutionMatching;
	
	@JsonProperty("MatchingPercentage")
	protected BigDecimal matchingPercentage;
	
	@JsonProperty("MatchingAmount")
	protected BigDecimal matchingAmount;
	
	@JsonProperty("PriorYearBaseAmount")
	protected BigDecimal priorYearBaseAmount;
	
	@JsonProperty("AllocationPercentage")
	protected BigDecimal allocationPercentage;
	
	@JsonProperty("DunsNumber")
	protected String dunsNumber;
	
	@JsonProperty("Station")
	protected String station;
	
	@JsonProperty("Region")
	protected String region;
	
	@JsonProperty("RegionalFundAmount")
	protected BigDecimal regionalFundAmount;
	
	@JsonProperty("NationalFundAmount")
	protected BigDecimal nationalFundAmount;
	
	@JsonProperty("NationalTrustRecipient")
	protected boolean nationalTrustRecipient;
	
	@JsonProperty("BaseAmount")
	protected BigDecimal baseAmount;
	
	@JsonProperty("ForestlandAcres")
	protected BigDecimal forestlandAcres;
	
	@JsonProperty("NetGrowthCubicFeet")
	protected BigDecimal netGrowthCubicFeet;
	
	@JsonProperty("GrazingLandAcres")
	protected BigDecimal grazingLandAcres;
	
	@JsonProperty("WoodIndustryEmployment")
	protected BigDecimal woodIndustryEmployment;
	
	@JsonProperty("TimberRemovalsCubicFeet")
	protected BigDecimal timberRemovalsCubicFeet;
	
	@JsonProperty("LandGrantType")
	protected String landGrantType;
	
	@JsonProperty("UrbanPopulation")
	protected Long urbanPopulation;
	
	@JsonProperty("TotalStatePopulation")
	protected Long totalStatePopulation;
	
	@JsonProperty("PovertyPopulation")
	protected Long povertyPopulation;
	
	@JsonProperty("WebNEERSRecipient")
	protected boolean webNEERSRecipient;
	
	@JsonProperty("PriorYearWebNEERSAmount")
	protected BigDecimal priorYearWebNEERSAmount;
	
	@JsonProperty("CommercialForestValue")
	protected BigDecimal commercialForestValue;
	
	@JsonProperty("TimberCutValue")
	protected BigDecimal timberCutValue;
	
	@JsonProperty("ForestryResearchValue")
	protected BigDecimal forestryResearchValue;
	
	@JsonProperty("PriorYearStateRank1")
	protected int priorYearStateRank1;
	
	@JsonProperty("PriorYearStateRank2")
	protected int priorYearStateRank2;
	
	@JsonProperty("LivestockValue")
	protected LivestockGroup livestockValue;

	@JsonProperty("LivestockIncome")
	protected LivestockGroup livestockIncome;
	
	@JsonProperty("ScientificYears")
	protected RecentAmountsContainer scientificYears;

	@JsonProperty("ResearchCapacity")
	protected RecentAmountsContainer researchCapacity;
	
	@JsonProperty("PriorYearCalculatedLivestockValue")
	protected BigDecimal priorYearCalculatedLivestockValue;

	@JsonProperty("PriorYearCalculatedLivestockIncome")
	protected BigDecimal priorYearCalculatedLivestockIncome;
	
	@JsonProperty("ApplyAdminAdjustment")
	protected boolean applyAdminAdjustment;

	@JsonProperty("AdminAdjustmentAmount")
	protected BigDecimal adminAdjustmentAmount;
	
	
	public String getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean getInsularArea() {
		return insularArea;
	}

	public void setInsularArea(boolean insularArea) {
		this.insularArea = insularArea;
	}

	public long getRuralPopulation() {
		return ruralPopulation;
	}

	public void setRuralPopulation(long ruralPopulation) {
		this.ruralPopulation = ruralPopulation;
	}

	public long getFarmPopulation() {
		return farmPopulation;
	}

	public void setFarmPopulation(long farmPopulation) {
		this.farmPopulation = farmPopulation;
	}

	public BigDecimal getMatchingPercentage() {
		return matchingPercentage;
	}

	public void setMatchingPercentage(BigDecimal matchingPercentage) {
		this.matchingPercentage = matchingPercentage;
	}

	public boolean isApplyInstitutionMatching() {
		return applyInstitutionMatching;
	}

	public void setApplyInstitutionMatching(boolean applyInstitutionMatching) {
		this.applyInstitutionMatching = applyInstitutionMatching;
	}

	public BigDecimal getMatchingAmount() {
		return matchingAmount;
	}

	public void setMatchingAmount(BigDecimal matchingAmount) {
		this.matchingAmount = matchingAmount;
	}

	public BigDecimal getPriorYearAllocation() {
		return priorYearAllocation;
	}

	public void setPriorYearAllocation(BigDecimal priorYearAllocation) {
		this.priorYearAllocation = priorYearAllocation;
	}

	public BigDecimal getPriorYearBaseAmount() {
		return priorYearBaseAmount;
	}

	public void setPriorYearBaseAmount(BigDecimal priorYearBaseAmount) {
		this.priorYearBaseAmount = priorYearBaseAmount;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public BigDecimal getAllocationPercentage() {
		return allocationPercentage;
	}

	public void setAllocationPercentage(BigDecimal allocationPercentage) {
		this.allocationPercentage = allocationPercentage;
	}

	public String getDunsNumber() {
		return dunsNumber;
	}

	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public BigDecimal getRegionalFundAmount() {
		return regionalFundAmount;
	}

	public void setRegionalFundAmount(BigDecimal regionalFundAmount) {
		this.regionalFundAmount = regionalFundAmount;
	}

	public BigDecimal getNationalFundAmount() {
		return nationalFundAmount;
	}

	public void setNationalFundAmount(BigDecimal interregionalFundAmount) {
		this.nationalFundAmount = interregionalFundAmount;
	}

	public boolean isNationalTrustRecipient() {
		return nationalTrustRecipient;
	}

	public void setNationalTrustRecipient(boolean nationalTrustRecipient) {
		this.nationalTrustRecipient = nationalTrustRecipient;
	}

	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public BigDecimal getForestlandAcres() {
		return forestlandAcres;
	}

	public void setForestlandAcres(BigDecimal forestlandAcres) {
		this.forestlandAcres = forestlandAcres;
	}

	public BigDecimal getNetGrowthCubicFeet() {
		return netGrowthCubicFeet;
	}

	public void setNetGrowthCubicFeet(BigDecimal netGrowthCubicFeet) {
		this.netGrowthCubicFeet = netGrowthCubicFeet;
	}

	public BigDecimal getGrazingLandAcres() {
		return grazingLandAcres;
	}

	public void setGrazingLandAcres(BigDecimal grazingLandAcres) {
		this.grazingLandAcres = grazingLandAcres;
	}

	public BigDecimal getWoodIndustryEmployment() {
		return woodIndustryEmployment;
	}

	public void setWoodIndustryEmployment(BigDecimal woodIndustryEmployment) {
		this.woodIndustryEmployment = woodIndustryEmployment;
	}

	public BigDecimal getTimberRemovalsCubicFeet() {
		return timberRemovalsCubicFeet;
	}

	public void setTimberRemovalsCubicFeet(BigDecimal timberRemovalsCubicFeet) {
		this.timberRemovalsCubicFeet = timberRemovalsCubicFeet;
	}

	public Long getUrbanPopulation() {
		return urbanPopulation;
	}

	public void setUrbanPopulation(Long urbanPopulation) {
		this.urbanPopulation = urbanPopulation;
	}

	public Long getTotalStatePopulation() {
		return totalStatePopulation;
	}

	public void setTotalStatePopulation(Long totalStatePopulation) {
		this.totalStatePopulation = totalStatePopulation;
	}

	public String getLandGrantType() {
		return landGrantType;
	}

	public void setLandGrantType(String landGrantType) {
		this.landGrantType = landGrantType;
	}

	public Long getPovertyPopulation() {
		return povertyPopulation;
	}

	public void setPovertyPopulation(Long povertyPopulation) {
		this.povertyPopulation = povertyPopulation;
	}

	public boolean isWebNEERSRecipient() {
		return webNEERSRecipient;
	}

	public void setWebNEERSRecipient(boolean webNEERSRecipient) {
		this.webNEERSRecipient = webNEERSRecipient;
	}

	public BigDecimal getPriorYearWebNEERSAmount() {
		return priorYearWebNEERSAmount;
	}

	public void setPriorYearWebNEERSAmount(BigDecimal priorYearWebNEERSAmount) {
		this.priorYearWebNEERSAmount = priorYearWebNEERSAmount;
	}

	public BigDecimal getCommercialForestValue() {
		return commercialForestValue;
	}

	public void setCommercialForestValue(BigDecimal commercialForestValue) {
		this.commercialForestValue = commercialForestValue;
	}

	public BigDecimal getTimberCutValue() {
		return timberCutValue;
	}

	public void setTimberCutValue(BigDecimal timberCutValue) {
		this.timberCutValue = timberCutValue;
	}

	public BigDecimal getForestryResearchValue() {
		return forestryResearchValue;
	}

	public void setForestryResearchValue(BigDecimal forestryResearchValue) {
		this.forestryResearchValue = forestryResearchValue;
	}

	public int getPriorYearStateRank1() {
		return priorYearStateRank1;
	}

	public void setPriorYearStateRank1(int priorYearStateRank1) {
		this.priorYearStateRank1 = priorYearStateRank1;
	}

	public int getPriorYearStateRank2() {
		return priorYearStateRank2;
	}

	public void setPriorYearStateRank2(int priorYearStateRank2) {
		this.priorYearStateRank2 = priorYearStateRank2;
	}

	public LivestockGroup getLivestockValue() {
		return livestockValue;
	}

	public void setLivestockValue(LivestockGroup livestockValue) {
		this.livestockValue = livestockValue;
	}

	public LivestockGroup getLivestockIncome() {
		return livestockIncome;
	}

	public void setLivestockIncome(LivestockGroup livestockIncome) {
		this.livestockIncome = livestockIncome;
	}

	public RecentAmountsContainer getScientificYears() {
		return scientificYears;
	}

	public void setScientificYears(RecentAmountsContainer scientificYears) {
		this.scientificYears = scientificYears;
	}

	public RecentAmountsContainer getResearchCapacity() {
		return researchCapacity;
	}

	public void setResearchCapacity(RecentAmountsContainer researchCapacity) {
		this.researchCapacity = researchCapacity;
	}

	public BigDecimal getPriorYearCalculatedLivestockValue() {
		return priorYearCalculatedLivestockValue;
	}

	public void setPriorYearCalculatedLivestockValue(BigDecimal priorYearCalculatedLivestockValue) {
		this.priorYearCalculatedLivestockValue = priorYearCalculatedLivestockValue;
	}

	public BigDecimal getPriorYearCalculatedLivestockIncome() {
		return priorYearCalculatedLivestockIncome;
	}

	public void setPriorYearCalculatedLivestockIncome(BigDecimal priorYearCalculatedLivestockIncome) {
		this.priorYearCalculatedLivestockIncome = priorYearCalculatedLivestockIncome;
	}

	public boolean isApplyAdminAdjustment() {
		return applyAdminAdjustment;
	}

	public void setApplyAdminAdjustment(boolean applyAdminAdjustment) {
		this.applyAdminAdjustment = applyAdminAdjustment;
	}

	public BigDecimal getAdminAdjustmentAmount() {
		return adminAdjustmentAmount;
	}

	public void setAdminAdjustmentAmount(BigDecimal adminAdjustmentAmount) {
		this.adminAdjustmentAmount = adminAdjustmentAmount;
	}
}