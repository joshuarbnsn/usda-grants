package gov.usda.grants.nifa.dto.calculations.in;

import java.math.BigDecimal;

public class Livestock {

	public String name;
	
	public BigDecimal amount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
