package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequestSubProgram;
import gov.usda.nifa.cgc.dto.BreakoutDTO;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.RREAProgram;
import gov.usda.nifa.cgc.model.SubProgram;

public class RreaBreakoutMapper extends BreakoutMapper {

	public BreakoutDTO map(BreakoutRequest request) {
		
		BreakoutDTO dto = new BreakoutDTO();
		
		//populate fiscal year on calculation input object
		Integer fiscalYear = new Integer(request.getProgram().getFiscalYear());
		dto.setFiscalYear(fiscalYear);

		//populate the program object for the Smith Lever Breakout Calculation
		RREAProgram program = new RREAProgram();
		program.setName(request.getProgram().getProgramId());
		program.setFullName(request.getProgram().getProgramDescription());
		program.setFiscalYear(fiscalYear);
		program.setAppropriationType(request.getProgram().getAppropriationType());
		
		if (request.getProgram().getPaymentToStates() != null){
			program.setAppropriation(request.getProgram().getPaymentToStates());
		}
		
		/*
		if (StringUtils.equals(AppropriationType.FINAL.getCode(), request.getProgram().getAppropriationType()) ||
				StringUtils.equals(AppropriationType.RECESSION.getCode(), request.getProgram().getAppropriationType())) {
		
			if (request.getProgram().getNffpSetAsideAmount() != null) {
				program.setNffpSetAsideAppropriation(request.getProgram().getNffpSetAsideAmount());
			}
		}
		*/
		
		if (request.getProgram().getNffpSetAsideAmount() != null) {
			program.setNffpSetAsideAppropriation(request.getProgram().getNffpSetAsideAmount());
		}
				
		dto.setProgram(program);

		//loop through subprograms in the request to create the calculation subprogram objects
		List<SubProgram> subProgramList = new ArrayList<SubProgram>();
		for ( BreakoutRequestSubProgram requestSub : request.getProgram().getSubProgramList()) {

			SubProgram udcSub = new SubProgram();
			udcSub.setName(requestSub.getSubProgramId());
			
			if (StringUtils.equals(AppropriationType.CONTINUING_RESOLUTIONS.getCode(), request.getProgram().getAppropriationType())) {
			
				udcSub.setAppropriation(requestSub.getPriorYearAmount());
			}
			
			subProgramList.add(udcSub);
		}

		//add subprogram list from request to calculation input object
		dto.setPreviousSubPrograms(subProgramList);
		
		return dto;
	}
}
