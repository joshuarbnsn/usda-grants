package gov.usda.grants.nifa.dto.calculations.in;

public interface ServiceRequest {

	public Program getProgram();
}
