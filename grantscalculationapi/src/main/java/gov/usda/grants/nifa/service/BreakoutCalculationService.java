package gov.usda.grants.nifa.service;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponse;

public interface BreakoutCalculationService {

	BreakoutResponse CalculateBreakout(BreakoutRequest request);
}
