package gov.usda.grants.nifa.dto.calculations.out;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class CalculationStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Code")
	public int code;

	@JsonProperty("Description")
	public String description;

	@JsonProperty("Message")
	public String message;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
