package gov.usda.grants.nifa.validation;

import gov.usda.grants.nifa.dto.calculations.in.ServiceRequest;
import gov.usda.grants.nifa.dto.calculations.out.ServiceResponse;

public interface RequestValidator {

	public boolean validate(ServiceRequest request, ServiceResponse response);
}
