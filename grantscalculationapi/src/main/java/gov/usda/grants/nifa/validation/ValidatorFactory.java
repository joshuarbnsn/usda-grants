package gov.usda.grants.nifa.validation;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.ServiceRequest;
import gov.usda.nifa.cgc.model.ProgramType;

public class ValidatorFactory {

	public static RequestValidator getValidator(ServiceRequest request) 
	{
		
		if (request  instanceof BreakoutRequest) 
		{
			if (StringUtils.equals(ProgramType.SMITH_LEVER.getName(), request.getProgram().getProgramId())) 
			{
				return new SmithLeverBreakoutValidator();
			}
			else 
			{
				return null;
			}
		}
		else if (request instanceof InstitutionRequest)
		{
			InstitutionRequest ir = (InstitutionRequest) request;
			
			if (StringUtils.equals(ProgramType.ANIMAL_HEALTH.getName(), ir.getProgram().getParentProgramId())) 
			{
				return new AHADRInstitutionValidator();
			}
			else 
				return null;
		} 
		else 
			return null;
	}
}
