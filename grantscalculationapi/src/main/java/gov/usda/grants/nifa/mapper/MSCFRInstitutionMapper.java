package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.EFNEPInstitution;
import gov.usda.nifa.cgc.model.EFNEPProgram;
import gov.usda.nifa.cgc.model.EFNEPState;
import gov.usda.nifa.cgc.model.HAMInstitution;
import gov.usda.nifa.cgc.model.HAMProgram;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.MSCFRInstitution;
import gov.usda.nifa.cgc.model.MSCFRProgram;
import gov.usda.nifa.cgc.model.MSCFRState;
import gov.usda.nifa.cgc.model.RREAInstitution;
import gov.usda.nifa.cgc.model.RREAProgram;
import gov.usda.nifa.cgc.model.RREAState;
import gov.usda.nifa.cgc.model.State;

public class MSCFRInstitutionMapper extends InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		
		InstitutionDTO input = super.map(request);
		
		/*  
		    The local map method must be called before the super class method
			in 	order to create the proper objects and avoid class cast exception		
		 */
		
		//Map local program level fields
		mapProgramLevel(request, input);
		
		// 	map the parent Program Fields
		super.mapProgramLevel(request, input);
		
		//reset input list of institutions
		input.setEligibleInstitutions(new ArrayList<>());
		
		List<MSCFRInstitution> institutionList = new ArrayList<>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			MSCFRInstitution institutionOut = new MSCFRInstitution();
			
			//Map RREA Institution fields first
			mapInstitutionLevel(request, institutionIn, institutionOut);
			
			super.mapInstitutionLevel(request, institutionIn, institutionOut);
			
			institutionList.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionList);
		
		return input;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		MSCFRProgram program = getProgram(input);
		
		if (request.getProgram().getCurrentYearGrossAmount() != null) {
			program.setGrossAppropriation(request.getProgram().getCurrentYearGrossAmount());
		}
		
		if (request.getProgram().getCurrentYearAdminAmount() != null) {
			program.setFedAdminAppropriation(request.getProgram().getCurrentYearAdminAmount());
		}
		
		if (request.getProgram().getSbirAmount() != null) {
			program.setSbirAppropriation(request.getProgram().getSbirAmount());
		}
		
		if (request.getProgram().getBraAmount() != null) {
			program.setBraAppropriation(request.getProgram().getBraAmount());
		}
		
		if (request.getProgram().getEqualAllocationAmount() != null) {
			program.setEqualAppropriation(request.getProgram().getEqualAllocationAmount());
		}
		
		if (request.getProgram().getInsularAreaThreshold() != null) {
			program.setInsularAreaThreshold(request.getProgram().getInsularAreaThreshold());
		}
		
		if (request.getProgram().getCommercialForestProportion() != null) {
			program.setNfcForestlandAreaWeight(request.getProgram().getCommercialForestProportion());
		}
		
		if (request.getProgram().getTimberCutProportion() != null) {
			program.setTcaStockVolumeWeight(request.getProgram().getTimberCutProportion());
		}
		
		if (request.getProgram().getForestryResearchProportion() != null) {
			program.setNfsfrTotalExpendituresWeight(request.getProgram().getForestryResearchProportion());
		}
		
		if (request.getProgram().getInsularAreaMatching() != null){
			program.setInsularWeight(request.getProgram().getInsularAreaMatching());
		}
		
		if (request.getProgram().getNonInsularAreaMatching() != null) {
			program.setRequiredMatchingPercentage(request.getProgram().getNonInsularAreaMatching());
		}
	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, MSCFRInstitution institutionOut) {
		
		MSCFRState state = getState(institutionOut);
		
		if (institutionIn.getCommercialForestValue() != null) {
			state.setNfcForestlandArea(institutionIn.getCommercialForestValue());
		}
		
		if (institutionIn.getTimberCutValue() != null) {
			state.setTcaStockVolume(institutionIn.getTimberCutValue());
		}
		
		if (institutionIn.getForestryResearchValue() != null) {
			state.setNfsfrTotalExpenditures(institutionIn.getForestryResearchValue());
		}
		
		state.setHistoricalFY1StateRank(institutionIn.getPriorYearStateRank1());
		state.setHistoricalFY2StateRank(institutionIn.getPriorYearStateRank2());
		
		if (institutionIn.getAllocationPercentage() != null) {
			institutionOut.setAllocWeight(institutionIn.getAllocationPercentage());
		}
		
		if (institutionIn.getStation() != null) {
			institutionOut.setStation(institutionIn.getStation());
		}
		
		state.setIsInsular(institutionIn.getInsularArea());
		
		if (institutionIn.isApplyInstitutionMatching()) {
			if (institutionIn.getMatchingPercentage() != null) {
				institutionOut.setRequiredMatchingPercentage(institutionIn.getMatchingPercentage());
			}
			if (institutionIn.getMatchingAmount() != null) {
				institutionOut.setMatchingAppropriation(institutionIn.getMatchingAmount());
			}
		}
		
	}
	
	public MSCFRProgram getProgram(InstitutionDTO input) {
		MSCFRProgram program = (MSCFRProgram) input.getProgram();
		
		if (program == null) {
			program = new MSCFRProgram();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public MSCFRProgram getPrevProgram(InstitutionDTO input) {
		MSCFRProgram program = (MSCFRProgram) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new MSCFRProgram();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public List<? extends Institution> getInstitutionListOut(InstitutionDTO input) {
		@SuppressWarnings("unchecked")
		List<MSCFRInstitution> institutionListOut = (List<MSCFRInstitution>) input.getEligibleInstitutions();
		
		if (institutionListOut == null) {
			institutionListOut = new ArrayList<>();
			input.setEligibleInstitutions(institutionListOut);
		}
		
		return institutionListOut;
	}
	
	public MSCFRState getState(MSCFRInstitution institutionOut) {
		MSCFRState state = (MSCFRState) institutionOut.getState();
		
		if (state == null) {
			state = new MSCFRState();
			institutionOut.setState(state);
		}
		
		return state;
	}
}