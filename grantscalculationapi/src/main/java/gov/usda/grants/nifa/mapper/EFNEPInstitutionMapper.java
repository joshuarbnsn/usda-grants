package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.EFNEPInstitution;
import gov.usda.nifa.cgc.model.EFNEPProgram;
import gov.usda.nifa.cgc.model.EFNEPState;
import gov.usda.nifa.cgc.model.HAMInstitution;
import gov.usda.nifa.cgc.model.HAMProgram;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.RREAInstitution;
import gov.usda.nifa.cgc.model.RREAProgram;
import gov.usda.nifa.cgc.model.RREAState;
import gov.usda.nifa.cgc.model.State;

public class EFNEPInstitutionMapper extends InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		
		InstitutionDTO input = super.map(request);
		
		//RREAProgram program = getProgram(input);
		
		/*  
		 *  The local map method must be called before the super class method
			in 	order to create the proper objects and avoid class cast exception		
		 */
		
		//Map local program level fields
		mapProgramLevel(request, input);
		
		// 	map the parent Program Fields
		super.mapProgramLevel(request, input);
		
		//reset input list of institutions
		input.setEligibleInstitutions(new ArrayList<>());
		
		List<EFNEPInstitution> institutionList = new ArrayList<>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			EFNEPInstitution institutionOut = new EFNEPInstitution();
			
			//Map RREA Institution fields first
			mapInstitutionLevel(request, institutionIn, institutionOut);
			
			super.mapInstitutionLevel(request, institutionIn, institutionOut);
			
			institutionList.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionList);
		
		return input;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		EFNEPProgram program = getProgram(input);
		
		if (request.getProgram().getWebNEERSAmount() != null) {
			program.setWebNeersSystemAppropriation(request.getProgram().getWebNEERSAmount());
		}
		
		if (request.getProgram().getFy1981BaseAmount() != null) {
			program.setFy1981Appropriation(request.getProgram().getFy1981BaseAmount());
		}
		
		if (request.getProgram().getFy2007BaseAmount() != null) {
			program.setFy2007Appropriation(request.getProgram().getFy2007BaseAmount());
		}
		
		if (request.getProgram().getEqualAllocationAmount() != null) {
			program.setEqualAppropriation(request.getProgram().getEqualAllocationAmount());
		}
		
		if (request.getProgram().getLgu1862FundProportion() != null) {
			program.setLgu1862FundWeight(request.getProgram().getLgu1862FundProportion());
		}
		
		if (request.getProgram().getLgu1890FundProportion() != null) {
			program.setLgu1890FundWeight(request.getProgram().getLgu1890FundProportion());
		}
	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, EFNEPInstitution institutionOut) {
		
		EFNEPState state = getState(institutionOut);
		
		if (institutionIn.getPovertyPopulation() != null) {
			state.setPovertyPopulation(institutionIn.getPovertyPopulation());
		}
		
		institutionOut.setLandGrantType(institutionIn.getLandGrantType());
		
		if (institutionIn.getBaseAmount() != null) {
			institutionOut.setBaseAppropriation(institutionIn.getBaseAmount());
		}
		
		institutionOut.setIsWebNeersSystemRecipient(institutionIn.isWebNEERSRecipient());
		
		if (institutionIn.getPriorYearWebNEERSAmount() != null) {
			institutionOut.setWebNeersSystemAppropriation(institutionIn.getPriorYearWebNEERSAmount());
		}
	}
	
	public EFNEPProgram getProgram(InstitutionDTO input) {
		EFNEPProgram program = (EFNEPProgram) input.getProgram();
		
		if (program == null) {
			program = new EFNEPProgram();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public EFNEPProgram getPrevProgram(InstitutionDTO input) {
		EFNEPProgram program = (EFNEPProgram) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new EFNEPProgram();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public List<? extends Institution> getInstitutionListOut(InstitutionDTO input) {
		@SuppressWarnings("unchecked")
		List<EFNEPInstitution> institutionListOut = (List<EFNEPInstitution>) input.getEligibleInstitutions();
		
		if (institutionListOut == null) {
			institutionListOut = new ArrayList<>();
			input.setEligibleInstitutions(institutionListOut);
		}
		
		return institutionListOut;
	}
	
	public EFNEPState getState(EFNEPInstitution institutionOut) {
		EFNEPState state = (EFNEPState) institutionOut.getState();
		
		if (state == null) {
			state = new EFNEPState();
			institutionOut.setState(state);
		}
		
		return state;
	}
}