package gov.usda.grants.nifa.dto.calculations.out;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class BreakoutResponseProgram implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("PaymentPercentage")
	protected BigDecimal paymentPercentage;
	
	@JsonProperty("SubPrograms")
	protected List<BreakoutResponseSubProgram> subProgramList;

	public List<BreakoutResponseSubProgram> getSubProgramList() {
		return subProgramList;
	}

	public void setSubProgramList(List<BreakoutResponseSubProgram> subProgramList) {
		this.subProgramList = subProgramList;
	}

	public BigDecimal getPaymentPercentage() {
		return paymentPercentage;
	}

	public void setPaymentPercentage(BigDecimal paymentPercentage) {
		this.paymentPercentage = paymentPercentage;
	}
	
}