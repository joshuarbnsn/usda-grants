package gov.usda.grants.nifa.dto.calculations.out;

import java.util.List;

public interface ServiceResponse {

	public List<CalculationStatus> getStatus();
}
