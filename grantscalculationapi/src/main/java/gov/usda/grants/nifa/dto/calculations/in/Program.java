package gov.usda.grants.nifa.dto.calculations.in;

public interface Program {

	public String getProgramId();
}
