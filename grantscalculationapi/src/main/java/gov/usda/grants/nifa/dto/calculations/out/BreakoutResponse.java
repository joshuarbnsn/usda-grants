package gov.usda.grants.nifa.dto.calculations.out;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponseProgram;

public class BreakoutResponse implements Serializable, ServiceResponse  {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Program")
	public BreakoutResponseProgram program;

	@JsonProperty("AuditLog")
	public String auditLog;
	
	@JsonProperty("Status")
	public List<CalculationStatus> status;
	
	public BreakoutResponseProgram getProgram() {
		return program;
	}

	public void setProgram(BreakoutResponseProgram program) {
		this.program = program;
	}

	public String getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(String auditLog) {
		this.auditLog = auditLog;
	}

	public List<CalculationStatus> getStatus() {
		if (status == null) {
			status = new ArrayList<CalculationStatus>();
		}
		return status;
	}

	public void setStatus(List<CalculationStatus> status) {
		this.status = status;
	}

}
