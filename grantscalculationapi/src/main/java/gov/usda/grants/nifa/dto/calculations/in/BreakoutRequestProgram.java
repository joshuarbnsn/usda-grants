package gov.usda.grants.nifa.dto.calculations.in;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class BreakoutRequestProgram implements Serializable, Program{

	private static final long serialVersionUID = 1L;

	@JsonProperty("Agency")
	protected String agencyId;
	
	@JsonProperty("ProgramId")
	protected String programId;
	
	@JsonProperty("ProgramDescription")
	protected String programDescription;
	
	@JsonProperty("FiscalYear")
	protected int fiscalYear;
	
	@JsonProperty("AppropriationType")
	protected String appropriationType;
	
	@JsonProperty("MESetAsideAmount")
	protected BigDecimal meSetAsideAmount;
	
	@JsonProperty("MBSetAsideAmount")
	protected BigDecimal mbSetAsideAmount;
	
	@JsonProperty("PperaIncreaseAmount")
	protected BigDecimal pperaIncreaseAmount;
	
	@JsonProperty("NffpSetAsideAmount")
	protected BigDecimal nffpSetAsideAmount;
	
	@JsonProperty("PaymentToStates")
	protected BigDecimal paymentToStates;
	
	@JsonProperty("ForecastedPaymentToStates")
	protected BigDecimal forecastedPaymentToStates;
	
	@JsonProperty("SubPrograms")
	protected List<BreakoutRequestSubProgram> subProgramList;

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public int getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(int fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public String getAppropriationType() {
		return appropriationType;
	}

	public void setAppropriationType(String appropriationType) {
		this.appropriationType = appropriationType;
	}

	public BigDecimal getMeSetAsideAmount() {
		return meSetAsideAmount;
	}

	public void setMeSetAsideAmount(BigDecimal meSetAsideAmount) {
		this.meSetAsideAmount = meSetAsideAmount;
	}

	public BigDecimal getMbSetAsideAmount() {
		return mbSetAsideAmount;
	}

	public void setMbSetAsideAmount(BigDecimal mbSetAsideAmount) {
		this.mbSetAsideAmount = mbSetAsideAmount;
	}

	public BigDecimal getPperaIncreaseAmount() {
		return pperaIncreaseAmount;
	}

	public void setPperaIncreaseAmount(BigDecimal pperaIncreaseAmount) {
		this.pperaIncreaseAmount = pperaIncreaseAmount;
	}

	public BigDecimal getPaymentToStates() {
		return paymentToStates;
	}

	public void setPaymentToStates(BigDecimal paymentToStates) {
		this.paymentToStates = paymentToStates;
	}

	public BigDecimal getForecastedPaymentToStates() {
		return forecastedPaymentToStates;
	}

	public void setForecastedPaymentToStates(BigDecimal forecastedPaymentToStates) {
		this.forecastedPaymentToStates = forecastedPaymentToStates;
	}

	public List<BreakoutRequestSubProgram> getSubProgramList() {
		return subProgramList;
	}

	public void setSubProgramList(List<BreakoutRequestSubProgram> subProgramList) {
		this.subProgramList = subProgramList;
	}

	public String getProgramDescription() {
		return programDescription;
	}

	public void setProgramDescription(String programDescription) {
		this.programDescription = programDescription;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public BigDecimal getNffpSetAsideAmount() {
		return nffpSetAsideAmount;
	}

	public void setNffpSetAsideAmount(BigDecimal nffpSetAsideAmount) {
		this.nffpSetAsideAmount = nffpSetAsideAmount;
	}
}