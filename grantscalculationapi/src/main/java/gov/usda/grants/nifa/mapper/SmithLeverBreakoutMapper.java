package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequestSubProgram;
import gov.usda.grants.nifa.validation.SmithLeverBreakoutValidator;
import gov.usda.nifa.cgc.dto.BreakoutDTO;
import gov.usda.nifa.cgc.model.ProgramType;
import gov.usda.nifa.cgc.model.SmithLeverProgram;
import gov.usda.nifa.cgc.model.SubProgram;

public class SmithLeverBreakoutMapper extends BreakoutMapper {

	public BreakoutDTO map(BreakoutRequest request) {
		
		BreakoutDTO dto = new BreakoutDTO();
		
		//populate fiscal year on calculation input object
		Integer fiscalYear = new Integer(request.getProgram().getFiscalYear());
		dto.setFiscalYear(fiscalYear);

		//populate the program object for the Smith Lever Breakout Calculation
		SmithLeverProgram program = new SmithLeverProgram();
		program.setName(request.getProgram().getProgramId());
		program.setFullName(request.getProgram().getProgramDescription());
		program.setFiscalYear(fiscalYear);
		program.setAppropriationType(request.getProgram().getAppropriationType());
		
		if (request.getProgram().getPaymentToStates() != null){
			program.setAppropriation(request.getProgram().getPaymentToStates());
		}
		
		if (request.getProgram().getForecastedPaymentToStates() !=  null){
			program.setForecastedAppropriation(request.getProgram().getForecastedPaymentToStates());
		}
		
		if (request.getProgram().getMbSetAsideAmount() != null){
			program.setSnpAppropriation(request.getProgram().getMbSetAsideAmount());
		}
		
		if (request.getProgram().getPperaIncreaseAmount() != null){
			program.setUdcIncreaseAppropriation(request.getProgram().getPperaIncreaseAmount());
		}
		
		dto.setProgram(program);

		//loop through subprograms in the request to create the calculation subprogram objects
		List<SubProgram> subProgramList = new ArrayList<SubProgram>();
		for ( BreakoutRequestSubProgram requestSub : request.getProgram().getSubProgramList()) {

			SubProgram udcSub = new SubProgram();
			udcSub.setName(requestSub.getSubProgramId());
			udcSub.setAppropriation(requestSub.getPriorYearAmount());
			subProgramList.add(udcSub);
		}

		//add subprogram list from request to calculation input object
		dto.setPreviousSubPrograms(subProgramList);
		
		return dto;
	}
}
