package gov.usda.grants.nifa.transformers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.nifa.cgc.api.SmithLeverCapacityCalculationApi;
import gov.usda.nifa.cgc.dto.BaseDTO;
import gov.usda.nifa.cgc.dto.BreakoutDTO;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.AuditLog;
import gov.usda.nifa.cgc.model.ProgramType;
import gov.usda.nifa.cgc.model.SmithLeverProgram;
import gov.usda.nifa.cgc.model.SubProgram;
import gov.usda.nifa.cgc.model.SubProgramType;


public class SmithLeverCalculator extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String response = "{ \"SmithLeverResult\": \"%s\"}";
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		BreakoutRequest request = new BreakoutRequest();
		//long l = Long.valueOf(Integer.valueOf(request.getNetToStates()).longValue());
		SmithLeverCapacityCalculationApi slc = new SmithLeverCapacityCalculationApi();
		BaseDTO inputBreakoutDTO = new BreakoutDTO();
		
		Integer fiscalYear = 2017;
		Integer previousYear = fiscalYear - 1;
		SmithLeverProgram program = new SmithLeverProgram();
		program.setName(ProgramType.SMITH_LEVER.getName());
		program.setFiscalYear(fiscalYear);
		program.setAppropriationType(AppropriationType.FINAL.getCode());
		program.setAppropriation(new BigDecimal(291742500L));
		program.setSnpAppropriation(new BigDecimal(462744L));
		program.setUdcIncreaseAppropriation(new BigDecimal(7800L));
		inputBreakoutDTO.setProgram(program);

		inputBreakoutDTO.setFiscalYear(fiscalYear);
		List<SubProgram> subProgramList = new ArrayList<SubProgram>();
		SubProgram udcSub = new SubProgram();
		udcSub.setName(SubProgramType.SL_UDC.getName());
		//udcSub.setFullName(SubProgramType.SL_UDC.getName());
		udcSub.setAppropriation(new BigDecimal("1196560"));
		subProgramList.add(udcSub);
		
		SubProgram snfSub = new SubProgram();
		snfSub.setName(SubProgramType.SL_SNF.getName());
		//snfSub.setName(SubProgramType.SL_SNF.getCode());
		//snfSub.setFullName(SubProgramType.SL_SNF.getName());
		snfSub.setAppropriation(new BigDecimal("1029979"));
		subProgramList.add(snfSub);
		
		SubProgram snpSub = new SubProgram();
		snpSub.setName(SubProgramType.SL_SNP.getName());
		//snpSub.setName(SubProgramType.SL_SNP.getCode());
		//snpSub.setFullName(SubProgramType.SL_SNP.getName());
		snpSub.setAppropriation(new BigDecimal("462744"));
		subProgramList.add(snpSub);
		
		SubProgram slSub = new SubProgram();
		slSub.setName(SubProgramType.SL_3B3C.getName());
		//slSub.setName(SubProgramType.SL_3B3C.getCode());
		//slSub.setFullName(SubProgramType.SL_3B3C.getName());
		slSub.setAppropriation(new BigDecimal("288676390"));
		subProgramList.add(slSub);
		
		/*SubProgram csrsSub = new SubProgram();
		csrsSub.setName(SubProgramType.SL_CSRS.getName());
		//csrsSub.setName(SubProgramType.SL_CSRS.getCode());
		//csrsSub.setFullName(SubProgramType.SL_CSRS.getName());
		csrsSub.setAppropriation(0L);
		subProgramList.add(csrsSub);
		
		SubProgram fersSub = new SubProgram();
		fersSub.setName(SubProgramType.SL_FERS.getName());
		//fersSub.setName(SubProgramType.SL_FERS.getCode());
		//fersSub.setFullName(SubProgramType.SL_FERS.getName());
		fersSub.setAppropriation(376827L);
		subProgramList.add(fersSub);
*/
		((BreakoutDTO) inputBreakoutDTO).setPreviousSubPrograms(subProgramList);
		
		try {
			//slc.startSmithLeverCalculation(program.getFiscalYear(), ProgramType.SMITH_LEVER.getCode(), "dev");
			inputBreakoutDTO = slc.calculateAppropriation(inputBreakoutDTO, "DEV");
			for (AuditLog alog : inputBreakoutDTO.getAuditLogs()) {
				log.info("inputBreakoutDTO: " + alog.getCalculationLog());
			}
			
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
        return String.format(response, "string");
	}
	
}
