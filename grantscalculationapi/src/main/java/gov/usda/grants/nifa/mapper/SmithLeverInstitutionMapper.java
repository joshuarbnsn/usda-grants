package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.constants.CalculationConstants;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.SmithLeverProgram;
import gov.usda.nifa.cgc.model.State;
import gov.usda.nifa.cgc.model.SubProgram;

public class SmithLeverInstitutionMapper extends InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		InstitutionDTO input = super.map(request);
		
		input.setFiscalYear(request.getProgram().getFiscalYear());
		
		//map Smith Lever specific attributes
		SmithLeverProgram program = new SmithLeverProgram();
		program.setName(request.getProgram().getParentProgramId());
		program.setFullName(request.getProgram().getParentProgramDescription());
		program.setFiscalYear(request.getProgram().getFiscalYear());
		program.setAppropriationType(request.getProgram().getAppropriationType());
		
		if (request.getProgram().getInsularAreaAllotment() != null) {
			program.setInsularAreaAppropriation(request.getProgram().getInsularAreaAllotment());
		}
		
		if (request.getProgram().getEqualProportion() != null) {
			program.setEqualWeight(request.getProgram().getEqualProportion());
		}
		
		if (request.getProgram().getRuralProportion() != null) {
			program.setRpWeight(request.getProgram().getRuralProportion());
		}
		
		if (request.getProgram().getFarmProportion() != null){
			program.setFpWeight(request.getProgram().getFarmProportion());
		}
		
		if (request.getProgram().getInsularAreaMatching() != null){
			program.setInsularWeight(request.getProgram().getInsularAreaMatching());
		}
		
		
		if (request.getProgram().getNonInsularAreaMatching() != null) {
			program.setRequiredMatchingPercentage(request.getProgram().getNonInsularAreaMatching());
		}
		
		
		//PPERA Increase maybe not needed
		if (request.getProgram().getPperaIncreaseAmount() != null) {
			program.setUdcIncreaseAppropriation(request.getProgram().getPperaIncreaseAmount());
		}
		input.setProgram(program);
		
		SubProgram subProgram = new SubProgram();
		subProgram.setName(request.getProgram().getProgramId());
		subProgram.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());
		subProgram.setApplyAdminAdjustment(request.getProgram().isApplyAdminAdjustment());
		subProgram.setAdminAdjustmentReason(request.getProgram().getAdminAdjustmentReason());
		
		input.setSubProgram(subProgram);
		
		SubProgram prevProgram = new SubProgram();
		prevProgram.setName(request.getProgram().getProgramId());
		prevProgram.setFullName(request.getProgram().getProgramDescription());
		prevProgram.setAppropriation(request.getProgram().getPriorYearAmount());
		
		input.setPreviousSubProgram(prevProgram);
		
		List<Institution> institutionListOut = new ArrayList<Institution>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			State state = new State();
			
			//use the state code when country = US otherwise use the country code as the state
			state.setAbbreviation(StringUtils.equals(institutionIn.getCountry(), 
					CalculationConstants.COUNTRY_CODE_US) ?
					institutionIn.getState() : institutionIn.getCountry());
			
			state.setFarmPopulation(institutionIn.getFarmPopulation());
			state.setRuralPopulation(institutionIn.getRuralPopulation());
			state.setYear(request.getProgram().getFiscalYear());
			state.setIsInsular(institutionIn.getInsularArea());
			
			Institution institutionOut = new Institution();
			institutionOut.setInstitutionId(institutionIn.getInstitutionId());
			institutionOut.setName(institutionIn.getInstitutionName());
			institutionOut.setState(state);
			institutionOut.setAppropriation(institutionIn.getPriorYearAllocation());
			institutionOut.setDunsNumber(institutionIn.getDunsNumber());
			institutionOut.setApplyAdminAdjustment(institutionIn.isApplyAdminAdjustment());
			
			if (institutionIn.getAdminAdjustmentAmount() != null) {
				institutionOut.setAdminAdjustmentAmount(institutionIn.getAdminAdjustmentAmount());
			}
			
			if (institutionIn.isApplyInstitutionMatching()) {
				if (institutionIn.getMatchingPercentage() != null) {
					institutionOut.setRequiredMatchingPercentage(institutionIn.getMatchingPercentage());
				}
				if (institutionIn.getMatchingAmount() != null) {
					institutionOut.setMatchingAppropriation(institutionIn.getMatchingAmount());
				}
			}
			
			institutionListOut.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionListOut);
		
		return input;
	}
}
