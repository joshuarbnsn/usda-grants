package gov.usda.grants.nifa.dto.calculations.in;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class BreakoutRequest implements Serializable, ServiceRequest {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Program")
	public BreakoutRequestProgram program;

	public BreakoutRequestProgram getProgram() {
		return program;
	}

	public void setProgram(BreakoutRequestProgram program) {
		this.program = program;
	}
}
