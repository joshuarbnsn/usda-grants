package gov.usda.grants.nifa.mapper;

import java.util.ArrayList;
import java.util.List;

import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.HAMInstitution;
import gov.usda.nifa.cgc.model.HAMProgram;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.RREAInstitution;
import gov.usda.nifa.cgc.model.RREAProgram;
import gov.usda.nifa.cgc.model.RREAState;
import gov.usda.nifa.cgc.model.State;

public class RREAInstitutionMapper extends InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) {
		
		InstitutionDTO input = super.map(request);
		
		//RREAProgram program = getProgram(input);
		
		/*  
		 *  The local map method must be called before the super class method
			in 	order to create the proper objects and avoid class cast exception		
		 */
		
		//Map local program level fields
		mapProgramLevel(request, input);
		
		// 	map the parent Program Fields
		super.mapProgramLevel(request, input);
		
		//reset input list of institutions
		input.setEligibleInstitutions(new ArrayList<>());
		
		List<RREAInstitution> institutionList = new ArrayList<>();
		
		for (InstitutionRequestInstitution institutionIn : request.getEligibleInstitutions()) {
			RREAInstitution institutionOut = new RREAInstitution();
			
			//Map RREA Institution fields first
			mapInstitutionLevel(request, institutionIn, institutionOut);
			
			super.mapInstitutionLevel(request, institutionIn, institutionOut);
			
			institutionList.add(institutionOut);
		}
		
		input.setEligibleInstitutions(institutionList);
		
		return input;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		RREAProgram program = getProgram(input);
		
		if (request.getProgram().getBase1862InsAmount() != null) {
			program.setBase1862InsAppropriation(request.getProgram().getBase1862InsAmount());
		}
		
		if (request.getProgram().getFixed1890InsAmount() != null) {
			program.setFixed1890InsAppropriation(request.getProgram().getFixed1890InsAmount());
		}
		
		if (request.getProgram().getForestlandProportion() != null) {
			program.setForestlandWeight(request.getProgram().getForestlandProportion());
		}
		
		if (request.getProgram().getNetGrowthProportion() != null) {
			program.setNetGrowthWeight(request.getProgram().getNetGrowthProportion());
		}
		
		if (request.getProgram().getGrazingLandProportion() != null) {
			program.setGrazingLandWeight(request.getProgram().getGrazingLandProportion());
		}
		
		if (request.getProgram().getWoodIndustryEmploymentProportion() != null) {
			program.setWoodIndustryEmploymentWeight(request.getProgram().getWoodIndustryEmploymentProportion());
		}
		
		if (request.getProgram().getTimberRemovalsProportion() != null) {
			program.setTimberRemovalsWeight(request.getProgram().getTimberRemovalsProportion());
		}
		
		if (request.getProgram().getUrbanPopulationProportion() != null) {
			program.setUrbanPopulationWeight(request.getProgram().getUrbanPopulationProportion());
		}
		
		if (request.getProgram().getTotalStatePopulationProportion() != null) {
			program.setTotalStatePopulationWeight(request.getProgram().getTotalStatePopulationProportion());
		}
		
		if (request.getProgram().getConstantFactorProportion() != null) {
			program.setConstantFactorWeight(request.getProgram().getConstantFactorProportion());
		}
		
		if (request.getProgram().getBaseRankCriteria() != null) {
			program.setBaseRankCriteria(request.getProgram().getBaseRankCriteria());
		}
	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, RREAInstitution institutionOut) {
		
		RREAState state = getState(institutionOut);
		
		if (institutionIn.getForestlandAcres() != null) {
			state.setForestlandAcres(institutionIn.getForestlandAcres());
		}
		
		if (institutionIn.getNetGrowthCubicFeet() != null) {
			state.setNetGrowthCubicFeet(institutionIn.getNetGrowthCubicFeet());
		}
		
		if (institutionIn.getGrazingLandAcres() != null) {
			state.setGrazingLandAcres(institutionIn.getGrazingLandAcres());
		}

		if (institutionIn.getWoodIndustryEmployment() != null) {
			state.setWoodIndustryEmployment(institutionIn.getWoodIndustryEmployment());
		}
		
		if (institutionIn.getTimberRemovalsCubicFeet() != null) {
			state.setTimberRemovalsCubicFeet(institutionIn.getTimberRemovalsCubicFeet());
		}
		state.setUrbanPopulation(institutionIn.getUrbanPopulation());
		state.setTotalStatePopulation(institutionIn.getTotalStatePopulation());
		
		institutionOut.setLandGrantType(institutionIn.getLandGrantType());
	}
	
	public RREAProgram getProgram(InstitutionDTO input) {
		RREAProgram program = (RREAProgram) input.getProgram();
		
		if (program == null) {
			program = new RREAProgram();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public RREAProgram getPrevProgram(InstitutionDTO input) {
		RREAProgram program = (RREAProgram) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new RREAProgram();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public List<? extends Institution> getInstitutionListOut(InstitutionDTO input) {
		@SuppressWarnings("unchecked")
		List<RREAInstitution> institutionListOut = (List<RREAInstitution>) input.getEligibleInstitutions();
		
		if (institutionListOut == null) {
			institutionListOut = new ArrayList<>();
			input.setEligibleInstitutions(institutionListOut);
		}
		
		return institutionListOut;
	}
	
	public RREAState getState(RREAInstitution institutionOut) {
		RREAState state = (RREAState) institutionOut.getState();
		
		if (state == null) {
			state = new RREAState();
			institutionOut.setState(state);
		}
		
		return state;
	}
}