package gov.usda.grants.nifa.transformers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.nifa.cgc.api.SmithLeverCapacityCalculationApi;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.AuditLog;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.Program;
import gov.usda.nifa.cgc.model.ProgramType;
import gov.usda.nifa.cgc.model.SmithLeverProgram;
import gov.usda.nifa.cgc.model.State;
import gov.usda.nifa.cgc.model.SubProgram;
import gov.usda.nifa.cgc.model.SubProgramType;


public class SmithLeverInstitutionCalculator extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String response = "{ \"SmithLeverResult\": \"%s\"}";
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		//test variables
		Integer fiscalYear = 2018;
		String appropriationType = AppropriationType.CONTINUING_RESOLUTIONS.getCode();
		BigDecimal crPercentage = new BigDecimal("" + 0);
		String programName = ProgramType.SMITH_LEVER.getName();
		String subProgramName = SubProgramType.SL_UDC.getName();
		String fullName = ProgramType.SMITH_LEVER.getDescription();
		
		InstitutionDTO institutionDtoInput = new InstitutionDTO();
		InstitutionDTO institutionDtoOutput = new InstitutionDTO();
		
		institutionDtoInput.setFiscalYear(fiscalYear);
		Program progObj = null;
		
		if(AppropriationType.CONTINUING_RESOLUTIONS.getCode().equalsIgnoreCase(appropriationType)) {
			progObj = createProgram(programName, fullName, fiscalYear, appropriationType, 52513448L, 291742500L, crPercentage, 7800L, 462744L, 100000L, 20, 40, 40, 50, 100);
			
			if(SubProgramType.SL_UDC.getName().equalsIgnoreCase(subProgramName)) {
				/**
				 * 0 percentage of required matching for UDC
				 */
				progObj.setRequiredMatchingPercentage(new BigDecimal(0));
				institutionDtoInput.setPreviousSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 1196560)));
				institutionDtoInput.setSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 215305), 1204360L));
				institutionDtoInput.setEligibleInstitutions(getUDCInstitutions());
			} else if (SubProgramType.SL_SNF.getName().equalsIgnoreCase(subProgramName)) {
				institutionDtoInput.setPreviousSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 1029979)));
				institutionDtoInput.setSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 183797), 1029979L));
				institutionDtoInput.setEligibleInstitutions(getSpecialNeedFormulaInstitutions());
			} else if (SubProgramType.SL_3B3C.getName().equalsIgnoreCase(subProgramName)) {
				institutionDtoInput.setPreviousSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 288676390)));
				institutionDtoInput.setSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 52030324), 289045417L));
				institutionDtoInput.setEligibleInstitutions(get3B3CInstitutions());
			}
		} else {
			progObj = createProgram(programName, fullName, fiscalYear, appropriationType, 291742500L, 0L, crPercentage, 7800L, 462744L, 100000L, 20, 40, 40, 50, 100);
			if(SubProgramType.SL_UDC.getName().equalsIgnoreCase(subProgramName)) {
				/**
				 * 0 percentage of required matching for UDC
				 */
				progObj.setRequiredMatchingPercentage(new BigDecimal(0));
				institutionDtoInput.setPreviousSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 1196560)));
				institutionDtoInput.setSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 1204360)));
				institutionDtoInput.setEligibleInstitutions(getUDCInstitutions());
			} else if (SubProgramType.SL_SNF.getName().equalsIgnoreCase(subProgramName)) {
				institutionDtoInput.setPreviousSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 1029979)));
				institutionDtoInput.setSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 1029979)));
				institutionDtoInput.setEligibleInstitutions(getSpecialNeedFormulaInstitutions());
			} else if (SubProgramType.SL_3B3C.getName().equalsIgnoreCase(subProgramName)) {
				institutionDtoInput.setPreviousSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 288676390)));
				institutionDtoInput.setSubProgram(getSubProgram(subProgramName, new BigDecimal("" + 289045417)));
				institutionDtoInput.setEligibleInstitutions(get3B3CInstitutions());
			}
			
		}
		institutionDtoInput.setProgram(progObj);
		try {
		SmithLeverCapacityCalculationApi slcca = new SmithLeverCapacityCalculationApi();
		institutionDtoOutput = (InstitutionDTO) slcca.calculateAppropriation(institutionDtoInput, "Dev");
		
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		for (AuditLog alog : institutionDtoOutput.getAuditLogs()) {
			log.info(alog.getCalculationLog());
		}
		
		return institutionDtoInput;
	}
	private static State findStateByAbreviation(String stateAbre) {
		State sObj = null;
		for (State s: stateList) {
			if (stateAbre.equalsIgnoreCase(s.getAbbreviation())) {
				sObj = s;
				break;
			}
		}
		return sObj;
	}
	
	private static SubProgram getSubProgram(String subProgramName, BigDecimal appropriationAmount) {
		return getSubProgram(subProgramName, appropriationAmount, null);
	}
	
	private static SubProgram getSubProgram(String subProgramName, BigDecimal appropriationAmount, Long forecastedAmount) {
		SubProgram subProg = null;
		Long programId = 0l;
		if(SubProgramType.SL_UDC.getName().equalsIgnoreCase(subProgramName)) {
			subProg = createSubProgram(SubProgramType.SL_UDC.getName(), SubProgramType.SL_UDC.getDescription(), programId, appropriationAmount);
		} else if (SubProgramType.SL_SNF.getName().equalsIgnoreCase(subProgramName)) {
			subProg = createSubProgram(SubProgramType.SL_SNF.getName(), SubProgramType.SL_SNF.getDescription(), programId, appropriationAmount);
		} else if (SubProgramType.SL_3B3C.getName().equalsIgnoreCase(subProgramName)) {
			subProg = createSubProgram(SubProgramType.SL_3B3C.getName(), SubProgramType.SL_3B3C.getDescription(), programId, appropriationAmount);
		}
		
		return subProg;
	}
	
	private static List<SubProgram> getSubPrograms(BigDecimal udcAmount, BigDecimal snfAmount, BigDecimal snpAmount, BigDecimal csrsAmount, 
			BigDecimal fersAmount, BigDecimal sl3B3CAmount) {
		List<SubProgram> subProgramList = new ArrayList<>();
		Long programId = 0l;
		SubProgram subProgram = createSubProgram(SubProgramType.SL_UDC.getName(), SubProgramType.SL_UDC.getDescription(), programId, udcAmount);
		subProgramList.add(subProgram);
		subProgram = createSubProgram(SubProgramType.SL_SNF.getName(), SubProgramType.SL_SNF.getDescription(), programId, snfAmount);
		subProgramList.add(subProgram);
		subProgram = createSubProgram(SubProgramType.SL_SNP.getName(), SubProgramType.SL_SNP.getDescription(), programId, snpAmount);
		subProgramList.add(subProgram);
		subProgram = createSubProgram(SubProgramType.SL_CSRS.getName(), SubProgramType.SL_CSRS.getDescription(), programId, csrsAmount);
		subProgramList.add(subProgram);
		subProgram = createSubProgram(SubProgramType.SL_FERS.getName(), SubProgramType.SL_FERS.getDescription(), programId, fersAmount);
		subProgramList.add(subProgram);
		subProgram = createSubProgram(SubProgramType.SL_3B3C.getName(), SubProgramType.SL_3B3C.getDescription(), programId, sl3B3CAmount);
		subProgramList.add(subProgram);
		return subProgramList;
	}
	
	private static List<Institution> getUDCInstitutions() {
		List<Institution> institutionList = new ArrayList<>();
		Long subProgramId = 0l;
		
		State state = findStateByAbreviation("DC");
		Institution insObj = createInstitution("1", "University of the District of Columbia", new BigDecimal("" + 1196560), 0L, subProgramId, state, 0);
		institutionList.add(insObj);
		
		return institutionList;
	}
	
	private static List<Institution> getSpecialNeedFormulaInstitutions() {
		List<Institution> institutionList = new ArrayList<>();
		Long subProgramId = 0l;
		
		State state = findStateByAbreviation("AK");
		Institution insObj = createInstitution("1", "University of Alaska-Fairbanks", new BigDecimal("" + 41081), 41081L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("AZ");
		insObj = createInstitution("2", "University of Arizona", new BigDecimal("" + 111504), 111504L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("CO");
		insObj = createInstitution("3", "Colorado State University", new BigDecimal("" + 68134), 68134L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("ID");
		insObj = createInstitution("4", "University of Idaho", new BigDecimal("" + 74412), 74412L, subProgramId, state);
		institutionList.add(insObj);
				
		state = findStateByAbreviation("KS");
		insObj = createInstitution("5", "Kansas State University", new BigDecimal("" + 37557), 37557L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MT");
		insObj = createInstitution("6", "Montana State University", new BigDecimal("" + 92179), 92179L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NE");
		insObj = createInstitution("7", "University of Nebraska", new BigDecimal("" + 42358), 42358L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NV");
		insObj = createInstitution("8","University of Nevada-Reno", new BigDecimal("" + 84519), 84519L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NM");
		insObj = createInstitution("9","New Mexico State University", new BigDecimal("" + 95522), 95522L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("ND");
		insObj = createInstitution("10","North Dakota State University", new BigDecimal("" + 72135), 72135L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("OR");
		insObj = createInstitution("11","Oregon State University", new BigDecimal("" + 36301), 36301L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("SD");
		insObj = createInstitution("12","South Dakota State University", new BigDecimal("" + 34570), 34570L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("TX");
		insObj = createInstitution("13","Texas A&M University", new BigDecimal("" + 91221), 91221L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("UT");
		insObj = createInstitution("14","Utah State University", new BigDecimal("" + 93188), 93188L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("VT");
		insObj = createInstitution("15","University of Vermont", new BigDecimal("" + 11347), 11347L, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("WY");
		insObj = createInstitution("16","University of Wyoming", new BigDecimal("" + 43951), 43951L, subProgramId, state);
		institutionList.add(insObj);
		
		return institutionList;
	}
	
	private static List<Institution> get3B3CInstitutions() {
		List<Institution> institutionList = new ArrayList<>();
		
		Long subProgramId = 0l;
		
		
		State state = findStateByAbreviation("AL");
		Long previousYearAppropriation = 7148951L;
		Long matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		Institution insObj = createInstitution("1", "Auburn University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("AK");
		previousYearAppropriation = 1193757L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("2", "University of Alaska-Fairbanks", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("AS");
		previousYearAppropriation = 1267749L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("3", "American Samoa Community College", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("AZ");
		previousYearAppropriation = 2040900L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("4", "University of Arizona", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("AR");
		previousYearAppropriation = 5999855L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("5", "University of Arkansas", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("CA");
		previousYearAppropriation = 8018691L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("6", "University of California", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
				
		state = findStateByAbreviation("CO");
		previousYearAppropriation = 3240737L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("7", "Colorado State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("CT");
		previousYearAppropriation = 2232008L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("8", "University of Connecticut", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("DE");
		previousYearAppropriation = 1348396L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("9", "University of Delaware", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("FL");
		previousYearAppropriation = 4807157L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("10", "University of Florida", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("GA");
		previousYearAppropriation = 8150158L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("11", "University of Georgia", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("GU");
		previousYearAppropriation = 1327771L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("12", "University of Guam", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("HI");
		previousYearAppropriation = 1391265L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("13", "University of Hawaii", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("ID");
		previousYearAppropriation = 2861074L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("14", "University of Idaho", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("IL");
		previousYearAppropriation = 9800780L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("15", "University of Illinois", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("IN");
		previousYearAppropriation = 9221219L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("16", "Purdue University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("IA");
		previousYearAppropriation = 9642552L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("17", "Iowa State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("KS");
		previousYearAppropriation = 5650682L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("18", "Kansas State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("KY");
		previousYearAppropriation = 9404861L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("19", "University of Kentucky", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("LA");
		previousYearAppropriation = 5255670L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("20", "Louisiana State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("ME");
		previousYearAppropriation = 2425014L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("21", "University of Maine", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MD");
		previousYearAppropriation = 3442946L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("22", "University of Maryland at College Park", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MA");
		previousYearAppropriation = 2748388L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("23", "University of Massachusetts", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MI");
		previousYearAppropriation = 9270969L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("24", "Michigan State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("FM");
		previousYearAppropriation = 1396075L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("25", "College of Micronesia", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MN");
		previousYearAppropriation = 9083932L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("26", "University of Minnesota", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MS");
		previousYearAppropriation = 7056470L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("27", "Mississippi State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MO");
		previousYearAppropriation = 9144244L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("28", "University of Missouri", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MT");
		previousYearAppropriation = 2742508L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("29", "Montana State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NE");
		previousYearAppropriation = 5125957L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("30", "University of Nebraska", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NV");
		previousYearAppropriation = 1205024L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("31", "University of Nevada-Reno", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NH");
		previousYearAppropriation = 1774879L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("32", "University of New Hampshire", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NJ");
		previousYearAppropriation = 2739504L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("33", "Rutgers University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NM");
		previousYearAppropriation = 2142512L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("34", "New Mexico State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NY");
		previousYearAppropriation = 8525428L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("35", "Cornell University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("NC");
		previousYearAppropriation = 11906502L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("36", "North Carolina State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("ND");
		previousYearAppropriation = 3440785L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("37", "North Dakota State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("MP");
		previousYearAppropriation = 1252755L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("38", "Northern Marianas College", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("OH");
		previousYearAppropriation = 11315915L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("39", "Ohio State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("OK");
		previousYearAppropriation = 5813537L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("40", "Oklahoma State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("OR");
		previousYearAppropriation = 3909333L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("41", "Oregon State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("PA");
		previousYearAppropriation = 10672929L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("42", "Pennsylvania State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("PR");
		previousYearAppropriation = 6664256L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("43", "University of Puerto Rico", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("RI");
		previousYearAppropriation = 1138886L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("44", "University of Rhode Island", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("SC");
		previousYearAppropriation = 5856616L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("45", "Clemson University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("SD");
		previousYearAppropriation = 3699648L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("46", "South Dakota State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("TN");
		previousYearAppropriation = 8959920L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("47", "University of Tennessee", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("TX");
		previousYearAppropriation = 13326759L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("48", "Texas A&M University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("UT");
		previousYearAppropriation = 1758422L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("49", "Utah State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("VT");
		previousYearAppropriation = 1899576L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("50", "University of Vermont", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("VA");
		previousYearAppropriation = 7440437L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("51", "Virginia Polytech Institute & State Univ.", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("VI");
		previousYearAppropriation = 1294083L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("52", "University of the Virgin Islands", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("WA");
		previousYearAppropriation = 4417690L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("53", "Washington State University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("WV");
		previousYearAppropriation = 4258111L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("54", "West Virginia University", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("WI");
		previousYearAppropriation = 9174725L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("55", "University of Wisconsin", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		state = findStateByAbreviation("WY");
		previousYearAppropriation = 1647422L;
		matchingAppropriation = getMatchingAppropriation(previousYearAppropriation, state.getIsInsular());
		insObj = createInstitution("56", "University of Wyoming", new BigDecimal("" + previousYearAppropriation), matchingAppropriation, subProgramId, state);
		institutionList.add(insObj);
		
		return institutionList;
	}
	
	private static Long getMatchingAppropriation(Long previousYearAppropriation, Boolean isInsular) {
		BigDecimal matchingAmount = new BigDecimal("0");
		Long matchingAppropriation = 0l;
		if(isInsular) {
			matchingAmount = new BigDecimal("" + previousYearAppropriation)
					.multiply(new BigDecimal("" + INSULAR_WEIGHT.doubleValue() / 100));
			matchingAmount = matchingAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
			matchingAppropriation = matchingAmount.longValue();
		} else {
			matchingAppropriation = previousYearAppropriation;
		}
		
		return matchingAppropriation;
	}
	
	private static SmithLeverProgram createProgram(String name, String fullName, Integer fiscalYear, String appropriationType, Long appropriation, 
			Long forecastedAppropriation, BigDecimal crPercentage, Long udcIncreaseAppropriation, Long snpAppropriation, Long insularAreaAppropriation, 
			Integer equalWeight, Integer rpWeight, Integer fpWeight, Integer insularWeight, Integer requiredMatchingWeight) {
		SmithLeverProgram program = new SmithLeverProgram();
		program.setName(name);
		program.setFiscalYear(fiscalYear);
		program.setAppropriationType(appropriationType);
		program.setAppropriation(new BigDecimal(appropriation));
		program.setForecastedAppropriation(new BigDecimal(forecastedAppropriation));
		program.setCrAppropriationPercentage(crPercentage);
		program.setUdcIncreaseAppropriation(new BigDecimal(udcIncreaseAppropriation));
		program.setSnpAppropriation(new BigDecimal(snpAppropriation));
		program.setInsularAreaAppropriation(new BigDecimal(insularAreaAppropriation));
		program.setEqualWeight(new BigDecimal(equalWeight));
		program.setRpWeight(new BigDecimal(rpWeight));
		program.setFpWeight(new BigDecimal(fpWeight));
		program.setInsularWeight(new BigDecimal(insularWeight));
		program.setRequiredMatchingPercentage(new BigDecimal(requiredMatchingWeight));
		
		return program;
	}
	
	private static State createState(String name, String abbreviation, Long ruralPopulation,
			Long farmPopulation, Boolean isInsular, Integer year) {
		State state = new State();
		state.setName(name);
		state.setAbbreviation(abbreviation);
		state.setIsInsular(isInsular);
		state.setRuralPopulation(ruralPopulation);
		state.setFarmPopulation(farmPopulation);
		state.setYear(year);

		return state;
	}
	
	private static SubProgram createSubProgram(String name, String fullName, Long programId, BigDecimal appropriation) {
		return createSubProgram(name, fullName, programId, appropriation, null);
	}
	
	private static SubProgram createSubProgram(String name, String fullName, Long programId, BigDecimal appropriation, Long forcastedAmount) {
		SubProgram subProgram = new SubProgram();
		subProgram.setName(name);
		subProgram.setFullName(fullName);
		subProgram.setProgramId(programId);
		subProgram.setAppropriation(appropriation);
		subProgram.setForecastedAppropriation(new BigDecimal(forcastedAmount));

		return subProgram;
	}
	private static Institution createInstitution(String institutionId, String name, BigDecimal appropriation, 
			Long matchinAppropriation, Long subProgramId, State state) {	
		return createInstitution(institutionId, name, appropriation, matchinAppropriation, subProgramId, state, 100);
	}
	private static Institution createInstitution(String institutionId, String name, BigDecimal appropriation, 
			Long matchinAppropriation, Long subProgramId, State state, Integer requiredMatchingWeight) {
		Institution institution = new Institution();
		institution.setInstitutionId(institutionId);
		institution.setName(name);
		institution.setAppropriation(appropriation);
		institution.setMatchingAppropriation(new BigDecimal(matchinAppropriation));
		institution.setSubProgramId(subProgramId);
		institution.setState(state);
		institution.setRequiredMatchingPercentage(new BigDecimal(requiredMatchingWeight));

		return institution;
	}
	
	private static final Integer CENSUS_DATA_YEAR = 2010;
	private static final Integer INSULAR_WEIGHT = 50;
	private static List<State> stateList = new ArrayList<State>();
	static {
		Boolean isInsular = false;
		State state = createState("Alabama", "AL", 1959948L, 36315L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Alaska", "AK", 240898L, 1000L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("American Samoa", "AS", 6874L, 247L, !isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Arizona", "AZ", 636652L, 4890L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Arkansas", "AR", 1283051L, 40630L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("California", "CA", 1849748L, 97985L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Colorado", "CO", 698048L, 38235L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Connecticut", "CT", 428863L, 4865L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Delaware", "DE", 151717L, 3360L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("District of Columbia", "DC", 0L, 0L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Florida", "FL", 1655119L, 33445L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Georgia", "GA", 2414891L, 39890L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Guam", "GU", 9440L, 1113L, !isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Hawaii", "HI", 102719L, 5935L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Idaho", "ID", 460275L, 29510L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Illinois", "IL", 1475765L, 99325L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Indiana", "IN", 1793245L, 115100L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Iowa", "IA", 1098861L, 128245L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Kansas", "KS", 738166L, 70040L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Kentucky", "KY", 1805993L, 89305L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Louisiana", "LA", 1212610L, 19225L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Maine", "ME", 813224L, 9955L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Maryland", "MD", 746848L, 22525L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Massachusetts","MA", 526535L, 7065L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Michigan", "MI", 2513443L, 85175L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Micronesia", "FM", 100520L, 3368L, !isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Minnesota", "MN", 1419332L, 115160L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Mississippi", "MS", 1502185L, 26830L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Missouri", "MO", 1773674L, 126470L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Montana", "MT", 436280L, 35030L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Nebraska", "NE", 488912L, 64415L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Nevada", "NV", 152636L, 4410L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("New Hampshire", "NH", 521406L, 5360L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("New Jersey", "NJ", 471450L, 13015L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("New Mexico", "NM", 455498L,11120L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("New York", "NY", 2354355L, 62215L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("North Carolina", "NC", 3238513L,55640L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("North Dakota", "ND", 271823L, 32695L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Northern Marianas", "MP", 4886L, 920L, !isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Ohio", "OH", 2545931L, 142715L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Oklahoma", "OK", 1268322L,72675L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Oregon", "OR", 725333L, 57150L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Pennsylvania", "PA", 2723296L, 91380L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Puerto Rico", "PR", 23286L, 4350L, !isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Rhode Island", "RI", 97941L, 465L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("South Carolina", "SC", 1554565L, 20645L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("South Dakota", "SD", 353693L, 44630L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Tennessee", "TN", 2128644L, 80285L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Texas", "TX", 3843439L, 177430L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Utah", "UT", 259181L, 12015L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Vermont", "VT", 383360L, 11105L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Virginia", "VA", 1974330L, 50175L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Virgin Islands", "VI", 5798L, 55L, !isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Washington", "WA", 1068919L, 43015L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("West Virginia", "WV", 953949L, 22855L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Wisconsin", "WI", 1704577L, 119125L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
		state = createState("Wyoming", "WY", 197299L, 17895L, isInsular, CENSUS_DATA_YEAR);
		stateList.add(state);
	}
}
