package gov.usda.grants.nifa.service;

import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.out.InstitutionResponse;

public interface InstitutionCalculationService {

	InstitutionResponse CalculateAllocations(InstitutionRequest request);
}
