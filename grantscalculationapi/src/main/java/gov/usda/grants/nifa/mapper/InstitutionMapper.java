package gov.usda.grants.nifa.mapper;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.constants.CalculationConstants;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.Institution;
import gov.usda.nifa.cgc.model.Program;
import gov.usda.nifa.cgc.model.State;
import gov.usda.nifa.cgc.model.SubProgram;

public abstract class InstitutionMapper {

	public InstitutionDTO map(InstitutionRequest request) 
	{
		InstitutionDTO dto = new InstitutionDTO();
		
		
		return dto;
	}
	
	public void mapProgramLevel(InstitutionRequest request, InstitutionDTO input) {
		
		input.setFiscalYear(request.getProgram().getFiscalYear());
		
		Program program = getProgram(input);
		program.setName(request.getProgram().getParentProgramId());
		program.setFullName(request.getProgram().getParentProgramDescription());
		program.setFiscalYear(request.getProgram().getFiscalYear());
		program.setAppropriationType(request.getProgram().getAppropriationType());
		
		Program prevProgram = getPrevProgram(input);
		prevProgram.setName(request.getProgram().getParentProgramId());
		prevProgram.setFullName(request.getProgram().getParentProgramDescription());
		prevProgram.setFiscalYear(request.getProgram().getFiscalYear() -1);
		
		SubProgram subProgram = getSubProgram(input);
		subProgram.setName(request.getProgram().getProgramId());
		subProgram.setFullName(request.getProgram().getProgramDescription());
		subProgram.setApplyAdminAdjustment(request.getProgram().isApplyAdminAdjustment());
		subProgram.setAdminAdjustmentReason(request.getProgram().getAdminAdjustmentReason());
		
		if (request.getProgram().getCurrentYearApprovedAmount() != null) {
			program.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());
			subProgram.setAppropriation(request.getProgram().getCurrentYearApprovedAmount());
		}
		
		SubProgram prevSubProgram = getPrevSubProgram(input);
		prevSubProgram.setName(request.getProgram().getProgramId());
		prevSubProgram.setFullName(request.getProgram().getProgramDescription());
		
		if (request.getProgram().getPriorYearAmount() != null) {
			prevSubProgram.setAppropriation(request.getProgram().getPriorYearAmount());
			prevProgram.setAppropriation(request.getProgram().getPriorYearAmount());
		}
	}
	
	public void mapInstitutionLevel(InstitutionRequest request, 
			InstitutionRequestInstitution institutionIn, Institution institutionOut) {
		
		State state = getState(institutionOut);
		//use the state code when country = US otherwise use the country code as the state
		state.setAbbreviation(StringUtils.equals(institutionIn.getCountry(), 
				CalculationConstants.COUNTRY_CODE_US) ?
				institutionIn.getState() : institutionIn.getCountry());
		state.setYear(request.getProgram().getFiscalYear());
		
		institutionOut.setInstitutionId(institutionIn.getInstitutionId());
		institutionOut.setName(institutionIn.getInstitutionName());
		institutionOut.setState(state);
		institutionOut.setAppropriation(institutionIn.getPriorYearAllocation());
		institutionOut.setDunsNumber(institutionIn.getDunsNumber());
		institutionOut.setApplyAdminAdjustment(institutionIn.isApplyAdminAdjustment());
		
		if (institutionIn.getAdminAdjustmentAmount() != null) {
			institutionOut.setAdminAdjustmentAmount(institutionIn.getAdminAdjustmentAmount());
		}	
	}
	
	public Program getProgram(InstitutionDTO input) {
		Program program = (Program) input.getProgram();
		
		if (program == null) {
			program = new Program();
			input.setProgram(program);
		}
		
		return program;
	}
	
	public Program getPrevProgram(InstitutionDTO input) {
		Program program = (Program) input.getPreviousYearProgram();
		
		if (program == null) {
			program = new Program();
			input.setPreviousYearProgram(program);
		}
		
		return program;
	}
	
	public SubProgram getSubProgram (InstitutionDTO input) {
		
		SubProgram subProgram = input.getSubProgram();
		if (subProgram == null) {
			subProgram = new SubProgram();
			input.setSubProgram(subProgram);
		}
		
		return subProgram;
	}
	
	public SubProgram getPrevSubProgram (InstitutionDTO input) {
		
		SubProgram subProgram = input.getPreviousSubProgram();
		if (subProgram == null) {
			subProgram = new SubProgram();
			input.setPreviousSubProgram(subProgram);
		}
		
		return subProgram;
	}
	
	public State getState(Institution institutionOut) {
		State state = institutionOut.getState();
		
		if (state == null) {
			state = new State();
			institutionOut.setState(state);
		}
		
		return state;
	}
	

}
