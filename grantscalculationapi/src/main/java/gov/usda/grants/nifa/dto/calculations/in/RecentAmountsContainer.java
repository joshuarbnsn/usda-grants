package gov.usda.grants.nifa.dto.calculations.in;

import java.math.BigDecimal;

public class RecentAmountsContainer {

	public BigDecimal currentYearAmount;
	
	public BigDecimal priorYearAmount;
	
	public BigDecimal priorYearMinusOneAmount;

	public BigDecimal getCurrentYearAmount() {
		return currentYearAmount;
	}

	public void setCurrentYearAmount(BigDecimal currentYearAmount) {
		this.currentYearAmount = currentYearAmount;
	}

	public BigDecimal getPriorYearAmount() {
		return priorYearAmount;
	}

	public void setPriorYearAmount(BigDecimal priorYearAmount) {
		this.priorYearAmount = priorYearAmount;
	}

	public BigDecimal getPriorYearMinusOneAmount() {
		return priorYearMinusOneAmount;
	}

	public void setPriorYearMinusOneAmount(BigDecimal priorYearMinusOneAmount) {
		this.priorYearMinusOneAmount = priorYearMinusOneAmount;
	}
}
