
package gov.usda.grants.nifa.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequestSubProgram;
import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponse;
import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponseProgram;
import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponseSubProgram;
import gov.usda.grants.nifa.dto.calculations.out.CalculationStatus;
import gov.usda.grants.nifa.mapper.BreakoutMapper;
import gov.usda.grants.nifa.mapper.CalculationFactory;
import gov.usda.grants.nifa.mapper.InstitutionMapper;
import gov.usda.grants.nifa.mapper.MapperFactory;
import gov.usda.grants.nifa.service.BreakoutCalculationService;
import gov.usda.grants.nifa.validation.RequestValidator;
import gov.usda.grants.nifa.validation.SmithLeverBreakoutValidator;
import gov.usda.grants.nifa.validation.ValidationMessages;
import gov.usda.grants.nifa.validation.ValidatorFactory;
import gov.usda.nifa.cgc.api.CalculationApi;
import gov.usda.nifa.cgc.api.SmithLeverCapacityCalculationApi;
import gov.usda.nifa.cgc.dto.BreakoutDTO;
import gov.usda.nifa.cgc.dto.InstitutionDTO;
import gov.usda.nifa.cgc.model.AuditLog;
import gov.usda.nifa.cgc.model.ProgramType;
import gov.usda.nifa.cgc.model.SmithLeverProgram;
import gov.usda.nifa.cgc.model.SubProgram;

public class BreakoutCalculationServiceImpl implements BreakoutCalculationService {

	private Logger log = Logger.getLogger(this.getClass().getName());

	@Override
	public BreakoutResponse CalculateBreakout(BreakoutRequest request) {

		//web service outputs
		BreakoutResponse response = new BreakoutResponse();
		BreakoutResponseProgram outProgram = new BreakoutResponseProgram();

		//validate inputs
		RequestValidator validator = ValidatorFactory.getValidator(request);
		boolean valid = false;
		
		if (validator != null) {
			
			valid = validator.validate(request, response);
			
			if (!valid) {
				return response;
			}
		}
		
		//calculation variables
		SmithLeverCapacityCalculationApi slc = new SmithLeverCapacityCalculationApi();
		BreakoutDTO input = new BreakoutDTO();
		BreakoutDTO output = new BreakoutDTO();

		BreakoutMapper mapper = MapperFactory.getBreakoutMapper(
				request.getProgram().getProgramId());
		
		CalculationApi calculator = CalculationFactory.getInstitutionCalculator(
				request.getProgram().getProgramId());

		try {

			input = mapper.map(request);
			output = (BreakoutDTO) calculator.calculateAppropriation(input, "Dev");

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.CALCULATION_ERROR_ID);
			status.setDescription(ValidationMessages.CALCULATION_ERROR_TEXT);
			status.setMessage(e.getMessage());
			response.getStatus().add(status);
		}

		/*
		 * Populate the response object 
		 */

		//set cr percentage amount on web service output object
		if (output.getProgram() == null) {
			
			log.error("Program is null");
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.NULL_PROGRAM_ERROR_ID);
			status.setDescription(ValidationMessages.NULL_PROGRAM_ERROR_TEXT);
			status.setMessage(ValidationMessages.NULL_PROGRAM_ERROR_TEXT);
			response.getStatus().add(status);
			
		} else {
			
			outProgram.setPaymentPercentage(output.getProgram().getCrAppropriationPercentage());
		}
		

		if (output.getSubPrograms() == null) {
			
			log.error("Subprogram list is null");
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.NULL_SUBPROGRAM_LIST_ERROR_ID);
			status.setDescription(ValidationMessages.NULL_SUBPROGRAM_LIST_ERROR_TEXT);
			status.setMessage(ValidationMessages.NULL_SUBPROGRAM_LIST_ERROR_TEXT);
			response.getStatus().add(status);
			
		} else {

			//loop through subprograms in calculation response to populate web service output object
			List<BreakoutResponseSubProgram> outSubProgramList = new ArrayList<BreakoutResponseSubProgram>();
			for (SubProgram outSubProgramCalc : output.getSubPrograms()) {
				BreakoutResponseSubProgram outSubProgram = new BreakoutResponseSubProgram();
				outSubProgram.setSubProgramId(outSubProgramCalc.getName());
				outSubProgram.setAmount(outSubProgramCalc.getAppropriation());

				//Forecasted Amount is only populated for CRs
				if (outSubProgramCalc.getForecastedAppropriation() != null) {
					outSubProgram.setForcastedAmount(outSubProgramCalc.getForecastedAppropriation());
				}

				outSubProgramList.add(outSubProgram);
			}

			outProgram.setSubProgramList(outSubProgramList);
		}

		response.setProgram(outProgram);

		if (output.getAuditLogs() == null) {
			
			log.error("Audit Log List is null");
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.NULL_AUDIT_LOG_ERROR_ID);
			status.setDescription(ValidationMessages.NULL_AUDIT_LOG_ERROR_TEXT);
			status.setMessage(ValidationMessages.NULL_AUDIT_LOG_ERROR_TEXT);
			response.getStatus().add(status);
			
		} else {
			
			StringBuilder sb = new StringBuilder();
			for (AuditLog alog : output.getAuditLogs()) {
				log.info(alog.getCalculationLog());
				sb.append(alog.getCalculationLog());
			}

			response.setAuditLog(sb.toString());
		}
		
		return response;
	}
}

