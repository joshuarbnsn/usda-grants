package gov.usda.grants.nifa.validation;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequestSubProgram;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequest;
import gov.usda.grants.nifa.dto.calculations.in.InstitutionRequestInstitution;
import gov.usda.grants.nifa.dto.calculations.in.ServiceRequest;
import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponse;
import gov.usda.grants.nifa.dto.calculations.out.CalculationStatus;
import gov.usda.grants.nifa.dto.calculations.out.ServiceResponse;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.SubProgramType;

public class AHADRInstitutionValidator implements RequestValidator {

	@Override
	public boolean validate(ServiceRequest requestIn, ServiceResponse response) {
		
		boolean success = false;
		InstitutionRequest request = null;
		
		if ( requestIn instanceof InstitutionRequest) {
			
			//need to cast the inbound request to a Breakout Request in order to access of the inputs
			request = (InstitutionRequest) requestIn;
			
			//Validate Program object exit
			if (request.getProgram() == null) {
				CalculationStatus status = new CalculationStatus();
				status.setCode(ValidationMessages.PROGRAM_REQUIRED_ID);
				status.setDescription(ValidationMessages.PROGRAM_REQUIRED_TEXT);
				response.getStatus().add(status);
			}
			
			//if not null then validate other program fields
			else {
				
				//Validate Fiscal Year
				if (request.getProgram().getFiscalYear() < 1000) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_FISCAL_YEAR_ID);
					status.setDescription(ValidationMessages.VALID_FISCAL_YEAR_TEXT);
					response.getStatus().add(status);
				}
				
				//Validate Appropriation Type
				if (request.getProgram().getAppropriationType() == null) {
					
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_APPROPRIATION_TYPE_ID);
					status.setDescription(ValidationMessages.VALID_APPROPRIATION_TYPE_TEXT);
					response.getStatus().add(status);
					
				} else {
					
					boolean matched = false;
					for ( int x=0; x < AppropriationType.values().length; x++) {
						if (StringUtils.equals(request.getProgram().getAppropriationType(), 
								AppropriationType.values()[x].getCode())) {
							matched = true;
						}
					}
					
					if (matched == false) {
						CalculationStatus status = new CalculationStatus();
						status.setCode(ValidationMessages.VALID_APPROPRIATION_TYPE_ID);
						status.setDescription(ValidationMessages.VALID_APPROPRIATION_TYPE_TEXT);
						response.getStatus().add(status);
					}
				}
				
				
				//validate Institutions
				if (request.getEligibleInstitutions() == null) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.SUBPROGRAM_LIST_ID);
					status.setDescription(ValidationMessages.SUBPROGRAM_LIST_TEXT);
					response.getStatus().add(status);
				}
				else {
					for (InstitutionRequestInstitution institution : request.getEligibleInstitutions()) {
						
						if (StringUtils.equals(request.getProgram().getAppropriationType(), 
								AppropriationType.CONTINUING_RESOLUTIONS.getCode())) {
							
							if (institution.getPriorYearAllocation().doubleValue() <= 0) {
								CalculationStatus status = new CalculationStatus();
								status.setCode(ValidationMessages.INSTITUTION_PRIOR_AMOUNT_ID);
								status.setDescription(ValidationMessages.INSTITUTION_PRIOR_AMOUNT_TEXT);
								response.getStatus().add(status);
							}
							
						}
						
					}
				}
			}
			
			//if no validation messages were added to the status, then success
			if (response.getStatus().size() == 0) {
				success = true;
				
				CalculationStatus status = new CalculationStatus();
				status.setCode(ValidationMessages.SUCCESS_ID);
				status.setDescription(ValidationMessages.SUCCESS_TEXT);
				response.getStatus().add(status);
			}
			
		} else {
			
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.VALIDATION_OBJECT_ERROR_ID);
			status.setDescription(ValidationMessages.VALIDATION_OBJECT_ERROR_TEXT);
			response.getStatus().add(status);
		}
		
		return success;
	}
}
