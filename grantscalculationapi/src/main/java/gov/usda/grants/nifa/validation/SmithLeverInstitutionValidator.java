package gov.usda.grants.nifa.validation;

import org.apache.commons.lang3.StringUtils;

import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequest;
import gov.usda.grants.nifa.dto.calculations.in.BreakoutRequestSubProgram;
import gov.usda.grants.nifa.dto.calculations.in.ServiceRequest;
import gov.usda.grants.nifa.dto.calculations.out.BreakoutResponse;
import gov.usda.grants.nifa.dto.calculations.out.CalculationStatus;
import gov.usda.grants.nifa.dto.calculations.out.ServiceResponse;
import gov.usda.nifa.cgc.model.AppropriationType;
import gov.usda.nifa.cgc.model.SubProgramType;

public class SmithLeverInstitutionValidator implements RequestValidator {

	@Override
	public boolean validate(ServiceRequest requestIn, ServiceResponse response) {
		
		boolean success = false;
		BreakoutRequest request = null;
		
		if ( requestIn instanceof BreakoutRequest) {
			
			//need to cast the inbound request to a Breakout Request in order to access of the inputs
			request = (BreakoutRequest) requestIn;
			
			//Validate Program object exit
			if (request.getProgram() == null) {
				CalculationStatus status = new CalculationStatus();
				status.setCode(ValidationMessages.PROGRAM_REQUIRED_ID);
				status.setDescription(ValidationMessages.PROGRAM_REQUIRED_TEXT);
				response.getStatus().add(status);
			}
			
			//if not null then validate other program fields
			else {
				
				//Validate Fiscal Year
				if (request.getProgram().getFiscalYear() < 1000) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_FISCAL_YEAR_ID);
					status.setDescription(ValidationMessages.VALID_FISCAL_YEAR_TEXT);
					response.getStatus().add(status);
				}
				
				//Validate Appropriation Type
				if (request.getProgram().getAppropriationType() == null) {
					
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_APPROPRIATION_TYPE_ID);
					status.setDescription(ValidationMessages.VALID_APPROPRIATION_TYPE_TEXT);
					response.getStatus().add(status);
					
				} else {
					
					boolean matched = false;
					for ( int x=0; x < AppropriationType.values().length; x++) {
						if (StringUtils.equals(request.getProgram().getAppropriationType(), 
								AppropriationType.values()[x].getCode())) {
							matched = true;
						}
					}
					
					if (matched == false) {
						CalculationStatus status = new CalculationStatus();
						status.setCode(ValidationMessages.VALID_APPROPRIATION_TYPE_ID);
						status.setDescription(ValidationMessages.VALID_APPROPRIATION_TYPE_TEXT);
						response.getStatus().add(status);
					}
				}
				
				//if appropriation amount is CR then forecasted appropriation is required
				if (StringUtils.equals(request.getProgram().getAppropriationType(), 
						AppropriationType.CONTINUING_RESOLUTIONS.getCode())) {
					
					if (request.getProgram().getForecastedPaymentToStates().doubleValue() <= 0) {
						CalculationStatus status = new CalculationStatus();
						status.setCode(ValidationMessages.VALID_FORECASTED_PAYMENT_ID);
						status.setDescription(ValidationMessages.VALID_FORECASTED_PAYMENT_TEXT);
						response.getStatus().add(status);
					}
				} 
				
				//payment to states is required
				if (request.getProgram().getPaymentToStates().doubleValue() <= 0) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_PAYMENT_TO_STATES_ID);
					status.setDescription(ValidationMessages.VALID_PAYMENT_TO_STATES_TEXT);
					response.getStatus().add(status);
				}
				
				//mb set aside amount is required
				if (request.getProgram().getMbSetAsideAmount().doubleValue() <= 0) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_MB_SET_ASIDE_ID);
					status.setDescription(ValidationMessages.VALID_MB_SET_ASIDE_TEXT);
					response.getStatus().add(status);
				}
				
				//ppera set aside amount is required
				if (request.getProgram().getPperaIncreaseAmount().doubleValue() <= 0) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.VALID_PPERA_INCREASE_ID);
					status.setDescription(ValidationMessages.VALID_PPERA_INCREASE_TEXT);
					response.getStatus().add(status);
				}
				
				//validate sub program prior amounts
				if (request.getProgram().getSubProgramList() == null) {
					CalculationStatus status = new CalculationStatus();
					status.setCode(ValidationMessages.SUBPROGRAM_LIST_ID);
					status.setDescription(ValidationMessages.SUBPROGRAM_LIST_TEXT);
					response.getStatus().add(status);
				}
				else {
					for (BreakoutRequestSubProgram sub : request.getProgram().getSubProgramList()) {
						if (sub.getSubProgramId() == null) {
							CalculationStatus status = new CalculationStatus();
							status.setCode(ValidationMessages.SUBPROGRAM_LIST_ID);
							status.setDescription(ValidationMessages.SUBPROGRAM_LIST_TEXT);
							response.getStatus().add(status);
						} else {
							boolean matched = false;
							for ( int x=0; x < SubProgramType.values().length; x++) {
								
								if (StringUtils.equals(sub.getSubProgramId(), 
										SubProgramType.values()[x].getName())) {
									matched = true;
								}
							}
							
							if (matched == false) {
								CalculationStatus status = new CalculationStatus();
								status.setCode(ValidationMessages.SUBPROGRAM_NAME_ID);
								status.setDescription(ValidationMessages.SUBPROGRAM_NAME_TEXT);
								response.getStatus().add(status);
							}
						}
						
						if (sub.getPriorYearAmount().doubleValue() <= 0) {
							CalculationStatus status = new CalculationStatus();
							status.setCode(ValidationMessages.SUBPROGRAM_LIST_ID);
							status.setDescription(ValidationMessages.SUBPROGRAM_LIST_TEXT);
							response.getStatus().add(status);
						}
					}
				}
			}
			
			//if no validation messages were added to the status, then success
			if (response.getStatus().size() == 0) {
				success = true;
				
				CalculationStatus status = new CalculationStatus();
				status.setCode(ValidationMessages.SUCCESS_ID);
				status.setDescription(ValidationMessages.SUCCESS_TEXT);
				response.getStatus().add(status);
			}
			
		} else {
			
			CalculationStatus status = new CalculationStatus();
			status.setCode(ValidationMessages.VALIDATION_OBJECT_ERROR_ID);
			status.setDescription(ValidationMessages.VALIDATION_OBJECT_ERROR_TEXT);
			response.getStatus().add(status);
		}
		
		return success;
	}
}
