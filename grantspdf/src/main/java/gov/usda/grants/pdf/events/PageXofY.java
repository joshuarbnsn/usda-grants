package gov.usda.grants.pdf.events;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
//import com.itextpdf.text.BaseColor;

import gov.usda.grants.nifa.bo.PdfFooter;

public class PageXofY implements IEventHandler {
	private Logger log = Logger.getLogger(this.getClass().getName());
	
	protected PdfFormXObject placeholder;
	protected List<PdfFooter> footers;
	protected PdfFont font;
	protected int fontSize = 7;
	protected float side = 20;
	protected float x = 300;
	protected float y = 5;
	protected float space = .5f;
	protected float descent = 3;
	protected int totalPageCount;
	protected boolean frontCover;
	protected boolean backCover;
	
	public PageXofY(List<PdfFooter> footers) {
		this.footers = footers;
		placeholder = new PdfFormXObject(new Rectangle(0, 0, side, side));
		
		try {
			font = PdfFontFactory.createFont(FontConstants.HELVETICA);
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	@Override
	public void handleEvent(Event event) {
		
		if (footers != null && footers.size() > 0) {
			
			PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
			PdfDocument pdf = docEvent.getDocument();
			PdfPage page = docEvent.getPage();
			Document doc = new Document(pdf);
			TextAlignment textAlign = TextAlignment.RIGHT;
			
			int pageNumber = pdf.getPageNumber(page);
			
			if (hasFrontCover() && pageNumber == 1) {
				log.info("Skipping footer for Front Cover page: " + pageNumber);
			} 
			else if (hasBackCover() && pageNumber == pdf.getNumberOfPages()) {
				log.info("Skipping footer for Back Cover page: " + pageNumber);
			}
			else {
				
				if (hasFrontCover()) {
					pageNumber = pageNumber - 1;
				}
				
				Rectangle pageSize = page.getPageSize();
				float width = pageSize.getWidth();
				PdfCanvas pdfCanvas = new PdfCanvas(page.getLastContentStream(), page.getResources(), pdf);
				Canvas canvas = new Canvas(pdfCanvas, pdf, pageSize);
							
				canvas.setFont(font).setFontSize(7).setBold();
				pdfCanvas.setFontAndSize(font, 7);
				
				for (PdfFooter footer : footers) {
					Paragraph p;
					
					if (StringUtils.equals(footer.getAlignment(), PdfFooter.Alignment.LEFT.toString())) {
						
						x = 22;
						textAlign = TextAlignment.LEFT;
						
					} else if (StringUtils.equals(footer.getAlignment(), PdfFooter.Alignment.RIGHT.toString())) {
						
						x = width -22;
						textAlign = TextAlignment.RIGHT;
						
					} else if (StringUtils.equals(footer.getAlignment(), PdfFooter.Alignment.CENTER.toString())) {
						
						x = width / 2;
						textAlign = TextAlignment.CENTER;
					}
					
					//TODO change to switch if possible to transform json input to enum 
					if (StringUtils.equals(footer.getType(), PdfFooter.Type.PAGE_NUMBER.toString())) {
						
						if (StringUtils.equals(footer.getFormat(), PdfFooter.Format.SIMPLE.toString())) {
							
							p = new Paragraph().add(String.valueOf(pageNumber)).add("/");
							pdfCanvas.addXObject(placeholder, x + space, y - descent);
							canvas.showTextAligned(p, x, y, TextAlignment.RIGHT);
							
						} else if (StringUtils.equals(footer.getFormat(), PdfFooter.Format.SIMPLE_WITH_TOTAL.toString())) {
							 
							p = new Paragraph().add("Page ").add(String.valueOf(pageNumber)).add(" of");
							pdfCanvas.addXObject(placeholder, x + space, y - descent);
							canvas.showTextAligned(p, x, y, TextAlignment.RIGHT);
							
						} else {
							
							p = new Paragraph().add(String.valueOf(pageNumber)).add("/");
							pdfCanvas.addXObject(placeholder, x-22 + space, y - descent);
							canvas.showTextAligned(p, x, y, TextAlignment.RIGHT);
							
						}
					} else if (StringUtils.equals(footer.getType(), PdfFooter.Type.TEXT.toString())) {
						p = new Paragraph().add(footer.getText());
						canvas.showTextAligned(p, x, y, textAlign);
					}
				}
				
				pdfCanvas.release();
				//canvas.close();
			}
		}
		
		
	}

	public void writeTotal(PdfDocument pdf, int total) {
		log.info("updated placeholder with total: " + pdf.getNumberOfPages());
		totalPageCount = pdf.getNumberOfPages();
		Canvas canvas = new Canvas(placeholder, pdf);
		canvas.setFont(font).setFontSize(fontSize).setBold();
		canvas.showTextAligned(String.valueOf(pdf.getNumberOfPages()), 0, descent, TextAlignment.LEFT);
	}

	public boolean hasFrontCover() {
		return frontCover;
	}

	public void setFrontCover(boolean frontCover) {
		this.frontCover = frontCover;
	}

	public boolean hasBackCover() {
		return backCover;
	}

	public void setBackCover(boolean backCover) {
		this.backCover = backCover;
	}
}
