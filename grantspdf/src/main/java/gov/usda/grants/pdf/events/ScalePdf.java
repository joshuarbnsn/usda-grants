package gov.usda.grants.pdf.events;

import java.util.Locale;

import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfResources;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Document;

public class ScalePdf implements IEventHandler {
	protected PdfFormXObject placeholder;
	protected float side = 20;
	protected float x = 300;
	protected float y = 5;
	protected float space = 4.5f;
	protected float descent = 3;

	public ScalePdf() {
	}

	@Override
	public void handleEvent(Event event) {
		PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
		PdfDocument pdf = docEvent.getDocument();
		PdfPage page = docEvent.getPage();
		Document doc = new Document(pdf);
		float percentage = 0.96f;
		float offsetX = (page.getPageSize().getWidth() * (1 - percentage)) / 2;
        float offsetY = (page.getPageSize().getHeight() * (1 - percentage)) / 2;
        new PdfCanvas(page.newContentStreamBefore(), new PdfResources(), pdf).writeLiteral(
                String.format(Locale.ENGLISH, "\nq %s 0 0 %s %s %s cm\nq\n",
                        percentage, percentage, offsetX, offsetY));
        new PdfCanvas(page.newContentStreamAfter(),
                new PdfResources(), pdf).writeLiteral("\nQ\nQ\n");
		
	}

}
