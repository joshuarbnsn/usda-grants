package gov.usda.grants.nifa.transformers;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.bo.PdfDocumentData;
import gov.usda.grants.nifa.bo.requests.PdfMerge;

public class SortDocuments extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String INT_CONCAT = "Internal-Concatenated-Application";
	private static final String EXT_CONCAT = "External-Concatenated-Application";
	private static final String PERSONAL = "Form RR_PersonalData";
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		@SuppressWarnings("unchecked")
		PdfMerge request = (PdfMerge) message.getPayload();
		
	    Collections.sort(request.getDocumentList(), new Comparator() {

	        public int compare(Object o1, Object o2) {

	            Integer x1 = ((PdfDocumentData) o1).getGroup();
	            Integer x2 = ((PdfDocumentData) o2).getGroup();
	            int comp1 = x1.compareTo(x2);

	            if (comp1 != 0) {
	               return comp1;
	            } else {
	               Integer y1 = ((PdfDocumentData) o1).getLevel();
	               Integer y2 = ((PdfDocumentData) o2).getLevel();
	               int comp2 = y1.compareTo(y2);
	               
	               if (comp2 != 0) {
		               return comp2;
		            } else {
		               String z1 = ((PdfDocumentData) o1).getBookmark();
		               String z2 = ((PdfDocumentData) o2).getBookmark();
		               return z1.compareTo(z2);
		            }
	            }
	    }});
	    
		return request;
	}
}
