package gov.usda.grants.nifa.bo;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class PdfDocumentData implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("uri")
	public String Uri;
	
	@JsonProperty("bookmark")
	public String bookmark;
	
	@JsonProperty("node")
	public String node;
	
	@JsonProperty("FileName")
	public String fileName;
	
	@JsonIgnore
	public int group;

	@JsonIgnore
	public int level;
	
	@JsonIgnore
	public boolean processed;
	
	public String getUri() {
		return Uri;
	}

	public void setUri(String uri) {
		Uri = uri;
	}

	public String getBookmark() {
		return bookmark;
	}

	public void setBookmark(String bookmark) {
		this.bookmark = bookmark;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

}
