package gov.usda.grants.nifa.transformers;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.util.Base64Utility;

public class DefaultPdfResponse extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	
	private String destination;

	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		String filePath = 	destination + "/defaultError.pdf";
		StringBuffer sb = new StringBuffer();
		
		try {
			String encodedFile = Base64Utility.encodeFileToBase64Binary(filePath);
			sb.append("{\"FileName\":\"default.pdf\",\"FileContent\":\"");
			sb.append(encodedFile);
			sb.append("\"}");
		} catch (IOException io) {
			log.error("unable to encode file: "+filePath, io);
		}
		
        return sb.toString();
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}
}
