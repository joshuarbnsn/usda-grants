	package gov.usda.grants.nifa.transformers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleContext;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.forms.PdfAcroForm;

import gov.usda.grants.nifa.bo.requests.PdfStitch;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;

public class CreateCoverSheets extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String COVER_SHEET_NAME = "front_cover.pdf";
	private static final String TRAILER_SHEET_NAME = "back_cover.pdf";
	private static final String TRAILER_SHEET_TEXT = "End of Grant Application";
	private static final String IMAGE_PATH = "USDA_logo.png";
	
	private String destination;
	
	@Inject private MuleContext muleContext;
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		PdfStitch request = (PdfStitch) message.getPayload();
		
		String fileDir = 	destination + "/" + message.getMessageRootId();
		String coverSheetDest = fileDir + "/" + COVER_SHEET_NAME;
		String trailerSheetDest = fileDir + "/" + TRAILER_SHEET_NAME;
		File coverFile = new File(coverSheetDest);
		File trailerFile = new File(trailerSheetDest);
		
		if (!coverFile.getParentFile().exists()) {
			coverFile.getParentFile().mkdirs();
		}
		
		URL resource = muleContext.getExecutionClassLoader().getResource(IMAGE_PATH);
		
		try { 
			createFrontPdf(request, coverSheetDest, resource.getPath());
			FileContent front = new FileContent();
			front.setFileName(COVER_SHEET_NAME);
			front.setFileLocation(coverSheetDest);
			request.getAttachments().add(front);
			
			createBackPdf(request, trailerSheetDest, resource.getPath());
			FileContent back = new FileContent();
			back.setFileName(TRAILER_SHEET_NAME);
			back.setFileLocation(trailerSheetDest);
			request.getAttachments().add(back);
			
		} catch (IOException ioe) {
			log.error(ioe.getMessage(), ioe);
		}
		
        
        return request;
	}
	
	public void createFrontPdf(PdfStitch request, String dest, String logoPath) throws IOException{
        //Initialize PDF writer
        PdfWriter writer = new PdfWriter(dest);
        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);
        // Initialize document
        Document document = new Document(pdf);
        //Add paragraph to the document
        
        log.info("loading image for cover sheets: " + logoPath);
        //String imagePath = String.format(IMAGE_PATH, request.getAgencyName().toLowerCase());
        Image logo = new Image(ImageDataFactory.create(logoPath));
        logo.scaleToFit(145.95f, 100f);
        
        Paragraph position0 = new Paragraph();
        position0.add(logo);
        document.showTextAligned(position0, 30f, 700, TextAlignment.LEFT);
        
        if (!StringUtils.isEmpty(request.getApplicationTitle())) {
        	Paragraph position1 = new Paragraph(request.getApplicationTitle());
            document.showTextAligned(position1, 297.5f, 550, TextAlignment.CENTER);
        }
        
        if (!StringUtils.isEmpty(request.getPhdName())) {
            Paragraph position2 = new Paragraph(request.getPhdName());
            document.showTextAligned(position2, 297.5f, 450, TextAlignment.CENTER);
        }
        
        if (!StringUtils.isEmpty(request.getOrganizationName())) {
            Paragraph position3 = new Paragraph(request.getOrganizationName());
            document.showTextAligned(position3, 297.5f, 390, TextAlignment.CENTER);
        }
        
        if (!StringUtils.isEmpty(request.getGrantsTrackingId())) {
        	Paragraph position4 = new Paragraph(request.getGrantsTrackingId());
            document.showTextAligned(position4, 297.5f, 290, TextAlignment.CENTER);            
        }
        
        //Close document
        document.close();
    }
	
	public void createBackPdf(PdfStitch request, String dest, String logoPath) throws IOException{
        //Initialize PDF writer
        PdfWriter writer = new PdfWriter(dest);
        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);
        // Initialize document
        Document document = new Document(pdf);
        //Add paragraph to the document
        
        //String imagePath = String.format(IMAGE_PATH, request.getAgencyName().toLowerCase());
        Image logo = new Image(ImageDataFactory.create(logoPath));
        logo.scaleToFit(145.95f, 100f);
        Paragraph position0 = new Paragraph();
        position0.add(logo);
        document.showTextAligned(position0, 30f, 700, TextAlignment.LEFT);
        
        Paragraph position1 = new Paragraph(TRAILER_SHEET_TEXT);
        position1.setFontSize(40f);
        document.showTextAligned(position1, 297.5f, 450, TextAlignment.CENTER);
        
        Paragraph position2 = new Paragraph(request.getGrantsTrackingId());
        document.showTextAligned(position2, 297.5f, 400, TextAlignment.CENTER);
        
        //Close document
        document.close();
    }

	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}

}
