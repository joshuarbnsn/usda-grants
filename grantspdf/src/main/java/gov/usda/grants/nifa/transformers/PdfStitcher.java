package gov.usda.grants.nifa.transformers;

import java.io.ByteArrayOutputStream;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.bo.requests.PdfMerge;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;
import gov.usda.grants.nifa.service.PdfStitchingService;

public class PdfStitcher extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String DOCUMENT_NAME = "application.pdf";
	private static final String DOCUMENT_NAME_FORMAT = "%s-%s";
	private String destination;
	private String coverPage;
	private String backPage;

	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		PdfMerge request = (PdfMerge) message.getPayload();
		FileContent response = new FileContent();
		
		if (request.getDocumentList().size() > 0) {

			PdfStitchingService service = new PdfStitchingService();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			try {
				service.mergePdf(request, request.getDocumentList(), baos);
			} catch (Exception e) {
				log.error("Exception merging PDFs", e);
			}

			if (baos == null || baos.toByteArray() == null) {
				log.error("Unable to merge PDFs");
			} else {
				response.setFileContent(Base64.getEncoder().encodeToString(baos.toByteArray()));

				if (StringUtils.isEmpty(request.getPrefix())) {
					response.setFileName(DOCUMENT_NAME);
				} else {
					response.setFileName(String.format(DOCUMENT_NAME_FORMAT, request.getPrefix(), DOCUMENT_NAME));
				}
			}
		}
		return response;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getCoverPage() {
		return coverPage;
	}


	public void setCoverPage(String coverPage) {
		this.coverPage = coverPage;
	}


	public String getBackPage() {
		return backPage;
	}


	public void setBackPage(String backPage) {
		this.backPage = backPage;
	}
}
