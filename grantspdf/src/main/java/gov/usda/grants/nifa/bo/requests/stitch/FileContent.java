package gov.usda.grants.nifa.bo.requests.stitch;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class FileContent implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("FileContent")
	private String fileContent;
	
	@JsonProperty("FileName")
	private String fileName;
	
	@JsonIgnore
	private String fileLocation;
	
	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

}
