package gov.usda.grants.nifa.bo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class PdfStitchRequestOld {
	
	@JsonProperty("title")
	public String title;
	
	@JsonProperty("pdName")
	public String pdName;
	
	@JsonProperty("schoolName")
	public String schoolName;
	
	@JsonProperty("proposalId")
	public String proposalId;
	
	@JsonProperty("documents")
	public List<PdfDocumentData> documentList;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPdName() {
		return pdName;
	}

	public void setPdName(String pdName) {
		this.pdName = pdName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getProposalId() {
		return proposalId;
	}

	public void setProposalId(String proposalId) {
		this.proposalId = proposalId;
	}

	public List<PdfDocumentData> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<PdfDocumentData> documentList) {
		this.documentList = documentList;
	}

}
