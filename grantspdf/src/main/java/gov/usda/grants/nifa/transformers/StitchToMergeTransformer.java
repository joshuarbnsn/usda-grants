package gov.usda.grants.nifa.transformers;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.bo.PdfDocumentData;
import gov.usda.grants.nifa.bo.PdfFooter;
import gov.usda.grants.nifa.bo.requests.PdfMerge;
import gov.usda.grants.nifa.bo.requests.PdfStitch;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;

public class StitchToMergeTransformer extends AbstractMessageTransformer{
	private Logger log = Logger.getLogger(this.getClass().getName());
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		log.info(message.getMessageRootId() +": Transforming Stitch Request into Merge Request");
		PdfStitch request = (PdfStitch) message.getPayload();
		PdfMerge mergeRequest = new PdfMerge();
		
		mergeRequest.setPrefix(request.grantsTrackingId);
		
		for(FileContent fileContent : request.getAttachments()) {
			
			if (!StringUtils.isEmpty(fileContent.getFileLocation())) {
				PdfDocumentData data = new PdfDocumentData();
				data.setUri(fileContent.getFileLocation());
				data.setFileName(fileContent.getFileName());
				mergeRequest.getDocumentList().add(data);
			}
		}
		
		//add footers
		PdfFooter idFooter = new PdfFooter();
		idFooter.setAlignment(PdfFooter.Alignment.LEFT.toString());
		idFooter.setType(PdfFooter.Type.TEXT.toString());
		idFooter.setText(request.getGrantsTrackingId());
		idFooter.setWidth(getLeftFooterWidth());
		mergeRequest.getFooterList().add(idFooter);
		
		PdfFooter pageNumber = new PdfFooter();
		pageNumber.setType(PdfFooter.Type.PAGE_NUMBER.toString());
		pageNumber.setFormat(PdfFooter.Format.SIMPLE.toString());
		pageNumber.setAlignment(PdfFooter.Alignment.RIGHT.toString());
		pageNumber.setWidth(getRightFooterWidth());
		mergeRequest.getFooterList().add(pageNumber);
		
		return mergeRequest;
	}
	
	private String leftFooterWidth;
	private String rightFooterWidth;

	public String getLeftFooterWidth() {
		return leftFooterWidth;
	}
	public void setLeftFooterWidth(String leftFooterWidth) {
		this.leftFooterWidth = leftFooterWidth;
	}
	public String getRightFooterWidth() {
		return rightFooterWidth;
	}
	public void setRightFooterWidth(String rightFooterWidth) {
		this.rightFooterWidth = rightFooterWidth;
	}
}
