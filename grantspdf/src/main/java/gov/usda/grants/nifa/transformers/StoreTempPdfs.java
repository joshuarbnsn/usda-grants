package gov.usda.grants.nifa.transformers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.itextpdf.tool.xml.xtra.xfa.MetaData;
import com.itextpdf.tool.xml.xtra.xfa.XFAFlattener;
import com.itextpdf.tool.xml.xtra.xfa.XFAFlattenerProperties;
import com.itextpdf.tool.xml.xtra.xfa.font.XFAFontSettings;

import gov.usda.grants.nifa.bo.requests.PdfStitch;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;

public class StoreTempPdfs extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	
	private String destination;
	private String coverPage;
	private String backPage;

	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		PdfStitch request = (PdfStitch) message.getPayload();
		
		String fileDir = 	destination + "/" + message.getMessageRootId();
		
		for (FileContent fileContent : request.getAttachments()) {
			
			if (StringUtils.isEmpty(fileContent.getFileName()) 
					|| StringUtils.isEmpty(fileContent.getFileContent())
					|| !StringUtils.endsWithIgnoreCase(fileContent.getFileName(), "pdf")) {
				log.warn(message.getMessageRootId() + ": Skipping file with missing file name or data at " +
			request.getAttachments().indexOf(fileContent));
			}
			else {
				log.info(message.getMessageRootId() + ": Storing temp file: " + fileContent.getFileName());
				
				String filePath = fileDir + "/" + fileContent.getFileName();
				
				File file = new File(filePath);
				
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}
				
				byte[] data = Base64.getDecoder().decode(fileContent.getFileContent());
				try (OutputStream stream = new FileOutputStream(file)) {
				    stream.write(data);
				    stream.close();
				    fileContent.setFileLocation(file.getCanonicalPath());
				} catch (FileNotFoundException fnf) {
					log.error("File not found: "+ filePath, fnf);
				} catch (IOException io) {
					log.error("Error writing file: "+ filePath, io);
				}
				
				/*//flatten XFA
				XFAFlattenerProperties flattenerProperties = new XFAFlattenerProperties()
		                .setPdfVersion(XFAFlattenerProperties.PDF_1_7)
		                .createXmpMetaData()
		                .setTagged()
		               // .setExtractXdpConcurrently(false)
		                .setMetaData(
		                        new MetaData()
		                            .setAuthor("iText Samples")
		                            .setLanguage("EN")
		                            .setSubject("Showing off our flattening skills")
		                            .setTitle("Flattened XFA"));
		 
		        XFAFlattener xfaf = new XFAFlattener()
		                .setFontSettings(new XFAFontSettings().setEmbedExternalFonts(true))
		                //.setExtraEventList(this.javascriptEvents)
		                .setFlattenerProperties(flattenerProperties)
		                .setViewMode(XFAFlattener.ViewMode.SCREEN);
		 
		        try {
		        	String outputPath = "flat_" + filePath;
		        	xfaf.flatten(new FileInputStream(filePath), new FileOutputStream(outputPath));
		        	
		        } catch (Exception e) { log.error(e); } */
			}
	        
			//remove encoded pdf to lighten the payload
			fileContent.setFileContent(null);
		}
        
        return request;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getCoverPage() {
		return coverPage;
	}


	public void setCoverPage(String coverPage) {
		this.coverPage = coverPage;
	}


	public String getBackPage() {
		return backPage;
	}


	public void setBackPage(String backPage) {
		this.backPage = backPage;
	}
}
