package gov.usda.grants.nifa.bo;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class PdfFooter implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("type")
	String type;
	
	@JsonProperty("format")
	String format;
	
	@JsonProperty("text")
	String text;
	
	@JsonProperty("alignment")
	String alignment;
	
	@JsonProperty("width")
	String width;
	
	public enum Type {
		TEXT, PAGE_NUMBER
	}
	
	public enum Format {
		SIMPLE, SIMPLE_WITH_TOTAL
	}
	
	public enum Alignment {
		LEFT, RIGHT, CENTER
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getAlignment() {
		return alignment;
	}
	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}

}
