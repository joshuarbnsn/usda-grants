package gov.usda.grants.nifa.bo.requests;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import gov.usda.grants.nifa.bo.PdfDocumentData;
import gov.usda.grants.nifa.bo.PdfFooter;

public class PdfMerge implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("documents")
	public List<PdfDocumentData> documentList;

	@JsonProperty("footers")
	public List<PdfFooter> footerList;
	
	@JsonProperty("prefix")
	public String prefix;
	
	public List<PdfDocumentData> getDocumentList() {
		if (documentList == null) {
			documentList = new ArrayList<PdfDocumentData>();
		}
		return documentList;
	}

	public void setDocumentList(List<PdfDocumentData> documentList) {
		this.documentList = documentList;
	}

	public List<PdfFooter> getFooterList() {
		if (footerList == null) {
			footerList = new ArrayList<PdfFooter>();
		}
		return footerList;
	}

	public void setFooterList(List<PdfFooter> footerList) {
		this.footerList = footerList;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
