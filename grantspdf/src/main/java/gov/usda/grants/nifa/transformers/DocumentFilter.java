package gov.usda.grants.nifa.transformers;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.bo.PdfDocumentData;
import gov.usda.grants.nifa.bo.requests.PdfMerge;

public class DocumentFilter extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String INT_CONCAT = "Internal-Concatenated-Application";
	private static final String EXT_CONCAT = "External-Concatenated-Application";
	private static final String PERSONAL = "Form RR_PersonalData";
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		@SuppressWarnings("unchecked")
		PdfMerge request = (PdfMerge) message.getPayload();
		
		for (Iterator<PdfDocumentData> i = request.getDocumentList().iterator(); i.hasNext();) {
			PdfDocumentData item = i.next();
		    if (StringUtils.containsIgnoreCase(item.fileName, INT_CONCAT) ||
		    		StringUtils.containsIgnoreCase(item.fileName, EXT_CONCAT) ||
		    				StringUtils.containsIgnoreCase(item.fileName, PERSONAL)
		    ) {
		    	log.info("filtering from collection: " + item.fileName);
		        i.remove();
		    }
		}
		return request;
	}
}
