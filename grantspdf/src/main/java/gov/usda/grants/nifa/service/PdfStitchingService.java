package gov.usda.grants.nifa.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.navigation.PdfExplicitDestination;
import com.itextpdf.kernel.utils.PdfMerger;

import gov.usda.grants.nifa.bo.PdfDocumentData;
import gov.usda.grants.nifa.bo.requests.PdfMerge;
import gov.usda.grants.pdf.events.PageXofY;
import gov.usda.grants.pdf.events.ScalePdf;

public class PdfStitchingService {
	private Logger log = Logger.getLogger(this.getClass().getName());
	private static final String FRONT_COVER_PAGE_NAME = "front_cover.pdf";
	private static final String BACK_COVER_PAGE_NAME = "back_cover.pdf";
	
	public void mergePdf(PdfMerge request, List<PdfDocumentData> documents, ByteArrayOutputStream baos) throws IOException {
		String frontCoverUri = null;
		String backCoverUri = null;
		
		PdfWriter writer = new PdfWriter(baos);
		PdfDocument pdfDoc = new PdfDocument(writer);
		PdfMerger merger = new PdfMerger(pdfDoc);
		PdfOutline rootOutline = pdfDoc.getOutlines(false);
		
		PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);

		PageXofY event = new PageXofY(request.getFooterList());
		pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, event);

		ScalePdf scalePdf = new ScalePdf();
		pdfDoc.addEventHandler(PdfDocumentEvent.INSERT_PAGE, scalePdf);
		
		int page = 1;

		for (PdfDocumentData data  : documents) {
			log.info("Attempting to read temp file at " + data.getUri());
			if (StringUtils.equalsIgnoreCase(data.getFileName(), this.FRONT_COVER_PAGE_NAME))
			{
				frontCoverUri = data.getUri();
			} 
			else if (StringUtils.equalsIgnoreCase(data.getFileName(), this.BACK_COVER_PAGE_NAME)) {
				backCoverUri = data.getUri();
			}
			else {
				PdfReader reader = new PdfReader(data.getUri());
				PdfDocument srcDoc = new PdfDocument(reader);
			
				merger.merge(srcDoc, 1, srcDoc.getNumberOfPages());

				List<PdfOutline> outlines = rootOutline.getAllChildren();
				
				Optional<PdfOutline> parentOutline = outlines.stream().filter(o -> StringUtils.endsWithIgnoreCase(o.getTitle(), data.getNode())).findAny();
				
				//This logic depends on the documents in the list to be in the correct order
				if (!StringUtils.isEmpty(data.getNode())) {
					
					if (parentOutline.isPresent()) {
						parentOutline.get().addOutline(data.getBookmark());
						parentOutline.get().addDestination(PdfExplicitDestination.createFit(pdfDoc.getPage(page)));
					} else {
						PdfOutline newParentOutline = rootOutline.addOutline(data.getNode());
						newParentOutline.addDestination(PdfExplicitDestination.createFit(pdfDoc.getPage(page)));
						
						//if the node and the bookmark are the same, then we don't want to duplicate the links. 
						if (!StringUtils.endsWithIgnoreCase(newParentOutline.getTitle(), data.getBookmark())) {
							PdfOutline childOutline  = newParentOutline.addOutline(data.getBookmark());
							childOutline.addDestination(PdfExplicitDestination.createFit(pdfDoc.getPage(page)));
							
						}
					}
				}
				
				else {
					PdfOutline outline = rootOutline.addOutline(data.getBookmark());
					outline.addDestination(PdfExplicitDestination.createFit(pdfDoc.getPage(page)));
				}
				
				page += srcDoc.getNumberOfPages();
				srcDoc.close();
			}	
		}
		
		event.writeTotal(pdfDoc, page);
		
		//Add Cover Pages
		if (!StringUtils.isEmpty(frontCoverUri)) {
			event.setFrontCover(true);
			PdfReader reader = new PdfReader(frontCoverUri);
			PdfDocument srcDoc = new PdfDocument(reader);
			srcDoc.copyPagesTo(1, 1, pdfDoc, 1);
			srcDoc.close();
		}
		
		if (!StringUtils.isEmpty(backCoverUri)) {
			event.setBackCover(true);
			PdfReader reader = new PdfReader(backCoverUri);
			PdfDocument srcDoc = new PdfDocument(reader);
			srcDoc.copyPagesTo(1, 1, pdfDoc);
			srcDoc.close();
		}
		
		pdfDoc.close();
	}
}
