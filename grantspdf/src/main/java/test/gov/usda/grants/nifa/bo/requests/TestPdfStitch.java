package test.gov.usda.grants.nifa.bo.requests;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import gov.usda.grants.nifa.bo.requests.stitch.FileContent;
import test.gov.usda.grants.nifa.bo.requests.stitch.FileLocation;

public class TestPdfStitch implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("AgencyName")
	public String agencyName;
	
	@JsonProperty("ApplicationTitle")
	public String applicationTitle;
	
	@JsonProperty("Author")
	public String author;
	
	@JsonProperty("CreatorName")
	public String creatorName;

	@JsonProperty("GrantsTrackingId")
	public String grantsTrackingId;
	
	@JsonProperty("Keywords")
	public String keywords;
	
	@JsonProperty("PhdName")
	public String phdName;
	
	@JsonProperty("OrganizationName")
	public String organizationName;
	
	@JsonProperty("SubmitterName")
	public String submitterName;
	
	@JsonProperty("Attachments")
	public List<FileLocation> attachments;

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getApplicationTitle() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle = applicationTitle;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getGrantsTrackingId() {
		return grantsTrackingId;
	}

	public void setGrantsTrackingId(String grantsTrackingId) {
		this.grantsTrackingId = grantsTrackingId;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getPhdName() {
		return phdName;
	}

	public void setPhdName(String phdName) {
		this.phdName = phdName;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getSubmitterName() {
		return submitterName;
	}

	public void setSubmitterName(String submitterName) {
		this.submitterName = submitterName;
	}

	public List<FileLocation> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<FileLocation> attachments) {
		this.attachments = attachments;
	}
	
}