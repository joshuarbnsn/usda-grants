package test.gov.usda.grants.nifa.transformers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.bo.requests.PdfStitch;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;

public class StoreMergeResult extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	
	private String destination;
	private String coverPage;
	private String backPage;

	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		FileContent fileContent = (FileContent) message.getPayload();

		if (StringUtils.isEmpty(fileContent.getFileName()) || StringUtils.isEmpty(fileContent.getFileContent())) {
			log.warn(message.getMessageRootId() + ": Skipping file with missing file name");
		}
		else {
			log.info(message.getMessageRootId() + ": Storing temp file: " + fileContent.getFileName());

			String fileDir = 	destination + "/" + message.getMessageRootId();
			String filePath = fileDir + "/" + fileContent.getFileName();
			File file = new File(filePath);

			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}

			byte[] data = Base64.getDecoder().decode(fileContent.getFileContent());
			try (OutputStream stream = new FileOutputStream(file)) {
				stream.write(data);
				stream.close();
				fileContent.setFileLocation(file.getCanonicalPath());
			} catch (FileNotFoundException fnf) {
				log.error("File not found: "+filePath, fnf);
			} catch (IOException io) {
				log.error("Error writing file: "+filePath, io);
			}
		}
		
		return fileContent;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getCoverPage() {
		return coverPage;
	}


	public void setCoverPage(String coverPage) {
		this.coverPage = coverPage;
	}


	public String getBackPage() {
		return backPage;
	}


	public void setBackPage(String backPage) {
		this.backPage = backPage;
	}
}
