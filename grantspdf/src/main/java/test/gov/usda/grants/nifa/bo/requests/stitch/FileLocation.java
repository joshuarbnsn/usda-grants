package test.gov.usda.grants.nifa.bo.requests.stitch;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class FileLocation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("FileLocation")
	private String fileLocation;

	@JsonProperty("FileName")
	private String fileName;
	
	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
