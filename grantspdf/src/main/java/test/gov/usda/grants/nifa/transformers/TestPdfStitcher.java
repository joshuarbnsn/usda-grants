package test.gov.usda.grants.nifa.transformers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.nifa.bo.requests.PdfStitch;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;
import gov.usda.grants.util.Base64Utility;
import test.gov.usda.grants.nifa.bo.requests.TestPdfStitch;
import test.gov.usda.grants.nifa.bo.requests.stitch.FileLocation;

public class TestPdfStitcher extends AbstractMessageTransformer{
	private Logger log = Logger.getLogger(this.getClass().getName());
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		@SuppressWarnings("unused")
		TestPdfStitch request = (TestPdfStitch) message.getPayload();
		PdfStitch stitchRequest = new PdfStitch();
		mapAttributes(request, stitchRequest);
		/*PdfMerge request = (PdfMerge) message.getPayload();
		
		String filePath = 	destination + "/" + message.getMessageRootId() + ".pdf";
		File file = new File(filePath);
		file.getParentFile().mkdirs();
        
        PdfStitchingService service = new PdfStitchingService();
        
        try {
        	service.mergePdf(request, request.getDocumentList(), filePath);
        } catch (Exception e) {
        	log.error("Exception merging PDFs", e);

        }
        
        return filePath;*/
		return stitchRequest;
	}

	private void mapAttributes(TestPdfStitch testRequest, PdfStitch stitchRequest) {
		stitchRequest.setAgencyName(testRequest.getAgencyName());
		stitchRequest.setApplicationTitle(testRequest.getApplicationTitle());
		stitchRequest.setAuthor(testRequest.getAuthor());
		stitchRequest.setGrantsTrackingId(testRequest.getGrantsTrackingId());
		stitchRequest.setKeywords(testRequest.getKeywords());
		stitchRequest.setOrganizationName(testRequest.getOrganizationName());
		stitchRequest.setPhdName(testRequest.getPhdName());
		stitchRequest.setSubmitterName(testRequest.getSubmitterName());
		
		List<FileContent> contentList = new ArrayList<FileContent>();
		for (FileLocation location : testRequest.getAttachments()) {
			FileContent content = new FileContent();
			
			try {
				content.setFileContent(Base64Utility.encodeFileToBase64Binary(location.getFileLocation()));
				content.setFileName(location.getFileName());
				contentList.add(content);
			} catch (IOException ioex) {
				log.error("Failed to encode attachment", ioex);
			}
			
			stitchRequest.setAttachments(contentList);
		}
	}
}
