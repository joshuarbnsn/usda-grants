package test.gov.usda.grants.nifa.transformers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

import gov.usda.grants.nifa.bo.requests.PdfStitch;
import gov.usda.grants.nifa.bo.requests.stitch.FileContent;

public class AddCoverSheets extends AbstractMessageTransformer{

	private Logger log = Logger.getLogger(this.getClass().getName());
	
	private String destination;
	private String coverPage;
	private String backPage;

	private static final String TITLE = "Grant Application Title";
	private static final String PD = "PD Name";
	private static final String SCHOOL = "Applicant Institution";
	private static final String ID = "Grant Tracking Number/ Proposal Number";
	
	private static final String COVER_SHEET_NAME = "cover.pdf";
	private static final String TRAILER_SHEET_NAME = "trailer.pdf";
	
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		File newFile = new File("newFile");
		
		String newPath = newFile.getAbsolutePath().toString();
		log.error(newPath);
		/*String fileDir = 	destination + "/" + message.getMessageRootId();
		String coverSheetDest = fileDir + "/" + COVER_SHEET_NAME;
		
		File coverFile = new File(coverSheetDest);
		
		if (!coverFile.getParentFile().exists()) {
			coverFile.getParentFile().mkdirs();
		}
		
		try {
			this.fillCoverSheet(coverSheetDest, coverSheetDest);
		} catch (Exception e) {
			log.error(e);
		}*/
		return null;
	}

	public void fillCoverSheet(String src, String dest) throws FileNotFoundException, IOException {
		
		//local.properties Cover_Front_Page.pdf
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("Cover_Front_Page.pdf");
		
		PdfDocument pdfDoc = new PdfDocument(new PdfReader(is), new PdfWriter(dest));
		 
	    PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDoc, true);
	    form.setGenerateAppearance(true);
	    //form.getField(TITLE).setValue(value, display);
	    form.getField(TITLE).setValue("TITLE 1");
	    form.getField(TITLE).setJustification(PdfFormField.ALIGN_CENTER);
	    form.getField(PD).setValue("TITLE 1").setJustification(PdfFormField.ALIGN_CENTER);
	    form.getField(SCHOOL).setValue("TITLE 1").setJustification(PdfFormField.ALIGN_CENTER);
	    form.getField(ID).setValue("TITLE 1").setJustification(PdfFormField.ALIGN_CENTER);
	    
	    form.flattenFields();
	    
	    pdfDoc.close();
	}

	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getCoverPage() {
		return coverPage;
	}


	public void setCoverPage(String coverPage) {
		this.coverPage = coverPage;
	}


	public String getBackPage() {
		return backPage;
	}


	public void setBackPage(String backPage) {
		this.backPage = backPage;
	}
}
