
package org.xmlsoap.schemas.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.xmlsoap.schemas.wsdl.soap.TAddress;
import org.xmlsoap.schemas.wsdl.soap.TBinding;
import org.xmlsoap.schemas.wsdl.soap.TBody;
import org.xmlsoap.schemas.wsdl.soap.THeader;
import org.xmlsoap.schemas.wsdl.soap.TOperation;


/**
 * <p>Java class for tExtensibilityElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tExtensibilityElement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute ref="{http://schemas.xmlsoap.org/wsdl/}required"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tExtensibilityElement")
@XmlSeeAlso({
    TBinding.class,
    TOperation.class,
    THeader.class,
    TAddress.class,
    TBody.class
})
public abstract class TExtensibilityElement {

    @XmlAttribute(name = "required", namespace = "http://schemas.xmlsoap.org/wsdl/")
    protected Boolean required;

    /**
     * Gets the value of the required property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequired() {
        return required;
    }

    /**
     * Sets the value of the required property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequired(Boolean value) {
        this.required = value;
    }

}
