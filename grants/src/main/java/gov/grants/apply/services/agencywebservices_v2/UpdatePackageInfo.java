
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;
import gov.grants.apply.system.grantspackage_v1.UpdatePackage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}NullifyMissingOptionalElements" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}UpdatePackage" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "nullifyMissingOptionalElements",
    "updatePackage"
})
@XmlRootElement(name = "UpdatePackageInfo")
public class UpdatePackageInfo {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String opportunityID;
    @XmlElement(name = "NullifyMissingOptionalElements", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", defaultValue = "N")
    @XmlSchemaType(name = "string")
    protected YesNoType nullifyMissingOptionalElements;
    @XmlElement(name = "UpdatePackage", namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", required = true)
    protected List<UpdatePackage> updatePackage;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the nullifyMissingOptionalElements property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getNullifyMissingOptionalElements() {
        return nullifyMissingOptionalElements;
    }

    /**
     * Sets the value of the nullifyMissingOptionalElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setNullifyMissingOptionalElements(YesNoType value) {
        this.nullifyMissingOptionalElements = value;
    }

    /**
     * Gets the value of the updatePackage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updatePackage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdatePackage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdatePackage }
     * 
     * 
     */
    public List<UpdatePackage> getUpdatePackage() {
        if (updatePackage == null) {
            updatePackage = new ArrayList<UpdatePackage>();
        }
        return this.updatePackage;
    }

}
