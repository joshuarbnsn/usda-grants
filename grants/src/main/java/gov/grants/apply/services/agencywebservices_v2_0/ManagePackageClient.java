package gov.grants.apply.services.agencywebservices_v2_0;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import gov.grants.apply.services.agencywebservices_v2.ManagePackageRequest;
import gov.grants.apply.services.agencywebservices_v2.ManagePackageResponse;

public class ManagePackageClient {

	private static final QName SERVICE_NAME = new QName("http://apply.grants.gov/services/AgencyWebServices-V2.0", "AgencyWebServices-V2.0");
	protected final Logger log = LogManager.getLogger(getClass());
	
	public ManagePackageResponse ManagePackage(ManagePackageRequest request, String wsdlFilePath, String manageTls) {
		URL wsdlURL = AgencyWebServicesV20.WSDL_LOCATION;
		File wsdlFile = new File(wsdlFilePath);
        try {
            if (wsdlFile.exists()) {
                wsdlURL = wsdlFile.toURI().toURL();
            } 
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
		AgencyWebServicesV20 ss = new AgencyWebServicesV20(wsdlURL, SERVICE_NAME);
        AgencyWebServicesPortType port = ss.getAgencyWebServicesSoapPort();
        
        if (StringUtils.equalsIgnoreCase(manageTls, Boolean.TRUE.toString()))
        {
        	try 
        	{
        		setupTLS(port);
        	}  catch (Exception e)
        	{
        		log.error(e);
        	}
        }
        
        ManagePackageResponse response = port.managePackage(request);
        
        return response;
	}
	
	private static void setupTLS(AgencyWebServicesPortType port) 
	        throws FileNotFoundException, IOException, GeneralSecurityException {
	        String keyStoreLoc = "src/main/resources/myks.jks";
	        String trustStoreLoc = "src/main/resources/cacerts.jks";
	        HTTPConduit httpConduit = (HTTPConduit) ClientProxy.getClient(port).getConduit();
	 
	        TLSClientParameters tlsCP = new TLSClientParameters();
	        String keyPassword = "Goags001";
	        KeyStore keyStore = KeyStore.getInstance("JKS");
	        keyStore.load(new FileInputStream(keyStoreLoc), keyPassword.toCharArray());
	        KeyManager[] myKeyManagers = getKeyManagers(keyStore, keyPassword);
	        tlsCP.setKeyManagers(myKeyManagers);
	 
	        KeyStore trustStore = KeyStore.getInstance("JKS");
	        String trustPassword = "changeit";
	        trustStore.load(new FileInputStream(trustStoreLoc), trustPassword.toCharArray());
	        TrustManager[] myTrustStoreKeyManagers = getTrustManagers(trustStore);
	        tlsCP.setTrustManagers(myTrustStoreKeyManagers);
	        
	        httpConduit.setTlsClientParameters(tlsCP);
	    }

	    private static TrustManager[] getTrustManagers(KeyStore trustStore) 
	        throws NoSuchAlgorithmException, KeyStoreException {
	        String alg = KeyManagerFactory.getDefaultAlgorithm();
	        TrustManagerFactory fac = TrustManagerFactory.getInstance(alg);
	        fac.init(trustStore);
	        return fac.getTrustManagers();
	    }
	    
	    private static KeyManager[] getKeyManagers(KeyStore keyStore, String keyPassword) 
	        throws GeneralSecurityException, IOException {
	        String alg = KeyManagerFactory.getDefaultAlgorithm();
	        char[] keyPass = keyPassword != null
	                     ? keyPassword.toCharArray()
	                     : null;
	        KeyManagerFactory fac = KeyManagerFactory.getInstance(alg);
	        fac.init(keyStore, keyPass);
	        return fac.getKeyManagers();
	    }
}
