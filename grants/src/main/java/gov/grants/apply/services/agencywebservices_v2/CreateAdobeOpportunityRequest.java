
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/services/AgencyWebServices-V2.0}CreateAdobeOpportunityInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createAdobeOpportunityInfo"
})
@XmlRootElement(name = "CreateAdobeOpportunityRequest")
public class CreateAdobeOpportunityRequest {

    @XmlElement(name = "CreateAdobeOpportunityInfo", required = true)
    protected CreateAdobeOpportunityInfo createAdobeOpportunityInfo;

    /**
     * Gets the value of the createAdobeOpportunityInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CreateAdobeOpportunityInfo }
     *     
     */
    public CreateAdobeOpportunityInfo getCreateAdobeOpportunityInfo() {
        return createAdobeOpportunityInfo;
    }

    /**
     * Sets the value of the createAdobeOpportunityInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateAdobeOpportunityInfo }
     *     
     */
    public void setCreateAdobeOpportunityInfo(CreateAdobeOpportunityInfo value) {
        this.createAdobeOpportunityInfo = value;
    }

}
