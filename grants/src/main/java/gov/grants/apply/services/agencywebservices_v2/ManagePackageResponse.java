
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.agencymanagepackage_v1.CreatePackageResult;
import gov.grants.apply.system.agencymanagepackage_v1.DeletePackageResult;
import gov.grants.apply.system.agencymanagepackage_v1.UpdatePackageResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}UserID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityNumber"/&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}CreatePackageResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}UpdatePackageResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}DeletePackageResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userID",
    "fundingOpportunityNumber",
    "success",
    "createPackageResult",
    "updatePackageResult",
    "deletePackageResult"
})
@XmlRootElement(name = "ManagePackageResponse")
public class ManagePackageResponse {

    @XmlElement(name = "UserID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String userID;
    @XmlElement(name = "FundingOpportunityNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityNumber;
    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "CreatePackageResult", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected List<CreatePackageResult> createPackageResult;
    @XmlElement(name = "UpdatePackageResult", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected List<UpdatePackageResult> updatePackageResult;
    @XmlElement(name = "DeletePackageResult", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected List<DeletePackageResult> deletePackageResult;

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the fundingOpportunityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityNumber() {
        return fundingOpportunityNumber;
    }

    /**
     * Sets the value of the fundingOpportunityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityNumber(String value) {
        this.fundingOpportunityNumber = value;
    }

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the createPackageResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createPackageResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatePackageResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatePackageResult }
     * 
     * 
     */
    public List<CreatePackageResult> getCreatePackageResult() {
        if (createPackageResult == null) {
            createPackageResult = new ArrayList<CreatePackageResult>();
        }
        return this.createPackageResult;
    }

    /**
     * Gets the value of the updatePackageResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updatePackageResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdatePackageResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdatePackageResult }
     * 
     * 
     */
    public List<UpdatePackageResult> getUpdatePackageResult() {
        if (updatePackageResult == null) {
            updatePackageResult = new ArrayList<UpdatePackageResult>();
        }
        return this.updatePackageResult;
    }

    /**
     * Gets the value of the deletePackageResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deletePackageResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeletePackageResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeletePackageResult }
     * 
     * 
     */
    public List<DeletePackageResult> getDeletePackageResult() {
        if (deletePackageResult == null) {
            deletePackageResult = new ArrayList<DeletePackageResult>();
        }
        return this.deletePackageResult;
    }

}
