
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsrelateddocument_v1.CreateFolder;
import gov.grants.apply.system.grantsrelateddocument_v1.DeleteFolder;
import gov.grants.apply.system.grantsrelateddocument_v1.UpdateFolder;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}CreateFolder" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}UpdateFolder" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}DeleteFolder" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "createFolder",
    "updateFolder",
    "deleteFolder"
})
@XmlRootElement(name = "ManageRelatedDocumentFoldersRequest")
public class ManageRelatedDocumentFoldersRequest {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String opportunityID;
    @XmlElement(name = "CreateFolder", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<CreateFolder> createFolder;
    @XmlElement(name = "UpdateFolder", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<UpdateFolder> updateFolder;
    @XmlElement(name = "DeleteFolder", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<DeleteFolder> deleteFolder;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the createFolder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createFolder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreateFolder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateFolder }
     * 
     * 
     */
    public List<CreateFolder> getCreateFolder() {
        if (createFolder == null) {
            createFolder = new ArrayList<CreateFolder>();
        }
        return this.createFolder;
    }

    /**
     * Gets the value of the updateFolder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateFolder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateFolder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateFolder }
     * 
     * 
     */
    public List<UpdateFolder> getUpdateFolder() {
        if (updateFolder == null) {
            updateFolder = new ArrayList<UpdateFolder>();
        }
        return this.updateFolder;
    }

    /**
     * Gets the value of the deleteFolder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deleteFolder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeleteFolder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeleteFolder }
     * 
     * 
     */
    public List<DeleteFolder> getDeleteFolder() {
        if (deleteFolder == null) {
            deleteFolder = new ArrayList<DeleteFolder>();
        }
        return this.deleteFolder;
    }

}
