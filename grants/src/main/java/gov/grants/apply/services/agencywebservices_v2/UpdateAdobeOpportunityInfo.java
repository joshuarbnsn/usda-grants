
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsfundingsynopsis_v2.FundingOppModSynopsis20;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}FundingOppModSynopsis_2_0" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fundingOppModSynopsis20"
})
@XmlRootElement(name = "UpdateAdobeOpportunityInfo")
public class UpdateAdobeOpportunityInfo {

    @XmlElement(name = "FundingOppModSynopsis_2_0", namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", required = true)
    protected List<FundingOppModSynopsis20> fundingOppModSynopsis20;

    /**
     * Gets the value of the fundingOppModSynopsis20 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundingOppModSynopsis20 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundingOppModSynopsis20().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundingOppModSynopsis20 }
     * 
     * 
     */
    public List<FundingOppModSynopsis20> getFundingOppModSynopsis20() {
        if (fundingOppModSynopsis20 == null) {
            fundingOppModSynopsis20 = new ArrayList<FundingOppModSynopsis20>();
        }
        return this.fundingOppModSynopsis20;
    }

}
