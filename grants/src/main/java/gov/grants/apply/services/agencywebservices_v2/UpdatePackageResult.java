
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import gov.grants.apply.system.grantscommonelements_v1.ErrorDetails;
import gov.grants.apply.system.grantspackage_v1.PackageFormsDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}PackageID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}InstructionFileLastUpdatedTimestamp" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}NumberOfNotificationsSent" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}PackageFormsDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ErrorDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "packageID",
    "instructionFileLastUpdatedTimestamp",
    "numberOfNotificationsSent",
    "packageFormsDetails",
    "errorDetails"
})
@XmlRootElement(name = "UpdatePackageResult")
public class UpdatePackageResult {

    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "PackageID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String packageID;
    @XmlElement(name = "InstructionFileLastUpdatedTimestamp", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar instructionFileLastUpdatedTimestamp;
    @XmlElement(name = "NumberOfNotificationsSent", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String numberOfNotificationsSent;
    @XmlElement(name = "PackageFormsDetails", namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0")
    protected PackageFormsDetails packageFormsDetails;
    @XmlElement(name = "ErrorDetails", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected List<ErrorDetails> errorDetails;

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the packageID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageID() {
        return packageID;
    }

    /**
     * Sets the value of the packageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageID(String value) {
        this.packageID = value;
    }

    /**
     * Gets the value of the instructionFileLastUpdatedTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInstructionFileLastUpdatedTimestamp() {
        return instructionFileLastUpdatedTimestamp;
    }

    /**
     * Sets the value of the instructionFileLastUpdatedTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInstructionFileLastUpdatedTimestamp(XMLGregorianCalendar value) {
        this.instructionFileLastUpdatedTimestamp = value;
    }

    /**
     * Gets the value of the numberOfNotificationsSent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfNotificationsSent() {
        return numberOfNotificationsSent;
    }

    /**
     * Sets the value of the numberOfNotificationsSent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfNotificationsSent(String value) {
        this.numberOfNotificationsSent = value;
    }

    /**
     * Gets the value of the packageFormsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PackageFormsDetails }
     *     
     */
    public PackageFormsDetails getPackageFormsDetails() {
        return packageFormsDetails;
    }

    /**
     * Sets the value of the packageFormsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageFormsDetails }
     *     
     */
    public void setPackageFormsDetails(PackageFormsDetails value) {
        this.packageFormsDetails = value;
    }

    /**
     * Gets the value of the errorDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetails }
     * 
     * 
     */
    public List<ErrorDetails> getErrorDetails() {
        if (errorDetails == null) {
            errorDetails = new ArrayList<ErrorDetails>();
        }
        return this.errorDetails;
    }

}
