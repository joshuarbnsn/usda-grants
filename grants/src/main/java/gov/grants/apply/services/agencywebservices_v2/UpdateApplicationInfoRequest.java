
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}GrantsGovTrackingNumber"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0}AssignAgencyTrackingNumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0}SaveAgencyNotes" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "grantsGovTrackingNumber",
    "assignAgencyTrackingNumber",
    "saveAgencyNotes"
})
@XmlRootElement(name = "UpdateApplicationInfoRequest")
public class UpdateApplicationInfoRequest {

    @XmlElement(name = "GrantsGovTrackingNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String grantsGovTrackingNumber;
    @XmlElement(name = "AssignAgencyTrackingNumber", namespace = "http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0")
    protected String assignAgencyTrackingNumber;
    @XmlElement(name = "SaveAgencyNotes", namespace = "http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0")
    protected String saveAgencyNotes;

    /**
     * Gets the value of the grantsGovTrackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrantsGovTrackingNumber() {
        return grantsGovTrackingNumber;
    }

    /**
     * Sets the value of the grantsGovTrackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrantsGovTrackingNumber(String value) {
        this.grantsGovTrackingNumber = value;
    }

    /**
     * Gets the value of the assignAgencyTrackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignAgencyTrackingNumber() {
        return assignAgencyTrackingNumber;
    }

    /**
     * Sets the value of the assignAgencyTrackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignAgencyTrackingNumber(String value) {
        this.assignAgencyTrackingNumber = value;
    }

    /**
     * Gets the value of the saveAgencyNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaveAgencyNotes() {
        return saveAgencyNotes;
    }

    /**
     * Sets the value of the saveAgencyNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaveAgencyNotes(String value) {
        this.saveAgencyNotes = value;
    }

}
