
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsrelateddocument_v1.AddFile;
import gov.grants.apply.system.grantsrelateddocument_v1.RemoveFile;
import gov.grants.apply.system.grantsrelateddocument_v1.ReplaceFile;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}AddFile" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}ReplaceFile" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}RemoveFile" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "addFile",
    "replaceFile",
    "removeFile"
})
@XmlRootElement(name = "ManageRelatedDocumentFilesRequest")
public class ManageRelatedDocumentFilesRequest {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String opportunityID;
    @XmlElement(name = "AddFile", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<AddFile> addFile;
    @XmlElement(name = "ReplaceFile", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<ReplaceFile> replaceFile;
    @XmlElement(name = "RemoveFile", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<RemoveFile> removeFile;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the addFile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addFile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddFile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddFile }
     * 
     * 
     */
    public List<AddFile> getAddFile() {
        if (addFile == null) {
            addFile = new ArrayList<AddFile>();
        }
        return this.addFile;
    }

    /**
     * Gets the value of the replaceFile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the replaceFile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReplaceFile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReplaceFile }
     * 
     * 
     */
    public List<ReplaceFile> getReplaceFile() {
        if (replaceFile == null) {
            replaceFile = new ArrayList<ReplaceFile>();
        }
        return this.replaceFile;
    }

    /**
     * Gets the value of the removeFile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the removeFile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemoveFile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemoveFile }
     * 
     * 
     */
    public List<RemoveFile> getRemoveFile() {
        if (removeFile == null) {
            removeFile = new ArrayList<RemoveFile>();
        }
        return this.removeFile;
    }

}
