
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsforecastsynopsis_v1.DeleteForecast;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}DeleteForecast"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "deleteForecast"
})
@XmlRootElement(name = "DeleteForecastInfo")
public class DeleteForecastInfo {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String opportunityID;
    @XmlElement(name = "DeleteForecast", namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", required = true)
    protected DeleteForecast deleteForecast;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the deleteForecast property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteForecast }
     *     
     */
    public DeleteForecast getDeleteForecast() {
        return deleteForecast;
    }

    /**
     * Sets the value of the deleteForecast property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteForecast }
     *     
     */
    public void setDeleteForecast(DeleteForecast value) {
        this.deleteForecast = value;
    }

}
