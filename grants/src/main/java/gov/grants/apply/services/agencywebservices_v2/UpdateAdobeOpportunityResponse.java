
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/services/AgencyWebServices-V2.0}UpdateAdobeOpportunityResult" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "updateAdobeOpportunityResult"
})
@XmlRootElement(name = "UpdateAdobeOpportunityResponse")
public class UpdateAdobeOpportunityResponse {

    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "UpdateAdobeOpportunityResult", required = true)
    protected List<UpdateAdobeOpportunityResult> updateAdobeOpportunityResult;

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the updateAdobeOpportunityResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateAdobeOpportunityResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateAdobeOpportunityResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateAdobeOpportunityResult }
     * 
     * 
     */
    public List<UpdateAdobeOpportunityResult> getUpdateAdobeOpportunityResult() {
        if (updateAdobeOpportunityResult == null) {
            updateAdobeOpportunityResult = new ArrayList<UpdateAdobeOpportunityResult>();
        }
        return this.updateAdobeOpportunityResult;
    }

}
