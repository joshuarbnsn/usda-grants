
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsopportunity_v1.UpdateOpportunity;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}UpdateOpportunity"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateOpportunity"
})
@XmlRootElement(name = "UpdateOpportunityInfo")
public class UpdateOpportunityInfo {

    @XmlElement(name = "UpdateOpportunity", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", required = true)
    protected UpdateOpportunity updateOpportunity;

    /**
     * Gets the value of the updateOpportunity property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateOpportunity }
     *     
     */
    public UpdateOpportunity getUpdateOpportunity() {
        return updateOpportunity;
    }

    /**
     * Sets the value of the updateOpportunity property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateOpportunity }
     *     
     */
    public void setUpdateOpportunity(UpdateOpportunity value) {
        this.updateOpportunity = value;
    }

}
