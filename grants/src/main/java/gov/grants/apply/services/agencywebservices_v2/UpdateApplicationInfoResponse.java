
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.agencyupdateapplicationinfo_v1.AssignAgencyTrackingNumberResult;
import gov.grants.apply.system.agencyupdateapplicationinfo_v1.SaveAgencyNotesResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}GrantsGovTrackingNumber"/&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0}AssignAgencyTrackingNumberResult" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0}SaveAgencyNotesResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "grantsGovTrackingNumber",
    "success",
    "assignAgencyTrackingNumberResult",
    "saveAgencyNotesResult"
})
@XmlRootElement(name = "UpdateApplicationInfoResponse")
public class UpdateApplicationInfoResponse {

    @XmlElement(name = "GrantsGovTrackingNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String grantsGovTrackingNumber;
    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "AssignAgencyTrackingNumberResult", namespace = "http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0")
    protected AssignAgencyTrackingNumberResult assignAgencyTrackingNumberResult;
    @XmlElement(name = "SaveAgencyNotesResult", namespace = "http://apply.grants.gov/system/AgencyUpdateApplicationInfo-V1.0")
    protected SaveAgencyNotesResult saveAgencyNotesResult;

    /**
     * Gets the value of the grantsGovTrackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrantsGovTrackingNumber() {
        return grantsGovTrackingNumber;
    }

    /**
     * Sets the value of the grantsGovTrackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrantsGovTrackingNumber(String value) {
        this.grantsGovTrackingNumber = value;
    }

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the assignAgencyTrackingNumberResult property.
     * 
     * @return
     *     possible object is
     *     {@link AssignAgencyTrackingNumberResult }
     *     
     */
    public AssignAgencyTrackingNumberResult getAssignAgencyTrackingNumberResult() {
        return assignAgencyTrackingNumberResult;
    }

    /**
     * Sets the value of the assignAgencyTrackingNumberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssignAgencyTrackingNumberResult }
     *     
     */
    public void setAssignAgencyTrackingNumberResult(AssignAgencyTrackingNumberResult value) {
        this.assignAgencyTrackingNumberResult = value;
    }

    /**
     * Gets the value of the saveAgencyNotesResult property.
     * 
     * @return
     *     possible object is
     *     {@link SaveAgencyNotesResult }
     *     
     */
    public SaveAgencyNotesResult getSaveAgencyNotesResult() {
        return saveAgencyNotesResult;
    }

    /**
     * Sets the value of the saveAgencyNotesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveAgencyNotesResult }
     *     
     */
    public void setSaveAgencyNotesResult(SaveAgencyNotesResult value) {
        this.saveAgencyNotesResult = value;
    }

}
