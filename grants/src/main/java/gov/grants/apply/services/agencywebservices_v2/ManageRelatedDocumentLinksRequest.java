
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsrelateddocument_v1.CreateLink;
import gov.grants.apply.system.grantsrelateddocument_v1.DeleteLink;
import gov.grants.apply.system.grantsrelateddocument_v1.UpdateLink;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}CreateLink" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}UpdateLink" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}DeleteLink" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "createLink",
    "updateLink",
    "deleteLink"
})
@XmlRootElement(name = "ManageRelatedDocumentLinksRequest")
public class ManageRelatedDocumentLinksRequest {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String opportunityID;
    @XmlElement(name = "CreateLink", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<CreateLink> createLink;
    @XmlElement(name = "UpdateLink", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<UpdateLink> updateLink;
    @XmlElement(name = "DeleteLink", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<DeleteLink> deleteLink;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the createLink property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createLink property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreateLink().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateLink }
     * 
     * 
     */
    public List<CreateLink> getCreateLink() {
        if (createLink == null) {
            createLink = new ArrayList<CreateLink>();
        }
        return this.createLink;
    }

    /**
     * Gets the value of the updateLink property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateLink property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateLink().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateLink }
     * 
     * 
     */
    public List<UpdateLink> getUpdateLink() {
        if (updateLink == null) {
            updateLink = new ArrayList<UpdateLink>();
        }
        return this.updateLink;
    }

    /**
     * Gets the value of the deleteLink property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deleteLink property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeleteLink().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeleteLink }
     * 
     * 
     */
    public List<DeleteLink> getDeleteLink() {
        if (deleteLink == null) {
            deleteLink = new ArrayList<DeleteLink>();
        }
        return this.deleteLink;
    }

}
