
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommonelements_v1.ErrorDetails;
import gov.grants.apply.system.grantscommontypes_v1.OperationStatusType;
import gov.grants.apply.system.grantsrelateddocument_v1.AddFileResult;
import gov.grants.apply.system.grantsrelateddocument_v1.RemoveFileResult;
import gov.grants.apply.system.grantsrelateddocument_v1.ReplaceFileResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompletionStatus"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ErrorDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}AddFileResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}ReplaceFileResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}RemoveFileResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "completionStatus",
    "errorDetails",
    "addFileResult",
    "replaceFileResult",
    "removeFileResult"
})
@XmlRootElement(name = "ManageRelatedDocumentFilesResponse")
public class ManageRelatedDocumentFilesResponse {

    @XmlElement(name = "CompletionStatus", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    @XmlSchemaType(name = "string")
    protected OperationStatusType completionStatus;
    @XmlElement(name = "ErrorDetails", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected ErrorDetails errorDetails;
    @XmlElement(name = "AddFileResult", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<AddFileResult> addFileResult;
    @XmlElement(name = "ReplaceFileResult", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<ReplaceFileResult> replaceFileResult;
    @XmlElement(name = "RemoveFileResult", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<RemoveFileResult> removeFileResult;

    /**
     * Gets the value of the completionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link OperationStatusType }
     *     
     */
    public OperationStatusType getCompletionStatus() {
        return completionStatus;
    }

    /**
     * Sets the value of the completionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationStatusType }
     *     
     */
    public void setCompletionStatus(OperationStatusType value) {
        this.completionStatus = value;
    }

    /**
     * Gets the value of the errorDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorDetails }
     *     
     */
    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    /**
     * Sets the value of the errorDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorDetails }
     *     
     */
    public void setErrorDetails(ErrorDetails value) {
        this.errorDetails = value;
    }

    /**
     * Gets the value of the addFileResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addFileResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddFileResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddFileResult }
     * 
     * 
     */
    public List<AddFileResult> getAddFileResult() {
        if (addFileResult == null) {
            addFileResult = new ArrayList<AddFileResult>();
        }
        return this.addFileResult;
    }

    /**
     * Gets the value of the replaceFileResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the replaceFileResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReplaceFileResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReplaceFileResult }
     * 
     * 
     */
    public List<ReplaceFileResult> getReplaceFileResult() {
        if (replaceFileResult == null) {
            replaceFileResult = new ArrayList<ReplaceFileResult>();
        }
        return this.replaceFileResult;
    }

    /**
     * Gets the value of the removeFileResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the removeFileResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemoveFileResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemoveFileResult }
     * 
     * 
     */
    public List<RemoveFileResult> getRemoveFileResult() {
        if (removeFileResult == null) {
            removeFileResult = new ArrayList<RemoveFileResult>();
        }
        return this.removeFileResult;
    }

}
