
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.services.agencywebservices_v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.services.agencywebservices_v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetApplicationListResponse }
     * 
     */
    public GetApplicationListResponse createGetApplicationListResponse() {
        return new GetApplicationListResponse();
    }

    /**
     * Create an instance of {@link GetApplicationListRequest }
     * 
     */
    public GetApplicationListRequest createGetApplicationListRequest() {
        return new GetApplicationListRequest();
    }

    /**
     * Create an instance of {@link GetApplicationListResponse.ApplicationInfo }
     * 
     */
    public GetApplicationListResponse.ApplicationInfo createGetApplicationListResponseApplicationInfo() {
        return new GetApplicationListResponse.ApplicationInfo();
    }

    /**
     * Create an instance of {@link ConfirmApplicationDeliveryRequest }
     * 
     */
    public ConfirmApplicationDeliveryRequest createConfirmApplicationDeliveryRequest() {
        return new ConfirmApplicationDeliveryRequest();
    }

    /**
     * Create an instance of {@link ConfirmApplicationDeliveryResponse }
     * 
     */
    public ConfirmApplicationDeliveryResponse createConfirmApplicationDeliveryResponse() {
        return new ConfirmApplicationDeliveryResponse();
    }

    /**
     * Create an instance of {@link GetApplicationRequest }
     * 
     */
    public GetApplicationRequest createGetApplicationRequest() {
        return new GetApplicationRequest();
    }

    /**
     * Create an instance of {@link GetApplicationResponse }
     * 
     */
    public GetApplicationResponse createGetApplicationResponse() {
        return new GetApplicationResponse();
    }

    /**
     * Create an instance of {@link GetApplicationZipRequest }
     * 
     */
    public GetApplicationZipRequest createGetApplicationZipRequest() {
        return new GetApplicationZipRequest();
    }

    /**
     * Create an instance of {@link GetApplicationZipResponse }
     * 
     */
    public GetApplicationZipResponse createGetApplicationZipResponse() {
        return new GetApplicationZipResponse();
    }

    /**
     * Create an instance of {@link AssignAgencyTrackingNumberRequest }
     * 
     */
    public AssignAgencyTrackingNumberRequest createAssignAgencyTrackingNumberRequest() {
        return new AssignAgencyTrackingNumberRequest();
    }

    /**
     * Create an instance of {@link AssignAgencyTrackingNumberResponse }
     * 
     */
    public AssignAgencyTrackingNumberResponse createAssignAgencyTrackingNumberResponse() {
        return new AssignAgencyTrackingNumberResponse();
    }

    /**
     * Create an instance of {@link DeleteAdobeOpportunityRequest }
     * 
     */
    public DeleteAdobeOpportunityRequest createDeleteAdobeOpportunityRequest() {
        return new DeleteAdobeOpportunityRequest();
    }

    /**
     * Create an instance of {@link DeleteAdobeOpportunityResponse }
     * 
     */
    public DeleteAdobeOpportunityResponse createDeleteAdobeOpportunityResponse() {
        return new DeleteAdobeOpportunityResponse();
    }

    /**
     * Create an instance of {@link DeleteAdobeSynopsisRequest }
     * 
     */
    public DeleteAdobeSynopsisRequest createDeleteAdobeSynopsisRequest() {
        return new DeleteAdobeSynopsisRequest();
    }

    /**
     * Create an instance of {@link DeleteAdobeSynopsisResponse }
     * 
     */
    public DeleteAdobeSynopsisResponse createDeleteAdobeSynopsisResponse() {
        return new DeleteAdobeSynopsisResponse();
    }

    /**
     * Create an instance of {@link CreateAdobeOpportunityInfo }
     * 
     */
    public CreateAdobeOpportunityInfo createCreateAdobeOpportunityInfo() {
        return new CreateAdobeOpportunityInfo();
    }

    /**
     * Create an instance of {@link CreateAdobeOpportunityRequest }
     * 
     */
    public CreateAdobeOpportunityRequest createCreateAdobeOpportunityRequest() {
        return new CreateAdobeOpportunityRequest();
    }

    /**
     * Create an instance of {@link CreateAdobeOpportunityResult }
     * 
     */
    public CreateAdobeOpportunityResult createCreateAdobeOpportunityResult() {
        return new CreateAdobeOpportunityResult();
    }

    /**
     * Create an instance of {@link CreateAdobeOpportunityResponse }
     * 
     */
    public CreateAdobeOpportunityResponse createCreateAdobeOpportunityResponse() {
        return new CreateAdobeOpportunityResponse();
    }

    /**
     * Create an instance of {@link UpdateAdobeOpportunityInfo }
     * 
     */
    public UpdateAdobeOpportunityInfo createUpdateAdobeOpportunityInfo() {
        return new UpdateAdobeOpportunityInfo();
    }

    /**
     * Create an instance of {@link UpdateAdobeOpportunityRequest }
     * 
     */
    public UpdateAdobeOpportunityRequest createUpdateAdobeOpportunityRequest() {
        return new UpdateAdobeOpportunityRequest();
    }

    /**
     * Create an instance of {@link UpdateAdobeOpportunityResult }
     * 
     */
    public UpdateAdobeOpportunityResult createUpdateAdobeOpportunityResult() {
        return new UpdateAdobeOpportunityResult();
    }

    /**
     * Create an instance of {@link UpdateAdobeOpportunityResponse }
     * 
     */
    public UpdateAdobeOpportunityResponse createUpdateAdobeOpportunityResponse() {
        return new UpdateAdobeOpportunityResponse();
    }

    /**
     * Create an instance of {@link UpdateApplicationInfoRequest }
     * 
     */
    public UpdateApplicationInfoRequest createUpdateApplicationInfoRequest() {
        return new UpdateApplicationInfoRequest();
    }

    /**
     * Create an instance of {@link UpdateApplicationInfoResponse }
     * 
     */
    public UpdateApplicationInfoResponse createUpdateApplicationInfoResponse() {
        return new UpdateApplicationInfoResponse();
    }

    /**
     * Create an instance of {@link ManagePackageRequest }
     * 
     */
    public ManagePackageRequest createManagePackageRequest() {
        return new ManagePackageRequest();
    }

    /**
     * Create an instance of {@link ManagePackageResponse }
     * 
     */
    public ManagePackageResponse createManagePackageResponse() {
        return new ManagePackageResponse();
    }

    /**
     * Create an instance of {@link UpdateOpportunityInfo }
     * 
     */
    public UpdateOpportunityInfo createUpdateOpportunityInfo() {
        return new UpdateOpportunityInfo();
    }

    /**
     * Create an instance of {@link UpdateOpportunityElementsRequest }
     * 
     */
    public UpdateOpportunityElementsRequest createUpdateOpportunityElementsRequest() {
        return new UpdateOpportunityElementsRequest();
    }

    /**
     * Create an instance of {@link UpdateOpportunityElementsResult }
     * 
     */
    public UpdateOpportunityElementsResult createUpdateOpportunityElementsResult() {
        return new UpdateOpportunityElementsResult();
    }

    /**
     * Create an instance of {@link UpdateOpportunityElementsResponse }
     * 
     */
    public UpdateOpportunityElementsResponse createUpdateOpportunityElementsResponse() {
        return new UpdateOpportunityElementsResponse();
    }

    /**
     * Create an instance of {@link ManageRelatedDocumentFoldersRequest }
     * 
     */
    public ManageRelatedDocumentFoldersRequest createManageRelatedDocumentFoldersRequest() {
        return new ManageRelatedDocumentFoldersRequest();
    }

    /**
     * Create an instance of {@link ManageRelatedDocumentFoldersResponse }
     * 
     */
    public ManageRelatedDocumentFoldersResponse createManageRelatedDocumentFoldersResponse() {
        return new ManageRelatedDocumentFoldersResponse();
    }

    /**
     * Create an instance of {@link ManageRelatedDocumentFilesRequest }
     * 
     */
    public ManageRelatedDocumentFilesRequest createManageRelatedDocumentFilesRequest() {
        return new ManageRelatedDocumentFilesRequest();
    }

    /**
     * Create an instance of {@link ManageRelatedDocumentFilesResponse }
     * 
     */
    public ManageRelatedDocumentFilesResponse createManageRelatedDocumentFilesResponse() {
        return new ManageRelatedDocumentFilesResponse();
    }

    /**
     * Create an instance of {@link ManageRelatedDocumentLinksRequest }
     * 
     */
    public ManageRelatedDocumentLinksRequest createManageRelatedDocumentLinksRequest() {
        return new ManageRelatedDocumentLinksRequest();
    }

    /**
     * Create an instance of {@link ManageRelatedDocumentLinksResponse }
     * 
     */
    public ManageRelatedDocumentLinksResponse createManageRelatedDocumentLinksResponse() {
        return new ManageRelatedDocumentLinksResponse();
    }

    /**
     * Create an instance of {@link CreateForecastInfo }
     * 
     */
    public CreateForecastInfo createCreateForecastInfo() {
        return new CreateForecastInfo();
    }

    /**
     * Create an instance of {@link CreateForecastRequest }
     * 
     */
    public CreateForecastRequest createCreateForecastRequest() {
        return new CreateForecastRequest();
    }

    /**
     * Create an instance of {@link CreateForecastResult }
     * 
     */
    public CreateForecastResult createCreateForecastResult() {
        return new CreateForecastResult();
    }

    /**
     * Create an instance of {@link CreateForecastResponse }
     * 
     */
    public CreateForecastResponse createCreateForecastResponse() {
        return new CreateForecastResponse();
    }

    /**
     * Create an instance of {@link UpdateForecastInfo }
     * 
     */
    public UpdateForecastInfo createUpdateForecastInfo() {
        return new UpdateForecastInfo();
    }

    /**
     * Create an instance of {@link UpdateForecastRequest }
     * 
     */
    public UpdateForecastRequest createUpdateForecastRequest() {
        return new UpdateForecastRequest();
    }

    /**
     * Create an instance of {@link UpdateForecastResult }
     * 
     */
    public UpdateForecastResult createUpdateForecastResult() {
        return new UpdateForecastResult();
    }

    /**
     * Create an instance of {@link UpdateForecastResponse }
     * 
     */
    public UpdateForecastResponse createUpdateForecastResponse() {
        return new UpdateForecastResponse();
    }

    /**
     * Create an instance of {@link DeleteForecastInfo }
     * 
     */
    public DeleteForecastInfo createDeleteForecastInfo() {
        return new DeleteForecastInfo();
    }

    /**
     * Create an instance of {@link DeleteForecastRequest }
     * 
     */
    public DeleteForecastRequest createDeleteForecastRequest() {
        return new DeleteForecastRequest();
    }

    /**
     * Create an instance of {@link DeleteForecastResult }
     * 
     */
    public DeleteForecastResult createDeleteForecastResult() {
        return new DeleteForecastResult();
    }

    /**
     * Create an instance of {@link DeleteForecastResponse }
     * 
     */
    public DeleteForecastResponse createDeleteForecastResponse() {
        return new DeleteForecastResponse();
    }

    /**
     * Create an instance of {@link CreatePackageInfo }
     * 
     */
    public CreatePackageInfo createCreatePackageInfo() {
        return new CreatePackageInfo();
    }

    /**
     * Create an instance of {@link CreatePackageRequest }
     * 
     */
    public CreatePackageRequest createCreatePackageRequest() {
        return new CreatePackageRequest();
    }

    /**
     * Create an instance of {@link CreatePackageResult }
     * 
     */
    public CreatePackageResult createCreatePackageResult() {
        return new CreatePackageResult();
    }

    /**
     * Create an instance of {@link CreatePackageResponse }
     * 
     */
    public CreatePackageResponse createCreatePackageResponse() {
        return new CreatePackageResponse();
    }

    /**
     * Create an instance of {@link UpdatePackageInfo }
     * 
     */
    public UpdatePackageInfo createUpdatePackageInfo() {
        return new UpdatePackageInfo();
    }

    /**
     * Create an instance of {@link UpdatePackageRequest }
     * 
     */
    public UpdatePackageRequest createUpdatePackageRequest() {
        return new UpdatePackageRequest();
    }

    /**
     * Create an instance of {@link UpdatePackageResult }
     * 
     */
    public UpdatePackageResult createUpdatePackageResult() {
        return new UpdatePackageResult();
    }

    /**
     * Create an instance of {@link UpdatePackageResponse }
     * 
     */
    public UpdatePackageResponse createUpdatePackageResponse() {
        return new UpdatePackageResponse();
    }

    /**
     * Create an instance of {@link DeletePackageInfo }
     * 
     */
    public DeletePackageInfo createDeletePackageInfo() {
        return new DeletePackageInfo();
    }

    /**
     * Create an instance of {@link DeletePackageRequest }
     * 
     */
    public DeletePackageRequest createDeletePackageRequest() {
        return new DeletePackageRequest();
    }

    /**
     * Create an instance of {@link DeletePackageResult }
     * 
     */
    public DeletePackageResult createDeletePackageResult() {
        return new DeletePackageResult();
    }

    /**
     * Create an instance of {@link DeletePackageResponse }
     * 
     */
    public DeletePackageResponse createDeletePackageResponse() {
        return new DeletePackageResponse();
    }

    /**
     * Create an instance of {@link GetOpportunityListRequest }
     * 
     */
    public GetOpportunityListRequest createGetOpportunityListRequest() {
        return new GetOpportunityListRequest();
    }

    /**
     * Create an instance of {@link GetOpportunityListResponse }
     * 
     */
    public GetOpportunityListResponse createGetOpportunityListResponse() {
        return new GetOpportunityListResponse();
    }

    /**
     * Create an instance of {@link GetForecastAndSynopsisDetailsRequest }
     * 
     */
    public GetForecastAndSynopsisDetailsRequest createGetForecastAndSynopsisDetailsRequest() {
        return new GetForecastAndSynopsisDetailsRequest();
    }

    /**
     * Create an instance of {@link GetForecastAndSynopsisDetailsResponse }
     * 
     */
    public GetForecastAndSynopsisDetailsResponse createGetForecastAndSynopsisDetailsResponse() {
        return new GetForecastAndSynopsisDetailsResponse();
    }

    /**
     * Create an instance of {@link GetRelatedDocumentDetailsRequest }
     * 
     */
    public GetRelatedDocumentDetailsRequest createGetRelatedDocumentDetailsRequest() {
        return new GetRelatedDocumentDetailsRequest();
    }

    /**
     * Create an instance of {@link GetRelatedDocumentDetailsResponse }
     * 
     */
    public GetRelatedDocumentDetailsResponse createGetRelatedDocumentDetailsResponse() {
        return new GetRelatedDocumentDetailsResponse();
    }

    /**
     * Create an instance of {@link GetPackageDetailsRequest }
     * 
     */
    public GetPackageDetailsRequest createGetPackageDetailsRequest() {
        return new GetPackageDetailsRequest();
    }

    /**
     * Create an instance of {@link GetPackageDetailsResponse }
     * 
     */
    public GetPackageDetailsResponse createGetPackageDetailsResponse() {
        return new GetPackageDetailsResponse();
    }

}
