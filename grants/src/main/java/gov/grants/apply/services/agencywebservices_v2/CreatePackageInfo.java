
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsopportunity_v1.CreateOpportunity;
import gov.grants.apply.system.grantspackage_v1.CreatePackage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID" minOccurs="0"/&gt;
 *           &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}CreateOpportunity" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}CreatePackage" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "createOpportunity",
    "createPackage"
})
@XmlRootElement(name = "CreatePackageInfo")
public class CreatePackageInfo {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String opportunityID;
    @XmlElement(name = "CreateOpportunity", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0")
    protected CreateOpportunity createOpportunity;
    @XmlElement(name = "CreatePackage", namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", required = true)
    protected List<CreatePackage> createPackage;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the createOpportunity property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOpportunity }
     *     
     */
    public CreateOpportunity getCreateOpportunity() {
        return createOpportunity;
    }

    /**
     * Sets the value of the createOpportunity property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOpportunity }
     *     
     */
    public void setCreateOpportunity(CreateOpportunity value) {
        this.createOpportunity = value;
    }

    /**
     * Gets the value of the createPackage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createPackage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatePackage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatePackage }
     * 
     * 
     */
    public List<CreatePackage> getCreatePackage() {
        if (createPackage == null) {
            createPackage = new ArrayList<CreatePackage>();
        }
        return this.createPackage;
    }

}
