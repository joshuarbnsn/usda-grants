
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.agencymanagepackage_v1.CreateOpportunityInfo;
import gov.grants.apply.system.agencymanagepackage_v1.CreatePackageInfo;
import gov.grants.apply.system.agencymanagepackage_v1.DeletePackageInfo;
import gov.grants.apply.system.agencymanagepackage_v1.UpdatePackageInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}UserID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityNumber"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}CreateOpportunityInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}CreatePackageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}UpdatePackageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}DeletePackageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userID",
    "fundingOpportunityNumber",
    "createOpportunityInfo",
    "createPackageInfo",
    "updatePackageInfo",
    "deletePackageInfo"
})
@XmlRootElement(name = "ManagePackageRequest")
public class ManagePackageRequest {

    @XmlElement(name = "UserID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String userID;
    @XmlElement(name = "FundingOpportunityNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityNumber;
    @XmlElement(name = "CreateOpportunityInfo", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected CreateOpportunityInfo createOpportunityInfo;
    @XmlElement(name = "CreatePackageInfo", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected List<CreatePackageInfo> createPackageInfo;
    @XmlElement(name = "UpdatePackageInfo", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected List<UpdatePackageInfo> updatePackageInfo;
    @XmlElement(name = "DeletePackageInfo", namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0")
    protected List<DeletePackageInfo> deletePackageInfo;

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the fundingOpportunityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityNumber() {
        return fundingOpportunityNumber;
    }

    /**
     * Sets the value of the fundingOpportunityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityNumber(String value) {
        this.fundingOpportunityNumber = value;
    }

    /**
     * Gets the value of the createOpportunityInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOpportunityInfo }
     *     
     */
    public CreateOpportunityInfo getCreateOpportunityInfo() {
        return createOpportunityInfo;
    }

    /**
     * Sets the value of the createOpportunityInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOpportunityInfo }
     *     
     */
    public void setCreateOpportunityInfo(CreateOpportunityInfo value) {
        this.createOpportunityInfo = value;
    }

    /**
     * Gets the value of the createPackageInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createPackageInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatePackageInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatePackageInfo }
     * 
     * 
     */
    public List<CreatePackageInfo> getCreatePackageInfo() {
        if (createPackageInfo == null) {
            createPackageInfo = new ArrayList<CreatePackageInfo>();
        }
        return this.createPackageInfo;
    }

    /**
     * Gets the value of the updatePackageInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updatePackageInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdatePackageInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdatePackageInfo }
     * 
     * 
     */
    public List<UpdatePackageInfo> getUpdatePackageInfo() {
        if (updatePackageInfo == null) {
            updatePackageInfo = new ArrayList<UpdatePackageInfo>();
        }
        return this.updatePackageInfo;
    }

    /**
     * Gets the value of the deletePackageInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deletePackageInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeletePackageInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeletePackageInfo }
     * 
     * 
     */
    public List<DeletePackageInfo> getDeletePackageInfo() {
        if (deletePackageInfo == null) {
            deletePackageInfo = new ArrayList<DeletePackageInfo>();
        }
        return this.deletePackageInfo;
    }

}
