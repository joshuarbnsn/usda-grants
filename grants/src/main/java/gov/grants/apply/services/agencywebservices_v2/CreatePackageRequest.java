
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/services/AgencyWebServices-V2.0}CreatePackageInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createPackageInfo"
})
@XmlRootElement(name = "CreatePackageRequest")
public class CreatePackageRequest {

    @XmlElement(name = "CreatePackageInfo", required = true)
    protected CreatePackageInfo createPackageInfo;

    /**
     * Gets the value of the createPackageInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CreatePackageInfo }
     *     
     */
    public CreatePackageInfo getCreatePackageInfo() {
        return createPackageInfo;
    }

    /**
     * Sets the value of the createPackageInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatePackageInfo }
     *     
     */
    public void setCreatePackageInfo(CreatePackageInfo value) {
        this.createPackageInfo = value;
    }

}
