
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsfundingsynopsis_v2.FundingOppSynopsis20;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}FundingOppSynopsis_2_0" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fundingOppSynopsis20"
})
@XmlRootElement(name = "CreateAdobeOpportunityInfo")
public class CreateAdobeOpportunityInfo {

    @XmlElement(name = "FundingOppSynopsis_2_0", namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", required = true)
    protected List<FundingOppSynopsis20> fundingOppSynopsis20;

    /**
     * Gets the value of the fundingOppSynopsis20 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundingOppSynopsis20 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundingOppSynopsis20().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundingOppSynopsis20 }
     * 
     * 
     */
    public List<FundingOppSynopsis20> getFundingOppSynopsis20() {
        if (fundingOppSynopsis20 == null) {
            fundingOppSynopsis20 = new ArrayList<FundingOppSynopsis20>();
        }
        return this.fundingOppSynopsis20;
    }

}
