
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommonelements_v1.ErrorDetails;
import gov.grants.apply.system.grantsopportunity_v1.OpportunityElementsDetails;
import gov.grants.apply.system.grantspackage_v1.PackageDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}OpportunityElementsDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}PackageDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ErrorDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "opportunityElementsDetails",
    "packageDetails",
    "errorDetails"
})
@XmlRootElement(name = "GetPackageDetailsResponse")
public class GetPackageDetailsResponse {

    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "OpportunityElementsDetails", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0")
    protected OpportunityElementsDetails opportunityElementsDetails;
    @XmlElement(name = "PackageDetails", namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0")
    protected List<PackageDetails> packageDetails;
    @XmlElement(name = "ErrorDetails", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected ErrorDetails errorDetails;

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the opportunityElementsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityElementsDetails }
     *     
     */
    public OpportunityElementsDetails getOpportunityElementsDetails() {
        return opportunityElementsDetails;
    }

    /**
     * Sets the value of the opportunityElementsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityElementsDetails }
     *     
     */
    public void setOpportunityElementsDetails(OpportunityElementsDetails value) {
        this.opportunityElementsDetails = value;
    }

    /**
     * Gets the value of the packageDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packageDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackageDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackageDetails }
     * 
     * 
     */
    public List<PackageDetails> getPackageDetails() {
        if (packageDetails == null) {
            packageDetails = new ArrayList<PackageDetails>();
        }
        return this.packageDetails;
    }

    /**
     * Gets the value of the errorDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorDetails }
     *     
     */
    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    /**
     * Sets the value of the errorDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorDetails }
     *     
     */
    public void setErrorDetails(ErrorDetails value) {
        this.errorDetails = value;
    }

}
