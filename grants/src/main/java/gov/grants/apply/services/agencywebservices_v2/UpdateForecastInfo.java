
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;
import gov.grants.apply.system.grantsforecastsynopsis_v1.UpdateForecast;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}NullifyMissingOptionalElements" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}UpdateForecast"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "nullifyMissingOptionalElements",
    "updateForecast"
})
@XmlRootElement(name = "UpdateForecastInfo")
public class UpdateForecastInfo {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String opportunityID;
    @XmlElement(name = "NullifyMissingOptionalElements", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", defaultValue = "N")
    @XmlSchemaType(name = "string")
    protected YesNoType nullifyMissingOptionalElements;
    @XmlElement(name = "UpdateForecast", namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", required = true)
    protected UpdateForecast updateForecast;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the nullifyMissingOptionalElements property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getNullifyMissingOptionalElements() {
        return nullifyMissingOptionalElements;
    }

    /**
     * Sets the value of the nullifyMissingOptionalElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setNullifyMissingOptionalElements(YesNoType value) {
        this.nullifyMissingOptionalElements = value;
    }

    /**
     * Gets the value of the updateForecast property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateForecast }
     *     
     */
    public UpdateForecast getUpdateForecast() {
        return updateForecast;
    }

    /**
     * Sets the value of the updateForecast property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateForecast }
     *     
     */
    public void setUpdateForecast(UpdateForecast value) {
        this.updateForecast = value;
    }

}
