
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommonelements_v1.LastUpdatedTimestampRangeFilter;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;
import gov.grants.apply.system.grantsopportunity_v1.ElementFilter;
import gov.grants.apply.system.grantsopportunity_v1.MultiDateRangeFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}ElementFilter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}LastUpdatedTimestampRangeFilter" minOccurs="0"/&gt;
 *           &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}MultiDateRangeFilter" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}IncludeDeletedOpportunities" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}IncludeSubAgencies" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "elementFilter",
    "lastUpdatedTimestampRangeFilter",
    "multiDateRangeFilter",
    "includeDeletedOpportunities",
    "includeSubAgencies"
})
@XmlRootElement(name = "GetOpportunityListRequest")
public class GetOpportunityListRequest {

    @XmlElement(name = "ElementFilter", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0")
    protected List<ElementFilter> elementFilter;
    @XmlElement(name = "LastUpdatedTimestampRangeFilter", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected LastUpdatedTimestampRangeFilter lastUpdatedTimestampRangeFilter;
    @XmlElement(name = "MultiDateRangeFilter", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0")
    protected MultiDateRangeFilter multiDateRangeFilter;
    @XmlElement(name = "IncludeDeletedOpportunities", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", defaultValue = "N")
    @XmlSchemaType(name = "string")
    protected YesNoType includeDeletedOpportunities;
    @XmlElement(name = "IncludeSubAgencies", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", defaultValue = "Y")
    @XmlSchemaType(name = "string")
    protected YesNoType includeSubAgencies;

    /**
     * Gets the value of the elementFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elementFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementFilter }
     * 
     * 
     */
    public List<ElementFilter> getElementFilter() {
        if (elementFilter == null) {
            elementFilter = new ArrayList<ElementFilter>();
        }
        return this.elementFilter;
    }

    /**
     * Gets the value of the lastUpdatedTimestampRangeFilter property.
     * 
     * @return
     *     possible object is
     *     {@link LastUpdatedTimestampRangeFilter }
     *     
     */
    public LastUpdatedTimestampRangeFilter getLastUpdatedTimestampRangeFilter() {
        return lastUpdatedTimestampRangeFilter;
    }

    /**
     * Sets the value of the lastUpdatedTimestampRangeFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link LastUpdatedTimestampRangeFilter }
     *     
     */
    public void setLastUpdatedTimestampRangeFilter(LastUpdatedTimestampRangeFilter value) {
        this.lastUpdatedTimestampRangeFilter = value;
    }

    /**
     * Gets the value of the multiDateRangeFilter property.
     * 
     * @return
     *     possible object is
     *     {@link MultiDateRangeFilter }
     *     
     */
    public MultiDateRangeFilter getMultiDateRangeFilter() {
        return multiDateRangeFilter;
    }

    /**
     * Sets the value of the multiDateRangeFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiDateRangeFilter }
     *     
     */
    public void setMultiDateRangeFilter(MultiDateRangeFilter value) {
        this.multiDateRangeFilter = value;
    }

    /**
     * Gets the value of the includeDeletedOpportunities property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getIncludeDeletedOpportunities() {
        return includeDeletedOpportunities;
    }

    /**
     * Sets the value of the includeDeletedOpportunities property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setIncludeDeletedOpportunities(YesNoType value) {
        this.includeDeletedOpportunities = value;
    }

    /**
     * Gets the value of the includeSubAgencies property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getIncludeSubAgencies() {
        return includeSubAgencies;
    }

    /**
     * Sets the value of the includeSubAgencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setIncludeSubAgencies(YesNoType value) {
        this.includeSubAgencies = value;
    }

}
