
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsforecastsynopsis_v1.CreateForecast;
import gov.grants.apply.system.grantsopportunity_v1.CreateOpportunity;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID" minOccurs="0"/&gt;
 *           &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}CreateOpportunity" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}CreateForecast"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "createOpportunity",
    "createForecast"
})
@XmlRootElement(name = "CreateForecastInfo")
public class CreateForecastInfo {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String opportunityID;
    @XmlElement(name = "CreateOpportunity", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0")
    protected CreateOpportunity createOpportunity;
    @XmlElement(name = "CreateForecast", namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", required = true)
    protected CreateForecast createForecast;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the createOpportunity property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOpportunity }
     *     
     */
    public CreateOpportunity getCreateOpportunity() {
        return createOpportunity;
    }

    /**
     * Sets the value of the createOpportunity property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOpportunity }
     *     
     */
    public void setCreateOpportunity(CreateOpportunity value) {
        this.createOpportunity = value;
    }

    /**
     * Gets the value of the createForecast property.
     * 
     * @return
     *     possible object is
     *     {@link CreateForecast }
     *     
     */
    public CreateForecast getCreateForecast() {
        return createForecast;
    }

    /**
     * Sets the value of the createForecast property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateForecast }
     *     
     */
    public void setCreateForecast(CreateForecast value) {
        this.createForecast = value;
    }

}
