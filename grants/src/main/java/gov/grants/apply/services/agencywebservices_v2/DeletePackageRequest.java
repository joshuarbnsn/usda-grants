
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/services/AgencyWebServices-V2.0}DeletePackageInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deletePackageInfo"
})
@XmlRootElement(name = "DeletePackageRequest")
public class DeletePackageRequest {

    @XmlElement(name = "DeletePackageInfo", required = true)
    protected DeletePackageInfo deletePackageInfo;

    /**
     * Gets the value of the deletePackageInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DeletePackageInfo }
     *     
     */
    public DeletePackageInfo getDeletePackageInfo() {
        return deletePackageInfo;
    }

    /**
     * Sets the value of the deletePackageInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeletePackageInfo }
     *     
     */
    public void setDeletePackageInfo(DeletePackageInfo value) {
        this.deletePackageInfo = value;
    }

}
