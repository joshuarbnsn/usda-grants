
package gov.grants.apply.services.agencywebservices_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommonelements_v1.ErrorDetails;
import gov.grants.apply.system.grantscommontypes_v1.OperationStatusType;
import gov.grants.apply.system.grantsrelateddocument_v1.CreateFolderResult;
import gov.grants.apply.system.grantsrelateddocument_v1.DeleteFolderResult;
import gov.grants.apply.system.grantsrelateddocument_v1.UpdateFolderResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompletionStatus"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ErrorDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}CreateFolderResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}UpdateFolderResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}DeleteFolderResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "completionStatus",
    "errorDetails",
    "createFolderResult",
    "updateFolderResult",
    "deleteFolderResult"
})
@XmlRootElement(name = "ManageRelatedDocumentFoldersResponse")
public class ManageRelatedDocumentFoldersResponse {

    @XmlElement(name = "CompletionStatus", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    @XmlSchemaType(name = "string")
    protected OperationStatusType completionStatus;
    @XmlElement(name = "ErrorDetails", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected ErrorDetails errorDetails;
    @XmlElement(name = "CreateFolderResult", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<CreateFolderResult> createFolderResult;
    @XmlElement(name = "UpdateFolderResult", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<UpdateFolderResult> updateFolderResult;
    @XmlElement(name = "DeleteFolderResult", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected List<DeleteFolderResult> deleteFolderResult;

    /**
     * Gets the value of the completionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link OperationStatusType }
     *     
     */
    public OperationStatusType getCompletionStatus() {
        return completionStatus;
    }

    /**
     * Sets the value of the completionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationStatusType }
     *     
     */
    public void setCompletionStatus(OperationStatusType value) {
        this.completionStatus = value;
    }

    /**
     * Gets the value of the errorDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorDetails }
     *     
     */
    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    /**
     * Sets the value of the errorDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorDetails }
     *     
     */
    public void setErrorDetails(ErrorDetails value) {
        this.errorDetails = value;
    }

    /**
     * Gets the value of the createFolderResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createFolderResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreateFolderResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateFolderResult }
     * 
     * 
     */
    public List<CreateFolderResult> getCreateFolderResult() {
        if (createFolderResult == null) {
            createFolderResult = new ArrayList<CreateFolderResult>();
        }
        return this.createFolderResult;
    }

    /**
     * Gets the value of the updateFolderResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateFolderResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateFolderResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateFolderResult }
     * 
     * 
     */
    public List<UpdateFolderResult> getUpdateFolderResult() {
        if (updateFolderResult == null) {
            updateFolderResult = new ArrayList<UpdateFolderResult>();
        }
        return this.updateFolderResult;
    }

    /**
     * Gets the value of the deleteFolderResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deleteFolderResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeleteFolderResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeleteFolderResult }
     * 
     * 
     */
    public List<DeleteFolderResult> getDeleteFolderResult() {
        if (deleteFolderResult == null) {
            deleteFolderResult = new ArrayList<DeleteFolderResult>();
        }
        return this.deleteFolderResult;
    }

}
