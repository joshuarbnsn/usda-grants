
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommonelements_v1.ErrorDetails;
import gov.grants.apply.system.grantsopportunity_v1.OpportunityElementsDetails;
import gov.grants.apply.system.grantsrelateddocument_v1.RelatedDocumentDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}OpportunityElementsDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}RelatedDocumentDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ErrorDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "opportunityElementsDetails",
    "relatedDocumentDetails",
    "errorDetails"
})
@XmlRootElement(name = "GetRelatedDocumentDetailsResponse")
public class GetRelatedDocumentDetailsResponse {

    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "OpportunityElementsDetails", namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0")
    protected OpportunityElementsDetails opportunityElementsDetails;
    @XmlElement(name = "RelatedDocumentDetails", namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0")
    protected RelatedDocumentDetails relatedDocumentDetails;
    @XmlElement(name = "ErrorDetails", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected ErrorDetails errorDetails;

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the opportunityElementsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityElementsDetails }
     *     
     */
    public OpportunityElementsDetails getOpportunityElementsDetails() {
        return opportunityElementsDetails;
    }

    /**
     * Sets the value of the opportunityElementsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityElementsDetails }
     *     
     */
    public void setOpportunityElementsDetails(OpportunityElementsDetails value) {
        this.opportunityElementsDetails = value;
    }

    /**
     * Gets the value of the relatedDocumentDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RelatedDocumentDetails }
     *     
     */
    public RelatedDocumentDetails getRelatedDocumentDetails() {
        return relatedDocumentDetails;
    }

    /**
     * Sets the value of the relatedDocumentDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelatedDocumentDetails }
     *     
     */
    public void setRelatedDocumentDetails(RelatedDocumentDetails value) {
        this.relatedDocumentDetails = value;
    }

    /**
     * Gets the value of the errorDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorDetails }
     *     
     */
    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    /**
     * Sets the value of the errorDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorDetails }
     *     
     */
    public void setErrorDetails(ErrorDetails value) {
        this.errorDetails = value;
    }

}
