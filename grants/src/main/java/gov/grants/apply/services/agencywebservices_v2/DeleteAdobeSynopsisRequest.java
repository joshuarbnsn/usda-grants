
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityNumber"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}UserID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}SendDeleteNotificationEmail" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}DeleteComments" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fundingOpportunityNumber",
    "userID",
    "sendDeleteNotificationEmail",
    "deleteComments"
})
@XmlRootElement(name = "DeleteAdobeSynopsisRequest")
public class DeleteAdobeSynopsisRequest {

    @XmlElement(name = "FundingOpportunityNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityNumber;
    @XmlElement(name = "UserID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String userID;
    @XmlElement(name = "SendDeleteNotificationEmail", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", defaultValue = "Y")
    @XmlSchemaType(name = "string")
    protected YesNoType sendDeleteNotificationEmail;
    @XmlElement(name = "DeleteComments", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String deleteComments;

    /**
     * Gets the value of the fundingOpportunityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityNumber() {
        return fundingOpportunityNumber;
    }

    /**
     * Sets the value of the fundingOpportunityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityNumber(String value) {
        this.fundingOpportunityNumber = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the sendDeleteNotificationEmail property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getSendDeleteNotificationEmail() {
        return sendDeleteNotificationEmail;
    }

    /**
     * Sets the value of the sendDeleteNotificationEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setSendDeleteNotificationEmail(YesNoType value) {
        this.sendDeleteNotificationEmail = value;
    }

    /**
     * Gets the value of the deleteComments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteComments() {
        return deleteComments;
    }

    /**
     * Sets the value of the deleteComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteComments(String value) {
        this.deleteComments = value;
    }

}
