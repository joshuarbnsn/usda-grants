
package gov.grants.apply.services.agencywebservices_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/services/AgencyWebServices-V2.0}UpdateAdobeOpportunityInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateAdobeOpportunityInfo"
})
@XmlRootElement(name = "UpdateAdobeOpportunityRequest")
public class UpdateAdobeOpportunityRequest {

    @XmlElement(name = "UpdateAdobeOpportunityInfo", required = true)
    protected UpdateAdobeOpportunityInfo updateAdobeOpportunityInfo;

    /**
     * Gets the value of the updateAdobeOpportunityInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateAdobeOpportunityInfo }
     *     
     */
    public UpdateAdobeOpportunityInfo getUpdateAdobeOpportunityInfo() {
        return updateAdobeOpportunityInfo;
    }

    /**
     * Sets the value of the updateAdobeOpportunityInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateAdobeOpportunityInfo }
     *     
     */
    public void setUpdateAdobeOpportunityInfo(UpdateAdobeOpportunityInfo value) {
        this.updateAdobeOpportunityInfo = value;
    }

}
