
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}CreateSingleProjectPackageForm"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}CreateMultiProjectPackageForm"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createSingleProjectPackageForm",
    "createMultiProjectPackageForm"
})
@XmlRootElement(name = "CreatePackageForms")
public class CreatePackageForms {

    @XmlElement(name = "CreateSingleProjectPackageForm")
    protected CreateSingleProjectPackageForm createSingleProjectPackageForm;
    @XmlElement(name = "CreateMultiProjectPackageForm")
    protected CreateMultiProjectPackageForm createMultiProjectPackageForm;

    /**
     * Gets the value of the createSingleProjectPackageForm property.
     * 
     * @return
     *     possible object is
     *     {@link CreateSingleProjectPackageForm }
     *     
     */
    public CreateSingleProjectPackageForm getCreateSingleProjectPackageForm() {
        return createSingleProjectPackageForm;
    }

    /**
     * Sets the value of the createSingleProjectPackageForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateSingleProjectPackageForm }
     *     
     */
    public void setCreateSingleProjectPackageForm(CreateSingleProjectPackageForm value) {
        this.createSingleProjectPackageForm = value;
    }

    /**
     * Gets the value of the createMultiProjectPackageForm property.
     * 
     * @return
     *     possible object is
     *     {@link CreateMultiProjectPackageForm }
     *     
     */
    public CreateMultiProjectPackageForm getCreateMultiProjectPackageForm() {
        return createMultiProjectPackageForm;
    }

    /**
     * Sets the value of the createMultiProjectPackageForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateMultiProjectPackageForm }
     *     
     */
    public void setCreateMultiProjectPackageForm(CreateMultiProjectPackageForm value) {
        this.createMultiProjectPackageForm = value;
    }

}
