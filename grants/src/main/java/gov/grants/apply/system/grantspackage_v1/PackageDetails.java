
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}PackageID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CFDANumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompetitionID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompetitionTitle" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ElectronicRequired" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ExpectedApplicationCount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ExpectedApplicationSizeMB" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}OpeningDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ClosingDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}GracePeriodDays" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}AgencyContactName" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}InstructionFileDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ApplicantType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}PackageType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}PackageFormsDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}LastUpdatedTimestamp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "packageID",
    "cfdaNumber",
    "competitionID",
    "competitionTitle",
    "electronicRequired",
    "expectedApplicationCount",
    "expectedApplicationSizeMB",
    "openingDate",
    "closingDate",
    "gracePeriodDays",
    "agencyContactName",
    "instructionFileDetails",
    "applicantType",
    "packageType",
    "packageFormsDetails",
    "lastUpdatedTimestamp"
})
@XmlRootElement(name = "PackageDetails")
public class PackageDetails {

    @XmlElement(name = "PackageID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String packageID;
    @XmlElement(name = "CFDANumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String cfdaNumber;
    @XmlElement(name = "CompetitionID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String competitionID;
    @XmlElement(name = "CompetitionTitle", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String competitionTitle;
    @XmlElement(name = "ElectronicRequired")
    @XmlSchemaType(name = "string")
    protected YesNoType electronicRequired;
    @XmlElement(name = "ExpectedApplicationCount")
    protected String expectedApplicationCount;
    @XmlElement(name = "ExpectedApplicationSizeMB")
    protected String expectedApplicationSizeMB;
    @XmlElement(name = "OpeningDate")
    protected String openingDate;
    @XmlElement(name = "ClosingDate")
    protected String closingDate;
    @XmlElement(name = "GracePeriodDays")
    protected String gracePeriodDays;
    @XmlElement(name = "AgencyContactName")
    protected String agencyContactName;
    @XmlElement(name = "InstructionFileDetails")
    protected InstructionFileDetails instructionFileDetails;
    @XmlElement(name = "ApplicantType")
    @XmlSchemaType(name = "string")
    protected ApplicantType applicantType;
    @XmlElement(name = "PackageType")
    @XmlSchemaType(name = "string")
    protected PackageType packageType;
    @XmlElement(name = "PackageFormsDetails")
    protected PackageFormsDetails packageFormsDetails;
    @XmlElement(name = "LastUpdatedTimestamp", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdatedTimestamp;

    /**
     * Gets the value of the packageID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageID() {
        return packageID;
    }

    /**
     * Sets the value of the packageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageID(String value) {
        this.packageID = value;
    }

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFDANumber() {
        return cfdaNumber;
    }

    /**
     * Sets the value of the cfdaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFDANumber(String value) {
        this.cfdaNumber = value;
    }

    /**
     * Gets the value of the competitionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompetitionID() {
        return competitionID;
    }

    /**
     * Sets the value of the competitionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompetitionID(String value) {
        this.competitionID = value;
    }

    /**
     * Gets the value of the competitionTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompetitionTitle() {
        return competitionTitle;
    }

    /**
     * Sets the value of the competitionTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompetitionTitle(String value) {
        this.competitionTitle = value;
    }

    /**
     * Gets the value of the electronicRequired property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getElectronicRequired() {
        return electronicRequired;
    }

    /**
     * Sets the value of the electronicRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setElectronicRequired(YesNoType value) {
        this.electronicRequired = value;
    }

    /**
     * Gets the value of the expectedApplicationCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationCount() {
        return expectedApplicationCount;
    }

    /**
     * Sets the value of the expectedApplicationCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationCount(String value) {
        this.expectedApplicationCount = value;
    }

    /**
     * Gets the value of the expectedApplicationSizeMB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationSizeMB() {
        return expectedApplicationSizeMB;
    }

    /**
     * Sets the value of the expectedApplicationSizeMB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationSizeMB(String value) {
        this.expectedApplicationSizeMB = value;
    }

    /**
     * Gets the value of the openingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpeningDate() {
        return openingDate;
    }

    /**
     * Sets the value of the openingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpeningDate(String value) {
        this.openingDate = value;
    }

    /**
     * Gets the value of the closingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the value of the closingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosingDate(String value) {
        this.closingDate = value;
    }

    /**
     * Gets the value of the gracePeriodDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGracePeriodDays() {
        return gracePeriodDays;
    }

    /**
     * Sets the value of the gracePeriodDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGracePeriodDays(String value) {
        this.gracePeriodDays = value;
    }

    /**
     * Gets the value of the agencyContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyContactName() {
        return agencyContactName;
    }

    /**
     * Sets the value of the agencyContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyContactName(String value) {
        this.agencyContactName = value;
    }

    /**
     * Gets the value of the instructionFileDetails property.
     * 
     * @return
     *     possible object is
     *     {@link InstructionFileDetails }
     *     
     */
    public InstructionFileDetails getInstructionFileDetails() {
        return instructionFileDetails;
    }

    /**
     * Sets the value of the instructionFileDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstructionFileDetails }
     *     
     */
    public void setInstructionFileDetails(InstructionFileDetails value) {
        this.instructionFileDetails = value;
    }

    /**
     * Gets the value of the applicantType property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicantType }
     *     
     */
    public ApplicantType getApplicantType() {
        return applicantType;
    }

    /**
     * Sets the value of the applicantType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicantType }
     *     
     */
    public void setApplicantType(ApplicantType value) {
        this.applicantType = value;
    }

    /**
     * Gets the value of the packageType property.
     * 
     * @return
     *     possible object is
     *     {@link PackageType }
     *     
     */
    public PackageType getPackageType() {
        return packageType;
    }

    /**
     * Sets the value of the packageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageType }
     *     
     */
    public void setPackageType(PackageType value) {
        this.packageType = value;
    }

    /**
     * Gets the value of the packageFormsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PackageFormsDetails }
     *     
     */
    public PackageFormsDetails getPackageFormsDetails() {
        return packageFormsDetails;
    }

    /**
     * Sets the value of the packageFormsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageFormsDetails }
     *     
     */
    public void setPackageFormsDetails(PackageFormsDetails value) {
        this.packageFormsDetails = value;
    }

    /**
     * Gets the value of the lastUpdatedTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    /**
     * Sets the value of the lastUpdatedTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdatedTimestamp(XMLGregorianCalendar value) {
        this.lastUpdatedTimestamp = value;
    }

}
