
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompetitionID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CFDANumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompetitionTitle" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ElectronicRequired"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ExpectedApplicationCount"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ExpectedApplicationSizeMB" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}OpeningDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ClosingDate"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}GracePeriodDays" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}AgencyContactName"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}InstructionFile"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}ApplicantType"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}CreatePackageForms"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "competitionID",
    "cfdaNumber",
    "competitionTitle",
    "electronicRequired",
    "expectedApplicationCount",
    "expectedApplicationSizeMB",
    "openingDate",
    "closingDate",
    "gracePeriodDays",
    "agencyContactName",
    "instructionFile",
    "applicantType",
    "createPackageForms"
})
@XmlRootElement(name = "CreatePackage")
public class CreatePackage {

    @XmlElement(name = "CompetitionID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String competitionID;
    @XmlElement(name = "CFDANumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String cfdaNumber;
    @XmlElement(name = "CompetitionTitle", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String competitionTitle;
    @XmlElement(name = "ElectronicRequired", required = true)
    @XmlSchemaType(name = "string")
    protected YesNoType electronicRequired;
    @XmlElement(name = "ExpectedApplicationCount", required = true)
    protected String expectedApplicationCount;
    @XmlElement(name = "ExpectedApplicationSizeMB")
    protected String expectedApplicationSizeMB;
    @XmlElement(name = "OpeningDate")
    protected String openingDate;
    @XmlElement(name = "ClosingDate", required = true)
    protected String closingDate;
    @XmlElement(name = "GracePeriodDays")
    protected String gracePeriodDays;
    @XmlElement(name = "AgencyContactName", required = true)
    protected String agencyContactName;
    @XmlElement(name = "InstructionFile", required = true)
    protected InstructionFile instructionFile;
    @XmlElement(name = "ApplicantType", required = true)
    @XmlSchemaType(name = "string")
    protected ApplicantType applicantType;
    @XmlElement(name = "CreatePackageForms", required = true)
    protected CreatePackageForms createPackageForms;

    /**
     * Gets the value of the competitionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompetitionID() {
        return competitionID;
    }

    /**
     * Sets the value of the competitionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompetitionID(String value) {
        this.competitionID = value;
    }

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFDANumber() {
        return cfdaNumber;
    }

    /**
     * Sets the value of the cfdaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFDANumber(String value) {
        this.cfdaNumber = value;
    }

    /**
     * Gets the value of the competitionTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompetitionTitle() {
        return competitionTitle;
    }

    /**
     * Sets the value of the competitionTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompetitionTitle(String value) {
        this.competitionTitle = value;
    }

    /**
     * Gets the value of the electronicRequired property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getElectronicRequired() {
        return electronicRequired;
    }

    /**
     * Sets the value of the electronicRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setElectronicRequired(YesNoType value) {
        this.electronicRequired = value;
    }

    /**
     * Gets the value of the expectedApplicationCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationCount() {
        return expectedApplicationCount;
    }

    /**
     * Sets the value of the expectedApplicationCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationCount(String value) {
        this.expectedApplicationCount = value;
    }

    /**
     * Gets the value of the expectedApplicationSizeMB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationSizeMB() {
        return expectedApplicationSizeMB;
    }

    /**
     * Sets the value of the expectedApplicationSizeMB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationSizeMB(String value) {
        this.expectedApplicationSizeMB = value;
    }

    /**
     * Gets the value of the openingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpeningDate() {
        return openingDate;
    }

    /**
     * Sets the value of the openingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpeningDate(String value) {
        this.openingDate = value;
    }

    /**
     * Gets the value of the closingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the value of the closingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosingDate(String value) {
        this.closingDate = value;
    }

    /**
     * Gets the value of the gracePeriodDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGracePeriodDays() {
        return gracePeriodDays;
    }

    /**
     * Sets the value of the gracePeriodDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGracePeriodDays(String value) {
        this.gracePeriodDays = value;
    }

    /**
     * Gets the value of the agencyContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyContactName() {
        return agencyContactName;
    }

    /**
     * Sets the value of the agencyContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyContactName(String value) {
        this.agencyContactName = value;
    }

    /**
     * Gets the value of the instructionFile property.
     * 
     * @return
     *     possible object is
     *     {@link InstructionFile }
     *     
     */
    public InstructionFile getInstructionFile() {
        return instructionFile;
    }

    /**
     * Sets the value of the instructionFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstructionFile }
     *     
     */
    public void setInstructionFile(InstructionFile value) {
        this.instructionFile = value;
    }

    /**
     * Gets the value of the applicantType property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicantType }
     *     
     */
    public ApplicantType getApplicantType() {
        return applicantType;
    }

    /**
     * Sets the value of the applicantType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicantType }
     *     
     */
    public void setApplicantType(ApplicantType value) {
        this.applicantType = value;
    }

    /**
     * Gets the value of the createPackageForms property.
     * 
     * @return
     *     possible object is
     *     {@link CreatePackageForms }
     *     
     */
    public CreatePackageForms getCreatePackageForms() {
        return createPackageForms;
    }

    /**
     * Sets the value of the createPackageForms property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatePackageForms }
     *     
     */
    public void setCreatePackageForms(CreatePackageForms value) {
        this.createPackageForms = value;
    }

}
