
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}GroupName"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}MinIterations"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}MaxIterations"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}TemplateName"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "groupName",
    "minIterations",
    "maxIterations",
    "templateName"
})
@XmlRootElement(name = "CreateSubApplicationGroup")
public class CreateSubApplicationGroup {

    @XmlElement(name = "GroupName", required = true)
    protected String groupName;
    @XmlElement(name = "MinIterations", required = true)
    protected String minIterations;
    @XmlElement(name = "MaxIterations", required = true)
    protected String maxIterations;
    @XmlElement(name = "TemplateName", required = true)
    protected String templateName;

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the minIterations property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinIterations() {
        return minIterations;
    }

    /**
     * Sets the value of the minIterations property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinIterations(String value) {
        this.minIterations = value;
    }

    /**
     * Gets the value of the maxIterations property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxIterations() {
        return maxIterations;
    }

    /**
     * Sets the value of the maxIterations property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxIterations(String value) {
        this.maxIterations = value;
    }

    /**
     * Gets the value of the templateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Sets the value of the templateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateName(String value) {
        this.templateName = value;
    }

}
