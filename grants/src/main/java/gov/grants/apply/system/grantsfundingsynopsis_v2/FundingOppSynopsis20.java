
package gov.grants.apply.system.grantsfundingsynopsis_v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.OpportunityCategoryType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}PostingDate"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}UserID"/&gt;
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}FundingInstrument" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}FundingActivityCategory" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}OtherFundingCategoryExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}ExpectedNumberOfAwards" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}EstimatedFunding" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}AwardCeiling"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}AwardFloor"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityTitle"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityNumber"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ClosingDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}ClosingDateExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ArchiveDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}FundingOpportunityDescription"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CFDANumber" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}EligibleApplicantTypes" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}OtherEligibleApplicantExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}CostSharingOrMatchingRequirement"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}AdditionalInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0}AgencyContact" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityCategory" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OtherOpportunityCategoryExplanation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "postingDate",
    "userID",
    "password",
    "fundingInstrument",
    "fundingActivityCategory",
    "otherFundingCategoryExplanation",
    "expectedNumberOfAwards",
    "estimatedFunding",
    "awardCeiling",
    "awardFloor",
    "fundingOpportunityTitle",
    "fundingOpportunityNumber",
    "closingDate",
    "closingDateExplanation",
    "archiveDate",
    "fundingOpportunityDescription",
    "cfdaNumber",
    "eligibleApplicantTypes",
    "otherEligibleApplicantExplanation",
    "costSharingOrMatchingRequirement",
    "additionalInformation",
    "agencyContact",
    "opportunityCategory",
    "otherOpportunityCategoryExplanation"
})
@XmlRootElement(name = "FundingOppSynopsis_2_0")
public class FundingOppSynopsis20 {

    @XmlElement(name = "PostingDate", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String postingDate;
    @XmlElement(name = "UserID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String userID;
    @XmlElement(name = "Password", required = true)
    protected String password;
    @XmlElement(name = "FundingInstrument", required = true)
    protected List<String> fundingInstrument;
    @XmlElement(name = "FundingActivityCategory", required = true)
    protected List<String> fundingActivityCategory;
    @XmlElement(name = "OtherFundingCategoryExplanation")
    protected String otherFundingCategoryExplanation;
    @XmlElement(name = "ExpectedNumberOfAwards")
    protected String expectedNumberOfAwards;
    @XmlElement(name = "EstimatedFunding")
    protected String estimatedFunding;
    @XmlElement(name = "AwardCeiling", required = true)
    protected String awardCeiling;
    @XmlElement(name = "AwardFloor", required = true)
    protected String awardFloor;
    @XmlElement(name = "FundingOpportunityTitle", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityTitle;
    @XmlElement(name = "FundingOpportunityNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityNumber;
    @XmlElement(name = "ClosingDate", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String closingDate;
    @XmlElement(name = "ClosingDateExplanation")
    protected String closingDateExplanation;
    @XmlElement(name = "ArchiveDate", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String archiveDate;
    @XmlElement(name = "FundingOpportunityDescription", required = true)
    protected String fundingOpportunityDescription;
    @XmlElement(name = "CFDANumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected List<String> cfdaNumber;
    @XmlElement(name = "EligibleApplicantTypes", required = true)
    protected List<String> eligibleApplicantTypes;
    @XmlElement(name = "OtherEligibleApplicantExplanation")
    protected String otherEligibleApplicantExplanation;
    @XmlElement(name = "CostSharingOrMatchingRequirement", required = true)
    protected String costSharingOrMatchingRequirement;
    @XmlElement(name = "AdditionalInformation")
    protected AdditionalInformation additionalInformation;
    @XmlElement(name = "AgencyContact", required = true)
    protected List<AgencyContact> agencyContact;
    @XmlElement(name = "OpportunityCategory", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    @XmlSchemaType(name = "string")
    protected OpportunityCategoryType opportunityCategory;
    @XmlElement(name = "OtherOpportunityCategoryExplanation", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String otherOpportunityCategoryExplanation;

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostingDate(String value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the fundingInstrument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundingInstrument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundingInstrument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFundingInstrument() {
        if (fundingInstrument == null) {
            fundingInstrument = new ArrayList<String>();
        }
        return this.fundingInstrument;
    }

    /**
     * Gets the value of the fundingActivityCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundingActivityCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundingActivityCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFundingActivityCategory() {
        if (fundingActivityCategory == null) {
            fundingActivityCategory = new ArrayList<String>();
        }
        return this.fundingActivityCategory;
    }

    /**
     * Gets the value of the otherFundingCategoryExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherFundingCategoryExplanation() {
        return otherFundingCategoryExplanation;
    }

    /**
     * Sets the value of the otherFundingCategoryExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherFundingCategoryExplanation(String value) {
        this.otherFundingCategoryExplanation = value;
    }

    /**
     * Gets the value of the expectedNumberOfAwards property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedNumberOfAwards() {
        return expectedNumberOfAwards;
    }

    /**
     * Sets the value of the expectedNumberOfAwards property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedNumberOfAwards(String value) {
        this.expectedNumberOfAwards = value;
    }

    /**
     * Gets the value of the estimatedFunding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedFunding() {
        return estimatedFunding;
    }

    /**
     * Sets the value of the estimatedFunding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedFunding(String value) {
        this.estimatedFunding = value;
    }

    /**
     * Gets the value of the awardCeiling property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardCeiling() {
        return awardCeiling;
    }

    /**
     * Sets the value of the awardCeiling property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardCeiling(String value) {
        this.awardCeiling = value;
    }

    /**
     * Gets the value of the awardFloor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardFloor() {
        return awardFloor;
    }

    /**
     * Sets the value of the awardFloor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardFloor(String value) {
        this.awardFloor = value;
    }

    /**
     * Gets the value of the fundingOpportunityTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityTitle() {
        return fundingOpportunityTitle;
    }

    /**
     * Sets the value of the fundingOpportunityTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityTitle(String value) {
        this.fundingOpportunityTitle = value;
    }

    /**
     * Gets the value of the fundingOpportunityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityNumber() {
        return fundingOpportunityNumber;
    }

    /**
     * Sets the value of the fundingOpportunityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityNumber(String value) {
        this.fundingOpportunityNumber = value;
    }

    /**
     * Gets the value of the closingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the value of the closingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosingDate(String value) {
        this.closingDate = value;
    }

    /**
     * Gets the value of the closingDateExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosingDateExplanation() {
        return closingDateExplanation;
    }

    /**
     * Sets the value of the closingDateExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosingDateExplanation(String value) {
        this.closingDateExplanation = value;
    }

    /**
     * Gets the value of the archiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchiveDate() {
        return archiveDate;
    }

    /**
     * Sets the value of the archiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchiveDate(String value) {
        this.archiveDate = value;
    }

    /**
     * Gets the value of the fundingOpportunityDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityDescription() {
        return fundingOpportunityDescription;
    }

    /**
     * Sets the value of the fundingOpportunityDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityDescription(String value) {
        this.fundingOpportunityDescription = value;
    }

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfdaNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCFDANumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCFDANumber() {
        if (cfdaNumber == null) {
            cfdaNumber = new ArrayList<String>();
        }
        return this.cfdaNumber;
    }

    /**
     * Gets the value of the eligibleApplicantTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eligibleApplicantTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEligibleApplicantTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEligibleApplicantTypes() {
        if (eligibleApplicantTypes == null) {
            eligibleApplicantTypes = new ArrayList<String>();
        }
        return this.eligibleApplicantTypes;
    }

    /**
     * Gets the value of the otherEligibleApplicantExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherEligibleApplicantExplanation() {
        return otherEligibleApplicantExplanation;
    }

    /**
     * Sets the value of the otherEligibleApplicantExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherEligibleApplicantExplanation(String value) {
        this.otherEligibleApplicantExplanation = value;
    }

    /**
     * Gets the value of the costSharingOrMatchingRequirement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostSharingOrMatchingRequirement() {
        return costSharingOrMatchingRequirement;
    }

    /**
     * Sets the value of the costSharingOrMatchingRequirement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostSharingOrMatchingRequirement(String value) {
        this.costSharingOrMatchingRequirement = value;
    }

    /**
     * Gets the value of the additionalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalInformation }
     *     
     */
    public AdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Sets the value of the additionalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalInformation }
     *     
     */
    public void setAdditionalInformation(AdditionalInformation value) {
        this.additionalInformation = value;
    }

    /**
     * Gets the value of the agencyContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agencyContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgencyContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgencyContact }
     * 
     * 
     */
    public List<AgencyContact> getAgencyContact() {
        if (agencyContact == null) {
            agencyContact = new ArrayList<AgencyContact>();
        }
        return this.agencyContact;
    }

    /**
     * Gets the value of the opportunityCategory property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityCategoryType }
     *     
     */
    public OpportunityCategoryType getOpportunityCategory() {
        return opportunityCategory;
    }

    /**
     * Sets the value of the opportunityCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityCategoryType }
     *     
     */
    public void setOpportunityCategory(OpportunityCategoryType value) {
        this.opportunityCategory = value;
    }

    /**
     * Gets the value of the otherOpportunityCategoryExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherOpportunityCategoryExplanation() {
        return otherOpportunityCategoryExplanation;
    }

    /**
     * Sets the value of the otherOpportunityCategoryExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherOpportunityCategoryExplanation(String value) {
        this.otherOpportunityCategoryExplanation = value;
    }

}
