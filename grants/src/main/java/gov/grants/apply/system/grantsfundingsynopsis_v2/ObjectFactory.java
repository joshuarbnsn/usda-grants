
package gov.grants.apply.system.grantsfundingsynopsis_v2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.system.grantsfundingsynopsis_v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FundingInstrument_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "FundingInstrument");
    private final static QName _FundingActivityCategory_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "FundingActivityCategory");
    private final static QName _OtherFundingCategoryExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "OtherFundingCategoryExplanation");
    private final static QName _ExpectedNumberOfAwards_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "ExpectedNumberOfAwards");
    private final static QName _EstimatedFunding_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "EstimatedFunding");
    private final static QName _AwardCeiling_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "AwardCeiling");
    private final static QName _AwardFloor_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "AwardFloor");
    private final static QName _ClosingDateExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "ClosingDateExplanation");
    private final static QName _FundingOpportunityDescription_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "FundingOpportunityDescription");
    private final static QName _EligibleApplicantTypes_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "EligibleApplicantTypes");
    private final static QName _OtherEligibleApplicantExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "OtherEligibleApplicantExplanation");
    private final static QName _CostSharingOrMatchingRequirement_QNAME = new QName("http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", "CostSharingOrMatchingRequirement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.system.grantsfundingsynopsis_v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FundingOppModSynopsis20 }
     * 
     */
    public FundingOppModSynopsis20 createFundingOppModSynopsis20() {
        return new FundingOppModSynopsis20();
    }

    /**
     * Create an instance of {@link AdditionalInformation }
     * 
     */
    public AdditionalInformation createAdditionalInformation() {
        return new AdditionalInformation();
    }

    /**
     * Create an instance of {@link AgencyContact }
     * 
     */
    public AgencyContact createAgencyContact() {
        return new AgencyContact();
    }

    /**
     * Create an instance of {@link FundingOppSynopsis20 }
     * 
     */
    public FundingOppSynopsis20 createFundingOppSynopsis20() {
        return new FundingOppSynopsis20();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "FundingInstrument")
    public JAXBElement<String> createFundingInstrument(String value) {
        return new JAXBElement<String>(_FundingInstrument_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "FundingActivityCategory")
    public JAXBElement<String> createFundingActivityCategory(String value) {
        return new JAXBElement<String>(_FundingActivityCategory_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "OtherFundingCategoryExplanation")
    public JAXBElement<String> createOtherFundingCategoryExplanation(String value) {
        return new JAXBElement<String>(_OtherFundingCategoryExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "ExpectedNumberOfAwards")
    public JAXBElement<String> createExpectedNumberOfAwards(String value) {
        return new JAXBElement<String>(_ExpectedNumberOfAwards_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "EstimatedFunding")
    public JAXBElement<String> createEstimatedFunding(String value) {
        return new JAXBElement<String>(_EstimatedFunding_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "AwardCeiling")
    public JAXBElement<String> createAwardCeiling(String value) {
        return new JAXBElement<String>(_AwardCeiling_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "AwardFloor")
    public JAXBElement<String> createAwardFloor(String value) {
        return new JAXBElement<String>(_AwardFloor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "ClosingDateExplanation")
    public JAXBElement<String> createClosingDateExplanation(String value) {
        return new JAXBElement<String>(_ClosingDateExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "FundingOpportunityDescription")
    public JAXBElement<String> createFundingOpportunityDescription(String value) {
        return new JAXBElement<String>(_FundingOpportunityDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "EligibleApplicantTypes")
    public JAXBElement<String> createEligibleApplicantTypes(String value) {
        return new JAXBElement<String>(_EligibleApplicantTypes_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "OtherEligibleApplicantExplanation")
    public JAXBElement<String> createOtherEligibleApplicantExplanation(String value) {
        return new JAXBElement<String>(_OtherEligibleApplicantExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsFundingSynopsis-V2.0", name = "CostSharingOrMatchingRequirement")
    public JAXBElement<String> createCostSharingOrMatchingRequirement(String value) {
        return new JAXBElement<String>(_CostSharingOrMatchingRequirement_QNAME, String.class, null, value);
    }

}
