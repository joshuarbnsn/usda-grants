
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice minOccurs="0"&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}SingleProjectFormDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}MultiProjectFormDetails" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "singleProjectFormDetails",
    "multiProjectFormDetails"
})
@XmlRootElement(name = "PackageFormsDetails")
public class PackageFormsDetails {

    @XmlElement(name = "SingleProjectFormDetails")
    protected SingleProjectFormDetails singleProjectFormDetails;
    @XmlElement(name = "MultiProjectFormDetails")
    protected MultiProjectFormDetails multiProjectFormDetails;

    /**
     * Gets the value of the singleProjectFormDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SingleProjectFormDetails }
     *     
     */
    public SingleProjectFormDetails getSingleProjectFormDetails() {
        return singleProjectFormDetails;
    }

    /**
     * Sets the value of the singleProjectFormDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleProjectFormDetails }
     *     
     */
    public void setSingleProjectFormDetails(SingleProjectFormDetails value) {
        this.singleProjectFormDetails = value;
    }

    /**
     * Gets the value of the multiProjectFormDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MultiProjectFormDetails }
     *     
     */
    public MultiProjectFormDetails getMultiProjectFormDetails() {
        return multiProjectFormDetails;
    }

    /**
     * Sets the value of the multiProjectFormDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiProjectFormDetails }
     *     
     */
    public void setMultiProjectFormDetails(MultiProjectFormDetails value) {
        this.multiProjectFormDetails = value;
    }

}
