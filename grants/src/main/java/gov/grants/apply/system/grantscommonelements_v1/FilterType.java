
package gov.grants.apply.system.grantscommonelements_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FilterType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FilterType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Status"/&gt;
 *     &lt;enumeration value="OpportunityID"/&gt;
 *     &lt;enumeration value="CFDANumber"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FilterType")
@XmlEnum
public enum FilterType {

    @XmlEnumValue("Status")
    STATUS("Status"),
    @XmlEnumValue("OpportunityID")
    OPPORTUNITY_ID("OpportunityID"),
    @XmlEnumValue("CFDANumber")
    CFDA_NUMBER("CFDANumber");
    private final String value;

    FilterType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FilterType fromValue(String v) {
        for (FilterType c: FilterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
