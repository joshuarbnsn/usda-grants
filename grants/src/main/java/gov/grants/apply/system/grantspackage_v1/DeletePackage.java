
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}PackageID"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}SendDeleteNotificationEmail" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}DeleteComments"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "packageID",
    "sendDeleteNotificationEmail",
    "deleteComments"
})
@XmlRootElement(name = "DeletePackage")
public class DeletePackage {

    @XmlElement(name = "PackageID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String packageID;
    @XmlElement(name = "SendDeleteNotificationEmail", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", defaultValue = "Y")
    @XmlSchemaType(name = "string")
    protected YesNoType sendDeleteNotificationEmail;
    @XmlElement(name = "DeleteComments", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String deleteComments;

    /**
     * Gets the value of the packageID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageID() {
        return packageID;
    }

    /**
     * Sets the value of the packageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageID(String value) {
        this.packageID = value;
    }

    /**
     * Gets the value of the sendDeleteNotificationEmail property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getSendDeleteNotificationEmail() {
        return sendDeleteNotificationEmail;
    }

    /**
     * Sets the value of the sendDeleteNotificationEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setSendDeleteNotificationEmail(YesNoType value) {
        this.sendDeleteNotificationEmail = value;
    }

    /**
     * Gets the value of the deleteComments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteComments() {
        return deleteComments;
    }

    /**
     * Sets the value of the deleteComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteComments(String value) {
        this.deleteComments = value;
    }

}
