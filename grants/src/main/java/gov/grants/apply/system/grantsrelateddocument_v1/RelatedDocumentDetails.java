
package gov.grants.apply.system.grantsrelateddocument_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}LinkDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsRelatedDocument-V1.0}FolderAndFileDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "linkDetails",
    "folderAndFileDetails"
})
@XmlRootElement(name = "RelatedDocumentDetails")
public class RelatedDocumentDetails {

    @XmlElement(name = "LinkDetails")
    protected List<LinkDetails> linkDetails;
    @XmlElement(name = "FolderAndFileDetails")
    protected List<FolderAndFileDetails> folderAndFileDetails;

    /**
     * Gets the value of the linkDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LinkDetails }
     * 
     * 
     */
    public List<LinkDetails> getLinkDetails() {
        if (linkDetails == null) {
            linkDetails = new ArrayList<LinkDetails>();
        }
        return this.linkDetails;
    }

    /**
     * Gets the value of the folderAndFileDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the folderAndFileDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFolderAndFileDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FolderAndFileDetails }
     * 
     * 
     */
    public List<FolderAndFileDetails> getFolderAndFileDetails() {
        if (folderAndFileDetails == null) {
            folderAndFileDetails = new ArrayList<FolderAndFileDetails>();
        }
        return this.folderAndFileDetails;
    }

}
