
package gov.grants.apply.system.agencymanagepackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}SendChangeNotificationEmail" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}ModificationComments"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendChangeNotificationEmail",
    "modificationComments"
})
@XmlRootElement(name = "EmailInfo")
public class EmailInfo {

    @XmlElement(name = "SendChangeNotificationEmail", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", defaultValue = "Y")
    protected String sendChangeNotificationEmail;
    @XmlElement(name = "ModificationComments", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String modificationComments;

    /**
     * Gets the value of the sendChangeNotificationEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendChangeNotificationEmail() {
        return sendChangeNotificationEmail;
    }

    /**
     * Sets the value of the sendChangeNotificationEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendChangeNotificationEmail(String value) {
        this.sendChangeNotificationEmail = value;
    }

    /**
     * Gets the value of the modificationComments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModificationComments() {
        return modificationComments;
    }

    /**
     * Sets the value of the modificationComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModificationComments(String value) {
        this.modificationComments = value;
    }

}
