
package gov.grants.apply.system.grantsrelateddocument_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FolderType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FolderType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FULL_ANNOUNCEMENT"/&gt;
 *     &lt;enumeration value="REVISED_FULL_ANNOUNCEMENT"/&gt;
 *     &lt;enumeration value="OTHER_SUPPORTING_DOCUMENTS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FolderType")
@XmlEnum
public enum FolderType {

    FULL_ANNOUNCEMENT,
    REVISED_FULL_ANNOUNCEMENT,
    OTHER_SUPPORTING_DOCUMENTS;

    public String value() {
        return name();
    }

    public static FolderType fromValue(String v) {
        return valueOf(v);
    }

}
