
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplicantType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplicantType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ORGANIZATIONS_ONLY"/&gt;
 *     &lt;enumeration value="INDIVIDUALS_ONLY"/&gt;
 *     &lt;enumeration value="INDIVIDUALS_AND_ORGANIZATIONS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApplicantType")
@XmlEnum
public enum ApplicantType {

    ORGANIZATIONS_ONLY,
    INDIVIDUALS_ONLY,
    INDIVIDUALS_AND_ORGANIZATIONS;

    public String value() {
        return name();
    }

    public static ApplicantType fromValue(String v) {
        return valueOf(v);
    }

}
