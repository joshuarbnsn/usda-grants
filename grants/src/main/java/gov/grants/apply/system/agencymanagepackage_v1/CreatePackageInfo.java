
package gov.grants.apply.system.agencymanagepackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommonelements_v1.CompetitionInfo;
import gov.grants.apply.system.grantscommonelements_v1.InstructionFileInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CFDANumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CompetitionInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}ElectronicSignatureRequired"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}ExpectedApplicationCount"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}ExpectedApplicationSizeMB" minOccurs="0"/&gt;
 *         &lt;element name="OpeningDate" type="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}OpportunityDateType" minOccurs="0"/&gt;
 *         &lt;element name="ClosingDate" type="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}OpportunityDateType"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}GracePeriodDays" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}AgencyContactInfo"/&gt;
 *         &lt;element name="TemplateName" type="{http://apply.grants.gov/system/GrantsCommonTypes-V1.0}TemplateNameType"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}InstructionFileInfo"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/AgencyManagePackage-V1.0}OpenToApplicant"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cfdaNumber",
    "competitionInfo",
    "electronicSignatureRequired",
    "expectedApplicationCount",
    "expectedApplicationSizeMB",
    "openingDate",
    "closingDate",
    "gracePeriodDays",
    "agencyContactInfo",
    "templateName",
    "instructionFileInfo",
    "openToApplicant"
})
@XmlRootElement(name = "CreatePackageInfo")
public class CreatePackageInfo {

    @XmlElement(name = "CFDANumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String cfdaNumber;
    @XmlElement(name = "CompetitionInfo", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected CompetitionInfo competitionInfo;
    @XmlElement(name = "ElectronicSignatureRequired", required = true)
    protected String electronicSignatureRequired;
    @XmlElement(name = "ExpectedApplicationCount", required = true)
    protected String expectedApplicationCount;
    @XmlElement(name = "ExpectedApplicationSizeMB")
    protected String expectedApplicationSizeMB;
    @XmlElement(name = "OpeningDate")
    protected String openingDate;
    @XmlElement(name = "ClosingDate", required = true)
    protected String closingDate;
    @XmlElement(name = "GracePeriodDays")
    protected String gracePeriodDays;
    @XmlElement(name = "AgencyContactInfo", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String agencyContactInfo;
    @XmlElement(name = "TemplateName", required = true)
    protected String templateName;
    @XmlElement(name = "InstructionFileInfo", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected InstructionFileInfo instructionFileInfo;
    @XmlElement(name = "OpenToApplicant", required = true)
    protected String openToApplicant;

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFDANumber() {
        return cfdaNumber;
    }

    /**
     * Sets the value of the cfdaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFDANumber(String value) {
        this.cfdaNumber = value;
    }

    /**
     * Gets the value of the competitionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CompetitionInfo }
     *     
     */
    public CompetitionInfo getCompetitionInfo() {
        return competitionInfo;
    }

    /**
     * Sets the value of the competitionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompetitionInfo }
     *     
     */
    public void setCompetitionInfo(CompetitionInfo value) {
        this.competitionInfo = value;
    }

    /**
     * Gets the value of the electronicSignatureRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElectronicSignatureRequired() {
        return electronicSignatureRequired;
    }

    /**
     * Sets the value of the electronicSignatureRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElectronicSignatureRequired(String value) {
        this.electronicSignatureRequired = value;
    }

    /**
     * Gets the value of the expectedApplicationCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationCount() {
        return expectedApplicationCount;
    }

    /**
     * Sets the value of the expectedApplicationCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationCount(String value) {
        this.expectedApplicationCount = value;
    }

    /**
     * Gets the value of the expectedApplicationSizeMB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationSizeMB() {
        return expectedApplicationSizeMB;
    }

    /**
     * Sets the value of the expectedApplicationSizeMB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationSizeMB(String value) {
        this.expectedApplicationSizeMB = value;
    }

    /**
     * Gets the value of the openingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpeningDate() {
        return openingDate;
    }

    /**
     * Sets the value of the openingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpeningDate(String value) {
        this.openingDate = value;
    }

    /**
     * Gets the value of the closingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the value of the closingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosingDate(String value) {
        this.closingDate = value;
    }

    /**
     * Gets the value of the gracePeriodDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGracePeriodDays() {
        return gracePeriodDays;
    }

    /**
     * Sets the value of the gracePeriodDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGracePeriodDays(String value) {
        this.gracePeriodDays = value;
    }

    /**
     * Gets the value of the agencyContactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyContactInfo() {
        return agencyContactInfo;
    }

    /**
     * Sets the value of the agencyContactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyContactInfo(String value) {
        this.agencyContactInfo = value;
    }

    /**
     * Gets the value of the templateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Sets the value of the templateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateName(String value) {
        this.templateName = value;
    }

    /**
     * Gets the value of the instructionFileInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InstructionFileInfo }
     *     
     */
    public InstructionFileInfo getInstructionFileInfo() {
        return instructionFileInfo;
    }

    /**
     * Sets the value of the instructionFileInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstructionFileInfo }
     *     
     */
    public void setInstructionFileInfo(InstructionFileInfo value) {
        this.instructionFileInfo = value;
    }

    /**
     * Gets the value of the openToApplicant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenToApplicant() {
        return openToApplicant;
    }

    /**
     * Sets the value of the openToApplicant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenToApplicant(String value) {
        this.openToApplicant = value;
    }

}
