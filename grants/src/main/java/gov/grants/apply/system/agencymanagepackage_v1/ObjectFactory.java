
package gov.grants.apply.system.agencymanagepackage_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.system.agencymanagepackage_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ElectronicSignatureRequired_QNAME = new QName("http://apply.grants.gov/system/AgencyManagePackage-V1.0", "ElectronicSignatureRequired");
    private final static QName _ExpectedApplicationCount_QNAME = new QName("http://apply.grants.gov/system/AgencyManagePackage-V1.0", "ExpectedApplicationCount");
    private final static QName _ExpectedApplicationSizeMB_QNAME = new QName("http://apply.grants.gov/system/AgencyManagePackage-V1.0", "ExpectedApplicationSizeMB");
    private final static QName _GracePeriodDays_QNAME = new QName("http://apply.grants.gov/system/AgencyManagePackage-V1.0", "GracePeriodDays");
    private final static QName _OpenToApplicant_QNAME = new QName("http://apply.grants.gov/system/AgencyManagePackage-V1.0", "OpenToApplicant");
    private final static QName _OpportunityCategoryExplanation_QNAME = new QName("http://apply.grants.gov/system/AgencyManagePackage-V1.0", "OpportunityCategoryExplanation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.system.agencymanagepackage_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreatePackageInfo }
     * 
     */
    public CreatePackageInfo createCreatePackageInfo() {
        return new CreatePackageInfo();
    }

    /**
     * Create an instance of {@link UpdatePackageInfo }
     * 
     */
    public UpdatePackageInfo createUpdatePackageInfo() {
        return new UpdatePackageInfo();
    }

    /**
     * Create an instance of {@link EmailInfo }
     * 
     */
    public EmailInfo createEmailInfo() {
        return new EmailInfo();
    }

    /**
     * Create an instance of {@link DeletePackageInfo }
     * 
     */
    public DeletePackageInfo createDeletePackageInfo() {
        return new DeletePackageInfo();
    }

    /**
     * Create an instance of {@link CreatePackageResult }
     * 
     */
    public CreatePackageResult createCreatePackageResult() {
        return new CreatePackageResult();
    }

    /**
     * Create an instance of {@link UpdatePackageResult }
     * 
     */
    public UpdatePackageResult createUpdatePackageResult() {
        return new UpdatePackageResult();
    }

    /**
     * Create an instance of {@link DeletePackageResult }
     * 
     */
    public DeletePackageResult createDeletePackageResult() {
        return new DeletePackageResult();
    }

    /**
     * Create an instance of {@link CreateOpportunityInfo }
     * 
     */
    public CreateOpportunityInfo createCreateOpportunityInfo() {
        return new CreateOpportunityInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0", name = "ElectronicSignatureRequired")
    public JAXBElement<String> createElectronicSignatureRequired(String value) {
        return new JAXBElement<String>(_ElectronicSignatureRequired_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0", name = "ExpectedApplicationCount")
    public JAXBElement<String> createExpectedApplicationCount(String value) {
        return new JAXBElement<String>(_ExpectedApplicationCount_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0", name = "ExpectedApplicationSizeMB")
    public JAXBElement<String> createExpectedApplicationSizeMB(String value) {
        return new JAXBElement<String>(_ExpectedApplicationSizeMB_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0", name = "GracePeriodDays")
    public JAXBElement<String> createGracePeriodDays(String value) {
        return new JAXBElement<String>(_GracePeriodDays_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0", name = "OpenToApplicant")
    public JAXBElement<String> createOpenToApplicant(String value) {
        return new JAXBElement<String>(_OpenToApplicant_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/AgencyManagePackage-V1.0", name = "OpportunityCategoryExplanation")
    public JAXBElement<String> createOpportunityCategoryExplanation(String value) {
        return new JAXBElement<String>(_OpportunityCategoryExplanation_QNAME, String.class, null, value);
    }

}
