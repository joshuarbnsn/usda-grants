
package gov.grants.apply.system.grantsfundingsynopsis_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AgencyContactDescription"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2500"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AgencyEmailAddress"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="130"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AgencyEmailDescription"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="102"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "agencyContactDescription",
    "agencyEmailAddress",
    "agencyEmailDescription"
})
@XmlRootElement(name = "AgencyContact")
public class AgencyContact {

    @XmlElement(name = "AgencyContactDescription", required = true)
    protected String agencyContactDescription;
    @XmlElement(name = "AgencyEmailAddress", required = true)
    protected String agencyEmailAddress;
    @XmlElement(name = "AgencyEmailDescription", required = true)
    protected String agencyEmailDescription;

    /**
     * Gets the value of the agencyContactDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyContactDescription() {
        return agencyContactDescription;
    }

    /**
     * Sets the value of the agencyContactDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyContactDescription(String value) {
        this.agencyContactDescription = value;
    }

    /**
     * Gets the value of the agencyEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyEmailAddress() {
        return agencyEmailAddress;
    }

    /**
     * Sets the value of the agencyEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyEmailAddress(String value) {
        this.agencyEmailAddress = value;
    }

    /**
     * Gets the value of the agencyEmailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyEmailDescription() {
        return agencyEmailDescription;
    }

    /**
     * Sets the value of the agencyEmailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyEmailDescription(String value) {
        this.agencyEmailDescription = value;
    }

}
