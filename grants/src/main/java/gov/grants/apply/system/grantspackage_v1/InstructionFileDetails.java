
package gov.grants.apply.system.grantspackage_v1;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FileName" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FileMIMEType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}LastUpdatedTimestamp" minOccurs="0"/&gt;
 *         &lt;element name="FileDataHandler" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fileName",
    "fileMIMEType",
    "lastUpdatedTimestamp",
    "fileDataHandler"
})
@XmlRootElement(name = "InstructionFileDetails")
public class InstructionFileDetails {

    @XmlElement(name = "FileName", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String fileName;
    @XmlElement(name = "FileMIMEType", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String fileMIMEType;
    @XmlElement(name = "LastUpdatedTimestamp", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdatedTimestamp;
    @XmlElement(name = "FileDataHandler")
    @XmlMimeType("application/octet-stream")
    protected DataHandler fileDataHandler;

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the fileMIMEType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileMIMEType() {
        return fileMIMEType;
    }

    /**
     * Sets the value of the fileMIMEType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileMIMEType(String value) {
        this.fileMIMEType = value;
    }

    /**
     * Gets the value of the lastUpdatedTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    /**
     * Sets the value of the lastUpdatedTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdatedTimestamp(XMLGregorianCalendar value) {
        this.lastUpdatedTimestamp = value;
    }

    /**
     * Gets the value of the fileDataHandler property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getFileDataHandler() {
        return fileDataHandler;
    }

    /**
     * Sets the value of the fileDataHandler property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setFileDataHandler(DataHandler value) {
        this.fileDataHandler = value;
    }

}
