
package gov.grants.apply.system.grantsopportunity_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElementDateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ElementDateType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ForecastPostDate"/&gt;
 *     &lt;enumeration value="ForecastArchiveDate"/&gt;
 *     &lt;enumeration value="SynopsisPostDate"/&gt;
 *     &lt;enumeration value="SynopsisCloseDate"/&gt;
 *     &lt;enumeration value="SynopsisArchiveDate"/&gt;
 *     &lt;enumeration value="PackageOpenDate"/&gt;
 *     &lt;enumeration value="PackageCloseDate"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ElementDateType")
@XmlEnum
public enum ElementDateType {

    @XmlEnumValue("ForecastPostDate")
    FORECAST_POST_DATE("ForecastPostDate"),
    @XmlEnumValue("ForecastArchiveDate")
    FORECAST_ARCHIVE_DATE("ForecastArchiveDate"),
    @XmlEnumValue("SynopsisPostDate")
    SYNOPSIS_POST_DATE("SynopsisPostDate"),
    @XmlEnumValue("SynopsisCloseDate")
    SYNOPSIS_CLOSE_DATE("SynopsisCloseDate"),
    @XmlEnumValue("SynopsisArchiveDate")
    SYNOPSIS_ARCHIVE_DATE("SynopsisArchiveDate"),
    @XmlEnumValue("PackageOpenDate")
    PACKAGE_OPEN_DATE("PackageOpenDate"),
    @XmlEnumValue("PackageCloseDate")
    PACKAGE_CLOSE_DATE("PackageCloseDate");
    private final String value;

    ElementDateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ElementDateType fromValue(String v) {
        for (ElementDateType c: ElementDateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
