
package gov.grants.apply.system.grantsopportunity_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Type" type="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}ElementDateType"/&gt;
 *         &lt;element name="BeginValue" type="{http://apply.grants.gov/system/GrantsCommonTypes-V1.0}MMDDYYYYFwdSlashType" minOccurs="0"/&gt;
 *         &lt;element name="EndValue" type="{http://apply.grants.gov/system/GrantsCommonTypes-V1.0}MMDDYYYYFwdSlashType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "type",
    "beginValue",
    "endValue"
})
@XmlRootElement(name = "DateRangeFilter")
public class DateRangeFilter {

    @XmlElement(name = "Type", required = true)
    @XmlSchemaType(name = "string")
    protected ElementDateType type;
    @XmlElement(name = "BeginValue")
    protected String beginValue;
    @XmlElement(name = "EndValue")
    protected String endValue;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ElementDateType }
     *     
     */
    public ElementDateType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElementDateType }
     *     
     */
    public void setType(ElementDateType value) {
        this.type = value;
    }

    /**
     * Gets the value of the beginValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeginValue() {
        return beginValue;
    }

    /**
     * Sets the value of the beginValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeginValue(String value) {
        this.beginValue = value;
    }

    /**
     * Gets the value of the endValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndValue() {
        return endValue;
    }

    /**
     * Sets the value of the endValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndValue(String value) {
        this.endValue = value;
    }

}
