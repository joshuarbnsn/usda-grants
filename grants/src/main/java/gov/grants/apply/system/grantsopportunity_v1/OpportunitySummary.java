
package gov.grants.apply.system.grantsopportunity_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityNumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}AgencyCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}AgencyName" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CFDANumber" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}OppFctSynLastUpdatedTimestamp" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}RelatedDocumentsLastUpdatedTimestamp" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}PackagesLastUpdatedTimestamp" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}ContainsForecast" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}ContainsSynopsis" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}ContainsRelatedDocuments" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}ContainsPackages" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityID",
    "fundingOpportunityNumber",
    "agencyCode",
    "agencyName",
    "cfdaNumber",
    "oppFctSynLastUpdatedTimestamp",
    "relatedDocumentsLastUpdatedTimestamp",
    "packagesLastUpdatedTimestamp",
    "containsForecast",
    "containsSynopsis",
    "containsRelatedDocuments",
    "containsPackages"
})
@XmlRootElement(name = "OpportunitySummary")
public class OpportunitySummary {

    @XmlElement(name = "OpportunityID", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String opportunityID;
    @XmlElement(name = "FundingOpportunityNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String fundingOpportunityNumber;
    @XmlElement(name = "AgencyCode", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String agencyCode;
    @XmlElement(name = "AgencyName", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String agencyName;
    @XmlElement(name = "CFDANumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected List<String> cfdaNumber;
    @XmlElement(name = "OppFctSynLastUpdatedTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar oppFctSynLastUpdatedTimestamp;
    @XmlElement(name = "RelatedDocumentsLastUpdatedTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar relatedDocumentsLastUpdatedTimestamp;
    @XmlElement(name = "PackagesLastUpdatedTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar packagesLastUpdatedTimestamp;
    @XmlElement(name = "ContainsForecast")
    @XmlSchemaType(name = "string")
    protected YesNoType containsForecast;
    @XmlElement(name = "ContainsSynopsis")
    @XmlSchemaType(name = "string")
    protected YesNoType containsSynopsis;
    @XmlElement(name = "ContainsRelatedDocuments")
    @XmlSchemaType(name = "string")
    protected YesNoType containsRelatedDocuments;
    @XmlElement(name = "ContainsPackages")
    @XmlSchemaType(name = "string")
    protected YesNoType containsPackages;

    /**
     * Gets the value of the opportunityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Sets the value of the opportunityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Gets the value of the fundingOpportunityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityNumber() {
        return fundingOpportunityNumber;
    }

    /**
     * Sets the value of the fundingOpportunityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityNumber(String value) {
        this.fundingOpportunityNumber = value;
    }

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the agencyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyName() {
        return agencyName;
    }

    /**
     * Sets the value of the agencyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyName(String value) {
        this.agencyName = value;
    }

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfdaNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCFDANumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCFDANumber() {
        if (cfdaNumber == null) {
            cfdaNumber = new ArrayList<String>();
        }
        return this.cfdaNumber;
    }

    /**
     * Gets the value of the oppFctSynLastUpdatedTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOppFctSynLastUpdatedTimestamp() {
        return oppFctSynLastUpdatedTimestamp;
    }

    /**
     * Sets the value of the oppFctSynLastUpdatedTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOppFctSynLastUpdatedTimestamp(XMLGregorianCalendar value) {
        this.oppFctSynLastUpdatedTimestamp = value;
    }

    /**
     * Gets the value of the relatedDocumentsLastUpdatedTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRelatedDocumentsLastUpdatedTimestamp() {
        return relatedDocumentsLastUpdatedTimestamp;
    }

    /**
     * Sets the value of the relatedDocumentsLastUpdatedTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRelatedDocumentsLastUpdatedTimestamp(XMLGregorianCalendar value) {
        this.relatedDocumentsLastUpdatedTimestamp = value;
    }

    /**
     * Gets the value of the packagesLastUpdatedTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPackagesLastUpdatedTimestamp() {
        return packagesLastUpdatedTimestamp;
    }

    /**
     * Sets the value of the packagesLastUpdatedTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPackagesLastUpdatedTimestamp(XMLGregorianCalendar value) {
        this.packagesLastUpdatedTimestamp = value;
    }

    /**
     * Gets the value of the containsForecast property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getContainsForecast() {
        return containsForecast;
    }

    /**
     * Sets the value of the containsForecast property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setContainsForecast(YesNoType value) {
        this.containsForecast = value;
    }

    /**
     * Gets the value of the containsSynopsis property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getContainsSynopsis() {
        return containsSynopsis;
    }

    /**
     * Sets the value of the containsSynopsis property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setContainsSynopsis(YesNoType value) {
        this.containsSynopsis = value;
    }

    /**
     * Gets the value of the containsRelatedDocuments property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getContainsRelatedDocuments() {
        return containsRelatedDocuments;
    }

    /**
     * Sets the value of the containsRelatedDocuments property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setContainsRelatedDocuments(YesNoType value) {
        this.containsRelatedDocuments = value;
    }

    /**
     * Gets the value of the containsPackages property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getContainsPackages() {
        return containsPackages;
    }

    /**
     * Sets the value of the containsPackages property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setContainsPackages(YesNoType value) {
        this.containsPackages = value;
    }

}
