
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice minOccurs="0"&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}UpdateSingleProjectPackageForm"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}UpdateMultiProjectPackageForm"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateSingleProjectPackageForm",
    "updateMultiProjectPackageForm"
})
@XmlRootElement(name = "UpdatePackageForms")
public class UpdatePackageForms {

    @XmlElement(name = "UpdateSingleProjectPackageForm")
    protected UpdateSingleProjectPackageForm updateSingleProjectPackageForm;
    @XmlElement(name = "UpdateMultiProjectPackageForm")
    protected UpdateMultiProjectPackageForm updateMultiProjectPackageForm;

    /**
     * Gets the value of the updateSingleProjectPackageForm property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSingleProjectPackageForm }
     *     
     */
    public UpdateSingleProjectPackageForm getUpdateSingleProjectPackageForm() {
        return updateSingleProjectPackageForm;
    }

    /**
     * Sets the value of the updateSingleProjectPackageForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSingleProjectPackageForm }
     *     
     */
    public void setUpdateSingleProjectPackageForm(UpdateSingleProjectPackageForm value) {
        this.updateSingleProjectPackageForm = value;
    }

    /**
     * Gets the value of the updateMultiProjectPackageForm property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateMultiProjectPackageForm }
     *     
     */
    public UpdateMultiProjectPackageForm getUpdateMultiProjectPackageForm() {
        return updateMultiProjectPackageForm;
    }

    /**
     * Sets the value of the updateMultiProjectPackageForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateMultiProjectPackageForm }
     *     
     */
    public void setUpdateMultiProjectPackageForm(UpdateMultiProjectPackageForm value) {
        this.updateMultiProjectPackageForm = value;
    }

}
