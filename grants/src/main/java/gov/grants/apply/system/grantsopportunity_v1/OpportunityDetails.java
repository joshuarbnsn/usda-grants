
package gov.grants.apply.system.grantsopportunity_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantsforecastsynopsis_v1.ForecastDetails;
import gov.grants.apply.system.grantsforecastsynopsis_v1.SynopsisDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsOpportunity-V1.0}OpportunityElementsDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}SynopsisDetails" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}ForecastDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opportunityElementsDetails",
    "synopsisDetails",
    "forecastDetails"
})
@XmlRootElement(name = "OpportunityDetails")
public class OpportunityDetails {

    @XmlElement(name = "OpportunityElementsDetails")
    protected OpportunityElementsDetails opportunityElementsDetails;
    @XmlElement(name = "SynopsisDetails", namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0")
    protected SynopsisDetails synopsisDetails;
    @XmlElement(name = "ForecastDetails", namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0")
    protected ForecastDetails forecastDetails;

    /**
     * Gets the value of the opportunityElementsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityElementsDetails }
     *     
     */
    public OpportunityElementsDetails getOpportunityElementsDetails() {
        return opportunityElementsDetails;
    }

    /**
     * Sets the value of the opportunityElementsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityElementsDetails }
     *     
     */
    public void setOpportunityElementsDetails(OpportunityElementsDetails value) {
        this.opportunityElementsDetails = value;
    }

    /**
     * Gets the value of the synopsisDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SynopsisDetails }
     *     
     */
    public SynopsisDetails getSynopsisDetails() {
        return synopsisDetails;
    }

    /**
     * Sets the value of the synopsisDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SynopsisDetails }
     *     
     */
    public void setSynopsisDetails(SynopsisDetails value) {
        this.synopsisDetails = value;
    }

    /**
     * Gets the value of the forecastDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ForecastDetails }
     *     
     */
    public ForecastDetails getForecastDetails() {
        return forecastDetails;
    }

    /**
     * Sets the value of the forecastDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ForecastDetails }
     *     
     */
    public void setForecastDetails(ForecastDetails value) {
        this.forecastDetails = value;
    }

}
