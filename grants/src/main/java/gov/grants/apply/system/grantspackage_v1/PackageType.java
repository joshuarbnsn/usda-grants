
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PackageType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PackageType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SingleProject"/&gt;
 *     &lt;enumeration value="MultiProject"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PackageType")
@XmlEnum
public enum PackageType {

    @XmlEnumValue("SingleProject")
    SINGLE_PROJECT("SingleProject"),
    @XmlEnumValue("MultiProject")
    MULTI_PROJECT("MultiProject");
    private final String value;

    PackageType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PackageType fromValue(String v) {
        for (PackageType c: PackageType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
