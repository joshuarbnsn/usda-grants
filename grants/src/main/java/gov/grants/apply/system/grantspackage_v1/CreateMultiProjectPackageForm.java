
package gov.grants.apply.system.grantspackage_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}AgencyDownloadUrl"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}CreateOverallPackage"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}CreateSubApplicationGroup" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "agencyDownloadUrl",
    "createOverallPackage",
    "createSubApplicationGroup"
})
@XmlRootElement(name = "CreateMultiProjectPackageForm")
public class CreateMultiProjectPackageForm {

    @XmlElement(name = "AgencyDownloadUrl", required = true)
    protected String agencyDownloadUrl;
    @XmlElement(name = "CreateOverallPackage", required = true)
    protected CreateOverallPackage createOverallPackage;
    @XmlElement(name = "CreateSubApplicationGroup")
    protected List<CreateSubApplicationGroup> createSubApplicationGroup;

    /**
     * Gets the value of the agencyDownloadUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyDownloadUrl() {
        return agencyDownloadUrl;
    }

    /**
     * Sets the value of the agencyDownloadUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyDownloadUrl(String value) {
        this.agencyDownloadUrl = value;
    }

    /**
     * Gets the value of the createOverallPackage property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOverallPackage }
     *     
     */
    public CreateOverallPackage getCreateOverallPackage() {
        return createOverallPackage;
    }

    /**
     * Sets the value of the createOverallPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOverallPackage }
     *     
     */
    public void setCreateOverallPackage(CreateOverallPackage value) {
        this.createOverallPackage = value;
    }

    /**
     * Gets the value of the createSubApplicationGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createSubApplicationGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreateSubApplicationGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateSubApplicationGroup }
     * 
     * 
     */
    public List<CreateSubApplicationGroup> getCreateSubApplicationGroup() {
        if (createSubApplicationGroup == null) {
            createSubApplicationGroup = new ArrayList<CreateSubApplicationGroup>();
        }
        return this.createSubApplicationGroup;
    }

}
