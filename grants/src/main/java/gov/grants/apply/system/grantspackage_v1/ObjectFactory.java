
package gov.grants.apply.system.grantspackage_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.system.grantspackage_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ElectronicRequired_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "ElectronicRequired");
    private final static QName _ExpectedApplicationCount_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "ExpectedApplicationCount");
    private final static QName _ExpectedApplicationSizeMB_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "ExpectedApplicationSizeMB");
    private final static QName _OpeningDate_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "OpeningDate");
    private final static QName _ClosingDate_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "ClosingDate");
    private final static QName _GracePeriodDays_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "GracePeriodDays");
    private final static QName _AgencyContactName_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "AgencyContactName");
    private final static QName _ApplicantType_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "ApplicantType");
    private final static QName _TemplateName_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "TemplateName");
    private final static QName _AgencyDownloadUrl_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "AgencyDownloadUrl");
    private final static QName _GroupName_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "GroupName");
    private final static QName _MinIterations_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "MinIterations");
    private final static QName _MaxIterations_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "MaxIterations");
    private final static QName _ShortFormName_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "ShortFormName");
    private final static QName _FormName_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "FormName");
    private final static QName _FormVersion_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "FormVersion");
    private final static QName _FormID_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "FormID");
    private final static QName _FormFamily_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "FormFamily");
    private final static QName _PackageType_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "PackageType");
    private final static QName _FormType_QNAME = new QName("http://apply.grants.gov/system/GrantsPackage-V1.0", "FormType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.system.grantspackage_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreatePackage }
     * 
     */
    public CreatePackage createCreatePackage() {
        return new CreatePackage();
    }

    /**
     * Create an instance of {@link InstructionFile }
     * 
     */
    public InstructionFile createInstructionFile() {
        return new InstructionFile();
    }

    /**
     * Create an instance of {@link CreatePackageForms }
     * 
     */
    public CreatePackageForms createCreatePackageForms() {
        return new CreatePackageForms();
    }

    /**
     * Create an instance of {@link CreateSingleProjectPackageForm }
     * 
     */
    public CreateSingleProjectPackageForm createCreateSingleProjectPackageForm() {
        return new CreateSingleProjectPackageForm();
    }

    /**
     * Create an instance of {@link CreateMultiProjectPackageForm }
     * 
     */
    public CreateMultiProjectPackageForm createCreateMultiProjectPackageForm() {
        return new CreateMultiProjectPackageForm();
    }

    /**
     * Create an instance of {@link CreateOverallPackage }
     * 
     */
    public CreateOverallPackage createCreateOverallPackage() {
        return new CreateOverallPackage();
    }

    /**
     * Create an instance of {@link CreateSubApplicationGroup }
     * 
     */
    public CreateSubApplicationGroup createCreateSubApplicationGroup() {
        return new CreateSubApplicationGroup();
    }

    /**
     * Create an instance of {@link UpdatePackage }
     * 
     */
    public UpdatePackage createUpdatePackage() {
        return new UpdatePackage();
    }

    /**
     * Create an instance of {@link UpdatePackageForms }
     * 
     */
    public UpdatePackageForms createUpdatePackageForms() {
        return new UpdatePackageForms();
    }

    /**
     * Create an instance of {@link UpdateSingleProjectPackageForm }
     * 
     */
    public UpdateSingleProjectPackageForm createUpdateSingleProjectPackageForm() {
        return new UpdateSingleProjectPackageForm();
    }

    /**
     * Create an instance of {@link UpdateMultiProjectPackageForm }
     * 
     */
    public UpdateMultiProjectPackageForm createUpdateMultiProjectPackageForm() {
        return new UpdateMultiProjectPackageForm();
    }

    /**
     * Create an instance of {@link UpdateOverallPackage }
     * 
     */
    public UpdateOverallPackage createUpdateOverallPackage() {
        return new UpdateOverallPackage();
    }

    /**
     * Create an instance of {@link UpdateSubApplicationGroup }
     * 
     */
    public UpdateSubApplicationGroup createUpdateSubApplicationGroup() {
        return new UpdateSubApplicationGroup();
    }

    /**
     * Create an instance of {@link DeletePackage }
     * 
     */
    public DeletePackage createDeletePackage() {
        return new DeletePackage();
    }

    /**
     * Create an instance of {@link PackageDetails }
     * 
     */
    public PackageDetails createPackageDetails() {
        return new PackageDetails();
    }

    /**
     * Create an instance of {@link InstructionFileDetails }
     * 
     */
    public InstructionFileDetails createInstructionFileDetails() {
        return new InstructionFileDetails();
    }

    /**
     * Create an instance of {@link PackageFormsDetails }
     * 
     */
    public PackageFormsDetails createPackageFormsDetails() {
        return new PackageFormsDetails();
    }

    /**
     * Create an instance of {@link SingleProjectFormDetails }
     * 
     */
    public SingleProjectFormDetails createSingleProjectFormDetails() {
        return new SingleProjectFormDetails();
    }

    /**
     * Create an instance of {@link FormDetails }
     * 
     */
    public FormDetails createFormDetails() {
        return new FormDetails();
    }

    /**
     * Create an instance of {@link MultiProjectFormDetails }
     * 
     */
    public MultiProjectFormDetails createMultiProjectFormDetails() {
        return new MultiProjectFormDetails();
    }

    /**
     * Create an instance of {@link OverallFormDetails }
     * 
     */
    public OverallFormDetails createOverallFormDetails() {
        return new OverallFormDetails();
    }

    /**
     * Create an instance of {@link SubApplicationGroupDetails }
     * 
     */
    public SubApplicationGroupDetails createSubApplicationGroupDetails() {
        return new SubApplicationGroupDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "ElectronicRequired")
    public JAXBElement<YesNoType> createElectronicRequired(YesNoType value) {
        return new JAXBElement<YesNoType>(_ElectronicRequired_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "ExpectedApplicationCount")
    public JAXBElement<String> createExpectedApplicationCount(String value) {
        return new JAXBElement<String>(_ExpectedApplicationCount_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "ExpectedApplicationSizeMB")
    public JAXBElement<String> createExpectedApplicationSizeMB(String value) {
        return new JAXBElement<String>(_ExpectedApplicationSizeMB_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "OpeningDate")
    public JAXBElement<String> createOpeningDate(String value) {
        return new JAXBElement<String>(_OpeningDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "ClosingDate")
    public JAXBElement<String> createClosingDate(String value) {
        return new JAXBElement<String>(_ClosingDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "GracePeriodDays")
    public JAXBElement<String> createGracePeriodDays(String value) {
        return new JAXBElement<String>(_GracePeriodDays_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "AgencyContactName")
    public JAXBElement<String> createAgencyContactName(String value) {
        return new JAXBElement<String>(_AgencyContactName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicantType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "ApplicantType")
    public JAXBElement<ApplicantType> createApplicantType(ApplicantType value) {
        return new JAXBElement<ApplicantType>(_ApplicantType_QNAME, ApplicantType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "TemplateName")
    public JAXBElement<String> createTemplateName(String value) {
        return new JAXBElement<String>(_TemplateName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "AgencyDownloadUrl")
    public JAXBElement<String> createAgencyDownloadUrl(String value) {
        return new JAXBElement<String>(_AgencyDownloadUrl_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "GroupName")
    public JAXBElement<String> createGroupName(String value) {
        return new JAXBElement<String>(_GroupName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "MinIterations")
    public JAXBElement<String> createMinIterations(String value) {
        return new JAXBElement<String>(_MinIterations_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "MaxIterations")
    public JAXBElement<String> createMaxIterations(String value) {
        return new JAXBElement<String>(_MaxIterations_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "ShortFormName")
    public JAXBElement<String> createShortFormName(String value) {
        return new JAXBElement<String>(_ShortFormName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "FormName")
    public JAXBElement<String> createFormName(String value) {
        return new JAXBElement<String>(_FormName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "FormVersion")
    public JAXBElement<String> createFormVersion(String value) {
        return new JAXBElement<String>(_FormVersion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "FormID")
    public JAXBElement<String> createFormID(String value) {
        return new JAXBElement<String>(_FormID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "FormFamily")
    public JAXBElement<String> createFormFamily(String value) {
        return new JAXBElement<String>(_FormFamily_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "PackageType")
    public JAXBElement<PackageType> createPackageType(PackageType value) {
        return new JAXBElement<PackageType>(_PackageType_QNAME, PackageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsPackage-V1.0", name = "FormType")
    public JAXBElement<FormType> createFormType(FormType value) {
        return new JAXBElement<FormType>(_FormType_QNAME, FormType.class, null, value);
    }

}
