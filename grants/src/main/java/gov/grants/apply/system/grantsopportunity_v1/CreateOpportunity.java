
package gov.grants.apply.system.grantsopportunity_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.OpportunityCategoryType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}AgencyCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityNumber"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}FundingOpportunityTitle"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityCategory"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}OpportunityCategoryExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsCommonElements-V1.0}CFDANumber" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "agencyCode",
    "fundingOpportunityNumber",
    "fundingOpportunityTitle",
    "opportunityCategory",
    "opportunityCategoryExplanation",
    "cfdaNumber"
})
@XmlRootElement(name = "CreateOpportunity")
public class CreateOpportunity {

    @XmlElement(name = "AgencyCode", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String agencyCode;
    @XmlElement(name = "FundingOpportunityNumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityNumber;
    @XmlElement(name = "FundingOpportunityTitle", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected String fundingOpportunityTitle;
    @XmlElement(name = "OpportunityCategory", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    @XmlSchemaType(name = "string")
    protected OpportunityCategoryType opportunityCategory;
    @XmlElement(name = "OpportunityCategoryExplanation", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0")
    protected String opportunityCategoryExplanation;
    @XmlElement(name = "CFDANumber", namespace = "http://apply.grants.gov/system/GrantsCommonElements-V1.0", required = true)
    protected List<String> cfdaNumber;

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the fundingOpportunityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityNumber() {
        return fundingOpportunityNumber;
    }

    /**
     * Sets the value of the fundingOpportunityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityNumber(String value) {
        this.fundingOpportunityNumber = value;
    }

    /**
     * Gets the value of the fundingOpportunityTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityTitle() {
        return fundingOpportunityTitle;
    }

    /**
     * Sets the value of the fundingOpportunityTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityTitle(String value) {
        this.fundingOpportunityTitle = value;
    }

    /**
     * Gets the value of the opportunityCategory property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityCategoryType }
     *     
     */
    public OpportunityCategoryType getOpportunityCategory() {
        return opportunityCategory;
    }

    /**
     * Sets the value of the opportunityCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityCategoryType }
     *     
     */
    public void setOpportunityCategory(OpportunityCategoryType value) {
        this.opportunityCategory = value;
    }

    /**
     * Gets the value of the opportunityCategoryExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityCategoryExplanation() {
        return opportunityCategoryExplanation;
    }

    /**
     * Sets the value of the opportunityCategoryExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityCategoryExplanation(String value) {
        this.opportunityCategoryExplanation = value;
    }

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfdaNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCFDANumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCFDANumber() {
        if (cfdaNumber == null) {
            cfdaNumber = new ArrayList<String>();
        }
        return this.cfdaNumber;
    }

}
