
package gov.grants.apply.system.grantsrelateddocument_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.system.grantsrelateddocument_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FolderName_QNAME = new QName("http://apply.grants.gov/system/GrantsRelatedDocument-V1.0", "FolderName");
    private final static QName _FolderType_QNAME = new QName("http://apply.grants.gov/system/GrantsRelatedDocument-V1.0", "FolderType");
    private final static QName _FileDescription_QNAME = new QName("http://apply.grants.gov/system/GrantsRelatedDocument-V1.0", "FileDescription");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.system.grantsrelateddocument_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateFolder }
     * 
     */
    public CreateFolder createCreateFolder() {
        return new CreateFolder();
    }

    /**
     * Create an instance of {@link UpdateFolder }
     * 
     */
    public UpdateFolder createUpdateFolder() {
        return new UpdateFolder();
    }

    /**
     * Create an instance of {@link DeleteFolder }
     * 
     */
    public DeleteFolder createDeleteFolder() {
        return new DeleteFolder();
    }

    /**
     * Create an instance of {@link AddFile }
     * 
     */
    public AddFile createAddFile() {
        return new AddFile();
    }

    /**
     * Create an instance of {@link ReplaceFile }
     * 
     */
    public ReplaceFile createReplaceFile() {
        return new ReplaceFile();
    }

    /**
     * Create an instance of {@link RemoveFile }
     * 
     */
    public RemoveFile createRemoveFile() {
        return new RemoveFile();
    }

    /**
     * Create an instance of {@link CreateLink }
     * 
     */
    public CreateLink createCreateLink() {
        return new CreateLink();
    }

    /**
     * Create an instance of {@link UpdateLink }
     * 
     */
    public UpdateLink createUpdateLink() {
        return new UpdateLink();
    }

    /**
     * Create an instance of {@link DeleteLink }
     * 
     */
    public DeleteLink createDeleteLink() {
        return new DeleteLink();
    }

    /**
     * Create an instance of {@link FileDetails }
     * 
     */
    public FileDetails createFileDetails() {
        return new FileDetails();
    }

    /**
     * Create an instance of {@link FolderDetails }
     * 
     */
    public FolderDetails createFolderDetails() {
        return new FolderDetails();
    }

    /**
     * Create an instance of {@link FolderAndFileDetails }
     * 
     */
    public FolderAndFileDetails createFolderAndFileDetails() {
        return new FolderAndFileDetails();
    }

    /**
     * Create an instance of {@link LinkDetails }
     * 
     */
    public LinkDetails createLinkDetails() {
        return new LinkDetails();
    }

    /**
     * Create an instance of {@link RelatedDocumentDetails }
     * 
     */
    public RelatedDocumentDetails createRelatedDocumentDetails() {
        return new RelatedDocumentDetails();
    }

    /**
     * Create an instance of {@link AddFileResult }
     * 
     */
    public AddFileResult createAddFileResult() {
        return new AddFileResult();
    }

    /**
     * Create an instance of {@link ReplaceFileResult }
     * 
     */
    public ReplaceFileResult createReplaceFileResult() {
        return new ReplaceFileResult();
    }

    /**
     * Create an instance of {@link RemoveFileResult }
     * 
     */
    public RemoveFileResult createRemoveFileResult() {
        return new RemoveFileResult();
    }

    /**
     * Create an instance of {@link CreateLinkResult }
     * 
     */
    public CreateLinkResult createCreateLinkResult() {
        return new CreateLinkResult();
    }

    /**
     * Create an instance of {@link UpdateLinkResult }
     * 
     */
    public UpdateLinkResult createUpdateLinkResult() {
        return new UpdateLinkResult();
    }

    /**
     * Create an instance of {@link DeleteLinkResult }
     * 
     */
    public DeleteLinkResult createDeleteLinkResult() {
        return new DeleteLinkResult();
    }

    /**
     * Create an instance of {@link CreateFolderResult }
     * 
     */
    public CreateFolderResult createCreateFolderResult() {
        return new CreateFolderResult();
    }

    /**
     * Create an instance of {@link UpdateFolderResult }
     * 
     */
    public UpdateFolderResult createUpdateFolderResult() {
        return new UpdateFolderResult();
    }

    /**
     * Create an instance of {@link DeleteFolderResult }
     * 
     */
    public DeleteFolderResult createDeleteFolderResult() {
        return new DeleteFolderResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0", name = "FolderName")
    public JAXBElement<String> createFolderName(String value) {
        return new JAXBElement<String>(_FolderName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FolderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0", name = "FolderType")
    public JAXBElement<FolderType> createFolderType(FolderType value) {
        return new JAXBElement<FolderType>(_FolderType_QNAME, FolderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsRelatedDocument-V1.0", name = "FileDescription")
    public JAXBElement<String> createFileDescription(String value) {
        return new JAXBElement<String>(_FileDescription_QNAME, String.class, null, value);
    }

}
