
package gov.grants.apply.system.grantsforecastsynopsis_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}FundingInstrument" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}FundingActivityCategory" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}FundingCategoryExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}ExpectedNumberOfAwards" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}CostSharingOrMatchingRequirement"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}PostingDate"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EstimatedSynopsisPostDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EstimatedApplicationDueDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EstimatedApplicationDueDateExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EstimatedAwardDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EstimatedProjectStartDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}FiscalYear"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}ArchiveDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EstimatedFunding" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}AwardCeiling" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}AwardFloor" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EligibleApplicantType" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}EligibleApplicantExplanation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}FundingOpportunityDescription"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}AdditionalInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0}GrantorContact"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fundingInstrument",
    "fundingActivityCategory",
    "fundingCategoryExplanation",
    "expectedNumberOfAwards",
    "costSharingOrMatchingRequirement",
    "postingDate",
    "estimatedSynopsisPostDate",
    "estimatedApplicationDueDate",
    "estimatedApplicationDueDateExplanation",
    "estimatedAwardDate",
    "estimatedProjectStartDate",
    "fiscalYear",
    "archiveDate",
    "estimatedFunding",
    "awardCeiling",
    "awardFloor",
    "eligibleApplicantType",
    "eligibleApplicantExplanation",
    "fundingOpportunityDescription",
    "additionalInformation",
    "grantorContact"
})
@XmlRootElement(name = "CreateForecast")
public class CreateForecast {

    @XmlElement(name = "FundingInstrument", required = true)
    protected List<String> fundingInstrument;
    @XmlElement(name = "FundingActivityCategory", required = true)
    protected List<String> fundingActivityCategory;
    @XmlElement(name = "FundingCategoryExplanation")
    protected String fundingCategoryExplanation;
    @XmlElement(name = "ExpectedNumberOfAwards")
    protected String expectedNumberOfAwards;
    @XmlElement(name = "CostSharingOrMatchingRequirement", required = true)
    @XmlSchemaType(name = "string")
    protected YesNoType costSharingOrMatchingRequirement;
    @XmlElement(name = "PostingDate", required = true)
    protected String postingDate;
    @XmlElement(name = "EstimatedSynopsisPostDate")
    protected String estimatedSynopsisPostDate;
    @XmlElement(name = "EstimatedApplicationDueDate")
    protected String estimatedApplicationDueDate;
    @XmlElement(name = "EstimatedApplicationDueDateExplanation")
    protected String estimatedApplicationDueDateExplanation;
    @XmlElement(name = "EstimatedAwardDate")
    protected String estimatedAwardDate;
    @XmlElement(name = "EstimatedProjectStartDate")
    protected String estimatedProjectStartDate;
    @XmlElement(name = "FiscalYear", required = true)
    protected String fiscalYear;
    @XmlElement(name = "ArchiveDate")
    protected String archiveDate;
    @XmlElement(name = "EstimatedFunding")
    protected String estimatedFunding;
    @XmlElement(name = "AwardCeiling")
    protected String awardCeiling;
    @XmlElement(name = "AwardFloor")
    protected String awardFloor;
    @XmlElement(name = "EligibleApplicantType", required = true)
    protected List<String> eligibleApplicantType;
    @XmlElement(name = "EligibleApplicantExplanation")
    protected String eligibleApplicantExplanation;
    @XmlElement(name = "FundingOpportunityDescription", required = true)
    protected String fundingOpportunityDescription;
    @XmlElement(name = "AdditionalInformation")
    protected AdditionalInformation additionalInformation;
    @XmlElement(name = "GrantorContact", required = true)
    protected GrantorContact grantorContact;

    /**
     * Gets the value of the fundingInstrument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundingInstrument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundingInstrument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFundingInstrument() {
        if (fundingInstrument == null) {
            fundingInstrument = new ArrayList<String>();
        }
        return this.fundingInstrument;
    }

    /**
     * Gets the value of the fundingActivityCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundingActivityCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundingActivityCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFundingActivityCategory() {
        if (fundingActivityCategory == null) {
            fundingActivityCategory = new ArrayList<String>();
        }
        return this.fundingActivityCategory;
    }

    /**
     * Gets the value of the fundingCategoryExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingCategoryExplanation() {
        return fundingCategoryExplanation;
    }

    /**
     * Sets the value of the fundingCategoryExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingCategoryExplanation(String value) {
        this.fundingCategoryExplanation = value;
    }

    /**
     * Gets the value of the expectedNumberOfAwards property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedNumberOfAwards() {
        return expectedNumberOfAwards;
    }

    /**
     * Sets the value of the expectedNumberOfAwards property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedNumberOfAwards(String value) {
        this.expectedNumberOfAwards = value;
    }

    /**
     * Gets the value of the costSharingOrMatchingRequirement property.
     * 
     * @return
     *     possible object is
     *     {@link YesNoType }
     *     
     */
    public YesNoType getCostSharingOrMatchingRequirement() {
        return costSharingOrMatchingRequirement;
    }

    /**
     * Sets the value of the costSharingOrMatchingRequirement property.
     * 
     * @param value
     *     allowed object is
     *     {@link YesNoType }
     *     
     */
    public void setCostSharingOrMatchingRequirement(YesNoType value) {
        this.costSharingOrMatchingRequirement = value;
    }

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostingDate(String value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the estimatedSynopsisPostDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedSynopsisPostDate() {
        return estimatedSynopsisPostDate;
    }

    /**
     * Sets the value of the estimatedSynopsisPostDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedSynopsisPostDate(String value) {
        this.estimatedSynopsisPostDate = value;
    }

    /**
     * Gets the value of the estimatedApplicationDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedApplicationDueDate() {
        return estimatedApplicationDueDate;
    }

    /**
     * Sets the value of the estimatedApplicationDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedApplicationDueDate(String value) {
        this.estimatedApplicationDueDate = value;
    }

    /**
     * Gets the value of the estimatedApplicationDueDateExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedApplicationDueDateExplanation() {
        return estimatedApplicationDueDateExplanation;
    }

    /**
     * Sets the value of the estimatedApplicationDueDateExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedApplicationDueDateExplanation(String value) {
        this.estimatedApplicationDueDateExplanation = value;
    }

    /**
     * Gets the value of the estimatedAwardDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedAwardDate() {
        return estimatedAwardDate;
    }

    /**
     * Sets the value of the estimatedAwardDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedAwardDate(String value) {
        this.estimatedAwardDate = value;
    }

    /**
     * Gets the value of the estimatedProjectStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedProjectStartDate() {
        return estimatedProjectStartDate;
    }

    /**
     * Sets the value of the estimatedProjectStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedProjectStartDate(String value) {
        this.estimatedProjectStartDate = value;
    }

    /**
     * Gets the value of the fiscalYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiscalYear() {
        return fiscalYear;
    }

    /**
     * Sets the value of the fiscalYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiscalYear(String value) {
        this.fiscalYear = value;
    }

    /**
     * Gets the value of the archiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchiveDate() {
        return archiveDate;
    }

    /**
     * Sets the value of the archiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchiveDate(String value) {
        this.archiveDate = value;
    }

    /**
     * Gets the value of the estimatedFunding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedFunding() {
        return estimatedFunding;
    }

    /**
     * Sets the value of the estimatedFunding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedFunding(String value) {
        this.estimatedFunding = value;
    }

    /**
     * Gets the value of the awardCeiling property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardCeiling() {
        return awardCeiling;
    }

    /**
     * Sets the value of the awardCeiling property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardCeiling(String value) {
        this.awardCeiling = value;
    }

    /**
     * Gets the value of the awardFloor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardFloor() {
        return awardFloor;
    }

    /**
     * Sets the value of the awardFloor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardFloor(String value) {
        this.awardFloor = value;
    }

    /**
     * Gets the value of the eligibleApplicantType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eligibleApplicantType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEligibleApplicantType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEligibleApplicantType() {
        if (eligibleApplicantType == null) {
            eligibleApplicantType = new ArrayList<String>();
        }
        return this.eligibleApplicantType;
    }

    /**
     * Gets the value of the eligibleApplicantExplanation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligibleApplicantExplanation() {
        return eligibleApplicantExplanation;
    }

    /**
     * Sets the value of the eligibleApplicantExplanation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligibleApplicantExplanation(String value) {
        this.eligibleApplicantExplanation = value;
    }

    /**
     * Gets the value of the fundingOpportunityDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFundingOpportunityDescription() {
        return fundingOpportunityDescription;
    }

    /**
     * Sets the value of the fundingOpportunityDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFundingOpportunityDescription(String value) {
        this.fundingOpportunityDescription = value;
    }

    /**
     * Gets the value of the additionalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalInformation }
     *     
     */
    public AdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Sets the value of the additionalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalInformation }
     *     
     */
    public void setAdditionalInformation(AdditionalInformation value) {
        this.additionalInformation = value;
    }

    /**
     * Gets the value of the grantorContact property.
     * 
     * @return
     *     possible object is
     *     {@link GrantorContact }
     *     
     */
    public GrantorContact getGrantorContact() {
        return grantorContact;
    }

    /**
     * Sets the value of the grantorContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link GrantorContact }
     *     
     */
    public void setGrantorContact(GrantorContact value) {
        this.grantorContact = value;
    }

}
