
package gov.grants.apply.system.grantsopportunity_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ElementType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FundingOpportunityNumber"/&gt;
 *     &lt;enumeration value="AgencyCode"/&gt;
 *     &lt;enumeration value="CFDANumber"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ElementType")
@XmlEnum
public enum ElementType {

    @XmlEnumValue("FundingOpportunityNumber")
    FUNDING_OPPORTUNITY_NUMBER("FundingOpportunityNumber"),
    @XmlEnumValue("AgencyCode")
    AGENCY_CODE("AgencyCode"),
    @XmlEnumValue("CFDANumber")
    CFDA_NUMBER("CFDANumber");
    private final String value;

    ElementType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ElementType fromValue(String v) {
        for (ElementType c: ElementType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
