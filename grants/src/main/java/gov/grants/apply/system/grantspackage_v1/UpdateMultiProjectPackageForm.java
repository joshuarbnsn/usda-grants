
package gov.grants.apply.system.grantspackage_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}AgencyDownloadUrl" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}UpdateOverallPackage" minOccurs="0"/&gt;
 *         &lt;element ref="{http://apply.grants.gov/system/GrantsPackage-V1.0}UpdateSubApplicationGroup" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "agencyDownloadUrl",
    "updateOverallPackage",
    "updateSubApplicationGroup"
})
@XmlRootElement(name = "UpdateMultiProjectPackageForm")
public class UpdateMultiProjectPackageForm {

    @XmlElement(name = "AgencyDownloadUrl")
    protected String agencyDownloadUrl;
    @XmlElement(name = "UpdateOverallPackage")
    protected UpdateOverallPackage updateOverallPackage;
    @XmlElement(name = "UpdateSubApplicationGroup")
    protected List<UpdateSubApplicationGroup> updateSubApplicationGroup;

    /**
     * Gets the value of the agencyDownloadUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyDownloadUrl() {
        return agencyDownloadUrl;
    }

    /**
     * Sets the value of the agencyDownloadUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyDownloadUrl(String value) {
        this.agencyDownloadUrl = value;
    }

    /**
     * Gets the value of the updateOverallPackage property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateOverallPackage }
     *     
     */
    public UpdateOverallPackage getUpdateOverallPackage() {
        return updateOverallPackage;
    }

    /**
     * Sets the value of the updateOverallPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateOverallPackage }
     *     
     */
    public void setUpdateOverallPackage(UpdateOverallPackage value) {
        this.updateOverallPackage = value;
    }

    /**
     * Gets the value of the updateSubApplicationGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateSubApplicationGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateSubApplicationGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateSubApplicationGroup }
     * 
     * 
     */
    public List<UpdateSubApplicationGroup> getUpdateSubApplicationGroup() {
        if (updateSubApplicationGroup == null) {
            updateSubApplicationGroup = new ArrayList<UpdateSubApplicationGroup>();
        }
        return this.updateSubApplicationGroup;
    }

}
