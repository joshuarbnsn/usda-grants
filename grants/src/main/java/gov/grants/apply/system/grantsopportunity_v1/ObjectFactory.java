
package gov.grants.apply.system.grantsopportunity_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.system.grantsopportunity_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ContainsForecast_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "ContainsForecast");
    private final static QName _ContainsSynopsis_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "ContainsSynopsis");
    private final static QName _ContainsRelatedDocuments_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "ContainsRelatedDocuments");
    private final static QName _ContainsPackages_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "ContainsPackages");
    private final static QName _IncludeDeletedOpportunities_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "IncludeDeletedOpportunities");
    private final static QName _IncludeSubAgencies_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "IncludeSubAgencies");
    private final static QName _DeleteTimestamp_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "DeleteTimestamp");
    private final static QName _OppFctSynLastUpdatedTimestamp_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "OppFctSynLastUpdatedTimestamp");
    private final static QName _PackagesLastUpdatedTimestamp_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "PackagesLastUpdatedTimestamp");
    private final static QName _RelatedDocumentsLastUpdatedTimestamp_QNAME = new QName("http://apply.grants.gov/system/GrantsOpportunity-V1.0", "RelatedDocumentsLastUpdatedTimestamp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.system.grantsopportunity_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateOpportunity }
     * 
     */
    public CreateOpportunity createCreateOpportunity() {
        return new CreateOpportunity();
    }

    /**
     * Create an instance of {@link UpdateOpportunity }
     * 
     */
    public UpdateOpportunity createUpdateOpportunity() {
        return new UpdateOpportunity();
    }

    /**
     * Create an instance of {@link ElementFilter }
     * 
     */
    public ElementFilter createElementFilter() {
        return new ElementFilter();
    }

    /**
     * Create an instance of {@link DateRangeFilter }
     * 
     */
    public DateRangeFilter createDateRangeFilter() {
        return new DateRangeFilter();
    }

    /**
     * Create an instance of {@link MultiDateRangeFilter }
     * 
     */
    public MultiDateRangeFilter createMultiDateRangeFilter() {
        return new MultiDateRangeFilter();
    }

    /**
     * Create an instance of {@link OpportunityElementsDetails }
     * 
     */
    public OpportunityElementsDetails createOpportunityElementsDetails() {
        return new OpportunityElementsDetails();
    }

    /**
     * Create an instance of {@link OpportunityDetails }
     * 
     */
    public OpportunityDetails createOpportunityDetails() {
        return new OpportunityDetails();
    }

    /**
     * Create an instance of {@link OpportunitySummary }
     * 
     */
    public OpportunitySummary createOpportunitySummary() {
        return new OpportunitySummary();
    }

    /**
     * Create an instance of {@link DeletedOpportunitySummary }
     * 
     */
    public DeletedOpportunitySummary createDeletedOpportunitySummary() {
        return new DeletedOpportunitySummary();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "ContainsForecast")
    public JAXBElement<YesNoType> createContainsForecast(YesNoType value) {
        return new JAXBElement<YesNoType>(_ContainsForecast_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "ContainsSynopsis")
    public JAXBElement<YesNoType> createContainsSynopsis(YesNoType value) {
        return new JAXBElement<YesNoType>(_ContainsSynopsis_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "ContainsRelatedDocuments")
    public JAXBElement<YesNoType> createContainsRelatedDocuments(YesNoType value) {
        return new JAXBElement<YesNoType>(_ContainsRelatedDocuments_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "ContainsPackages")
    public JAXBElement<YesNoType> createContainsPackages(YesNoType value) {
        return new JAXBElement<YesNoType>(_ContainsPackages_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "IncludeDeletedOpportunities", defaultValue = "N")
    public JAXBElement<YesNoType> createIncludeDeletedOpportunities(YesNoType value) {
        return new JAXBElement<YesNoType>(_IncludeDeletedOpportunities_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "IncludeSubAgencies", defaultValue = "Y")
    public JAXBElement<YesNoType> createIncludeSubAgencies(YesNoType value) {
        return new JAXBElement<YesNoType>(_IncludeSubAgencies_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "DeleteTimestamp")
    public JAXBElement<XMLGregorianCalendar> createDeleteTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DeleteTimestamp_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "OppFctSynLastUpdatedTimestamp")
    public JAXBElement<XMLGregorianCalendar> createOppFctSynLastUpdatedTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OppFctSynLastUpdatedTimestamp_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "PackagesLastUpdatedTimestamp")
    public JAXBElement<XMLGregorianCalendar> createPackagesLastUpdatedTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PackagesLastUpdatedTimestamp_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsOpportunity-V1.0", name = "RelatedDocumentsLastUpdatedTimestamp")
    public JAXBElement<XMLGregorianCalendar> createRelatedDocumentsLastUpdatedTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelatedDocumentsLastUpdatedTimestamp_QNAME, XMLGregorianCalendar.class, null, value);
    }

}
