
package gov.grants.apply.system.grantsforecastsynopsis_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import gov.grants.apply.system.grantscommontypes_v1.YesNoType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.grants.apply.system.grantsforecastsynopsis_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PostingDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "PostingDate");
    private final static QName _FundingInstrument_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "FundingInstrument");
    private final static QName _FundingActivityCategory_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "FundingActivityCategory");
    private final static QName _FundingCategoryExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "FundingCategoryExplanation");
    private final static QName _ExpectedNumberOfAwards_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "ExpectedNumberOfAwards");
    private final static QName _EstimatedFunding_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EstimatedFunding");
    private final static QName _AwardCeiling_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "AwardCeiling");
    private final static QName _AwardFloor_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "AwardFloor");
    private final static QName _ClosingDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "ClosingDate");
    private final static QName _ClosingDateExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "ClosingDateExplanation");
    private final static QName _ArchiveDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "ArchiveDate");
    private final static QName _FundingOpportunityDescription_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "FundingOpportunityDescription");
    private final static QName _EligibleApplicantType_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EligibleApplicantType");
    private final static QName _EligibleApplicantExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EligibleApplicantExplanation");
    private final static QName _CostSharingOrMatchingRequirement_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "CostSharingOrMatchingRequirement");
    private final static QName _Name_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "Name");
    private final static QName _EmailAddress_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EmailAddress");
    private final static QName _EmailDescription_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EmailDescription");
    private final static QName _EstimatedSynopsisPostDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EstimatedSynopsisPostDate");
    private final static QName _EstimatedApplicationDueDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EstimatedApplicationDueDate");
    private final static QName _EstimatedApplicationDueDateExplanation_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EstimatedApplicationDueDateExplanation");
    private final static QName _EstimatedAwardDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EstimatedAwardDate");
    private final static QName _EstimatedProjectStartDate_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "EstimatedProjectStartDate");
    private final static QName _FiscalYear_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "FiscalYear");
    private final static QName _Phone_QNAME = new QName("http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", "Phone");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.grants.apply.system.grantsforecastsynopsis_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SynopsisDetails }
     * 
     */
    public SynopsisDetails createSynopsisDetails() {
        return new SynopsisDetails();
    }

    /**
     * Create an instance of {@link AdditionalInformationDetails }
     * 
     */
    public AdditionalInformationDetails createAdditionalInformationDetails() {
        return new AdditionalInformationDetails();
    }

    /**
     * Create an instance of {@link AgencyContactDetails }
     * 
     */
    public AgencyContactDetails createAgencyContactDetails() {
        return new AgencyContactDetails();
    }

    /**
     * Create an instance of {@link ForecastDetails }
     * 
     */
    public ForecastDetails createForecastDetails() {
        return new ForecastDetails();
    }

    /**
     * Create an instance of {@link GrantorContactDetails }
     * 
     */
    public GrantorContactDetails createGrantorContactDetails() {
        return new GrantorContactDetails();
    }

    /**
     * Create an instance of {@link CreateForecast }
     * 
     */
    public CreateForecast createCreateForecast() {
        return new CreateForecast();
    }

    /**
     * Create an instance of {@link AdditionalInformation }
     * 
     */
    public AdditionalInformation createAdditionalInformation() {
        return new AdditionalInformation();
    }

    /**
     * Create an instance of {@link GrantorContact }
     * 
     */
    public GrantorContact createGrantorContact() {
        return new GrantorContact();
    }

    /**
     * Create an instance of {@link UpdateForecast }
     * 
     */
    public UpdateForecast createUpdateForecast() {
        return new UpdateForecast();
    }

    /**
     * Create an instance of {@link DeleteForecast }
     * 
     */
    public DeleteForecast createDeleteForecast() {
        return new DeleteForecast();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "PostingDate")
    public JAXBElement<String> createPostingDate(String value) {
        return new JAXBElement<String>(_PostingDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "FundingInstrument")
    public JAXBElement<String> createFundingInstrument(String value) {
        return new JAXBElement<String>(_FundingInstrument_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "FundingActivityCategory")
    public JAXBElement<String> createFundingActivityCategory(String value) {
        return new JAXBElement<String>(_FundingActivityCategory_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "FundingCategoryExplanation")
    public JAXBElement<String> createFundingCategoryExplanation(String value) {
        return new JAXBElement<String>(_FundingCategoryExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "ExpectedNumberOfAwards")
    public JAXBElement<String> createExpectedNumberOfAwards(String value) {
        return new JAXBElement<String>(_ExpectedNumberOfAwards_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EstimatedFunding")
    public JAXBElement<String> createEstimatedFunding(String value) {
        return new JAXBElement<String>(_EstimatedFunding_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "AwardCeiling")
    public JAXBElement<String> createAwardCeiling(String value) {
        return new JAXBElement<String>(_AwardCeiling_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "AwardFloor")
    public JAXBElement<String> createAwardFloor(String value) {
        return new JAXBElement<String>(_AwardFloor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "ClosingDate")
    public JAXBElement<String> createClosingDate(String value) {
        return new JAXBElement<String>(_ClosingDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "ClosingDateExplanation")
    public JAXBElement<String> createClosingDateExplanation(String value) {
        return new JAXBElement<String>(_ClosingDateExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "ArchiveDate")
    public JAXBElement<String> createArchiveDate(String value) {
        return new JAXBElement<String>(_ArchiveDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "FundingOpportunityDescription")
    public JAXBElement<String> createFundingOpportunityDescription(String value) {
        return new JAXBElement<String>(_FundingOpportunityDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EligibleApplicantType")
    public JAXBElement<String> createEligibleApplicantType(String value) {
        return new JAXBElement<String>(_EligibleApplicantType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EligibleApplicantExplanation")
    public JAXBElement<String> createEligibleApplicantExplanation(String value) {
        return new JAXBElement<String>(_EligibleApplicantExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link YesNoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "CostSharingOrMatchingRequirement")
    public JAXBElement<YesNoType> createCostSharingOrMatchingRequirement(YesNoType value) {
        return new JAXBElement<YesNoType>(_CostSharingOrMatchingRequirement_QNAME, YesNoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "Name")
    public JAXBElement<String> createName(String value) {
        return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EmailAddress")
    public JAXBElement<String> createEmailAddress(String value) {
        return new JAXBElement<String>(_EmailAddress_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EmailDescription")
    public JAXBElement<String> createEmailDescription(String value) {
        return new JAXBElement<String>(_EmailDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EstimatedSynopsisPostDate")
    public JAXBElement<String> createEstimatedSynopsisPostDate(String value) {
        return new JAXBElement<String>(_EstimatedSynopsisPostDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EstimatedApplicationDueDate")
    public JAXBElement<String> createEstimatedApplicationDueDate(String value) {
        return new JAXBElement<String>(_EstimatedApplicationDueDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EstimatedApplicationDueDateExplanation")
    public JAXBElement<String> createEstimatedApplicationDueDateExplanation(String value) {
        return new JAXBElement<String>(_EstimatedApplicationDueDateExplanation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EstimatedAwardDate")
    public JAXBElement<String> createEstimatedAwardDate(String value) {
        return new JAXBElement<String>(_EstimatedAwardDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "EstimatedProjectStartDate")
    public JAXBElement<String> createEstimatedProjectStartDate(String value) {
        return new JAXBElement<String>(_EstimatedProjectStartDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "FiscalYear")
    public JAXBElement<String> createFiscalYear(String value) {
        return new JAXBElement<String>(_FiscalYear_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apply.grants.gov/system/GrantsForecastSynopsis-V1.0", name = "Phone")
    public JAXBElement<String> createPhone(String value) {
        return new JAXBElement<String>(_Phone_QNAME, String.class, null, value);
    }

}
