package gov.usda.grants.pega.transformers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.grants.apply.services.agencywebservices_v2.ManagePackageResponse;
import gov.grants.apply.system.agencymanagepackage_v1.CreatePackageResult;
import gov.grants.apply.system.agencymanagepackage_v1.DeletePackageResult;
import gov.grants.apply.system.agencymanagepackage_v1.UpdatePackageResult;
import gov.usda.grants.pega.service.DTMPackRes;
import gov.usda.grants.pega.service.DTMPackResult;
import gov.usda.grants.pega.service.DTMPackRes.MPackRes;
import gov.usda.grants.pega.service.DTMPackResult.ErrorDetails;

public class ManagePackageResponseTransformer extends AbstractMessageTransformer 
{
	protected final Logger log = LogManager.getLogger(getClass());
	private static final String PROCESS_MESSAGE = "Status for opportunity number %s: %s";
	private static final String STATUS_CODE = "Status Code: %s";
	private static final String STATUS_MESSAGE = "Status Message: %s";
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException 
	{
		ManagePackageResponse inResponse = (ManagePackageResponse) message.getPayload();
		return transformResponse(inResponse);
		
	}
	
	public DTMPackRes transformResponse(ManagePackageResponse inResponse) {
		
		DTMPackRes responseContainer = new DTMPackRes();
		MPackRes outResponse = new MPackRes();
		
		
		outResponse.setUserID(inResponse.getUserID());
		outResponse.setFundingOpportunityNumber(inResponse.getFundingOpportunityNumber());
		outResponse.setSuccess(Boolean.toString(inResponse.isSuccess()));
		
		log.info(String.format(PROCESS_MESSAGE, 
				inResponse.getFundingOpportunityNumber(), 
				Boolean.toString(inResponse.isSuccess())));
		
		if (inResponse.getCreatePackageResult() != null)
		{
			
			for (CreatePackageResult inCreatePackage : inResponse.getCreatePackageResult())
			{
				DTMPackResult outCreatePackage = new DTMPackResult();
				outCreatePackage.setCFDANumber(inCreatePackage.getCFDANumber());
				outCreatePackage.setCompetitionID(inCreatePackage.getCompetitionID());
				
				if (inCreatePackage.getErrorDetails() != null)
				{
					ErrorDetails errorDetails = new ErrorDetails();
					errorDetails.setCode(inCreatePackage.getErrorDetails().getCode());
					errorDetails.setMessage(inCreatePackage.getErrorDetails().getMessage());
					outCreatePackage.setErrorDetails(errorDetails);
					
					log.error("Create Package Results");
					log.error(String.format(STATUS_CODE, errorDetails.getCode()));
					log.error(String.format(STATUS_MESSAGE, errorDetails.getMessage()));
				}
				
				outResponse.getCreatePackRes().add(outCreatePackage);
			}
		}
		
		if (inResponse.getDeletePackageResult() != null)
		{
			for (DeletePackageResult inDeletePackage : inResponse.getDeletePackageResult())
			{
				DTMPackResult outDeletePackage = new DTMPackResult();
				outDeletePackage.setCFDANumber(inDeletePackage.getCFDANumber());
				outDeletePackage.setCompetitionID(inDeletePackage.getCompetitionID());
				
				if (inDeletePackage.getNotificationCount() != null ) 
				{
					outDeletePackage.setNotificationCount(inDeletePackage.getNotificationCount().toString());
				}
				
				
				if (inDeletePackage.getErrorDetails() != null)
				{
					ErrorDetails errorDetails = new ErrorDetails();
					errorDetails.setCode(inDeletePackage.getErrorDetails().getCode());
					errorDetails.setMessage(inDeletePackage.getErrorDetails().getMessage());
					outDeletePackage.setErrorDetails(errorDetails);
					
					log.error("Delete Package Results");
					log.error(String.format(STATUS_CODE, errorDetails.getCode()));
					log.error(String.format(STATUS_MESSAGE, errorDetails.getMessage()));
				}
				
				outResponse.getDeletePackRes().add(outDeletePackage);
			}
		}
		
		if (inResponse.getUpdatePackageResult() != null)
		{
			for (UpdatePackageResult inUpdatePackage : inResponse.getUpdatePackageResult())
			{
				DTMPackResult outUpdatePackage = new DTMPackResult();
				outUpdatePackage.setCFDANumber(inUpdatePackage.getCFDANumber());
				outUpdatePackage.setCompetitionID(inUpdatePackage.getCompetitionID());
				
				if (inUpdatePackage.getNotificationCount() != null )
				{
					outUpdatePackage.setNotificationCount(inUpdatePackage.getNotificationCount().toString());
				}
				

				if (inUpdatePackage.getErrorDetails() != null)
				{
					ErrorDetails errorDetails = new ErrorDetails();
					errorDetails.setCode(inUpdatePackage.getErrorDetails().getCode());
					errorDetails.setMessage(inUpdatePackage.getErrorDetails().getMessage());
					outUpdatePackage.setErrorDetails(errorDetails);
					
					log.error("Update Package Results");
					log.error(String.format(STATUS_CODE, errorDetails.getCode()));
					log.error(String.format(STATUS_MESSAGE, errorDetails.getMessage()));
				}
				
				outResponse.getUpdatePackRes().add(outUpdatePackage);
			}
		}
		
		responseContainer.setMPackRes(outResponse);
		return responseContainer;
	}
}
