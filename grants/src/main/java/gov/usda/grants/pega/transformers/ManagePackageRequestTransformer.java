package gov.usda.grants.pega.transformers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.grants.apply.services.agencywebservices_v2.ManagePackageRequest;
import gov.grants.apply.system.agencymanagepackage_v1.CreateOpportunityInfo;
import gov.grants.apply.system.agencymanagepackage_v1.CreatePackageInfo;
import gov.grants.apply.system.agencymanagepackage_v1.DeletePackageInfo;
import gov.grants.apply.system.agencymanagepackage_v1.EmailInfo;
import gov.grants.apply.system.agencymanagepackage_v1.UpdatePackageInfo;
import gov.grants.apply.system.grantscommonelements_v1.CompetitionInfo;
import gov.grants.apply.system.grantscommonelements_v1.InstructionFileInfo;
import gov.grants.apply.system.grantscommontypes_v1.OpportunityCategoryType;
import gov.usda.grants.pega.service.DTMPackInfo;
import gov.usda.grants.pega.service.DTMPackReq;
import gov.usda.grants.pega.service.DTMPackInfo.InstFileInfo;
import gov.usda.grants.pega.service.DTMPackReq.MPackReq.DeletePackInfo;

public class ManagePackageRequestTransformer  extends AbstractMessageTransformer 
{
	protected final Logger log = LogManager.getLogger(getClass());
	private static final String PROCESS_MESSAGE = "Processing message for user id: %s and opportunity number: %s";
	private String filePath;
	public String getFilePath()
	{
		return filePath;
	}
	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException 
	{
		
		DTMPackReq inContainer = (DTMPackReq) message.getPayload();
		
		return transformRequest(inContainer);
	}
	
	public ManagePackageRequest transformRequest(DTMPackReq inContainer) {
		
		DTMPackReq.MPackReq in = inContainer.getMPackReq();
		ManagePackageRequest out = new ManagePackageRequest();
		
		out.setUserID(in.getUserID());
		out.setFundingOpportunityNumber(in.getFundingOpportunityNumber());
		
		log.info(String.format(PROCESS_MESSAGE, in.getUserID(), in.getFundingOpportunityNumber()));
		
		if (in.getCreateOppInfo() != null) 
		{		
			mapCreateOpportunityInfo(in, out);
		}
		
		if (in.getCreatePackInfo() != null) 
		{
			mapCreatePackageInfo(in, out);
		}
		
		if (in.getUpdatePackInfo() != null)
		{
			mapUpdatePackageInfo(in, out);
		}
		
		if (in.getDeletePackInfo() != null)
		{
			mapDeletePackageInfo(in, out);
		}
		
		return out;
	}
	
	private void mapCreateOpportunityInfo(DTMPackReq.MPackReq  in, ManagePackageRequest out)
	{
		CreateOpportunityInfo createOp = new CreateOpportunityInfo();
		createOp.setFundingOpportunityTitle(in.getCreateOppInfo().getFundingOpportunityTitle());
		
		OpportunityCategoryType type = mapOpportunityCategoryType(in.getCreateOppInfo().getOpportunityCategory());
		createOp.setOpportunityCategory(type);
		
		if (!StringUtils.isEmpty(in.getCreateOppInfo().getOpportunityCategoryExplanation()))
		{
			createOp.setOpportunityCategoryExplanation(in.getCreateOppInfo().getOpportunityCategoryExplanation());
		}
		
		if (in.getCreateOppInfo().getCFDANumber() != null) 
		{
			for ( DTMPackReq.MPackReq.CreateOppInfo.CFDANumber cfdaNumber :
				in.getCreateOppInfo().getCFDANumber()) 
			{
				createOp.getCFDANumber().add(cfdaNumber.getValue());
			}
		}
		
		out.setCreateOpportunityInfo(createOp);
	}
	
	private void mapCreatePackageInfo(DTMPackReq.MPackReq  in, ManagePackageRequest out) 
	{
		for (DTMPackInfo inCreatePackage : in.getCreatePackInfo())
		{
			CreatePackageInfo outCreatePackage = new CreatePackageInfo();
			outCreatePackage.setAgencyContactInfo(inCreatePackage.getAgencyContactInfo());
			outCreatePackage.setClosingDate(inCreatePackage.getClosingDate());
			
			if (!StringUtils.isEmpty(inCreatePackage.getCFDANumber())) 
			{
				outCreatePackage.setCFDANumber(inCreatePackage.getCFDANumber());
			}
			
			if ((inCreatePackage.getCompInfo() != null) &&
					(!StringUtils.isEmpty(inCreatePackage.getCompInfo().getCompetitionID()) ||
							!StringUtils.isEmpty(inCreatePackage.getCompInfo().getCompetitionTitle())
					) 
				)
			{
				CompetitionInfo outCompetitionInfo = new CompetitionInfo();
				
				if (inCreatePackage.getCompInfo().getCompetitionID() == null)
				{
					outCompetitionInfo.setCompetitionID("");
				}
				else 
				{
					outCompetitionInfo.setCompetitionID(inCreatePackage.getCompInfo().getCompetitionID());
				}
				
				if (inCreatePackage.getCompInfo().getCompetitionTitle() == null)
				{
					outCompetitionInfo.setCompetitionTitle("");
				}
				else 
				{
					outCompetitionInfo.setCompetitionTitle(inCreatePackage.getCompInfo().getCompetitionTitle());
				}
				
				outCreatePackage.setCompetitionInfo(outCompetitionInfo);
			}
			
			outCreatePackage.setElectronicSignatureRequired(inCreatePackage.getElectronicSignatureRequired());
			outCreatePackage.setExpectedApplicationCount(inCreatePackage.getExpectedApplicationCount());
			
			if (!StringUtils.isEmpty(inCreatePackage.getExpectedApplicationSizeMB()))
			{
				outCreatePackage.setExpectedApplicationSizeMB(inCreatePackage.getExpectedApplicationSizeMB());
			}
			
			if (!StringUtils.isEmpty(inCreatePackage.getGracePeriodDays()))
			{
				outCreatePackage.setGracePeriodDays(inCreatePackage.getGracePeriodDays());
			}
			
			if (inCreatePackage.getInstFileInfo() != null)
			{
				InstructionFileInfo outInstructionFileInfo = new InstructionFileInfo();
				outInstructionFileInfo.setFileContentId(inCreatePackage.getInstFileInfo().getFileContentId());
				outInstructionFileInfo.setFileDataHandler(mapDataHandler(inCreatePackage.getInstFileInfo()));
				outInstructionFileInfo.setFileExtension(inCreatePackage.getInstFileInfo().getFileExtension());
				outInstructionFileInfo.setFileName(inCreatePackage.getInstFileInfo().getFileName());
				
				outCreatePackage.setInstructionFileInfo(outInstructionFileInfo);
			}
			
			if (!StringUtils.isEmpty(inCreatePackage.getOpeningDate()))
			{
				outCreatePackage.setOpeningDate(inCreatePackage.getOpeningDate());	
			}
			
			outCreatePackage.setOpenToApplicant(inCreatePackage.getOpenToApplicant());
			outCreatePackage.setTemplateName(inCreatePackage.getTemplateName());
			
			//outCreatePackage.setInstructionFileInfo();
			out.getCreatePackageInfo().add(outCreatePackage);
		}
	}
	
	private void mapUpdatePackageInfo(DTMPackReq.MPackReq  in, ManagePackageRequest out) 
	{
		for (DTMPackInfo inUpdatePackage : in.getUpdatePackInfo())
		{
			UpdatePackageInfo outUpdatePackage = new UpdatePackageInfo();
			
			if (!StringUtils.isEmpty(inUpdatePackage.getAgencyContactInfo()))
			{
				outUpdatePackage.setAgencyContactInfo(inUpdatePackage.getAgencyContactInfo());
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getCFDANumber()))
			{
				outUpdatePackage.setCFDANumber(inUpdatePackage.getCFDANumber());
			}
			
			//required
			outUpdatePackage.setClosingDate(inUpdatePackage.getClosingDate());
			
			if (inUpdatePackage.getCompInfo() != null)
			{
				if (!StringUtils.isEmpty(inUpdatePackage.getCompInfo().getCompetitionID()))
				{
					outUpdatePackage.setCompetitionID(inUpdatePackage.getCompInfo().getCompetitionID());
				}
				
				if (!StringUtils.isEmpty(inUpdatePackage.getCompInfo().getCompetitionTitle()))
				{
					outUpdatePackage.setCompetitionTitle(inUpdatePackage.getCompInfo().getCompetitionTitle());
				}
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getElectronicSignatureRequired()))
			{
				outUpdatePackage.setElectronicSignatureRequired(inUpdatePackage.getElectronicSignatureRequired());
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getExpectedApplicationCount()))
			{
				outUpdatePackage.setExpectedApplicationCount(inUpdatePackage.getExpectedApplicationCount());
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getExpectedApplicationSizeMB()))
			{
				outUpdatePackage.setExpectedApplicationSizeMB(inUpdatePackage.getExpectedApplicationSizeMB());
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getGracePeriodDays()))
			{
				outUpdatePackage.setGracePeriodDays(inUpdatePackage.getGracePeriodDays());
			}
			
			if (inUpdatePackage.getInstFileInfo() != null)
			{
				InstructionFileInfo outInstructionFileInfo = new InstructionFileInfo();
				outInstructionFileInfo.setFileContentId(inUpdatePackage.getInstFileInfo().getFileContentId());
				outInstructionFileInfo.setFileDataHandler(mapDataHandler(inUpdatePackage.getInstFileInfo()));
				outInstructionFileInfo.setFileExtension(inUpdatePackage.getInstFileInfo().getFileExtension());
				outInstructionFileInfo.setFileName(inUpdatePackage.getInstFileInfo().getFileName());
				
				outUpdatePackage.setInstructionFileInfo(outInstructionFileInfo);
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getOpeningDate()))
			{
				outUpdatePackage.setOpeningDate(inUpdatePackage.getOpeningDate());
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getOpenToApplicant()))
			{
				outUpdatePackage.setOpenToApplicant(inUpdatePackage.getOpenToApplicant());
			}
			
			if (!StringUtils.isEmpty(inUpdatePackage.getTemplateName()))
			{
				outUpdatePackage.setTemplateName(inUpdatePackage.getTemplateName());
			}
			
			if (inUpdatePackage.getEmailInfo() != null)
			{
				EmailInfo outEmailInfo = new EmailInfo();
				outEmailInfo.setModificationComments(inUpdatePackage.getEmailInfo().getModificationComments());
				outEmailInfo.setSendChangeNotificationEmail(inUpdatePackage.getEmailInfo().getSendChangeNotificationEmail());
				outUpdatePackage.setEmailInfo(outEmailInfo);
			}
			
			//outUpdatePackage.setInstructionFileInfo();
			out.getUpdatePackageInfo().add(outUpdatePackage);
		}
	}
	
	private void mapDeletePackageInfo(DTMPackReq.MPackReq  in, ManagePackageRequest out) 
	{
		for (DeletePackInfo inDeletePackage : in.getDeletePackInfo())
		{
			DeletePackageInfo outDeletePackage = new DeletePackageInfo();
			
			if (!StringUtils.isEmpty(inDeletePackage.getCFDANumber()))
			{
				outDeletePackage.setCFDANumber(inDeletePackage.getCFDANumber());
			}
			
			if (!StringUtils.isEmpty(inDeletePackage.getCompetitionID()))
			{
				outDeletePackage.setCompetitionID(inDeletePackage.getCompetitionID());
			}
			
			out.getDeletePackageInfo().add(outDeletePackage);
		}
	}
	
	private OpportunityCategoryType mapOpportunityCategoryType(String s) 
	{
		for (OpportunityCategoryType type : OpportunityCategoryType.values()) 
		{
			if (type.toString().equalsIgnoreCase(s)) 
			{
				return type;
			}
		}
		
		throw new IllegalArgumentException( s );
	}
	
	private DataHandler mapDataHandler(InstFileInfo  info)
	{
		File file = saveFile (this.getFilePath(), info.getFileName(), info.getFileDataHandler());
		
		DataHandler fileDataHandler = null;
		
		if (file.exists() && file.isFile()) {			
			DataSource source = new FileDataSource(file);
			fileDataHandler = new DataHandler(source);
		}
		
		return fileDataHandler;
	}
	
	private File saveFile (String fileDir, String fileName,  byte[] content)
	{
		File f = new File(fileDir + File.separator + fileName);
		
	    // Create tmp upload folder
	    f.getParentFile().mkdirs();    

	    try 
	    {
	        writeByteArraysToFile(f, content);
		} 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    } 
	    
	    return f;
	}
	
	private void writeByteArraysToFile(File file, byte[] content) throws IOException 
	{	 
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
        writer.write(content);
        writer.flush();
        writer.close();
 
    }
}
