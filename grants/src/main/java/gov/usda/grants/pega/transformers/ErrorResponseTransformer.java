package gov.usda.grants.pega.transformers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import gov.usda.grants.pega.service.DTMPackInfo;
import gov.usda.grants.pega.service.DTMPackReq;
import gov.usda.grants.pega.service.DTMPackReq.MPackReq.DeletePackInfo;
import gov.usda.grants.pega.service.DTMPackRes;
import gov.usda.grants.pega.service.DTMPackResult;
import gov.usda.grants.pega.service.DTMPackResult.ErrorDetails;
import gov.usda.grants.pega.service.DTMPackRes.MPackRes;

public class ErrorResponseTransformer extends AbstractMessageTransformer 
{
	protected final Logger log = LogManager.getLogger(getClass());
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException 
	{
		DTMPackReq inContainer = (DTMPackReq) message.getInvocationProperty("oPayload");
		DTMPackReq.MPackReq inRequest = inContainer.getMPackReq();
		
		DTMPackRes responseContainer = new DTMPackRes();
		MPackRes outResponse = new MPackRes();
		
		outResponse.setUserID(inRequest.getUserID());
		outResponse.setFundingOpportunityNumber(inRequest.getFundingOpportunityNumber());
		outResponse.setSuccess(Boolean.FALSE.toString());
		
		
		if (inRequest.getCreatePackInfo() != null)
		{
			List<DTMPackResult> createPackRes = new ArrayList<DTMPackResult>();
			
			for (DTMPackInfo inPackageInfo : inRequest.getCreatePackInfo())
			{
				DTMPackResult outPackageResult = new DTMPackResult();
				outPackageResult.setCFDANumber(inPackageInfo.getCFDANumber());
				ErrorDetails errorDetails = new ErrorDetails();
				errorDetails.setCode("99");
				errorDetails.setMessage(message.getExceptionPayload().getMessage());
				outPackageResult.setErrorDetails(errorDetails);
				createPackRes.add(outPackageResult);
			}
			outResponse.setCreatePackRes(createPackRes);
		}
		
		if (inRequest.getDeletePackInfo() != null)
		{
			List<DTMPackResult>  deletePackRes = new ArrayList<DTMPackResult>();
			
			for (DeletePackInfo inPackageInfo : inRequest.getDeletePackInfo())
			{
				DTMPackResult outPackageResult = new DTMPackResult();
				outPackageResult.setCFDANumber(inPackageInfo.getCFDANumber());
				ErrorDetails errorDetails = new ErrorDetails();
				errorDetails.setCode("99");
				errorDetails.setMessage(message.getExceptionPayload().getMessage());
				outPackageResult.setErrorDetails(errorDetails);
				deletePackRes.add(outPackageResult);
				
			}
			
			outResponse.setDeletePackRes(deletePackRes);
		}
		
		if (inRequest.getUpdatePackInfo() != null)
		{
			List<DTMPackResult>  updatePackRes = new ArrayList<DTMPackResult>();
			
			for (DTMPackInfo inPackageInfo : inRequest.getUpdatePackInfo())
			{
				DTMPackResult outPackageResult = new DTMPackResult();
				outPackageResult.setCFDANumber(inPackageInfo.getCFDANumber());
				ErrorDetails errorDetails = new ErrorDetails();
				errorDetails.setCode("99");
				errorDetails.setMessage(message.getExceptionPayload().getMessage());
				outPackageResult.setErrorDetails(errorDetails);
				updatePackRes.add(outPackageResult);
				
			}
			
			outResponse.setUpdatePackRes(updatePackRes);
		}
		
		responseContainer.setMPackRes(outResponse);
		return responseContainer;
	}
}
