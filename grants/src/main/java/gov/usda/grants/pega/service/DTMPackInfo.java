
package gov.usda.grants.pega.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Common Data Type for PEGA GRANTS.GOV Manage Package Request
 * 
 * <p>Java class for DT_MPackInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_MPackInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CFDANumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CompetitionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CompetitionTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ElectronicSignatureRequired">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Y"/>
 *               &lt;enumeration value="N"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ExpectedApplicationCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExpectedApplicationSizeMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OpeningDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GracePeriodDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyContactInfo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TemplateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InstFileInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FileGuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FileExtension" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FileContentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FileDataHandler" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OpenToApplicant">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ORGANIZATIONS_ONLY"/>
 *               &lt;enumeration value="INDIVIDUALS_ONLY"/>
 *               &lt;enumeration value="INDIVIDUALS_AND_ORGANIZATIONS"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EmailInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SendChangeNotificationEmail" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="Y"/>
 *                         &lt;enumeration value="N"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ModificationComments">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="2000"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_MPackInfo", namespace = "http://fmmi.usda.gov/GRANTSGOV/Common", propOrder = {
    "cfdaNumber",
    "compInfo",
    "electronicSignatureRequired",
    "expectedApplicationCount",
    "expectedApplicationSizeMB",
    "openingDate",
    "closingDate",
    "gracePeriodDays",
    "agencyContactInfo",
    "templateName",
    "instFileInfo",
    "openToApplicant",
    "emailInfo"
})
public class DTMPackInfo {

    @XmlElement(name = "CFDANumber")
    protected String cfdaNumber;
    @XmlElement(name = "CompInfo")
    protected DTMPackInfo.CompInfo compInfo;
    @XmlElement(name = "ElectronicSignatureRequired", required = true)
    protected String electronicSignatureRequired;
    @XmlElement(name = "ExpectedApplicationCount", required = true)
    protected String expectedApplicationCount;
    @XmlElement(name = "ExpectedApplicationSizeMB")
    protected String expectedApplicationSizeMB;
    @XmlElement(name = "OpeningDate")
    protected String openingDate;
    @XmlElement(name = "ClosingDate", required = true)
    protected String closingDate;
    @XmlElement(name = "GracePeriodDays")
    protected String gracePeriodDays;
    @XmlElement(name = "AgencyContactInfo", required = true)
    protected String agencyContactInfo;
    @XmlElement(name = "TemplateName", required = true)
    protected String templateName;
    @XmlElement(name = "InstFileInfo", required = true)
    protected DTMPackInfo.InstFileInfo instFileInfo;
    @XmlElement(name = "OpenToApplicant", required = true)
    protected String openToApplicant;
    @XmlElement(name = "EmailInfo")
    protected DTMPackInfo.EmailInfo emailInfo;

    /**
     * Gets the value of the cfdaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFDANumber() {
        return cfdaNumber;
    }

    /**
     * Sets the value of the cfdaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFDANumber(String value) {
        this.cfdaNumber = value;
    }

    /**
     * Gets the value of the compInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DTMPackInfo.CompInfo }
     *     
     */
    public DTMPackInfo.CompInfo getCompInfo() {
        return compInfo;
    }

    /**
     * Sets the value of the compInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTMPackInfo.CompInfo }
     *     
     */
    public void setCompInfo(DTMPackInfo.CompInfo value) {
        this.compInfo = value;
    }

    /**
     * Gets the value of the electronicSignatureRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElectronicSignatureRequired() {
        return electronicSignatureRequired;
    }

    /**
     * Sets the value of the electronicSignatureRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElectronicSignatureRequired(String value) {
        this.electronicSignatureRequired = value;
    }

    /**
     * Gets the value of the expectedApplicationCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationCount() {
        return expectedApplicationCount;
    }

    /**
     * Sets the value of the expectedApplicationCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationCount(String value) {
        this.expectedApplicationCount = value;
    }

    /**
     * Gets the value of the expectedApplicationSizeMB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedApplicationSizeMB() {
        return expectedApplicationSizeMB;
    }

    /**
     * Sets the value of the expectedApplicationSizeMB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedApplicationSizeMB(String value) {
        this.expectedApplicationSizeMB = value;
    }

    /**
     * Gets the value of the openingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpeningDate() {
        return openingDate;
    }

    /**
     * Sets the value of the openingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpeningDate(String value) {
        this.openingDate = value;
    }

    /**
     * Gets the value of the closingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the value of the closingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosingDate(String value) {
        this.closingDate = value;
    }

    /**
     * Gets the value of the gracePeriodDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGracePeriodDays() {
        return gracePeriodDays;
    }

    /**
     * Sets the value of the gracePeriodDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGracePeriodDays(String value) {
        this.gracePeriodDays = value;
    }

    /**
     * Gets the value of the agencyContactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyContactInfo() {
        return agencyContactInfo;
    }

    /**
     * Sets the value of the agencyContactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyContactInfo(String value) {
        this.agencyContactInfo = value;
    }

    /**
     * Gets the value of the templateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Sets the value of the templateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateName(String value) {
        this.templateName = value;
    }

    /**
     * Gets the value of the instFileInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DTMPackInfo.InstFileInfo }
     *     
     */
    public DTMPackInfo.InstFileInfo getInstFileInfo() {
        return instFileInfo;
    }

    /**
     * Sets the value of the instFileInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTMPackInfo.InstFileInfo }
     *     
     */
    public void setInstFileInfo(DTMPackInfo.InstFileInfo value) {
        this.instFileInfo = value;
    }

    /**
     * Gets the value of the openToApplicant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenToApplicant() {
        return openToApplicant;
    }

    /**
     * Sets the value of the openToApplicant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenToApplicant(String value) {
        this.openToApplicant = value;
    }

    /**
     * Gets the value of the emailInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DTMPackInfo.EmailInfo }
     *     
     */
    public DTMPackInfo.EmailInfo getEmailInfo() {
        return emailInfo;
    }

    /**
     * Sets the value of the emailInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTMPackInfo.EmailInfo }
     *     
     */
    public void setEmailInfo(DTMPackInfo.EmailInfo value) {
        this.emailInfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CompetitionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CompetitionTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "competitionID",
        "competitionTitle"
    })
    public static class CompInfo {

        @XmlElement(name = "CompetitionID")
        protected String competitionID;
        @XmlElement(name = "CompetitionTitle")
        protected String competitionTitle;

        /**
         * Gets the value of the competitionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompetitionID() {
            return competitionID;
        }

        /**
         * Sets the value of the competitionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompetitionID(String value) {
            this.competitionID = value;
        }

        /**
         * Gets the value of the competitionTitle property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompetitionTitle() {
            return competitionTitle;
        }

        /**
         * Sets the value of the competitionTitle property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompetitionTitle(String value) {
            this.competitionTitle = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SendChangeNotificationEmail" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="Y"/>
     *               &lt;enumeration value="N"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="ModificationComments">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="2000"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendChangeNotificationEmail",
        "modificationComments"
    })
    public static class EmailInfo {

        @XmlElement(name = "SendChangeNotificationEmail")
        protected String sendChangeNotificationEmail;
        @XmlElement(name = "ModificationComments", required = true)
        protected String modificationComments;

        /**
         * Gets the value of the sendChangeNotificationEmail property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendChangeNotificationEmail() {
            return sendChangeNotificationEmail;
        }

        /**
         * Sets the value of the sendChangeNotificationEmail property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendChangeNotificationEmail(String value) {
            this.sendChangeNotificationEmail = value;
        }

        /**
         * Gets the value of the modificationComments property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModificationComments() {
            return modificationComments;
        }

        /**
         * Sets the value of the modificationComments property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModificationComments(String value) {
            this.modificationComments = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FileGuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FileExtension" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FileContentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FileDataHandler" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fileGuid",
        "fileName",
        "fileExtension",
        "fileContentId",
        "fileDataHandler"
    })
    public static class InstFileInfo {

        @XmlElement(name = "FileGuid")
        protected String fileGuid;
        @XmlElement(name = "FileName", required = true)
        protected String fileName;
        @XmlElement(name = "FileExtension", required = true)
        protected String fileExtension;
        @XmlElement(name = "FileContentId", required = true)
        protected String fileContentId;
        @XmlElement(name = "FileDataHandler", required = true)
        protected byte[] fileDataHandler;

        /**
         * Gets the value of the fileGuid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileGuid() {
            return fileGuid;
        }

        /**
         * Sets the value of the fileGuid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileGuid(String value) {
            this.fileGuid = value;
        }

        /**
         * Gets the value of the fileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileName() {
            return fileName;
        }

        /**
         * Sets the value of the fileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileName(String value) {
            this.fileName = value;
        }

        /**
         * Gets the value of the fileExtension property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileExtension() {
            return fileExtension;
        }

        /**
         * Sets the value of the fileExtension property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileExtension(String value) {
            this.fileExtension = value;
        }

        /**
         * Gets the value of the fileContentId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileContentId() {
            return fileContentId;
        }

        /**
         * Sets the value of the fileContentId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileContentId(String value) {
            this.fileContentId = value;
        }

        /**
         * Gets the value of the fileDataHandler property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getFileDataHandler() {
            return fileDataHandler;
        }

        /**
         * Sets the value of the fileDataHandler property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setFileDataHandler(byte[] value) {
            this.fileDataHandler = value;
        }

    }

}
