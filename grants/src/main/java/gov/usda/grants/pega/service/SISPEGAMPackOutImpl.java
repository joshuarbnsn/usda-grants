package gov.usda.grants.pega.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import gov.grants.apply.services.agencywebservices_v2.ManagePackageRequest;
import gov.grants.apply.services.agencywebservices_v2.ManagePackageResponse;
import gov.grants.apply.services.agencywebservices_v2_0.ManagePackageClient;
import gov.usda.grants.pega.transformers.ManagePackageRequestTransformer;
import gov.usda.grants.pega.transformers.ManagePackageResponseTransformer;

public class SISPEGAMPackOutImpl implements SISPEGAMPackOut {
	protected final Logger log = LogManager.getLogger(getClass());
	
	private String wsdlFilePath;
	private String manageTls;
	
	public String getManageTls() {
		return manageTls;
	}

	public void setManageTls(String manageTls) {
		this.manageTls = manageTls;
	}

	public String getWsdlFilePath() {
		return wsdlFilePath;
	}

	public void setWsdlFilePath(String wsdlFilePath) {
		this.wsdlFilePath = wsdlFilePath;
	}
	
	@Override
	public DTMPackRes sisPEGAMPackOut(DTMPackReq mtMPackReq) {
		//ManagePackageRequestTransformer requestTransformer = new ManagePackageRequestTransformer();
		//ManagePackageRequest grantsRequest = requestTransformer.transformRequest(mtMPackReq);
		
		//ManagePackageClient client = new ManagePackageClient();
		//ManagePackageResponse grantsResponse = client.ManagePackage(grantsRequest, wsdlFilePath, manageTls);
		
		//ManagePackageResponseTransformer responseTransformer = new ManagePackageResponseTransformer();
		//DTMPackRes pegaResponse = responseTransformer.transformResponse(grantsResponse);
		
		//return pegaResponse;
		return null;
	}
}
