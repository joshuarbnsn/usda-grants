
package gov.usda.grants.pega.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.usda.grants.pega.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTMPackRes_QNAME = new QName("http://fmmi.usda.gov/GRANTSGOV//ManagePackage", "MT_MPack_Res");
    private final static QName _MTMPackReq_QNAME = new QName("http://fmmi.usda.gov/GRANTSGOV//ManagePackage", "MT_MPack_Req");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.usda.grants.pega.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTMPackResult }
     * 
     */
    public DTMPackResult createDTMPackResult() {
        return new DTMPackResult();
    }

    /**
     * Create an instance of {@link DTMPackInfo }
     * 
     */
    public DTMPackInfo createDTMPackInfo() {
        return new DTMPackInfo();
    }

    /**
     * Create an instance of {@link DTMPackReq }
     * 
     */
    public DTMPackReq createDTMPackReq() {
        return new DTMPackReq();
    }

    /**
     * Create an instance of {@link DTMPackReq.MPackReq }
     * 
     */
    public DTMPackReq.MPackReq createDTMPackReqMPackReq() {
        return new DTMPackReq.MPackReq();
    }

    /**
     * Create an instance of {@link DTMPackReq.MPackReq.CreateOppInfo }
     * 
     */
    public DTMPackReq.MPackReq.CreateOppInfo createDTMPackReqMPackReqCreateOppInfo() {
        return new DTMPackReq.MPackReq.CreateOppInfo();
    }

    /**
     * Create an instance of {@link DTMPackRes }
     * 
     */
    public DTMPackRes createDTMPackRes() {
        return new DTMPackRes();
    }

    /**
     * Create an instance of {@link DTMPackResult.ErrorDetails }
     * 
     */
    public DTMPackResult.ErrorDetails createDTMPackResultErrorDetails() {
        return new DTMPackResult.ErrorDetails();
    }

    /**
     * Create an instance of {@link DTMPackInfo.CompInfo }
     * 
     */
    public DTMPackInfo.CompInfo createDTMPackInfoCompInfo() {
        return new DTMPackInfo.CompInfo();
    }

    /**
     * Create an instance of {@link DTMPackInfo.InstFileInfo }
     * 
     */
    public DTMPackInfo.InstFileInfo createDTMPackInfoInstFileInfo() {
        return new DTMPackInfo.InstFileInfo();
    }

    /**
     * Create an instance of {@link DTMPackInfo.EmailInfo }
     * 
     */
    public DTMPackInfo.EmailInfo createDTMPackInfoEmailInfo() {
        return new DTMPackInfo.EmailInfo();
    }

    /**
     * Create an instance of {@link DTMPackReq.MPackReq.DeletePackInfo }
     * 
     */
    public DTMPackReq.MPackReq.DeletePackInfo createDTMPackReqMPackReqDeletePackInfo() {
        return new DTMPackReq.MPackReq.DeletePackInfo();
    }

    /**
     * Create an instance of {@link DTMPackReq.MPackReq.CreateOppInfo.CFDANumber }
     * 
     */
    public DTMPackReq.MPackReq.CreateOppInfo.CFDANumber createDTMPackReqMPackReqCreateOppInfoCFDANumber() {
        return new DTMPackReq.MPackReq.CreateOppInfo.CFDANumber();
    }

    /**
     * Create an instance of {@link DTMPackRes.MPackRes }
     * 
     */
    public DTMPackRes.MPackRes createDTMPackResMPackRes() {
        return new DTMPackRes.MPackRes();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTMPackRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fmmi.usda.gov/GRANTSGOV//ManagePackage", name = "MT_MPack_Res")
    public JAXBElement<DTMPackRes> createMTMPackRes(DTMPackRes value) {
        return new JAXBElement<DTMPackRes>(_MTMPackRes_QNAME, DTMPackRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTMPackReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fmmi.usda.gov/GRANTSGOV//ManagePackage", name = "MT_MPack_Req")
    public JAXBElement<DTMPackReq> createMTMPackReq(DTMPackReq value) {
        return new JAXBElement<DTMPackReq>(_MTMPackReq_QNAME, DTMPackReq.class, null, value);
    }

}
