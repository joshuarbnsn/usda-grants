
package gov.usda.grants.pega.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Data Type for PEGA ManagePackage Response Interface to GRANTS.GOV
 * 
 * <p>Java class for DT_MPack_Res complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_MPack_Res">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MPackRes">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FundingOpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreatePackRes" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackResult" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="UpdatePackRes" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackResult" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="DeletePackRes" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackResult" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_MPack_Res", propOrder = {
    "mPackRes"
})
public class DTMPackRes {

    @XmlElement(name = "MPackRes", required = true)
    protected DTMPackRes.MPackRes mPackRes;

    /**
     * Gets the value of the mPackRes property.
     * 
     * @return
     *     possible object is
     *     {@link DTMPackRes.MPackRes }
     *     
     */
    public DTMPackRes.MPackRes getMPackRes() {
        return mPackRes;
    }

    /**
     * Sets the value of the mPackRes property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTMPackRes.MPackRes }
     *     
     */
    public void setMPackRes(DTMPackRes.MPackRes value) {
        this.mPackRes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FundingOpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreatePackRes" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackResult" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="UpdatePackRes" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackResult" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="DeletePackRes" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackResult" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userID",
        "fundingOpportunityNumber",
        "success",
        "createPackRes",
        "updatePackRes",
        "deletePackRes"
    })
    public static class MPackRes {

        @XmlElement(name = "UserID", required = true)
        protected String userID;
        @XmlElement(name = "FundingOpportunityNumber", required = true)
        protected String fundingOpportunityNumber;
        @XmlElement(name = "Success", required = true)
        protected String success;
        @XmlElement(name = "CreatePackRes")
        protected List<DTMPackResult> createPackRes;
        @XmlElement(name = "UpdatePackRes")
        protected List<DTMPackResult> updatePackRes;
        @XmlElement(name = "DeletePackRes")
        protected List<DTMPackResult> deletePackRes;

        /**
         * Gets the value of the userID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserID() {
            return userID;
        }

        /**
         * Sets the value of the userID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserID(String value) {
            this.userID = value;
        }

        /**
         * Gets the value of the fundingOpportunityNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFundingOpportunityNumber() {
            return fundingOpportunityNumber;
        }

        /**
         * Sets the value of the fundingOpportunityNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFundingOpportunityNumber(String value) {
            this.fundingOpportunityNumber = value;
        }

        /**
         * Gets the value of the success property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuccess() {
            return success;
        }

        /**
         * Sets the value of the success property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuccess(String value) {
            this.success = value;
        }

        /**
         * Gets the value of the createPackRes property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the createPackRes property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCreatePackRes().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTMPackResult }
         * 
         * 
         */
        public List<DTMPackResult> getCreatePackRes() {
            if (createPackRes == null) {
                createPackRes = new ArrayList<DTMPackResult>();
            }
            return this.createPackRes;
        }

        /**
         * Gets the value of the updatePackRes property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the updatePackRes property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUpdatePackRes().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTMPackResult }
         * 
         * 
         */
        public List<DTMPackResult> getUpdatePackRes() {
            if (updatePackRes == null) {
                updatePackRes = new ArrayList<DTMPackResult>();
            }
            return this.updatePackRes;
        }

        /**
         * Gets the value of the deletePackRes property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the deletePackRes property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletePackRes().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTMPackResult }
         * 
         * 
         */
        public List<DTMPackResult> getDeletePackRes() {
            if (deletePackRes == null) {
                deletePackRes = new ArrayList<DTMPackResult>();
            }
            return this.deletePackRes;
        }

        /**
         * Sets the value of the createPackRes property.
         * 
         * @param createPackRes
         *     allowed object is
         *     {@link DTMPackResult }
         *     
         */
        public void setCreatePackRes(List<DTMPackResult> createPackRes) {
            this.createPackRes = createPackRes;
        }

        /**
         * Sets the value of the updatePackRes property.
         * 
         * @param updatePackRes
         *     allowed object is
         *     {@link DTMPackResult }
         *     
         */
        public void setUpdatePackRes(List<DTMPackResult> updatePackRes) {
            this.updatePackRes = updatePackRes;
        }

        /**
         * Sets the value of the deletePackRes property.
         * 
         * @param deletePackRes
         *     allowed object is
         *     {@link DTMPackResult }
         *     
         */
        public void setDeletePackRes(List<DTMPackResult> deletePackRes) {
            this.deletePackRes = deletePackRes;
        }

    }

}
