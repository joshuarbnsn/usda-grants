
package gov.usda.grants.pega.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Data Type for PEGA ManagePackage Request Interface to GRANTS.GOV
 * 
 * <p>Java class for DT_MPack_Req complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_MPack_Req">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MPackReq">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FundingOpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreateOppInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FundingOpportunityTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OpportunityCategory">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="D"/>
 *                                   &lt;enumeration value="M"/>
 *                                   &lt;enumeration value="C"/>
 *                                   &lt;enumeration value="E"/>
 *                                   &lt;enumeration value="O"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="OpportunityCategoryExplanation" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="100"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="CFDANumber" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CreatePackInfo" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="UpdatePackInfo" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="DeletePackInfo" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CFDANumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CompetitionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_MPack_Req", propOrder = {
    "mPackReq"
})
public class DTMPackReq {

    @XmlElement(name = "MPackReq", required = true)
    protected DTMPackReq.MPackReq mPackReq;

    /**
     * Gets the value of the mPackReq property.
     * 
     * @return
     *     possible object is
     *     {@link DTMPackReq.MPackReq }
     *     
     */
    public DTMPackReq.MPackReq getMPackReq() {
        return mPackReq;
    }

    /**
     * Sets the value of the mPackReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTMPackReq.MPackReq }
     *     
     */
    public void setMPackReq(DTMPackReq.MPackReq value) {
        this.mPackReq = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FundingOpportunityNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreateOppInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="FundingOpportunityTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OpportunityCategory">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="D"/>
     *                         &lt;enumeration value="M"/>
     *                         &lt;enumeration value="C"/>
     *                         &lt;enumeration value="E"/>
     *                         &lt;enumeration value="O"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="OpportunityCategoryExplanation" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="100"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="CFDANumber" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CreatePackInfo" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackInfo" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="UpdatePackInfo" type="{http://fmmi.usda.gov/GRANTSGOV/Common}DT_MPackInfo" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="DeletePackInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CFDANumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CompetitionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userID",
        "fundingOpportunityNumber",
        "createOppInfo",
        "createPackInfo",
        "updatePackInfo",
        "deletePackInfo"
    })
    public static class MPackReq {

        @XmlElement(name = "UserID", required = true)
        protected String userID;
        @XmlElement(name = "FundingOpportunityNumber", required = true)
        protected String fundingOpportunityNumber;
        @XmlElement(name = "CreateOppInfo")
        protected DTMPackReq.MPackReq.CreateOppInfo createOppInfo;
        @XmlElement(name = "CreatePackInfo")
        protected List<DTMPackInfo> createPackInfo;
        @XmlElement(name = "UpdatePackInfo")
        protected List<DTMPackInfo> updatePackInfo;
        @XmlElement(name = "DeletePackInfo")
        protected List<DTMPackReq.MPackReq.DeletePackInfo> deletePackInfo;

        /**
         * Gets the value of the userID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserID() {
            return userID;
        }

        /**
         * Sets the value of the userID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserID(String value) {
            this.userID = value;
        }

        /**
         * Gets the value of the fundingOpportunityNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFundingOpportunityNumber() {
            return fundingOpportunityNumber;
        }

        /**
         * Sets the value of the fundingOpportunityNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFundingOpportunityNumber(String value) {
            this.fundingOpportunityNumber = value;
        }

        /**
         * Gets the value of the createOppInfo property.
         * 
         * @return
         *     possible object is
         *     {@link DTMPackReq.MPackReq.CreateOppInfo }
         *     
         */
        public DTMPackReq.MPackReq.CreateOppInfo getCreateOppInfo() {
            return createOppInfo;
        }

        /**
         * Sets the value of the createOppInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link DTMPackReq.MPackReq.CreateOppInfo }
         *     
         */
        public void setCreateOppInfo(DTMPackReq.MPackReq.CreateOppInfo value) {
            this.createOppInfo = value;
        }

        /**
         * Gets the value of the createPackInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the createPackInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCreatePackInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTMPackInfo }
         * 
         * 
         */
        public List<DTMPackInfo> getCreatePackInfo() {
            if (createPackInfo == null) {
                createPackInfo = new ArrayList<DTMPackInfo>();
            }
            return this.createPackInfo;
        }

        /**
         * Gets the value of the updatePackInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the updatePackInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUpdatePackInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTMPackInfo }
         * 
         * 
         */
        public List<DTMPackInfo> getUpdatePackInfo() {
            if (updatePackInfo == null) {
                updatePackInfo = new ArrayList<DTMPackInfo>();
            }
            return this.updatePackInfo;
        }

        /**
         * Gets the value of the deletePackInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the deletePackInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletePackInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTMPackReq.MPackReq.DeletePackInfo }
         * 
         * 
         */
        public List<DTMPackReq.MPackReq.DeletePackInfo> getDeletePackInfo() {
            if (deletePackInfo == null) {
                deletePackInfo = new ArrayList<DTMPackReq.MPackReq.DeletePackInfo>();
            }
            return this.deletePackInfo;
        }

        /**
         * Sets the value of the createPackInfo property.
         * 
         * @param createPackInfo
         *     allowed object is
         *     {@link DTMPackInfo }
         *     
         */
        public void setCreatePackInfo(List<DTMPackInfo> createPackInfo) {
            this.createPackInfo = createPackInfo;
        }

        /**
         * Sets the value of the updatePackInfo property.
         * 
         * @param updatePackInfo
         *     allowed object is
         *     {@link DTMPackInfo }
         *     
         */
        public void setUpdatePackInfo(List<DTMPackInfo> updatePackInfo) {
            this.updatePackInfo = updatePackInfo;
        }

        /**
         * Sets the value of the deletePackInfo property.
         * 
         * @param deletePackInfo
         *     allowed object is
         *     {@link DTMPackReq.MPackReq.DeletePackInfo }
         *     
         */
        public void setDeletePackInfo(List<DTMPackReq.MPackReq.DeletePackInfo> deletePackInfo) {
            this.deletePackInfo = deletePackInfo;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="FundingOpportunityTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OpportunityCategory">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="D"/>
         *               &lt;enumeration value="M"/>
         *               &lt;enumeration value="C"/>
         *               &lt;enumeration value="E"/>
         *               &lt;enumeration value="O"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="OpportunityCategoryExplanation" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="100"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="CFDANumber" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fundingOpportunityTitle",
            "opportunityCategory",
            "opportunityCategoryExplanation",
            "cfdaNumber"
        })
        public static class CreateOppInfo {

            @XmlElement(name = "FundingOpportunityTitle", required = true)
            protected String fundingOpportunityTitle;
            @XmlElement(name = "OpportunityCategory", required = true)
            protected String opportunityCategory;
            @XmlElement(name = "OpportunityCategoryExplanation")
            protected String opportunityCategoryExplanation;
            @XmlElement(name = "CFDANumber")
            protected List<DTMPackReq.MPackReq.CreateOppInfo.CFDANumber> cfdaNumber;

            /**
             * Gets the value of the fundingOpportunityTitle property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFundingOpportunityTitle() {
                return fundingOpportunityTitle;
            }

            /**
             * Sets the value of the fundingOpportunityTitle property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFundingOpportunityTitle(String value) {
                this.fundingOpportunityTitle = value;
            }

            /**
             * Gets the value of the opportunityCategory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpportunityCategory() {
                return opportunityCategory;
            }

            /**
             * Sets the value of the opportunityCategory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpportunityCategory(String value) {
                this.opportunityCategory = value;
            }

            /**
             * Gets the value of the opportunityCategoryExplanation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpportunityCategoryExplanation() {
                return opportunityCategoryExplanation;
            }

            /**
             * Sets the value of the opportunityCategoryExplanation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpportunityCategoryExplanation(String value) {
                this.opportunityCategoryExplanation = value;
            }

            /**
             * Gets the value of the cfdaNumber property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the cfdaNumber property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCFDANumber().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DTMPackReq.MPackReq.CreateOppInfo.CFDANumber }
             * 
             * 
             */
            public List<DTMPackReq.MPackReq.CreateOppInfo.CFDANumber> getCFDANumber() {
                if (cfdaNumber == null) {
                    cfdaNumber = new ArrayList<DTMPackReq.MPackReq.CreateOppInfo.CFDANumber>();
                }
                return this.cfdaNumber;
            }

            /**
             * Sets the value of the cfdaNumber property.
             * 
             * @param cfdaNumber
             *     allowed object is
             *     {@link DTMPackReq.MPackReq.CreateOppInfo.CFDANumber }
             *     
             */
            public void setCFDANumber(List<DTMPackReq.MPackReq.CreateOppInfo.CFDANumber> cfdaNumber) {
                this.cfdaNumber = cfdaNumber;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class CFDANumber {

                @XmlElement(name = "Value", required = true)
                protected String value;

                /**
                 * Gets the value of the value property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Sets the value of the value property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CFDANumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CompetitionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cfdaNumber",
            "competitionID"
        })
        public static class DeletePackInfo {

            @XmlElement(name = "CFDANumber")
            protected String cfdaNumber;
            @XmlElement(name = "CompetitionID")
            protected String competitionID;

            /**
             * Gets the value of the cfdaNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCFDANumber() {
                return cfdaNumber;
            }

            /**
             * Sets the value of the cfdaNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCFDANumber(String value) {
                this.cfdaNumber = value;
            }

            /**
             * Gets the value of the competitionID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompetitionID() {
                return competitionID;
            }

            /**
             * Sets the value of the competitionID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompetitionID(String value) {
                this.competitionID = value;
            }

        }

    }

}
